//
//  Localization.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

enum Localization {
    
    enum Common {
        
        /// Отмена
        static let cancelButtonTitle = "COMMON_CANCEL_BUTTON_TITLE".localized
        
    }
    
    enum StatusBarMessageView {
        
        /// Интернет не доступен
        static let internetNotConnected = "STATUSBAR_MESSAGE_VIEW_NO_INTERNET".localized
    }
    
    enum LoadingView {
        
        /// Загрузка
        static let title = "LOADING_VIEW_TITLE".localized
        
    }
    
    enum TabBar {
        
        /// Линия
        static let lineBarTitle = "TABBAR_LINE_ITEM_TITLE".localized
        /// Избранное
        static let favoritesBarTitle = "TABBAR_FAVORITES_ITEM_TITLE".localized
        /// Профиль
        static let profileBarTitle = "TABBAR_PROFILE_ITEM_TITLE".localized
        /// Купонник
        static let cartBarTitle = "TABBAR_CART_ITEM_TITLE".localized
        
    }
    
    enum Line {
        
        /// Линия
        static let navigationBarTitle = "LINE_NAVIGATION_BAR_TITLE".localized
        /// LIVE СОБЫТИЯ
        static let liveEventsSectionTitle = "LINE_SECTION_LIVE_TITLE".localized
        /// Все
        static let liveEventsAllTitle = "LINE_LIVE_ALL_TITLE".localized
        /// Видеотрансляции
        static let liveEventsVideoTranslationsTitle = "LINE_LIVE_VIDEO_TITLE".localized
        /// Заголовок секции: ИЗБРАННЫЕ ВИДЫ СПОРТА
        static let favoritesSportsSectionTitle = "LINE_SECTION_FAVORITES_TITLE".localized
        /// Кнопка в заголовке секции: Редактировать
        static let favoritesSportsSectionEditButtonTitle = "LINE_SECTION_FAVORITES_EDIT_BUTTON_TITLE".localized
        /// Заголовок секции: ВСЕ ВИДЫ СПОРТА
        static let allSportsSectionTitle = "LINE_SECTION_ALL_TITLE".localized
        
    }
    
    enum LineSearch {
        
        /// Заголовок секции: ТУРНИРЫ
        static let tournamentsSectionTitle = "LINESEARCH_SECTION_TOURNAMENTS_TITLE".localized
        /// Заголовок секции: СОБЫТИЯ
        static let eventsSectionTitle = "LINESEARCH_SECTION_EVENTS_TITLE".localized
        
    }
    
    enum LiveEvents {
        
        /// Live
        static let liveNavigationBarTitle = "LIVEEVENTS_LIVE_NAVIGATION_BAR_TITLE".localized
        /// Видеотрансляции
        static let videoTranslationsNavigationBarTitle = "LIVEEVENTS_VIDEOTRANSLATIONS_NAVIGATION_BAR_TITLE".localized
        
    }
    
    enum SportTournaments {
        
        /// Заголовок секции: ТУРНИРЫ
        static let tournamentsSectionTitle = "SPORTTOURNAMENTS_SECTION_TOURNAMENTS_TITLE".localized
        
    }
    
    enum TournamentEvents {
        
        /// Заголовок экрана
        static func navigationBarTitle(withTournamentsCount count: Int) -> String {
            let format = "TOURNAMENTEVENTS_NAVIGATION_BAR_TITLE".localized
            let text = String.localizedStringWithFormat(format, count)
            
            return text
        }
        
    }
    
    enum SportsFilter {
        
        /// Все
        static let allTitle = "SPORTSFILTER_ALL_TITLE".localized
        
    }
    
    enum Favorites {
        
        /// Избранное
        static let navigationBarTitle = "FAVORITES_NAVIGATION_BAR_TITLE".localized
        
    }
    
    enum Profile {
        
        /// Профиль
        static let navigationBarTitle = "PROFILE_NAVIGATION_BAR_TITLE".localized
        
    }
    
    enum Cart {
        
        /// Купонник
        static let navigationBarTitle = "CART_NAVIGATION_BAR_TITLE".localized
        
    }
    
}
