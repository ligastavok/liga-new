//
//  Array+Unique.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 22.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Array where Element: Equatable {
    
    /// Уникальные элементы
    var unique: [Element] {
        var uniqueValues: [Element] = []
        for item in self where !uniqueValues.contains(item) {
            uniqueValues.append(item)
        }
        
        return uniqueValues
    }
    
}
