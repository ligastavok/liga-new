//
//  Date+GMT.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 28.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Время по Гринвичу
    var gmt: Date {
        let secondsFromGMT = TimeZone.current.secondsFromGMT(for: self)
        return addingTimeInterval(-1 * TimeInterval(secondsFromGMT))
    }
    
}
