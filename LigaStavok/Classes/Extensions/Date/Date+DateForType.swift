//
//  Date+DateForType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 29.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Тип вычисляемой даты
    enum DateForType {
        
        /// Начало дня
        case startOfDay
        /// Конец дня
        case endOfDay
        /// Начало недели
        case startOfWeek
        /// Конец недели
        case endOfWeek
        /// Начало месяца
        case startOfMonth
        /// Конец месяца
        case endOfMonth
        /// Завтра
        case tomorrow
        /// Вчера
        case yesterday
        
    }
    
}
