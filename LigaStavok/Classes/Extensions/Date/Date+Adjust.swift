//
//  Date+Adjust.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 28.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Дата с измененным значением
    func adjust(_ component: ComponentType, offset: Int, in calendar: Calendar = .current) -> Date {
        var dateComponents = DateComponents()
        switch component {
        case .year:
            dateComponents.year = offset
        case .month:
            dateComponents.month = offset
        case .day:
            dateComponents.day = offset
        case .hour:
            dateComponents.hour = offset
        case .minute:
            dateComponents.minute = offset
        case .second:
            dateComponents.second = offset
        case .weekday:
            dateComponents.weekday = offset
        case .weekdayOrdinal:
            dateComponents.weekdayOrdinal = offset
        case .weekOfMonth:
            dateComponents.weekOfMonth = offset
        case .weekOfYear:
            dateComponents.weekOfYear = offset
        case .nanosecond:
            dateComponents.nanosecond = offset
        }
        
        guard let date = calendar.date(byAdding: dateComponents, to: self) else { return self }
        return date
    }
    
    /// Дата с измененными значениями
    func adjust(month: Int? = nil,
                day: Int? = nil,
                hour: Int? = nil,
                minute: Int? = nil,
                second: Int? = nil,
                in calendar: Calendar = .current) -> Date {
        
        var components = self.components(calendar: calendar)
        components.month = month ?? components.month
        components.day = day ?? components.day
        components.hour = hour ?? components.hour
        components.minute = minute ?? components.minute
        components.second = second ?? components.second
        
        guard let date = calendar.date(from: components) else { return self }
        return date
    }
    
}
