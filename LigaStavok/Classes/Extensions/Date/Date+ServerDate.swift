//
//  Date+ServerDate.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 28.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Время на сервере
    var serverDate: Date {
        let moscowTimeZone: TimeInterval = 10800
        return self.gmt.addingTimeInterval(moscowTimeZone)
    }
    
}
