//
//  Date+Components.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 28.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Получить компоненты даты
    func components(_ components: Set<Calendar.Component>? = nil, calendar: Calendar = .current) -> DateComponents {
        let components = components ?? [.year,
                                        .month,
                                        .day,
                                        .hour,
                                        .minute,
                                        .second,
                                        .weekday,
                                        .weekdayOrdinal,
                                        .weekOfMonth,
                                        .weekOfYear,
                                        .nanosecond,
                                        .timeZone]
        return calendar.dateComponents(components, from: self)
    }
    
}
