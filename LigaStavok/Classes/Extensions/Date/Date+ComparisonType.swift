//
//  Date+ComparisonType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 29.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Тип сравнения
    enum ComparisonType {
        
        // MARK: Дни
        
        /// Проверка на сегодня
        case isToday
        /// Проверка на завтра
        case isTomorrow
        /// Проверка на вчера
        case isYesterday
        /// Сравнение дат
        case isSameDay(`as`: Date)
        
        
        // MARK: Недели
        
        /// Проверка на неделю
        case isThisWeek
        /// Проверка на следующую неделю
        case isNextWeek
        /// Проверка на прошлую неделю
        case isLastWeek
        /// Сравнение недель
        case isSameWeek(`as`: Date)
        
        
        // MARK: Месяцы
        
        /// Проверка на месяц
        case isThisMonth
        /// Проверка на следующий месяц
        case isNextMonth
        /// Проверка на прошлый месяц
        case isLastMonth
        /// Сравнение месяцев
        case isSameMonth(`as`: Date)
        
        
        // MARK: Годы
        
        /// Проверка на год
        case isThisYear
        /// Проверка на следующий год
        case isNextYear
        /// Проверка на предыдущий год
        case isLastYear
        /// Сравнение годов
        case isSameYear(`as`: Date)
        
        
        // MARK: Прочее
        
        /// Проверка на будущее
        case isInTheFuture
        /// Проверка на прошлое
        case isInThePast
        /// Сравнение на то, какая дата раньше
        case isEarlier(than: Date)
        /// Сравнение на то, какая дата позже
        case isLater(than: Date)
        /// Проверка на рабочий день
        case isWeekday
        /// Проверка на выходной
        case isWeekend
        
    }
    
}
