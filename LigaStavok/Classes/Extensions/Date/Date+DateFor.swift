//
//  Date+DateFor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 28.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Получить дату
    func dateFor(_ type: DateForType, calendar: Calendar = .current) -> Date {
        switch type {
        case .startOfDay:
            return adjust(hour: 0, minute: 0, second: 0, in: calendar)
        case .endOfDay:
            return adjust(hour: 23, minute: 59, second: 59, in: calendar)
        case .startOfWeek:
            let components = self.components([.yearForWeekOfYear, .weekOfYear], calendar: calendar)
            return calendar.date(from: components) ?? self
        case .endOfWeek:
            return dateFor(.startOfWeek, calendar: calendar)
                .adjust(.day, offset: 6, in: calendar)
                .dateFor(.endOfDay, calendar: calendar)
        case .startOfMonth:
            return adjust(day: 1, hour: 0, minute: 0, second: 0, in: calendar)
        case .endOfMonth:
            guard var month = component(.month) else { return self }
            
            month += 1
            return adjust(month: month, day: 0, hour: 23, minute: 59, second: 59, in: calendar)
        case .tomorrow:
            return adjust(.day, offset: 1, in: calendar)
        case .yesterday:
            return adjust(.day, offset: -1, in: calendar)
        }
    }
    
}
