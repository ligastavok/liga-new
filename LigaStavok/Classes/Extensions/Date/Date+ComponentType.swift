//
//  Date+ComponentType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 29.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Тип компонента
    enum ComponentType {
        
        /// Год
        case year
        /// Месяц
        case month
        /// День
        case day
        /// Час
        case hour
        /// Минута
        case minute
        /// Секунда
        case second
        /// День недели
        case weekday
        /// Порядковый день недели
        case weekdayOrdinal
        /// Неделя в месяце
        case weekOfMonth
        /// Неделя в году
        case weekOfYear
        /// Наносекунда
        case nanosecond
        
    }
    
}


// MARK: - Публичные свойства

extension Date.ComponentType {
    
    /// Компонент календаря
    var calendarComponent: Calendar.Component {
        switch self {
        case .year:
            return .year
        case .month:
            return .month
        case .day:
            return .day
        case .hour:
            return .hour
        case .minute:
            return .minute
        case .second:
            return .second
        case .weekday:
            return .weekday
        case .weekdayOrdinal:
            return .weekdayOrdinal
        case .weekOfMonth:
            return .weekOfMonth
        case .weekOfYear:
            return .weekOfYear
        case .nanosecond:
            return .nanosecond
        }
    }
    
}
