//
//  Date+Compare.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 29.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Сравнение дат
    func compare(_ comparison: ComparisonType, calendar: Calendar = .current) -> Bool {
        switch comparison {
        case .isToday:
            return compare(.isSameDay(as: .now))
        case .isTomorrow:
            let comparison = Date().adjust(.day, offset: 1)
            return compare(.isSameDay(as: comparison))
        case .isYesterday:
            let comparison = Date().adjust(.day, offset: -1)
            return compare(.isSameDay(as: comparison))
        case .isSameDay(let date):
            return compare(.isSameMonth(as: date)) && component(.day) == date.component(.day)
            
        case .isThisWeek:
            return compare(.isSameWeek(as: .now))
        case .isNextWeek:
            let comparison = Date().adjust(.weekOfYear, offset: 1)
            return compare(.isSameWeek(as: comparison))
        case .isLastWeek:
            let comparison = Date().adjust(.weekOfYear, offset: -1)
            return compare(.isSameWeek(as: comparison))
        case .isSameWeek(let date):
            guard component(.weekOfYear) == date.component(.weekOfYear) else { return false }
            
            let weekInSeconds: Double = 604800
            return abs(timeIntervalSince(date)) < weekInSeconds
            
        case .isThisMonth:
            return compare(.isSameMonth(as: .now))
        case .isNextMonth:
            let comparison = Date().adjust(.month, offset: 1)
            return compare(.isSameMonth(as: comparison))
        case .isLastMonth:
            let comparison = Date().adjust(.month, offset: -1)
            return compare(.isSameMonth(as: comparison))
        case .isSameMonth(let date):
            return compare(.isSameYear(as: date)) && component(.month) == date.component(.month)
            
        case .isThisYear:
            return compare(.isSameYear(as: .now))
        case .isNextYear:
            let comparison = Date().adjust(.year, offset: 1)
            return compare(.isSameYear(as: comparison))
        case .isLastYear:
            let comparison = Date().adjust(.year, offset: -1)
            return compare(.isSameYear(as: comparison))
        case .isSameYear(let date):
            return component(.year) == date.component(.year)
            
        case .isInTheFuture:
            return compare(.isLater(than: .now))
        case .isInThePast:
            return compare(.isEarlier(than: .now))
        case .isEarlier(let date):
            return (self as NSDate).earlierDate(date) == self
        case .isLater(let date):
            return (self as NSDate).laterDate(date) == self
        case .isWeekday:
            return !compare(.isWeekend)
        case .isWeekend:
            guard let range = calendar.maximumRange(of: .weekday) else { return false }
            return component(.weekday) == range.lowerBound || component(.weekday) == range.upperBound - range.lowerBound
        }
        
    }
    
}
