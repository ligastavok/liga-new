//
//  Date+Now.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 29.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Текущая дата
    static var now: Date {
        return .init()
    }
    
}
