//
//  Date+Component.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 28.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Date {
    
    /// Получить значение для компонента
    func component(_ component: ComponentType) -> Int? {
        let components = self.components([component.calendarComponent])
        switch component {
        case .year:
            return components.year
        case .month:
            return components.month
        case .day:
            return components.day
        case .hour:
            return components.hour
        case .minute:
            return components.minute
        case .second:
            return components.second
        case .weekday:
            return components.weekday
        case .weekdayOrdinal:
            return components.weekdayOrdinal
        case .weekOfMonth:
            return components.weekOfMonth
        case .weekOfYear:
            return components.weekOfYear
        case .nanosecond:
            return components.nanosecond
        }
    }
    
}
