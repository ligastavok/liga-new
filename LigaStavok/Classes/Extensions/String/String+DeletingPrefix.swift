//
//  String+DeletingPrefix.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 07.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension String {
    
    /// Строка без префикса
    func deletingPrefix(_ prefix: String) -> String {
        guard hasPrefix(prefix) else { return self }
        return String(dropFirst(prefix.count))
    }
    
}
