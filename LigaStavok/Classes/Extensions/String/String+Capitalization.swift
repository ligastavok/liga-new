//
//  String+Capitalization.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension String {
    
    /// Строка с первой прописной буквой
    var capitalizingFirstLetter: String {
        guard !self.isEmpty else { return self }
        
        let first = prefix(1).capitalized
        let other = dropFirst()
        
        return first + other
    }
    
    /// Сделать первую букву прописной
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter
    }
    
}
