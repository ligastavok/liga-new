//
//  String+Size.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension String {
    
    /// Размер текста
    func size(usingFont font: UIFont) -> CGSize {
        return size(withAttributes: [.font: font])
    }
    
    /// Ширина текста
    func width(usingFont font: UIFont) -> CGFloat {
        return size(usingFont: font).width
    }
    
    /// Высота текста
    func height(usingFont font: UIFont) -> CGFloat {
        return size(usingFont: font).height
    }
    
}
