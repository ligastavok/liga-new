//
//  String+Empty.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

extension String {
    
    /// Пустая строка
    static let empty = ""
    
}
