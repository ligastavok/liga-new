//
//  String+Localized.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension String {
    
    /// Локализованное значение
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
}
