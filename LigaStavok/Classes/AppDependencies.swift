//
//  AppDependencies.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import AlamofireNetworkActivityIndicator

final class AppDependencies {
    
    // MARK: - Приватные свойства
    
    /// Версия базы данных
    private let schemaVersion: UInt64 = 0
    
    /// Версия приложения
    private let applicationVersion: Int = 0
    
    
    // MARK: - Инициализация
    
    init() {
        configureDependencies()
    }
    
}


// MARK: - Публичные методы

extension AppDependencies {
    
    /// Установить корневой view controller
    func installRootViewController(into window: UIWindow) {
        window.makeKeyAndVisible()
        window.rootViewController = TabBarModuleAssembly.assembleModule()
    }
    
}


// MARK: - Приватные методы

private extension AppDependencies {
    
    /// Настройка зависимостей
    func configureDependencies() {
        
        // Автоматический ActivityIndicator при выполнении запросов Alamofire
        NetworkActivityIndicatorManager.shared.isEnabled = true
        NetworkActivityIndicatorManager.shared.startDelay = 0.5
        
        // Миграция базы данных
        DatabaseManager.assignDatabaseVersion(self.schemaVersion)
        print(DatabaseManager.databaseUrl?.absoluteString ?? "the database does not exist")
        
        // Миграция приложения
        MigrationManager.configureDefaults(defaultVersion: self.applicationVersion)
        MigrationManager.assignApplicationVersion(self.applicationVersion)
    }
    
}
