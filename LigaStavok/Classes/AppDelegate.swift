//
//  AppDelegate.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 25.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    // MARK: - Публичные свойства
    
    var window: UIWindow?
    
    
    // MARK: - UIApplicationDelegate
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        self.window = window
        
        let appDependencies = AppDependencies()
        appDependencies.installRootViewController(into: window)
        
        return true
    }
    
}
