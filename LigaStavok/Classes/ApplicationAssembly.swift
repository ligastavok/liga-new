//
//  ApplicationAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Swinject

final class ApplicationAssembly {
    
    // MARK: - Публичные свойства
    
    /// Сборщик блоков
    var assembler: Assembler
    
    
    // MARK: - Инициалиазация
    
    init(with assemblies: [Assembly]) {
        self.assembler = Assembler(assemblies)
    }
    
}


// MARK: - Публичные свойства

extension ApplicationAssembly {
    
    /// Сборщик блоков по-умолчанию
    static var assembler: Assembler = {
        let assemblies: [Assembly] = [ServiceComponentsAssembly(), UIComponentsAssembly()]
        return Assembler(assemblies)
    }()
    
}
