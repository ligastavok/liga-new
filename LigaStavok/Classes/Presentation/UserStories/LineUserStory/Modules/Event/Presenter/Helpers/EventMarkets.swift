//
//  EventMarkets.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 03.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class EventMarkets {
    
    // MARK: - Инициализация
    
    private init() { }
    
}


// MARK: - Публичные методы

extension EventMarkets {
    
    /// Обновление модели победитель
    static func updateWinnerModels(oldWinnerModels: inout [WinnerViewModel], newWinnerModels: inout [WinnerViewModel]) {
        for index in 0..<newWinnerModels.count {
            guard let oldWinnerItem = oldWinnerModels.first(where: { $0.id == newWinnerModels[index].id}) else { continue }
            
            newWinnerModels[index].animateValueChange = oldWinnerItem.animateValueChange
            guard newWinnerModels[index].coefficient != oldWinnerItem.coefficient else { continue }
            
            let lastUpdate = Date.timeIntervalSinceReferenceDate
            if newWinnerModels[index].coefficient > oldWinnerItem.coefficient {
                newWinnerModels[index].animateValueChange = BetValueChangeType.ascending(lastUpdate: lastUpdate)
            } else {
                newWinnerModels[index].animateValueChange = BetValueChangeType.descending(lastUpdate: lastUpdate)
            }
        }
    }
    
    /// Обновление модели фора домашней команды
    static func updateAllowanceHomeCells(oldAllowanceModels: inout [AllowanceViewModel], newAllowanceModels: inout [AllowanceViewModel]) {
        for index in 0..<newAllowanceModels.count {
            guard let oldAllowanceItem = oldAllowanceModels.first(where: { $0.idHomeTeam == newAllowanceModels[index].idHomeTeam}) else { continue }
            
            newAllowanceModels[index].animateValueChangeHomeTeam = oldAllowanceItem.animateValueChangeHomeTeam
            guard newAllowanceModels[index].homeTeamF2Value != oldAllowanceItem.homeTeamF2Value else { continue }
            
            let lastUpdate = Date.timeIntervalSinceReferenceDate
            if newAllowanceModels[index].homeTeamF2Value > oldAllowanceItem.homeTeamF2Value {
                newAllowanceModels[index].animateValueChangeHomeTeam = BetValueChangeType.ascending(lastUpdate: lastUpdate)
            } else {
                newAllowanceModels[index].animateValueChangeHomeTeam = BetValueChangeType.descending(lastUpdate: lastUpdate)
            }
        }
    }
    
    /// Обновление модели фора гостевой команды
    static func updateAllowanceGuestCells(oldAllowanceModels: inout [AllowanceViewModel], newAllowanceModels: inout [AllowanceViewModel]) {
        for index in 0..<newAllowanceModels.count {
            guard let oldAllowanceItem = oldAllowanceModels.first(where: { $0.idGuestTeam == newAllowanceModels[index].idGuestTeam}) else { continue }
            
            newAllowanceModels[index].animateValueChangeGuestTeam = oldAllowanceItem.animateValueChangeGuestTeam
            guard newAllowanceModels[index].guestTeamF2Value != oldAllowanceItem.guestTeamF2Value else { continue }
            
            let lastUpdate = Date.timeIntervalSinceReferenceDate
            if newAllowanceModels[index].guestTeamF2Value > oldAllowanceItem.guestTeamF2Value {
                newAllowanceModels[index].animateValueChangeGuestTeam = BetValueChangeType.ascending(lastUpdate: lastUpdate)
            } else {
                newAllowanceModels[index].animateValueChangeGuestTeam = BetValueChangeType.descending(lastUpdate: lastUpdate)
            }
        }
    }
    
}
