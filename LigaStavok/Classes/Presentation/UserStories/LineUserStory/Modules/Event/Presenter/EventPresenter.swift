//
//  EventPresenter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class EventPresenter {

    // MARK: - Типы данных

    private struct LocalData {
        
        /// События
        var events: [EventObject] = []

        var baseHeaderSections: [EventHeaderSectionViewModel] = []
        var markets: [MarketViewModel] = []
    }

    private struct LocalViewData {
        
        /// Событие с свойством json
        var eventObjectWithJSON: SynchronizedValue<EventObjectWithJSON>?
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// View
    weak var view: EventViewInput?
    /// Interactor
    var interactor: EventInteractorInput!
    /// Router
    var router: EventRouterInput!
    
    
    // MARK: - Приватные свойства
    
    /// Очередь для патча событий
    private lazy var patchEventsOperationQueue: OperationQueue = {
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        operationQueue.qualityOfService = .utility
        
        return operationQueue
    }()
    
    /// Локальные данные
    private var data = LocalData()
    /// Данные представления
    private var viewData = LocalViewData()
    
    
    // MARK: - Инициализация
    
    init(event: EventObject) {
        self.data.events.append(event)
    }
    
}


// MARK: - EventViewOutput

extension EventPresenter: EventViewOutput {

    func didTriggerViewReadyEvent() {
        self.view?.setupInitialState()
        self.obtainEventFromNetwork(for: self.data.events)
    }

}


// MARK: - Приватные методы

private extension EventPresenter {
    
    /// Получить эвент по интернету
    func obtainEventFromNetwork(for events: [EventObject]) {
        guard !events.isEmpty else { return }
        
        let ids = events.map { $0.id }
        self.interactor.obtainEventFromNetwork(withIds: ids)
    }
    
    /// Подписаться на обновления
    func subscribeToUpdates(for events: [EventObject]) {
        let ids = events.map { $0.id }
        self.interactor.subscribeToEventsUpdates(withIds: ids)
    }
    
    /// Обновление данных в маркетах
    func reloadData(event: EventObjectWithJSON) {

        // Сохранение старых данных
        let oldMarkets = (self.view?.getMarkets())!

        // Запись новых данных
        initData(event: event, isCallReloadData: false)

        for newMarket in self.data.markets {
            // Обновление данных в маркетах
            switch newMarket.type {
            case .win:
                // Обновление данных в маркете победитель
                guard let oldMarket = oldMarkets.first(where: { $0.id == newMarket.id && $0.type == .win }) else { continue }

                newMarket.eventHeaderSection?.isOpen = (oldMarket.eventHeaderSection?.isOpen)!

                guard var oldWinnerModels = oldMarket.object as? [WinnerViewModel] else { continue }

                guard var newWinnerModels = newMarket.object as? [WinnerViewModel] else { continue }

                EventMarkets.updateWinnerModels(oldWinnerModels: &oldWinnerModels, newWinnerModels: &newWinnerModels)
                newMarket.object = newWinnerModels
            case .han:
                // Обновление данных в маркете фора
                for oldMarket in oldMarkets.filter({ $0.type == .han }) {
                    let idsMarkets = oldMarket.ids.filter { newMarket.ids.contains($0) }

                    if !idsMarkets.isEmpty {
                        newMarket.eventHeaderSection?.isOpen = (oldMarket.eventHeaderSection?.isOpen)!

                        guard var oldAllowanceModels = oldMarket.object as? [AllowanceViewModel] else { continue }

                        guard var newAllowanceModels = newMarket.object as? [AllowanceViewModel] else { continue }

                        EventMarkets.updateAllowanceHomeCells(oldAllowanceModels: &oldAllowanceModels, newAllowanceModels: &newAllowanceModels)
                        EventMarkets.updateAllowanceGuestCells(oldAllowanceModels: &oldAllowanceModels, newAllowanceModels: &newAllowanceModels)
                        newMarket.object = newAllowanceModels
                        break
                    }
                }
            case .exa:
                // Обновление данных в маркете точный счет
                guard let oldMarket = oldMarkets.first(where: { $0.id == newMarket.id && $0.type == .exa }) else { continue }

                newMarket.eventHeaderSection?.isOpen = (oldMarket.eventHeaderSection?.isOpen)!

                guard let oldAccurateAccount = oldMarket.object as? AccurateAccountModelView else { break }

                guard let newAccurateAccount = newMarket.object as? AccurateAccountModelView else { break }

                oldAccurateAccount.isChangeCoef(newOutcomes: newAccurateAccount.outcomes)
                oldAccurateAccount.outcomes = newAccurateAccount.outcomes
                oldAccurateAccount.generateAccurateAccountString()
                newMarket.object = oldAccurateAccount
            case .ttl:
                // Обновление данных маркета
                for oldMarket in oldMarkets.filter({ $0.type == .ttl }) {
                    let idsMarkets = oldMarket.ids.filter { newMarket.ids.contains($0) }

                    if !idsMarkets.isEmpty {
                        newMarket.eventHeaderSection?.isOpen = (oldMarket.eventHeaderSection?.isOpen)!

                        guard let oldTotalGoals = oldMarket.object as? TotalGoalsViewModel else { break }

                        guard let newTotalGoals = newMarket.object as? TotalGoalsViewModel else { break }

                        oldTotalGoals.updateData(newTotalGoalsCollection: newTotalGoals.goalsCells)
                        break
                    }
                }
            default:
                continue
            }
        }

        DispatchQueue.main.async {
            self.view?.reloadData(markets: self.data.markets)
        }
    }

    
    /// Инициализация данных маркетов
    func initData(event: EventObjectWithJSON, isCallReloadData: Bool) {
        self.data.markets.removeAll()
        self.data.baseHeaderSections.removeAll()

        guard let outcomes = event.outcomes else { return }
        for outcome in outcomes {

            // Исход с форой
            var allowanceViewModels: [AllowanceViewModel] = []
            var eventAllowanceSectionHeader: EventHeaderSectionViewModel?
            // Всего голов
            var goalsCollectionCellViewModels: [TotalGoalsCollectionCellViewModel] = []
            var goalsSectionHeader: EventHeaderSectionViewModel?

            // Позиция маркета
            var posMarket = -1

            /* Идентификаторы маркетов собранных воедино */
            // Идентификаторы маркета исход с форой
            var idsAllowanceMarkets: [Int] = []
            // Идентификаторы маркета всего голов
            var idsTotalGoalsMarkets: [Int] = []


            self.data.baseHeaderSections.append(EventHeaderSectionViewModel(title: outcome.value.title, isOpen: false, isActiveBlock: false))
            let indexInsertBaseSectionHeader = self.data.markets.count

            for nsub in outcome.value.nsub {
                switch nsub.value.type {
                case MarketType.win.rawValue:
                    // Парсинг данных маркета победитель
                    let winnerViewModels = nsub.value.nsub.map { WinnerViewModel(outcomeWinner: $0.value) }.sorted(by: { $0.sort > $1.sort })
                    let eventSectionHeader = EventHeaderSectionViewModel(title: nsub.value.title)

                    self.data.markets.append(MarketViewModel(type: MarketType.win,
                                                        eventHeaderSection: eventSectionHeader,
                                                        object: winnerViewModels,
                                                        id: nsub.value.id,
                                                        position: nsub.value.position))
                    
                case MarketType.han.rawValue:
                    // Парсинг данных маркета фора
                    if allowanceViewModels.isEmpty {
                        guard let outcomeFirst = nsub.value.nsub[OutcomeKey.first] else { return }
                        eventAllowanceSectionHeader = EventHeaderSectionViewModel(title: nsub.value.title)
                        posMarket = nsub.value.position
                        allowanceViewModels.append(AllowanceViewModel(
                            homeTeamName: outcomeFirst.eventTeam.first, guestTeamName: outcomeFirst.eventTeam.second ?? ""))
                    }

                    idsAllowanceMarkets.append(nsub.value.id)

                    allowanceViewModels.append(AllowanceViewModel(
                    homeTeamF1Value: nsub.value.nsub[OutcomeKey.first]?.title,
                    homeTeamF2Value: nsub.value.nsub[OutcomeKey.first]?.value,
                    guestTeamF1Value: nsub.value.nsub[OutcomeKey.second]?.title,
                    guestTeamF2Value: nsub.value.nsub[OutcomeKey.second]?.value,
                    idHomeCell: nsub.value.nsub[OutcomeKey.first]?.id,
                    idGuestCell: nsub.value.nsub[OutcomeKey.second]?.id))
                case MarketType.exa.rawValue:
                    // Парсинг точный счет
                    var accurateAccountViewModel: AccurateAccountModelView?
                    let eventSectionHeader = EventHeaderSectionViewModel(title: nsub.value.title)
                    var outcomesAccount: [Outcome] = []
                    
                    for item in nsub.value.nsub {
                        outcomesAccount.append(item.value)
                    }
                    
                    accurateAccountViewModel = AccurateAccountModelView(firstTeamGoals: 0,
                                                                              secondTeamGoals: 0,
                                                                              outcomes: outcomesAccount)
                    
                    self.data.markets.append(MarketViewModel(type: MarketType.exa,
                                                             eventHeaderSection: eventSectionHeader,
                                                             object: accurateAccountViewModel!,
                                                             id: nsub.value.id,
                                                             position: nsub.value.position))
                    /*
                case MarketType.err.rawValue:
                    // Парсинг количество голов
                    /*var numberOfHostsViewModel: NumberOfHostsViewModel?
                    let eventSectionHeader = EventHeaderSectionViewModel(title: nsub.value.title)
                    var outcomesNumberOfHosts: [Outcome] = []
                    
                    for item in nsub.value.nsub {
                        outcomesNumberOfHosts.append(item.value)
                    }
                    
                    numberOfHostsViewModel = NumberOfHostsViewModel(outcomes: outcomesNumberOfHosts)
                    
                    self.data.markets.append(MarketViewModel(type: MarketType.err,
                                                             eventHeaderSection: eventSectionHeader,
                                                             object: numberOfHostsViewModel!,
                                                             id: nsub.value.id,
                                                             position: nsub.value.position))*/
                    break*/
                case MarketType.ttl.rawValue:
                    // Парсинг всего голов
                    if goalsCollectionCellViewModels.isEmpty {
                        goalsSectionHeader = EventHeaderSectionViewModel(title: nsub.value.title)
                        posMarket = nsub.value.position
                    }

                    idsTotalGoalsMarkets.append(nsub.value.id)

                    let totalGoalsCollectionViewCell = TotalGoalsCollectionCellViewModel()

                    if nsub.value.nsub[OutcomeKey.less] != nil {
                        totalGoalsCollectionViewCell.appendLessValues(lessTitle: (nsub.value.nsub[OutcomeKey.less]?.title)!,
                                                                      lessValue: (nsub.value.nsub[OutcomeKey.less]?.value)!)
                        totalGoalsCollectionViewCell.idLess = (nsub.value.nsub[OutcomeKey.less]?.id)!
                    }

                    if nsub.value.nsub[OutcomeKey.gross] != nil {
                        totalGoalsCollectionViewCell.appendGrossValues(grossTitle: (nsub.value.nsub[OutcomeKey.gross]?.title)!,
                                                                       grossValue: (nsub.value.nsub[OutcomeKey.gross]?.value)!)
                        totalGoalsCollectionViewCell.idGross = (nsub.value.nsub[OutcomeKey.gross]?.id)!
                    }

                    totalGoalsCollectionViewCell.setTitle(grossOutcome: nsub.value.nsub[OutcomeKey.gross],
                                                          lessOutcome: nsub.value.nsub[OutcomeKey.less])

                    goalsCollectionCellViewModels.append(totalGoalsCollectionViewCell)
                default:
                    continue
                }
            }
            
            // Добавление маркета фора
            if !allowanceViewModels.isEmpty {
                self.data.markets.append(MarketViewModel(type: MarketType.han,
                                                     eventHeaderSection: eventAllowanceSectionHeader,
                                                     object: allowanceViewModels,
                                                     position: posMarket))
                self.data.markets.last?.ids = idsAllowanceMarkets
            }

            // Добавление маркета всего голов
            let totalGoalsViewModel = TotalGoalsViewModel(goalsCells: goalsCollectionCellViewModels)
            if !goalsCollectionCellViewModels.isEmpty {
                self.data.markets.append(MarketViewModel(type: MarketType.ttl,
                                                         eventHeaderSection: goalsSectionHeader,
                                                         object: totalGoalsViewModel,
                                                         position: posMarket))
                self.data.markets.last?.ids = idsTotalGoalsMarkets
            }

            if indexInsertBaseSectionHeader < self.data.markets.count {
                self.data.markets[indexInsertBaseSectionHeader].baseSectionHeader = self.data.baseHeaderSections.last
            }
        }
        if isCallReloadData {
            self.view?.initData(markets: self.data.markets)
        }
    }
    
}


// MARK: - EventInteractorOutput

extension EventPresenter: EventInteractorOutput {
    
    func didObtainEventFromNetwork(_ events: [EventObjectWithJSON]) {
        guard !events.isEmpty else { return }
        
        let eventObjectWithJSON = SynchronizedValue(events[0])
        self.viewData.eventObjectWithJSON = eventObjectWithJSON
        
        self.view?.reloadDataHeaderView(event: eventObjectWithJSON.get())
        self.initData(event: eventObjectWithJSON.get(), isCallReloadData: true)
        self.subscribeToUpdates(for: events)
    }
    
    func didFailToObtainEventFromNetwork(with error: Error) {
        print(error)
    }
    
    func didObtainEventsUpdates(_ updates: [DiffMapModel]) {    
        print("Callback didObtainEventsUpdates")
        let patchEventsOperation = PatchEventsOperation(event: self.viewData.eventObjectWithJSON!, updates: updates)
        
        patchEventsOperation.completionBlock = { [weak self, weak patchEventsOperation] in
            guard let patchEventsOperation = patchEventsOperation, !patchEventsOperation.isCancelled else { return }
            guard let eventObjectWithJSON = self?.viewData.eventObjectWithJSON?.get() else { return }
            
            self?.reloadData(event: eventObjectWithJSON)

            DispatchQueue.main.async {
                self?.view?.reloadDataHeaderView(event: eventObjectWithJSON)
            }
        }
        
        self.patchEventsOperationQueue.addOperations([patchEventsOperation], waitUntilFinished: false)
    }
    
}
