//
//  EventModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EventModuleAssembly {

    /// Собрать модуль
    static func assembleModule(event: EventObject) -> UIViewController {
        let storyboard = UIStoryboard(storyboard: .event)
        let view: EventViewController = storyboard.instantiateViewController()

        let resolver = ApplicationAssembly.assembler.resolver
        guard let eventsService = resolver.resolve(EventsService.self) else { preconditionFailure() }
        guard let broadcastService = resolver.resolve(BroadcastService.self) else { preconditionFailure() }
        guard let observerService = resolver.resolve(ObserverService.self) else { preconditionFailure() }
        
        let presenter = EventPresenter(event: event)
        let interactor = EventInteractor(eventsService: eventsService, broadcastService: broadcastService, observerService: observerService)
        let router = EventRouter()

        view.output = presenter

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        interactor.output = presenter

        router.viewController = view

        return view
    }

}
