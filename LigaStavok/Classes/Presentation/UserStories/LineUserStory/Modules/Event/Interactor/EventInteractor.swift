//
//  EventInteractor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class EventInteractor {

    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Операция на получение событий
        weak var obtainingEventsFromNetworkOperation: Operation?
        /// Операция на получение обновлений по событиям
        weak var subscribingToEventsUpdatesOperation: Operation?
        /// Операция на получение сообщений о новых событиях
        weak var subscribingToNewEventsOperation: Operation?
        /// Операция на получение сообщений об удалении событий
        weak var subscribingToRemoveEventsOperation: Operation?
        
    }
    
    
    // MARK: - Публичные свойства

    /// Presenter
    weak var output: EventInteractorOutput!


    // MARK: - Приватные свойства
    
    /// Сервис для работы с событиями
    private let eventsService: EventsService
    /// Сервис для получения публичной информации
    private let broadcastService: BroadcastService
    /// Сервис для получения обновления по событиям
    private let observerService: ObserverService
    
    /// Локальные данные
    private var data = LocalData()
    
    
    // MARK: - Инициализация
    
    init(eventsService: EventsService,
         broadcastService: BroadcastService,
         observerService: ObserverService) {
        
        self.eventsService = eventsService
        self.broadcastService = broadcastService
        self.observerService = observerService
    }

}


// MARK: - EventInteractorInput

extension EventInteractor: EventInteractorInput {

    func obtainEventFromNetwork(withIds ids: [Int]) {
        cancelObtainingEventsFromNetwork()
        
        let operation = self.eventsService.obtainEventsListWithJSONs(gameId: nil,
                                                                     eventIds: ids,
                                                                     categoryIds: nil,
                                                                     tournamentIds: nil,
                                                                     topicId: nil,
                                                                     period: nil,
                                                                     limit: nil,
                                                                     namespace: .live,
                                                                     matchDay: nil,
                                                                     fullEvents: true,
                                                                     topEvents: nil) { [weak self] result in
            switch result {
            case .success(let events):
                self?.output.didObtainEventFromNetwork(events)
            case .failure(let error):
                self?.output.didFailToObtainEventFromNetwork(with: error)
            }
        }
        self.data.obtainingEventsFromNetworkOperation = operation
    }
    
    func cancelObtainingEventsFromNetwork() {
        self.data.obtainingEventsFromNetworkOperation?.cancel()
    }
    
    func subscribeToEventsUpdates(withIds ids: [Int]) {
        cancelSubscribingToEventsUpdates()
        
        let operation = self.observerService.subscribeToEventsUpdates(ids: ids) { [weak self] updates in
            self?.output.didObtainEventsUpdates(updates)
        }
        self.data.subscribingToEventsUpdatesOperation = operation
    }
    
    func cancelSubscribingToEventsUpdates() {
        self.data.subscribingToEventsUpdatesOperation?.cancel()
    }

}
