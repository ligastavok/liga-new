//
//  EventInteractorOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol EventInteractorOutput: class {
    
    /// Получены события из интернета
    func didObtainEventFromNetwork(_ events: [EventObjectWithJSON])
    /// При получении событий из интернета произошла ошибка
    func didFailToObtainEventFromNetwork(with error: Error)
    
    /// Получены изменения событий
    func didObtainEventsUpdates(_ updates: [DiffMapModel])
    
}
