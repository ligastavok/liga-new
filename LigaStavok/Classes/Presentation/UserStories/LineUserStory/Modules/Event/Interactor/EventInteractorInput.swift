//
//  EventInteractorInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol EventInteractorInput: class {
    
    /// Получить события из интернета
    func obtainEventFromNetwork(withIds id: [Int])
    /// Отмена получения событий из интернета
    func cancelObtainingEventsFromNetwork()
    
    /// Подписаться на изменения событий
    func subscribeToEventsUpdates(withIds ids: [Int])
    /// Отмена подписки на изменения событий
    func cancelSubscribingToEventsUpdates()
    
}
