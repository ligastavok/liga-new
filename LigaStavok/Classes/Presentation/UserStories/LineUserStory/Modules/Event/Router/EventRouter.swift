//
//  EventRouter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EventRouter: Router {

    // MARK: - Публичные свойства

    weak var viewController: UIViewController?


    // MARK: - Инициализация

    init(viewController: UIViewController? = nil) {
        self.viewController = viewController
    }

}


// MARK: - EventRouterInput

extension EventRouter: EventRouterInput { }
