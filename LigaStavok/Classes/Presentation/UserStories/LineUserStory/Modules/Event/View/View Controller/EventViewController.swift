//
//  EventViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import DeepDiff
import ExpyTableView

final class EventViewController: UIViewController {
    
    // MARK: - Типы данных

    private struct LocalData {
        
        /// Массив маркетов
        var markets: [MarketViewModel] = []
    }

    // MARK: - Outlet
    
    @IBOutlet
    private weak var tableView: ExpyTableView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            
            // Регистрация ячеек
            newValue.register(EventHeaderSectionTableViewCell.self)
            newValue.register(WinnerTableViewCell.self)
            newValue.register(AccurateAccountTableViewCell.self)
            newValue.register(AllowanceTableViewCell.self)
            newValue.register(OutcomeTableViewCell.self)
            newValue.register(EventOutcomeHeaderTableViewCell.self)
            newValue.register(EventAllowanceHeaderTableViewCell.self)
            newValue.register(TotalGoalsTableViewCell.self)
            newValue.register(NumberOfHostsTableViewCell.self)
            newValue.register(GlobalHeaderTableViewCell.self)
        }
    }
    

    // MARK: - Публичные свойства

    /// Presenter
    var output: EventViewOutput!
    
    /// Данные для истории матча (при тапе на счет)
    var scoreTeam: ScoreTeamCollectionViewModel!
    
    
    // MARK: - Приватные свойства
    
    /// Шапка таблицы
    private weak var headerView: EventHeaderView! {
        willSet {
            //newValue.setCollectionViewdelegate(delegate: self)
            newValue.initActions()
        }
    }
    
    /// Локальные данные
    private var data = LocalData()

    /// Колекция для (выбора/отображение) количества голов в маркете 'Всего голов'
    private weak var totalGoalsCollection: UICollectionView?

    /// Колекция для (выбора/отображение) количества голов в маркете 'Количество голов хозяев'
    private weak var numberOfHostsCollection: UICollectionView?
    
}


// MARK: - UIViewController

extension EventViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.output.didTriggerViewReadyEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.headerView.clearData()
        
        if let navigationController = self.navigationController as? NavigationController {
            if let tabBarController = tabBarController as? TabBarController {
                navigationController.followScrollView(self.tableView, delay: 40, scrollSpeedFactor: 2,
                                                      followers: [NavigationBarFollower(view: tabBarController.tabBar, direction: .scrollDown)])
            } else {
                navigationController.followScrollView(self.tableView, delay: 40, scrollSpeedFactor: 2)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navigationController = self.navigationController as? NavigationController {
            navigationController.stopFollowingScrollView(showingNavigationBar: true)
        }
    }
    
}


// MARK: - Приватные методы

private extension EventViewController {
    
    /// Перерисовать ячейки без анимации
    func reloadRowsNoneAnimated(indexPath: IndexPath) {
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: [indexPath], with: .none)
        self.tableView.endUpdates()
    }
    
}


// MARK: - EventViewInput

extension EventViewController: EventViewInput {
    
    func setupInitialState() {
        
        // Настройка шапки таблицы
        let headerView = EventHeaderView.loadFromNib()
        self.tableView.tableHeaderView = headerView
        self.headerView = headerView
        
        // Настройка сепараторов таблицы
        self.tableView.separatorStyle = .none
    }
    
    func reloadDataHeaderView(event: EventObjectWithJSON) {
        self.headerView.initDataHeaderView(event: event)
    }
    
    func getMarkets() -> [MarketViewModel] {
        var marketsCopy: [MarketViewModel] = []
        for market in self.data.markets {
            guard let copy = market.copy() as? MarketViewModel else { continue }
            marketsCopy.append(copy)
        }
        return marketsCopy
    }
    
    func reloadData(markets: [MarketViewModel]) {
        self.data.markets = markets
        // TODO: - reload sections or rows
        print("reloadData")
        tableView.reloadData()

        // Поддержка состояния секций таблицы
        for index in 0..<self.data.markets.count {
            if (self.data.markets[index].eventHeaderSection?.isOpen)! {
                self.tableView.expand(index)
            } else {
                self.tableView.collapse(index)
            }
        }
    }

    func initData(markets: [MarketViewModel]) {
        self.data.markets = markets
        print("initReloadData")
        tableView.reloadData()
    }
    
}


// MARK: - ExpyTableViewDataSource

extension EventViewController: ExpyTableViewDataSource {
    
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: EventHeaderSectionTableViewCell.self))
            as? EventHeaderSectionTableViewCell else { return UITableViewCell() }
        guard self.data.markets[section].eventHeaderSection != nil else { return UITableViewCell() }
        
        cell.configure(with: self.data.markets[section].eventHeaderSection!)
        cell.layoutMargins = UIEdgeInsets.zero
        
        return cell
    }
    
}


// MARK: - ExpyTableViewDelegate

extension EventViewController: ExpyTableViewDelegate {
    
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        switch state {
        case .willExpand:
            self.data.markets[section].eventHeaderSection?.isOpen = true
        case .willCollapse:
            self.data.markets[section].eventHeaderSection?.isOpen = false
        case .didExpand:
            self.data.markets[section].eventHeaderSection?.isOpen = true
        case .didCollapse:
            self.data.markets[section].eventHeaderSection?.isOpen = false
        }
    }
    
}


// MARK: - UITableViewDataSource

extension EventViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.markets.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let countObjs = (self.data.markets[section].object as? NSArray)?.count else {
            switch self.data.markets[section].type {
            case .exa, .ttl:
                return 2
            default:
                return 0
            }
        }
        return countObjs > 0 ? countObjs + 1 : countObjs
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch self.data.markets[indexPath.section].type {
        case .exa, .ttl:
            if (self.data.markets[indexPath.section].eventHeaderSection?.isOpen)! {
                // TODO: - Подумать возможен ли более правильный подход к вычислению размера секции 2-ой и 3-ей вложенности
                if indexPath.row == 0 {
                    return 53
                } else {
                    return 104
                }
            } else {
                return 53
            }
        default:
            return 53
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        guard let isOpenMarket = self.data.markets[section].eventHeaderSection?.isOpen else { return 0.1 }
        if isOpenMarket {
            return 10
        } else {
            return 0.1
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if self.data.markets[section].baseSectionHeader != nil {
            return 53
        } else {
            return 0.1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let baseHeaderSection = tableView.dequeueReusableHeaderFooterView(withIdentifier:
            String(describing: GlobalHeaderTableViewCell.self)) as? GlobalHeaderTableViewCell else { return nil }
        
        guard let title = self.data.markets[section].baseSectionHeader?.title else { return nil }
        
        baseHeaderSection.configure(title: title)
        return baseHeaderSection
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let objType = self.data.markets[indexPath.section].type
        switch objType {
        case .win:
            guard let winnerData = self.data.markets[indexPath.section].object
                as? [WinnerViewModel] else { return UITableViewCell() }
            
            let cell = tableView.dequeueReusableCell(for: indexPath) as WinnerTableViewCell
            cell.delegate = self
            cell.configure(with: winnerData[indexPath.row - 1])
            
            return cell
        case .han:
            guard let allowanceData = self.data.markets[indexPath.section].object
                as? [AllowanceViewModel] else { return UITableViewCell() }
            
            if allowanceData[indexPath.row - 1].isHeader {
                let cell = tableView.dequeueReusableCell(for: indexPath) as EventAllowanceHeaderTableViewCell
                cell.configure(with: allowanceData[indexPath.row - 1])
                
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(for: indexPath) as AllowanceTableViewCell
                cell.delegate = self
                cell.configure(with: allowanceData[indexPath.row - 1])
                
                return cell
            }
        case .exa:
            guard let accurateAccount = self.data.markets[indexPath.section].object
                as? AccurateAccountModelView else { return UITableViewCell() }
    
            let cell = tableView.dequeueReusableCell(for: indexPath) as AccurateAccountTableViewCell
            cell.delegate = self
            cell.configure(with: accurateAccount)
            
            return cell
        case .ttl:
            guard let totalGoals = self.data.markets[indexPath.section].object
                as? TotalGoalsViewModel else { return UITableViewCell() }
            let cell = tableView.dequeueReusableCell(for: indexPath) as TotalGoalsTableViewCell
            cell.setCollectionViewdelegate(delegate: self, forId: indexPath.section)
            cell.delegate = self
            cell.configure(with: totalGoals)

            return cell
            /*
        case .numberOfHosts:
            let numberOfHosts = self.data.numberOfHosts[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(for: indexPath) as NumberOfHostsTableViewCell
            cell.setCollectionViewdelegate(delegate: self, forRow: indexPath.row)
            self.numberOfHostsCollection = cell.numberOfHostsCollection
            cell.configure(with: numberOfHosts)
            
            return cell
        case .wholeMatch:
            return UITableViewCell()
        }*/
        case .odd:
            break
        case .nxg:
            break
        case .dbl:
            break
        case .err:
            break
        case .win2:
            break
        case .strz:
            break
        }
        return UITableViewCell()
    }
    
}


// MARK: - UICollectionViewDelegate & UICollectionViewDataSource

extension EventViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let goals = self.data.markets[collectionView.tag].object as? GoalsCollection else { return 0 }
        return goals.getCountGoalsCells()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch self.data.markets[collectionView.tag].type {
        case .ttl:
            guard let totalGoals = self.data.markets[collectionView.tag].object as? TotalGoalsViewModel else { return UICollectionViewCell() }
            let cell = collectionView.dequeueReusableCell(for: indexPath) as TotalGoalsCollectionViewCell
            cell.configure(with: totalGoals.goalsCells[indexPath.row])
            return cell
        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.data.markets[collectionView.tag].type {
        case .ttl:
            guard let totalGoals = self.data.markets[collectionView.tag].object as? TotalGoalsViewModel else { return }
            totalGoals.selectCell(row: indexPath.row)
            self.tableView.reloadSections([collectionView.tag], with: .none)
        default:
            break
        }
    }
    
}


// MARK: - TotalGoalsTableViewCellDelegate

extension EventViewController: TotalGoalsTableViewCellDelegate {
    func tapOnCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard let totalGoals = self.data.markets[indexPath.section].object as? TotalGoalsViewModel else { return }
        totalGoals.setSelectOther()
        tableView.reloadSections([indexPath.section], with: .none)
    }

    func tapOnGrossCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard let totalGoals = self.data.markets[indexPath.section].object as? TotalGoalsViewModel else { return }
        totalGoals.setSelectGross()
        tableView.reloadSections([indexPath.section], with: .none)
    }

    func tapOnLessCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard let totalGoals = self.data.markets[indexPath.section].object as? TotalGoalsViewModel else { return }
        totalGoals.setSelectLess()
        tableView.reloadSections([indexPath.section], with: .none)
    }
}


// MARK: - WinnerTableViewCellDelegate

extension EventViewController: WinnerTableViewCellDelegate {

    func tapOnWinnerCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard var outcomes = self.data.markets[indexPath.section].object
            as? [WinnerViewModel] else { return }
        outcomes[indexPath.row - 1].denialSelected()
        self.data.markets[indexPath.section].object = outcomes
        self.reloadRowsNoneAnimated(indexPath: indexPath)
    }

}


// MARK: - AllowanceTableViewCellDelegate

extension EventViewController: AllowanceTableViewCellDelegate {

    func tapOnAllowanceHomeTeamCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard var outcomes = self.data.markets[indexPath.section].object
            as? [AllowanceViewModel] else { return }
        outcomes[indexPath.row - 1].denialHomeSelected()
        self.data.markets[indexPath.section].object = outcomes
        self.reloadRowsNoneAnimated(indexPath: indexPath)
    }

    func tapOnAllowanceGuestTeamCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard var outcomes = self.data.markets[indexPath.section].object
            as? [AllowanceViewModel] else { return }
        outcomes[indexPath.row - 1].denialGuestSelected()
        self.data.markets[indexPath.section].object = outcomes
        self.reloadRowsNoneAnimated(indexPath: indexPath)
    }

}


// MARK: - AccurateAccountTableViewCellDelegate

extension EventViewController: AccurateAccountTableViewCellDelegate {
    
    func tapOnCellAccurateAccount(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard var accurateAccount = self.data.markets[indexPath.section].object
            as? AccurateAccountModelView else { return }

        accurateAccount.denialSelected()
        self.data.markets[indexPath.section].object = accurateAccount
        self.reloadRowsNoneAnimated(indexPath: indexPath)
    }

    func tapOnAccuratePlusHomeCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard var accurateAccount = self.data.markets[indexPath.section].object
            as? AccurateAccountModelView else { return }
        
        accurateAccount.firstTeamCountGoals += 1
        accurateAccount.animateValueChange = .none
        accurateAccount.generateAccurateAccountString()
        self.data.markets[indexPath.section].object = accurateAccount
        self.reloadRowsNoneAnimated(indexPath: indexPath)
    }

    func tapOnAccurateMinusHomeCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard var accurateAccount = self.data.markets[indexPath.section].object
            as? AccurateAccountModelView else { return }
        
        if accurateAccount.firstTeamCountGoals > 0 {
            accurateAccount.firstTeamCountGoals -= 1
            accurateAccount.animateValueChange = .none
            accurateAccount.generateAccurateAccountString()
            self.data.markets[indexPath.section].object = accurateAccount
            self.reloadRowsNoneAnimated(indexPath: indexPath)
        }
    }

    func tapOnAccuratePlusGuestCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard var accurateAccount = self.data.markets[indexPath.section].object
            as? AccurateAccountModelView else { return }
        
        accurateAccount.secondTeamCountGoals += 1
        accurateAccount.animateValueChange = .none
        accurateAccount.generateAccurateAccountString()
        self.data.markets[indexPath.section].object = accurateAccount
        self.reloadRowsNoneAnimated(indexPath: indexPath)
    }

    func tapOnAccurateMinusGuestCell(cell: UITableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: cell) else { return }
        guard var accurateAccount = self.data.markets[indexPath.section].object
            as? AccurateAccountModelView else { return }
        
        if accurateAccount.secondTeamCountGoals > 0 {
            accurateAccount.secondTeamCountGoals -= 1
            accurateAccount.animateValueChange = .none
            accurateAccount.generateAccurateAccountString()
            self.data.markets[indexPath.section].object = accurateAccount
            self.reloadRowsNoneAnimated(indexPath: indexPath)
        }
    }
}
