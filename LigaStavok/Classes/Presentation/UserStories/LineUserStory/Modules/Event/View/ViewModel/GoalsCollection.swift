//
//  GoalsCollection.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 01.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol GoalsCollection {

    /// Вернуть количество ячеек голов
    func getCountGoalsCells() -> Int
}
