//
//  NumberOfHostsTableViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 18.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class NumberOfHostsTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Колекция значений голов
    @IBOutlet
    weak var numberOfHostsCollection: UICollectionView! {
        willSet {
            
            // Регистрация ячеек
            newValue.register(TotalGoalsCollectionViewCell.self)
        }
    }
    
    /// Задний план колекции колекции
    @IBOutlet
    private weak var numberOfHostsCollectionBackView: UIView!
    
    /// Задний план отображения количества забитых голов
    @IBOutlet
    private weak var numberOfHostsBackView: UIView!
    
    /// Количество забитых голов
    @IBOutlet
    private weak var numberOfHosts: UILabel!
    
    /// Коэффициент
    @IBOutlet
    private weak var coefficient: UILabel!
    
}


// MARK: - NSObject

extension NumberOfHostsTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /// Радиус округления углов
        self.numberOfHostsCollectionBackView.layer.cornerRadius = 6
        self.numberOfHostsCollection.layer.cornerRadius = 6
        self.numberOfHostsBackView.layer.cornerRadius = 6
        
        /// Тень
        self.numberOfHostsCollectionBackView.dropShadow()
        self.numberOfHostsBackView.dropShadow()
    }
    
}


// MARK: - Публичные методы

extension NumberOfHostsTableViewCell {
    
    /// Настройка
    func configure(with viewModel: NumberOfHostsViewModel) {
        self.numberOfHosts.text = viewModel.numberOfGoalsScored
        self.coefficient.text = String(viewModel.coefficient)
    }
    
    /// Назначить делегат и источник данных для коллекции
    func setCollectionViewdelegate<D: UICollectionViewDelegate & UICollectionViewDataSource>(delegate: D, forRow row: Int) {
        self.numberOfHostsCollection.delegate = delegate
        self.numberOfHostsCollection.dataSource = delegate
        self.numberOfHostsCollection.tag = row
        
        self.numberOfHostsCollection.reloadData()
    }
    
}
