//
//  AccurateAccountTableViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class AccurateAccountTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet

    /// Задний план меню кнопок первой команды
    @IBOutlet
    private weak var firstTeamBackView: UIView!
    
    /// Задний план меню кнопок второй команды
    @IBOutlet
    private weak var secondTeamBackView: UIView!
    
    /// Задний план меню отображения точного счета матча
    @IBOutlet
    private weak var backViewAccurateAccount: UIView!
    
    /// Кнопка удалить первой команды
    @IBOutlet
    private weak var firstTeamBtnDelValue: UIButton!
    
    /// Кнопка добавить первой команды
    @IBOutlet
    private weak var firstTeamBtnAddValue: UIButton!
    
    /// Кнопка удалить второй команды
    @IBOutlet
    weak var secondTeamBtnDelValue: UIButton!
    
    /// Кнопка добавить второй команды
    @IBOutlet
    weak var secondTeamBtnAddValue: UIButton!
    
    /// Количество голов первой команды
    @IBOutlet
    private weak var firstTeamCountGoalsLabel: UILabel!
    
    /// Количество голов второй команды
    @IBOutlet
    private weak var secondTeamCountGoalsLabel: UILabel!
    
    /// Точный счет матча
    @IBOutlet
    private weak var accurateAccountLabel: UILabel!
    
    /// Коэфициент
    @IBOutlet
    private weak var coefficient: UILabel!
    
    /// Тень первого меню кнопок
    @IBOutlet
    private weak var firstTeamShadowView: UIView!
    
    /// Тень второго меню кнопок
    @IBOutlet
    private weak var secondTeamShadowView: UIView!
    
    
    // MARK: - Публичные свойства
    
    /// Делегат
    weak var delegate: AccurateAccountTableViewCellDelegate?
    
}


// MARK: - NSObject

extension AccurateAccountTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Округление углов заднего плана
        let cornerRadius: CGFloat = 6
        self.firstTeamBackView.layer.cornerRadius = cornerRadius
        self.secondTeamBackView.layer.cornerRadius = cornerRadius
        self.backViewAccurateAccount.layer.cornerRadius = cornerRadius
        self.secondTeamShadowView.layer.cornerRadius = cornerRadius
        self.firstTeamShadowView.layer.cornerRadius = cornerRadius
        
        // Тень
        self.secondTeamShadowView.dropShadow()
        self.firstTeamShadowView.dropShadow()
        self.backViewAccurateAccount.dropShadow()
        
        // Подписка кнопок на событие тап
        let tapGestureRecognizer1 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnAccuratePlusHomeTeam))
        self.firstTeamBtnAddValue.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnAccurateMinusHomeTeam))
        self.firstTeamBtnDelValue.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnAccuratePlusGuestTeam))
        self.secondTeamBtnAddValue.addGestureRecognizer(tapGestureRecognizer3)
        
        let tapGestureRecognizer4 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnAccurateMinusGuestTeam))
        self.secondTeamBtnDelValue.addGestureRecognizer(tapGestureRecognizer4)
        
        let tapGestureRecognizer5 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnCellAccurateAccount))
        self.backViewAccurateAccount.addGestureRecognizer(tapGestureRecognizer5)
    }
    
}


// MARK: - Публичные методы

extension AccurateAccountTableViewCell {
    
    /// Настройка
    func configure(with viewModel: AccurateAccountModelView) {
        changeStateCell(isSelected: viewModel.isSelected,
                        cell: backViewAccurateAccount,
                        titleLabel: accurateAccountLabel,
                        coefficient: coefficient)
        
        self.firstTeamCountGoalsLabel.text = String(viewModel.firstTeamCountGoals)
        self.secondTeamCountGoalsLabel.text = String(viewModel.secondTeamCountGoals)
        self.accurateAccountLabel.text = viewModel.getAccurateAccountString()
        self.coefficient.text = viewModel.getCoefficient() == -1 ? "" : String(viewModel.getCoefficient())
        
        startAnimateCoefficientChange(valueChangeType: viewModel.animateValueChange)
    }
}


// MARK: - Приватные методы

private extension AccurateAccountTableViewCell {
    
    /// Изменение состояния ячейки
    func changeStateCell(isSelected: Bool, cell: UIView, titleLabel: UILabel, coefficient: UILabel) {
        if isSelected {
            cell.backgroundColor = #colorLiteral(red: 0, green: 0.5960784314, blue: 0.4235294118, alpha: 1)
            titleLabel.textColor = .white
            coefficient.textColor = .white
        } else {
            cell.backgroundColor = .white
            titleLabel.textColor = .black
            coefficient.textColor = .black
        }
    }
    
    /// Анимация изменения
    func startAnimateCoefficientChange(valueChangeType: BetValueChangeType) {
        var timeInterval: TimeInterval = 3
        
        switch valueChangeType {
        case .ascending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)
            
            if self.backViewAccurateAccount.backgroundColor == .white && timeInterval > 0 {
                self.coefficient.textColor = #colorLiteral(red: 0, green: 0.831372549, blue: 0.6431372549, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .descending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)
            
            if self.backViewAccurateAccount.backgroundColor == .white && timeInterval > 0 {
                self.coefficient.textColor = #colorLiteral(red: 0.8745098039, green: 0.1960784314, blue: 0.1411764706, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .none:
            return
        }
        
        let timer = Timer(timeInterval: timeInterval,
                          target: self,
                          selector: #selector(stopHighlightingValue(_:)),
                          userInfo: nil,
                          repeats: false)
        
        RunLoop.main.add(timer, forMode: .defaultRunLoopMode)
    }
    
    /// Остановить анимацию
    @objc
    func stopHighlightingValue(_ timer: Timer) {
        guard self.backViewAccurateAccount.backgroundColor == .white else { return }
        
        self.coefficient.textColor = .black
        setNeedsLayout()
    }
    
    /// Вычисление времени анимации
    func calculateAnimatingStopDelay(_ stopDelay: inout TimeInterval, withLastUpdate lastUpdate: TimeInterval) {
        let now = Date.timeIntervalSinceReferenceDate
        stopDelay += lastUpdate - now
        
        guard stopDelay > 0 else { return }
        stopDelay = TimeInterval(ceil(stopDelay))
    }

    /// Тап на кнопку добавить гол домашней команды блок точный счет
    @objc
    func tapOnAccuratePlusHomeTeam(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnAccuratePlusHomeCell(cell: self)
    }

    /// Тап на кнопку убавить гол гостевой команды блок точный счет
    @objc
    func tapOnAccurateMinusHomeTeam(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnAccurateMinusHomeCell(cell: self)
    }

    /// Тап на кнопку добавить гол домашней команды блок точный счет
    @objc
    func tapOnAccuratePlusGuestTeam(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnAccuratePlusGuestCell(cell: self)
    }

    /// Тап на кнопку убарить гол гостевой команды блок точный счет
    @objc
    func tapOnAccurateMinusGuestTeam(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnAccurateMinusGuestCell(cell: self)
    }
    
    /// Тап на ячейку точного счета
    @objc
    func tapOnCellAccurateAccount(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnCellAccurateAccount(cell: self)
    }
}
