//
//  EventSectionHeaderViewModel.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct EventHeaderSectionViewModel: Hashable {
    
    // MARK: - Публичные свойства
    
    /// Название заголовка секции
    var title: String
    /// Состояние ячейки
    var isOpen: Bool
    /// Раскрывающийся ли блок
    var isActiveBlock: Bool
    /// Отображен ли блок
    var isVisible: Bool
    /// Позиция (для сортировки)
    
    
    // MARK: Инициализация
    
    init(title: String, isOpen: Bool = false, isActiveBlock: Bool = true) {
        self.title = title
        self.isOpen = isOpen
        self.isActiveBlock = isActiveBlock
        self.isVisible = false
    }
    
}
