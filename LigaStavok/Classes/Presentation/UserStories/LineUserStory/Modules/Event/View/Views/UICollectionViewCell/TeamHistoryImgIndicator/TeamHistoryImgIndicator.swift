//
//  TeamHistoryImgIndicator.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 19.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TeamHistoryImgIndicator: UICollectionViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Задний план ячейки колекции "История встреч команд"
    @IBOutlet
    private weak var cellBackground: UIView!
     
}


// MARK: - Публичные методы

extension TeamHistoryImgIndicator {
    
    /// Настройка
    func configure(with viewModel: ScoreTeamCollectionViewModel) {
        self.cellBackground.layer.cornerRadius = self.cellBackground.frame.height / 2
    }
    
}
