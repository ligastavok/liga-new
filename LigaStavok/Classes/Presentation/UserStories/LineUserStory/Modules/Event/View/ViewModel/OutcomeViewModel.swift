//
//  OutcomeViewModel.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 15.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct OutcomeViewModel {
    
    // MARK: - Публичные свойства

    // TODO: идентификаторы каждой ячейкт

    /// Домашняя команда значение 1
    var homeTeamF1Value: String = .empty
    /// Домашняя команда значение 2
    var homeTeamF2Value: Double = 0
    /// Команда на выезде значение 1
    var guestTeamF1Value: String = .empty
    /// Команда на выезде значение 2
    var guestTeamF2Value: Double = 0
    /// Ничья значение 1
    var drawF1Value: String = .empty
    /// Ничья значение 2
    var drawF2Value: Double = 0
    /// Шапка
    var isHeader = false
    /// Имя домашней команды
    var homeTeamName: String? = .empty
    /// Имя команды на выезде
    var guestTeamName: String? = .empty
    /// Ничья
    var drawName: String? = .empty
    /// Состояние ячейки домашней команды
    var isSelectedHomeTeamCell = false
    /// Состояние ячейки ничьей
    var isSelectedDrawCell = false
    /// Состояние ячейки гостевой команды
    var isSelectedGuestTeamCell = false

    // TODO: анимация каждой ячейки
    
    
    // MARK: - Инициализация
    
    init(homeTeamF1Value: String?,
         homeTeamF2Value: Double?,
         guestTeamF1Value: String?,
         guestTeamF2Value: Double?,
         drawF1Value: String?,
         drawF2Value: Double?,
         isHeader: Bool = false,
         homeTeamName: String = .empty,
         guestTeamName: String = .empty,
         drawName: String = .empty) {
        
        self.homeTeamF1Value = homeTeamF1Value ?? ""
        self.homeTeamF2Value = homeTeamF2Value ?? 0
        self.guestTeamF1Value = guestTeamF1Value ?? ""
        self.guestTeamF2Value = guestTeamF2Value ?? 0
        self.drawF1Value = drawF1Value ?? ""
        self.drawF2Value = drawF2Value ?? 0
        self.isHeader = isHeader
        self.homeTeamName = homeTeamName
        self.guestTeamName = guestTeamName
        self.drawName = drawName
    }
    
    init(homeTeamName: String = .empty,
         guestTeamName: String = .empty,
         drawName: String = .empty) {
        self.isHeader = true
        self.homeTeamName = homeTeamName
        self.guestTeamName = guestTeamName
        self.drawName = drawName
    }

    // TODO: логика анимации
}


// MARK: - Публичные методы

extension OutcomeViewModel {
    
    /// Присвоение отрицания свойства домашней команды
    mutating func denialHomeSelected() {
        self.isSelectedHomeTeamCell = !self.isSelectedHomeTeamCell
    }
    
    /// Присвоение отрицания свойства гостевой команды
    mutating func denialGuestSelected() {
        self.isSelectedGuestTeamCell = !self.isSelectedGuestTeamCell
    }
    
    /// Присвоение отрицания свойства ничьей
    mutating func denialDrawSelected() {
        self.isSelectedDrawCell = !self.isSelectedDrawCell
    }
    
}
