//
//  TotalGoalsCollectionViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 17.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TotalGoalsCollectionViewCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Задний план ячейки колекции "Всего голов"
    @IBOutlet
    private weak var cellBackground: UIView!
    /// Заголовок ячейки колекции "Всего голов"
    @IBOutlet
    private weak var cellTitle: UILabel!
    
}


// MARK: - Публичные методы

extension TotalGoalsCollectionViewCell {
    
    /// Настройка
    func configure(with viewModel: TotalGoalsCollectionCellViewModel) {
        self.cellBackground.backgroundColor = viewModel.isActive ? #colorLiteral(red: 0, green: 0.5960784314, blue: 0.4235294118, alpha: 1) : .white
        
        self.cellTitle.textColor = viewModel.isActive ? .white : .black
        self.cellTitle.text = viewModel.title

        if viewModel.animateTitle != .none {
            startAnimateCoefficientChange(valueChangeType: viewModel.animateTitle)
        }
    }
    
}


// MARK: - Приватные методы

private extension TotalGoalsCollectionViewCell {

    /// Анимация изменения
    func startAnimateCoefficientChange(valueChangeType: BetValueChangeType) {
        var timeInterval: TimeInterval = 3

        switch valueChangeType {
        case .ascending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)

            if self.cellBackground.backgroundColor == .white && timeInterval > 0 {
                self.cellTitle.textColor = #colorLiteral(red: 0, green: 0.831372549, blue: 0.6431372549, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .descending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)

            if self.cellBackground.backgroundColor == .white && timeInterval > 0 {
                self.cellTitle.textColor = #colorLiteral(red: 0.8745098039, green: 0.1960784314, blue: 0.1411764706, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .none:
            return
        }

        let timer = Timer(timeInterval: timeInterval,
                          target: self,
                          selector: #selector(stopHighlightingValue(_:)),
                          userInfo: nil,
                          repeats: false)

        RunLoop.main.add(timer, forMode: .defaultRunLoopMode)
    }

    /// Остановить анимацию
    @objc
    func stopHighlightingValue(_ timer: Timer) {
        guard self.cellBackground.backgroundColor == .white else { return }

        self.cellTitle.textColor = .black
        setNeedsLayout()
    }

    /// Вычисление времени анимации
    func calculateAnimatingStopDelay(_ stopDelay: inout TimeInterval, withLastUpdate lastUpdate: TimeInterval) {
        let now = Date.timeIntervalSinceReferenceDate
        stopDelay += lastUpdate - now

        guard stopDelay > 0 else { return }
        stopDelay = TimeInterval(ceil(stopDelay))
    }
}
