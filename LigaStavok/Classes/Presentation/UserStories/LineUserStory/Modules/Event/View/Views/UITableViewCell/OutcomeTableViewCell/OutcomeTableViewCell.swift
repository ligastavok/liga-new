//
//  OutcomeTableViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 15.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class OutcomeTableViewCell: UITableViewCell, NibLoadableView {

    // MARK: - Outlet

    /// Домашняя команда значение 1
    @IBOutlet
    private weak var homeTeamF1Value: UILabel!

    /// Домашняя команда значение 2
    @IBOutlet
    private weak var homeTeamF2Value: UILabel!
    
    /// Команда на выезде значение 1
    @IBOutlet
    private weak var guestTeamF1Value: UILabel!

    /// Команда на выезде значение 2
    @IBOutlet
    private weak var guestTeamF2Value: UILabel!
    
    /// Ничья значение 1
    @IBOutlet
    private weak var drawF1Value: UILabel!

    /// Ничья значение 2
    @IBOutlet
    private weak var drawF2Value: UILabel!
    
    /// Задний план домашней команды
    @IBOutlet
    weak var homeTeamBackView: UIView!

    /// Задний план команды на выезде
    @IBOutlet
    weak var guestTeamBackView: UIView!
    
    /// Задний план ничьей
    @IBOutlet
    weak var drawBackView: UIView!
    
    
    // MARK: - Публичные свойства
    
    /// Делегат
    weak var delegate: OutcomeTableViewCellDelegate?
    
}


// MARK: - NSObject

extension OutcomeTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Округление углов заднего плана
        let cornerRadius: CGFloat = 6
        self.homeTeamBackView.layer.cornerRadius = cornerRadius
        self.guestTeamBackView.layer.cornerRadius = cornerRadius
        self.drawBackView.layer.cornerRadius = cornerRadius
        
        // Тень
        self.homeTeamBackView.dropShadow()
        self.drawBackView.dropShadow()
        self.guestTeamBackView.dropShadow()
        
        // Подписка на событие
        let tapGestureRecognizer1 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnOutcomeHomeTeamCell))
        self.homeTeamBackView.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnOutcomeDrawTeamCell))
        self.drawBackView.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnOutcomeGuestTeamCell))
        self.guestTeamBackView.addGestureRecognizer(tapGestureRecognizer3)
    }
    
}


// MARK: - Публичные методы

extension OutcomeTableViewCell {
    
    /// Настройка
    func configure(with viewModel: OutcomeViewModel) {
        changeStateCell(isSelected: viewModel.isSelectedHomeTeamCell,
                        cell: self.homeTeamBackView,
                        f1Value: self.homeTeamF1Value,
                        f2Value: self.homeTeamF2Value)
        
        changeStateCell(isSelected: viewModel.isSelectedDrawCell,
                        cell: self.drawBackView,
                        f1Value: self.drawF1Value,
                        f2Value: self.drawF2Value)
        
        changeStateCell(isSelected: viewModel.isSelectedGuestTeamCell,
                        cell: self.guestTeamBackView,
                        f1Value: self.guestTeamF1Value,
                        f2Value: self.guestTeamF2Value)

        // Присвоение данных
        self.homeTeamF1Value.text = viewModel.homeTeamF1Value
        self.homeTeamF2Value.text = String(viewModel.homeTeamF2Value)
        self.guestTeamF1Value.text = viewModel.guestTeamF1Value
        self.guestTeamF2Value.text = String(viewModel.guestTeamF2Value)
        self.drawF1Value.text = viewModel.drawF1Value
        self.drawF2Value.text = String(viewModel.drawF2Value)
    }
    
}


// MARK: - Приватные методы

private extension OutcomeTableViewCell {
    
    /// Изменение состояния ячейки
    func changeStateCell(isSelected: Bool, cell: UIView, f1Value: UILabel, f2Value: UILabel) {
        if isSelected {
            cell.backgroundColor = #colorLiteral(red: 0, green: 0.5960784314, blue: 0.4235294118, alpha: 1)
            f1Value.textColor = .white
            f2Value.textColor = .white
        } else {
            cell.backgroundColor = .white
            f1Value.textColor = .black
            f2Value.textColor = .black
        }
    }

    /// Тап на ячейку домашней команды блока исход с форой
    @objc func tapOnOutcomeHomeTeamCell(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnOutcomeHomeTeamCell(cell: self)
    }

    /// Тап на ячейку ничьей блока исход с форой
    @objc func tapOnOutcomeDrawTeamCell(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnOutcomeDrawTeamCell(cell: self)
    }

    /// Тап на ячейку гостевой команды блока исход с форой
    @objc func tapOnOutcomeGuestTeamCell(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnOutcomeGuestTeamCell(cell: self)
    }
    
}
