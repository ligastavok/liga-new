//
//  WholeMatchTableViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 20.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import ExpyTableView

final class GlobalHeaderTableViewCell: UITableViewHeaderFooterView, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Заголовок
    @IBOutlet
    private weak var title: UILabel!
    
}


// MARK: - Публичные методы

extension GlobalHeaderTableViewCell {
    
    /// Настройка
    func configure(title: String) {
        self.title.text = title
    }
    
}
