//
//  ScoreTeamCollectionViewModel.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 19.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct ScoreTeamCollectionViewModel {
    
    // MARK: - Публичные свойства
    
    /// Счет домашней команды
    let scoreHomeTeam: String
    /// Счет гостевой команды
    let scoreGuestTeam: String
    /// Отображение состояние ячейки
    let isActive: Bool
    /// Отображения индикатора
    let isIndicater: Bool
    
    
    // MARK: - Инициализация
    
    init(scoreHomeTeam: String = .empty, scoreGuestTeam: String = .empty, isActive: Bool = false, isIndicater: Bool = false) {
        self.scoreHomeTeam = scoreHomeTeam
        self.scoreGuestTeam = scoreGuestTeam
        self.isActive = isActive
        self.isIndicater = isIndicater
    }
    
}
