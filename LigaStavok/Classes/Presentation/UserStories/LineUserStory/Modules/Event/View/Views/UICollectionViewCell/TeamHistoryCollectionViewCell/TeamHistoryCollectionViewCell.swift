//
//  TeamHistoryCollectionViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 19.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TeamHistoryCollectionViewCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Задний план ячейки колекции "История встреч команд"
    @IBOutlet
    private weak var cellBackground: UIView!
    
    /// Количество голов хозяев
    @IBOutlet
    private weak var scoreHomeTeam: UILabel!
    /// Количество голов гостей
    @IBOutlet
    private weak var scoreGuestTeam: UILabel!
    
}


// MARK: - Публичные методы

extension TeamHistoryCollectionViewCell {
    
    /// Настройка
    func configure(with viewModel: ScoreTeamCollectionViewModel) {
        self.cellBackground.backgroundColor = viewModel.isActive ? .lightGray : .white
        self.scoreHomeTeam.text = viewModel.scoreHomeTeam
        self.scoreGuestTeam.text = viewModel.scoreGuestTeam
    }
    
}
