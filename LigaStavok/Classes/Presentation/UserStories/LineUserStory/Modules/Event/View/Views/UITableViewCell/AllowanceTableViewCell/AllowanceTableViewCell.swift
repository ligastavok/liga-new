//
//  AllowanceTableViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 15.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class AllowanceTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Домашняя команда значение 1
    @IBOutlet
    private weak var homeTeamF1Value: UILabel!

    /// Домашняя команда значение 2
    @IBOutlet
    private weak var homeTeamF2Value: UILabel!
    
    /// Команда на выезде значение 1
    @IBOutlet
    private weak var guestTeamF1Value: UILabel!

    /// Команда на выезде значение 2
    @IBOutlet
    private weak var guestTeamF2Value: UILabel!
    
    /// Задний план команды на выезде
    @IBOutlet
    weak var guestTeamBackView: UIView!

    /// Задний план домашней команды
    @IBOutlet
    weak var homeTeamBackView: UIView!
    
    
    // MARK: - Публичные свойства
    
    /// Делегат
    weak var delegate: AllowanceTableViewCellDelegate?
    
}


// MARK: - NSObject

extension AllowanceTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Округление углов заднего плана
        let cornerRadius: CGFloat = 6
        self.homeTeamBackView.layer.cornerRadius = cornerRadius
        self.guestTeamBackView.layer.cornerRadius = cornerRadius
        
        // Тень
        self.homeTeamBackView.dropShadow()
        self.guestTeamBackView.dropShadow()
        
        /// Подписка на событие тап
        let tapGestureRecognizer1 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnAllowanceHomeTeamCell))
        self.homeTeamBackView.addGestureRecognizer(tapGestureRecognizer1)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnAllowanceGuestTeamCell))
        self.guestTeamBackView.addGestureRecognizer(tapGestureRecognizer2)
    }
    
}


// MARK: - Публичные методы

extension AllowanceTableViewCell {
    
    /// Настройка
    func configure(with viewModel: AllowanceViewModel) {
        
        // Настройка состояния ячейки домашней команды
        changeStateCell(isSelected: viewModel.isSelectedHomeTeamCell,
                        cell: self.homeTeamBackView,
                        f1Value: self.homeTeamF1Value,
                        f2Value: homeTeamF2Value)
        
        // Настройка состояния ячейки гостевой команды
        changeStateCell(isSelected: viewModel.isSelectedGuestTeamCell,
                        cell: self.guestTeamBackView,
                        f1Value: self.guestTeamF1Value,
                        f2Value: self.guestTeamF2Value)
        
        self.homeTeamF1Value.text = viewModel.homeTeamF1Value
        self.homeTeamF2Value.text = String(viewModel.homeTeamF2Value)
        
        self.homeTeamBackView.isHidden = viewModel.homeTeamF2Value == -1 ? true : false
        self.guestTeamBackView.isHidden = viewModel.guestTeamF2Value == -1 ? true : false
        
        self.guestTeamF1Value.text = viewModel.guestTeamF1Value
        self.guestTeamF2Value.text = String(viewModel.guestTeamF2Value)
        
        if viewModel.animateValueChangeHomeTeam != .none {
            startAnimateCoefficientChange(valueChangeType: viewModel.animateValueChangeHomeTeam,
                                          backView: self.homeTeamBackView,
                                          label: self.homeTeamF2Value,
                                          type: .homeTeam)
        }
        
        if viewModel.animateValueChangeGuestTeam != .none {
            startAnimateCoefficientChange(valueChangeType: viewModel.animateValueChangeGuestTeam,
                                          backView: self.guestTeamBackView,
                                          label: self.guestTeamF2Value,
                                          type: .guestTeam)
        }
    }

}


// MARK: - Приватные методы

private extension AllowanceTableViewCell {
    
    /// Изменение состояния ячейки
    func changeStateCell(isSelected: Bool, cell: UIView, f1Value: UILabel, f2Value: UILabel) {
        if isSelected {
            cell.backgroundColor = #colorLiteral(red: 0, green: 0.5960784314, blue: 0.4235294118, alpha: 1)
            f1Value.textColor = .white
            f2Value.textColor = .white
        } else {
            cell.backgroundColor = .white
            f1Value.textColor = .black
            f2Value.textColor = .black
        }
    }

    /// Анимация изменения
    func startAnimateCoefficientChange(valueChangeType: BetValueChangeType, backView: UIView, label: UILabel, type: AllowanceType) {
        var timeInterval: TimeInterval = 3
        
        switch valueChangeType {
        case .ascending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)
            
            if backView.backgroundColor == .white && timeInterval > 0 {
                label.textColor = #colorLiteral(red: 0, green: 0.831372549, blue: 0.6431372549, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .descending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)
            
            if backView.backgroundColor == .white && timeInterval > 0 {
                label.textColor = #colorLiteral(red: 0.8745098039, green: 0.1960784314, blue: 0.1411764706, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .none:
            return
        }

        var timer = Timer.init()
        switch type {
        case .homeTeam:
            timer = Timer(timeInterval: timeInterval,
                              target: self,
                              selector: #selector(stopHighlightingValueHomeTeam(_:)),
                              userInfo: nil,
                              repeats: false)
        case .guestTeam:
            timer = Timer(timeInterval: timeInterval,
                              target: self,
                              selector: #selector(stopHighlightingValueGuestTeam(_:)),
                              userInfo: nil,
                              repeats: false)
        }

        
        RunLoop.main.add(timer, forMode: .defaultRunLoopMode)
    }
    
    /// Остановить анимацию домашней команды
    @objc
    func stopHighlightingValueHomeTeam(_ timer: Timer) {
        guard self.homeTeamBackView.backgroundColor == .white else { return }
        
        self.homeTeamF2Value.textColor = .black
        setNeedsLayout()
    }
    
    /// Остановить анимацию гостевой команды
    @objc
    func stopHighlightingValueGuestTeam(_ timer: Timer) {
        guard self.guestTeamBackView.backgroundColor == .white else { return }
        
        self.guestTeamF2Value.textColor = .black
        setNeedsLayout()
    }
    
    /// Вычисление времени анимации
    func calculateAnimatingStopDelay(_ stopDelay: inout TimeInterval, withLastUpdate lastUpdate: TimeInterval) {
        let now = Date.timeIntervalSinceReferenceDate
        stopDelay += lastUpdate - now
        
        guard stopDelay > 0 else { return }
        stopDelay = TimeInterval(ceil(stopDelay))
    }

    /// Тап на ячейку домашней команды блока фора
    @objc
    func tapOnAllowanceHomeTeamCell(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnAllowanceHomeTeamCell(cell: self)
    }

    /// Тап на ячейку гостевой команды блока фора
    @objc
    func tapOnAllowanceGuestTeamCell(gesture: UIGestureRecognizer) {
        self.delegate?.tapOnAllowanceGuestTeamCell(cell: self)
    }

}
