//
//  EventAllowanceHeaderTableViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 15.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EventAllowanceHeaderTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Домашняя команда
    @IBOutlet
    private weak var homeTeamName: UILabel!
    /// Команда на выезде
    @IBOutlet
    private weak var guestTeamName: UILabel!
    
    /// Задний план домашней команды
    @IBOutlet
    private weak var homeTeamBackView: UIView!
    /// Задний план команды на выезде
    @IBOutlet
    private weak var guestTeamBackView: UIView!
    
}


// MARK: - NSObject

extension EventAllowanceHeaderTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Округление углов заднего плана
        let cornerRadius: CGFloat = 6
        self.homeTeamBackView.layer.cornerRadius = cornerRadius
        self.guestTeamBackView.layer.cornerRadius = cornerRadius
    }
    
}


// MARK: - Публичные методы

extension EventAllowanceHeaderTableViewCell {
    
    /// Настройка
    func configure(with viewModel: AllowanceViewModel) {
        self.homeTeamName.text = viewModel.homeTeamName
        self.guestTeamName.text = viewModel.guestTeamName
    }
    
}
