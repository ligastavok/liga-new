//
//  EventHeaderTableViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import ExpyTableView

final class EventHeaderSectionTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Заголовок
    @IBOutlet
    private weak var titleLabel: UILabel!
    
    /// Логотип состояния ячейки (закрыта/открыта)
    @IBOutlet
    private weak var stateCellImage: UIImageView!
    
    
    // MARK: - Публичные свойства
    
    weak var delegate: HeaderSectionViewDelegate?
    
}


// MARK: - Публичные методы

extension EventHeaderSectionTableViewCell {
    
    /// Настройка
    func configure(with viewModel: EventHeaderSectionViewModel) {
        self.titleLabel.text = viewModel.title
        if self.stateCellImage.image == nil {
            self.stateCellImage.image = viewModel.isOpen ? .init(asset: .iconCloseCell) : .init(asset: .iconOpenCell)
        }
    }
    
}


// MARK: - Приватные методы

private extension EventHeaderSectionTableViewCell {
    
    /// Анимация стрелки вверх
    private func arrowDown(animated: Bool) {
        UIView.animate(withDuration: (animated ? 0.3 : 0)) {
            self.stateCellImage.transform = CGAffineTransform(rotationAngle: (CGFloat.pi))
        }
    }
    
    /// Анимация стрелки вниз
    private func arrowUp(animated: Bool) {
        UIView.animate(withDuration: (animated ? 0.3 : 0)) {
            self.stateCellImage.transform = CGAffineTransform(rotationAngle: 0)
        }
    }
    
}


// MARK: - ExpyTableViewHeaderCell

extension EventHeaderSectionTableViewCell: ExpyTableViewHeaderCell {
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        switch state {
        case .willExpand:
            //print("WILL EXPAND")
            arrowDown(animated: true)
        case .willCollapse:
            //print("WILL COLLAPSE")
            arrowUp(animated: true)
        case .didExpand: break
            //print("DID EXPAND")
        case .didCollapse: break
            //print("DID COLLAPSE")
        }
    }
    
}
