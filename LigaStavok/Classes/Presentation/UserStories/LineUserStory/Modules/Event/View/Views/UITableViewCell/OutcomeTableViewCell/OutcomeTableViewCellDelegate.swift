//
//  OutcomeTableViewCellDelegate.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 06.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol OutcomeTableViewCellDelegate: class {
    
    ///
    func tapOnOutcomeHomeTeamCell(cell: UITableViewCell)
    ///
    func tapOnOutcomeDrawTeamCell(cell: UITableViewCell)
    ///
    func tapOnOutcomeGuestTeamCell(cell: UITableViewCell)
    
}
