//
//  AllowanceType.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 02.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

enum AllowanceType {
    case homeTeam
    case guestTeam
}
