//
//  NumberOfHostsViewModel.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 18.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct NumberOfHostsViewModel {
    
    // MARK: - Публичные свойства
    
    /// Варианты ячеек голов
    var goalsCells: [GoalCollectionCellViewModel]
    /// Пул выделенных ячеек
    var selectedGoalsCells: [GoalCollectionCellViewModel]
    /// Коэффициент
    var coefficient: Double
    /// Количество забитых голов
    var numberOfGoalsScored: String
    /// Ауткамы
    var outcomes: [Outcome]

    
    // MARK: - Инициализация
    init(outcomes: [Outcome]) {
        self.outcomes = outcomes
        self.goalsCells = []
        self.coefficient = -1
        self.numberOfGoalsScored = .empty
        self.selectedGoalsCells = []
        
        self.initNumberOfHosts()
    }
}


// MARK: - Приватные методы

private extension NumberOfHostsViewModel {
    
    /// Инициализация ячеек голов
    mutating func initNumberOfHosts() {
        // TODO: - Возможно тут нужно будет сохранять выделеные ранее ячейки
        for outcome in self.outcomes {
            goalsCells.append(GoalCollectionCellViewModel(title: outcome.title,
                                                          isActive: false,
                                                          valueCoef: outcome.value))
        }
        self.calculateCoefficient()
        self.generateNumberOfHostsString()
    }
    
    /// Вычисление кэфа
    mutating func calculateCoefficient() {
        guard let lastSelectedCell = self.selectedGoalsCells.last else {
            self.coefficient = -1
            return
        }
        self.coefficient = lastSelectedCell.valueCoef
    }
    
    /// Формирование строки сколько будет забито голов
    mutating func generateNumberOfHostsString() {
        let countGoals = self.selectedGoalsCells.last == nil ? "-" : self.selectedGoalsCells.last?.title
        self.numberOfGoalsScored = "Будет забито \(countGoals ?? "-") гол(а)"
    }
}
