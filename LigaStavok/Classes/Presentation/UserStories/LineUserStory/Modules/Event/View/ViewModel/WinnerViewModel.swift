//
//  WinnerViewModel.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct WinnerViewModel: Hashable {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор
    var id: Int
    /// Заголовок
    var title: String
    /// Коэффициент
    var coefficient: Double
    /// Выделен ли объект
    var isSeleted: Bool
    /// Флаг подтверждающий, что событие содержит незаблокированные исходы
    var isUnlocked: Bool
    /// Сортировка
    var sort: Int
    /// Анимация
    var animateValueChange: BetValueChangeType = .none
    /// Позиция (для сортировки)
    //var position: Int
    
    
    // MARK: - Инициализация

    init(outcomeWinner: Outcome) {
        self.id = outcomeWinner.id
        self.title = outcomeWinner.title
        self.coefficient = outcomeWinner.value
        self.isSeleted = false
        self.isUnlocked = true
        self.sort = outcomeWinner.position
        self.animateValueChange = .none
        
        // TODO: Узнать имя команды
        // TODO: Сервер возврашает в title русскую или английскую (большую или маленькую) букву X - x
        switch outcomeWinner.title {
        case "1":
            self.title = outcomeWinner.eventTeam.first
        case "2":
            self.title = outcomeWinner.eventTeam.second ?? outcomeWinner.title
        default:
            self.title = "Ничья"
        }
    }
    
}


// MARK: - Публичные методы

extension WinnerViewModel {
    
    /// Присвоение отрицания свойства isSelected
    mutating func denialSelected() {
        self.isSeleted = !self.isSeleted
    }
    
}
