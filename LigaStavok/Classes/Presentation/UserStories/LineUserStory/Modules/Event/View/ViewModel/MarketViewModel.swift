//
//  Markets.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 16.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

class MarketViewModel: NSCopying {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор маркета
    var id: Int
    var ids: [Int]
    /// Тип маркета
    var type: MarketType = .win
    /// Заголовок Маркета
    var eventHeaderSection: EventHeaderSectionViewModel?
    /// Массив объектов
    var object: Any
    /// Заголовок базовой секции
    var baseSectionHeader: EventHeaderSectionViewModel?
    /// Позиция (для сортировки)
    var position: Int
    
    
    // MARK: - Инициализация
    init(type: MarketType, eventHeaderSection: EventHeaderSectionViewModel?, object: Any, id: Int = -1, position: Int) {
        self.object = object
        self.eventHeaderSection = eventHeaderSection
        self.type = type
        self.baseSectionHeader = nil
        self.id = id
        self.position = position
        self.ids = []
    }


    init(id: Int,
         ids: [Int],
         type: MarketType,
         eventHeader: EventHeaderSectionViewModel?,
         object: Any,
         baseHeader: EventHeaderSectionViewModel?,
         position: Int) {

        self.id = id
        self.ids = ids
        self.type = type
        self.eventHeaderSection = eventHeader
        self.object = object
        self.baseSectionHeader = baseHeader
        self.position = position
    }

    func copy(with zone: NSZone? = nil) -> Any {
        let copy = MarketViewModel(id: self.id,
                                   ids: self.ids,
                                   type: self.type,
                                   eventHeader: self.eventHeaderSection,
                                   object: self.object,
                                   baseHeader: self.baseSectionHeader,
                                   position: self.position)
        return copy
    }
}
