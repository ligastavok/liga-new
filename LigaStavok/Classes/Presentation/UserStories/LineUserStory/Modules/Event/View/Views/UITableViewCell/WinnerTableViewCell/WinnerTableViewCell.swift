//
//  WinnerTableViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class WinnerTableViewCell: UITableViewCell, NibLoadableView {    
    
    // MARK: - Outlet

    /// Заголовок
    @IBOutlet
    private weak var titleLabel: UILabel!
    
    /// Коэффициент
    @IBOutlet
    private weak var coefficient: UILabel!
    
    /// Задний план
    @IBOutlet
    private weak var backView: UIView!
    
    
    // MARK: - Публичные свойства

    /// Делегат
    weak var delegate: WinnerTableViewCellDelegate?
}


// MARK: - NSObject

extension WinnerTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /// Округление углов заднего плана
        let cornerRadius: CGFloat = 6
        self.backView.layer.cornerRadius = cornerRadius
        
        /// Тень
        self.backView.dropShadow()
        
        // Подписка на события
        let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapOnWinnerCell))
        self.backView.addGestureRecognizer(tapGestureRecognizer)
    }
    
}


// MARK: - Публичные методы

extension WinnerTableViewCell {
    
    /// Настройка
    func configure(with viewModel: WinnerViewModel) {
        changeStateCell(isSelected: viewModel.isSeleted, cell: backView, titleLabel: titleLabel, coefficient: coefficient)
        
        self.titleLabel.text = viewModel.title
        self.coefficient.text = String(viewModel.coefficient)
        
        guard viewModel.animateValueChange != .none else { return }

        startAnimateCoefficientChange(valueChangeType: viewModel.animateValueChange)
    }
}


// MARK: - Приватные методы

private extension WinnerTableViewCell {
    
    /// Изменение состояния ячейки
    func changeStateCell(isSelected: Bool, cell: UIView, titleLabel: UILabel, coefficient: UILabel) {
        if isSelected {
            cell.backgroundColor = #colorLiteral(red: 0, green: 0.5960784314, blue: 0.4235294118, alpha: 1)
            titleLabel.textColor = .white
            coefficient.textColor = .white
        } else {
            cell.backgroundColor = .white
            titleLabel.textColor = .black
            coefficient.textColor = .black
        }
    }
    
    /// Анимация изменения
    func startAnimateCoefficientChange(valueChangeType: BetValueChangeType) {
        var timeInterval: TimeInterval = 3
        
        switch valueChangeType {
        case .ascending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)
            
            if self.backView.backgroundColor == .white && timeInterval > 0 {
                self.coefficient.textColor = #colorLiteral(red: 0, green: 0.831372549, blue: 0.6431372549, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .descending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)

            if self.backView.backgroundColor == .white && timeInterval > 0 {
                self.coefficient.textColor = #colorLiteral(red: 0.8745098039, green: 0.1960784314, blue: 0.1411764706, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .none:
            return
        }

        let timer = Timer(timeInterval: timeInterval,
                          target: self,
                          selector: #selector(stopHighlightingValue(_:)),
                          userInfo: nil,
                          repeats: false)
        
        RunLoop.main.add(timer, forMode: .defaultRunLoopMode)
    }
    
    /// Остановить анимацию
    @objc
    func stopHighlightingValue(_ timer: Timer) {
        guard self.backView.backgroundColor == .white else { return }
        
        self.coefficient.textColor = .black
        setNeedsLayout()
    }
    
    /// Вычисление времени анимации
    func calculateAnimatingStopDelay(_ stopDelay: inout TimeInterval, withLastUpdate lastUpdate: TimeInterval) {
        let now = Date.timeIntervalSinceReferenceDate
        stopDelay += lastUpdate - now
        
        guard stopDelay > 0 else { return }
        stopDelay = TimeInterval(ceil(stopDelay))
    }

    /// Тап на задний план ячейки
    @objc
    func tapOnWinnerCell() {
        self.delegate?.tapOnWinnerCell(cell: self)
    }
    
}
