//
//  EventHeaderView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EventHeaderView: UIView, NibLoadableView {

    // MARK: - Outlet

    
    /// Заголовок
    @IBOutlet
    private weak var titleLabel: UILabel! {
        willSet {
            newValue.text = "Сборные. Чемпионат мира 2018."
        }
    }
    
    /// Счет
    @IBOutlet
    private weak var scoreLabel: UILabel! {
        willSet {
            newValue.text = "0:1"
        }
    }
    
    /// Информация
    @IBOutlet
    private weak var infoLabel: UILabel! {
        willSet {
            newValue.text = "Второй тайм"
        }
    }
    
    /// Коллекция истории встреч команд
    @IBOutlet
    weak var collectionHistoryScore: UICollectionView! {
        willSet {
            
            // Регистрация ячеек
            newValue.register(TeamHistoryCollectionViewCell.self)
            newValue.register(TeamHistoryImgIndicator.self)
        }
    }
    
    /// Логотип первой команды
    @IBOutlet
    private weak var firstLogoImageView: UIImageView! {
        willSet {
            newValue.image = UIImage(asset: .placeholderBarcelona)
        }
    }
    /// Название первой команды
    @IBOutlet
    private weak var firstNameLabel: UILabel! {
        willSet {
            newValue.text = "Барселона"
        }
    }
    
    /// Логотип второй команды
    @IBOutlet
    private weak var secondLogoImageView: UIImageView! {
        willSet {
            newValue.image = UIImage(asset: .placeholderManchesterUnited)
        }
    }
    
    /// Название второй команды
    @IBOutlet
    private weak var secondNameLabel: UILabel! {
        willSet {
            newValue.text = "Манчестер Юнайтед"
        }
    }
    
    /// Live индикатор view
    @IBOutlet weak var liveView: UIView!
    
    /// Кнопка "Видеотрансляция"
    @IBOutlet
    private weak var videoTranslationButton: UIButton!

    /// Constraint live view
    @IBOutlet weak var liveViewTopConstraint: NSLayoutConstraint!
    
    /// Constraint info label
    @IBOutlet weak var infoLabelTopConstraint: NSLayoutConstraint!
    
    /// Info view
    @IBOutlet weak var infoView: UIView!
}


extension EventHeaderView {
    
    /// Назначить делегат и источник данных для коллекции
    func setCollectionViewdelegate<D: UICollectionViewDelegate & UICollectionViewDataSource>(delegate: D) {
        self.collectionHistoryScore.delegate = delegate
        self.collectionHistoryScore.dataSource = delegate
        self.collectionHistoryScore.reloadData()
    }
    
    /// Настройка
    func initDataHeaderView(event: EventObjectWithJSON) {
        self.liveView.isHidden = event.gameTimestamp.timeIntervalSinceReferenceDate >= Date.timeIntervalSinceReferenceDate ? true : false
        self.infoLabel.text = event.event.statusTranslated
        self.titleLabel.text = event.event.topicTitle
        self.secondNameLabel.text = event.event.firstTeamName
        self.firstNameLabel.text = event.event.secondTeamName ?? .empty
        self.scoreLabel.text = "\(event.scores.current?.firstTeamScore ?? "0"):\(event.scores.current?.secondTeamScore ?? "0")"
    }
    
    func clearData() {
        self.liveView.isHidden = true
        self.infoLabel.text = ""
        self.titleLabel.text = ""
        self.secondNameLabel.text = ""
        self.firstNameLabel.text = ""
        self.scoreLabel.text = ""
    }
    
    func initActions() {

        /// Подписываем счет на событие 'тап'
        let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapOnScoreTeam))
        self.scoreLabel.addGestureRecognizer(tapGestureRecognizer)

        /// Подписываем колекцию истории счета на событие 'тап'
        let tapGestureRecognizer2 = UITapGestureRecognizer.init(target: self,
                                                                action: #selector(tapOnCollectionHistoryStore))
        self.collectionHistoryScore.addGestureRecognizer(tapGestureRecognizer2)
    }
    
}


// MARK: - Приватные методы

private extension EventHeaderView {
    
    @objc
    func tapOnScoreTeam(gesture: UIGestureRecognizer) {
        self.scoreLabel.isHidden = true
        self.collectionHistoryScore.isHidden = false
        self.startAnimationLiveViewTop()
        self.startAnimationInfoLabelBottom()
    }
    
    @objc
    func tapOnCollectionHistoryStore(gesture: UIGestureRecognizer) {
        self.collectionHistoryScore.isHidden = true
        self.scoreLabel.isHidden = false
        self.startAnimationLiveViewBottom()
        self.startAnimationInfoLabelTop()
    }
    
    /// Анимация live индикатора вверх
    func startAnimationLiveViewTop() {
        UIView.animate(withDuration: 0.25, animations: {
            self.liveViewTopConstraint.constant -= 5
            self.infoView.layoutIfNeeded()
        })
    }
    
    /// Анимация live индикатора вниз
    func startAnimationLiveViewBottom() {
        UIView.animate(withDuration: 0.25, animations: {
            self.liveViewTopConstraint.constant += 5
            self.infoView.layoutIfNeeded()
        })
    }
    
    /// Анимация info label вверх
    func startAnimationInfoLabelTop() {
        UIView.animate(withDuration: 0.25, animations: {
            self.infoLabelTopConstraint.constant -= 9
            self.infoView.layoutIfNeeded()
        })
    }
    
    /// Анимация info label вниз
    func startAnimationInfoLabelBottom() {
        UIView.animate(withDuration: 0.25, animations: {
            self.infoLabelTopConstraint.constant += 9
            self.infoView.layoutIfNeeded()
        })
    }
    
}
