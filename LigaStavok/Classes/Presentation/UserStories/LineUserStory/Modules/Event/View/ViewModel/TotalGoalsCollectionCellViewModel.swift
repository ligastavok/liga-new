//
//  TotalGoalsCollectionCellViewModel.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 31.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

class TotalGoalsCollectionCellViewModel {

    // MARK: - Публичные свойства

    /// Идентификаторы исхода
    var idLess: Int = -1
    var idGross: Int = -1
    /// Заголовок
    var title: String = .empty
    /// Отображение состояние ячейки
    var isActive: Bool = false
    /// Кэф больше
    var grossValue: Double = -1
    /// Заголовок больше
    var grossTitle: String = .empty
    /// Кэф меньше
    var lessValue: Double = -1
    /// Заголовок меньше
    var lessTitle: String = .empty
    /// Выделен ли задний план gross
    var isSelectGrossBackView: Bool = false
    /// Выделен ли задний план less
    var isSelectLessBackView: Bool = false
    /// Выделен ли общий задний план
    var isSelectBackView: Bool = false
    /// Анимация
    var animateValueChangeGross: BetValueChangeType = .none
    var animateValueChangeLess: BetValueChangeType = .none
    var animateValueChangeOther: BetValueChangeType = .none
    var animateTitle: BetValueChangeType = .none
    
    // MARK: - Публичные методы

    /// Добавление gross значений
    func appendGrossValues(grossTitle: String, grossValue: Double) {
        self.grossTitle = grossTitle
        self.grossValue = grossValue
    }

    /// Добавление less значений
    func appendLessValues(lessTitle: String, lessValue: Double) {
        self.lessTitle = lessTitle
        self.lessValue = lessValue
    }

    /// Назначить заголовок ячейки
    func setTitle(grossOutcome: Outcome?, lessOutcome: Outcome?) {
        if grossOutcome != nil {
            self.title = (grossOutcome?.adValue)!
        } else if lessOutcome != nil {
            self.title = (lessOutcome?.adValue)!
        }
    }
}
