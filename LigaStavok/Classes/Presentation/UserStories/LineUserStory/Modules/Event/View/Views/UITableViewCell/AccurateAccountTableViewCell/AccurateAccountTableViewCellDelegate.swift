//
//  AccurateAccountTableViewCellDelegate.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 06.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol AccurateAccountTableViewCellDelegate: class {
    
    ///
    func tapOnAccuratePlusHomeCell(cell: UITableViewCell)
    ///
    func tapOnAccurateMinusHomeCell(cell: UITableViewCell)
    ///
    func tapOnAccuratePlusGuestCell(cell: UITableViewCell)
    ///
    func tapOnAccurateMinusGuestCell(cell: UITableViewCell)
    /// Выделение ячейки точного счета
    func tapOnCellAccurateAccount(cell: UITableViewCell)
}
