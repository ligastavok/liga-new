//
//  TotalGoalsTableViewCellDelegate.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 01.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol TotalGoalsTableViewCellDelegate: class {

    ///
    func tapOnGrossCell(cell: UITableViewCell)

    ///
    func tapOnLessCell(cell: UITableViewCell)

    ///
    func tapOnCell(cell: UITableViewCell)
}
