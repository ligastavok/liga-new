//
//  TotalGoalsViewModel.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 26.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

class TotalGoalsViewModel: GoalsCollection {
    
    // MARK: - Публичные свойства
    
    /// Варианты ячеек голов
    var goalsCells: [TotalGoalsCollectionCellViewModel]
    

    // MARK: - Инициализация

    init(goalsCells: [TotalGoalsCollectionCellViewModel]) {
        self.goalsCells = goalsCells
    }
}


// MARK: - Публичные методы
extension TotalGoalsViewModel {

    /// Выделить ячейку
    func selectCell(row: Int) {
        diselecAllCells()
        self.goalsCells[row].isActive = true
    }

    /// Метод нахождения текущей выделенной ячейки
    func getSelectCell() -> TotalGoalsCollectionCellViewModel? {
        for cell in self.goalsCells where cell.isActive {
            return cell
        }
        return nil
    }

    /// Выделить ячейку gross
    func setSelectGross() {
        if let cell = self.getSelectCell() {
            cell.isSelectGrossBackView = !cell.isSelectGrossBackView
        }
    }

    /// Выделить ячейку less
    func setSelectLess() {
        if let cell = self.getSelectCell() {
            cell.isSelectLessBackView = !cell.isSelectLessBackView
        }
    }

    /// Выделить либо less либо gross ячейку
    func setSelectOther() {
        if let cell = self.getSelectCell() {
            cell.isSelectBackView = !cell.isSelectBackView
        }
    }

    /// Обновление данных
    func updateData(newTotalGoalsCollection: [TotalGoalsCollectionCellViewModel]) {

        // Обновление всех ячеек
        for cell in newTotalGoalsCollection {
            guard let newAdValue = NumberFormatter.coefficientNumberFormater.number(from: cell.title)?.doubleValue else { break }
            for oldCell in self.goalsCells where oldCell.idLess == cell.idLess || oldCell.idGross == cell.idGross {
                guard let oldAdValue = NumberFormatter.coefficientNumberFormater.number(from: oldCell.title)?.doubleValue else { break }
                let lastUpdate = Date.timeIntervalSinceReferenceDate
                if newAdValue != oldAdValue {
                    cell.animateTitle = newAdValue > oldAdValue ?
                        .ascending(lastUpdate: lastUpdate) : .descending(lastUpdate: lastUpdate)
                }
            }
        }

        guard let selectCell = getSelectCell() else { return }
        for cell in newTotalGoalsCollection {
            if selectCell.idLess == cell.idLess || selectCell.idGross == cell.idGross {
                cell.isActive = true
                let lastUpdate = Date.timeIntervalSinceReferenceDate
                if cell.grossValue != -1 && cell.lessValue != -1 {
                    if cell.grossValue != selectCell.grossValue {
                        // Анимация для ячейки больше
                        cell.animateValueChangeGross = cell.grossValue > selectCell.grossValue ?
                            .ascending(lastUpdate: lastUpdate) : .descending(lastUpdate: lastUpdate)
                    }

                    if cell.lessValue != selectCell.lessValue {
                        // Анимация для общей меньше
                        cell.animateValueChangeLess = cell.lessValue > selectCell.lessValue ?
                            .ascending(lastUpdate: lastUpdate) : .descending(lastUpdate: lastUpdate)
                    }
                } else if cell.grossValue != -1 {
                    if cell.grossValue != selectCell.grossValue {
                        // Анимация для общей ячейки
                        cell.animateValueChangeOther = cell.grossValue > selectCell.grossValue ?
                            .ascending(lastUpdate: lastUpdate) : .descending(lastUpdate: lastUpdate)
                    }
                } else if cell.lessValue != -1 {
                    if cell.lessValue != selectCell.lessValue {
                        // Анимация для общей ячейки
                        cell.animateValueChangeOther = cell.lessValue > selectCell.lessValue ?
                            .ascending(lastUpdate: lastUpdate) : .descending(lastUpdate: lastUpdate)
                    }
                }
            }
        }
    }
}

// MARK: - Приватные методы
private extension TotalGoalsViewModel {

    /// Снять выделение всех ячеек
    func diselecAllCells() {
        for cell in self.goalsCells where cell.isActive {
            cell.isActive = false
        }
    }
}


// MARK: - GoalsCollection
extension TotalGoalsViewModel {

    ///Вернуть количество ячеек голов
    func getCountGoalsCells() -> Int {
        return self.goalsCells.count
    }
}
