//
//  EventViewOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol EventViewOutput: class {

    /// View загружено в память
    func didTriggerViewReadyEvent()
    
}
