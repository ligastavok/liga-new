//
//  TotalGoalsTableViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 17.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TotalGoalsTableViewCell: UITableViewCell, NibLoadableView {


    // MARK: Outlet
    
    /// Колекция тотал голов
    @IBOutlet
    weak var totalGoalsCollection: UICollectionView! {
        willSet {
            
            // Регистрация ячеек
            newValue.register(TotalGoalsCollectionViewCell.self)
        }
    }
    
    /// Задний план колекции тотал голов
    @IBOutlet
    private weak var totalGoalsCollectionBackView: UIView!
    
    /// Задний план отображения количества голов и меньше
    @IBOutlet
    private weak var totalGoalsLessBackView: UIView!

    /// Задний план отображения количества одного любого гола
    @IBOutlet
    private weak var totalGoalsBackView: UIView!

    /// Количество забитого гола
    @IBOutlet
    private weak var totalGoals: UILabel!

    /// Коэффициент гола
    @IBOutlet
    private weak var coefficient: UILabel!

    /// Задний план отображения количества голов и больше
    @IBOutlet
    private weak var totalGoalsGrossBackView: UIView!
    
    /// Количество забитых голов и меньше
    @IBOutlet
    private weak var totalGoalsLess: UILabel!
    
    /// Коэффициент голов и меньше
    @IBOutlet
    private weak var coefficientLess: UILabel!
    
    /// Количество забитых голов и более
    @IBOutlet
    private weak var totalGoalsGross: UILabel!
    
    /// Коэффициент голов и более
    @IBOutlet
    private weak var coefficientGross: UILabel!


    // MARK: - Публичные свойства

    /// Делегат
    weak var delegate: TotalGoalsTableViewCellDelegate?
}


// MARK: - NSObject

extension TotalGoalsTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()

        //self.totalGoalsCollection.alwaysBounceVertical = false
        //self.totalGoalsCollection.alwaysBounceHorizontal = false
        
        /// Радиус округления углов
        self.totalGoalsCollection.layer.cornerRadius = 6
        self.totalGoalsCollectionBackView.layer.cornerRadius = 6
        self.totalGoalsLessBackView.layer.cornerRadius = 6
        self.totalGoalsGrossBackView.layer.cornerRadius = 6
        self.totalGoalsBackView.layer.cornerRadius = 6
        
        /// Тень
        self.totalGoalsCollectionBackView.dropShadow()
        self.totalGoalsLessBackView.dropShadow()
        self.totalGoalsGrossBackView.dropShadow()
        self.totalGoalsBackView.dropShadow()

        // Подписка на события
        let tapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(tapOnGrossCell))
        self.totalGoalsGrossBackView.addGestureRecognizer(tapGestureRecognizer)

        let tapGestureRecognizer1 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnLessCell))
        self.totalGoalsLessBackView.addGestureRecognizer(tapGestureRecognizer1)

        let tapGestureRecognizer2 = UITapGestureRecognizer.init(target: self, action: #selector(tapOnCell))
        self.totalGoalsBackView.addGestureRecognizer(tapGestureRecognizer2)
        
    }
    
}


// MARK: - Публичные методы

extension TotalGoalsTableViewCell {
    
    /// Настройка
    func configure(with viewModel: TotalGoalsViewModel) {
        if let totalGoalCell = viewModel.getSelectCell() {
            if totalGoalCell.grossValue != -1 && totalGoalCell.lessValue != -1 {
                // Отображаем и gross и less значения
                self.totalGoalsBackView.isHidden = true
                self.totalGoalsLessBackView.isHidden = false
                self.totalGoalsGrossBackView.isHidden = false

                changeStateCell(isSelected: totalGoalCell.isSelectLessBackView,
                                cell: self.totalGoalsLessBackView,
                                titleLabel: self.totalGoalsLess,
                                coefficient: self.coefficientLess)

                self.totalGoalsLess.text = totalGoalCell.lessTitle
                self.coefficientLess.text = String(totalGoalCell.lessValue)

                changeStateCell(isSelected: totalGoalCell.isSelectGrossBackView,
                                cell: self.totalGoalsGrossBackView,
                                titleLabel: self.totalGoalsGross,
                                coefficient: self.coefficientGross)

                self.totalGoalsGross.text = totalGoalCell.grossTitle
                self.coefficientGross.text = String(totalGoalCell.grossValue)
            } else {
                if totalGoalCell.grossValue != -1 {
                    // отображаем только gross значения
                    self.totalGoalsBackView.isHidden = false
                    self.totalGoalsLessBackView.isHidden = true
                    self.totalGoalsGrossBackView.isHidden = true

                    changeStateCell(isSelected: totalGoalCell.isSelectBackView,
                                    cell: self.totalGoalsBackView,
                                    titleLabel: self.totalGoals,
                                    coefficient: self.coefficient)

                    self.totalGoals.text = totalGoalCell.grossTitle
                    self.coefficient.text = String(totalGoalCell.grossValue)
                }

                if totalGoalCell.lessValue != -1 {
                    // отображаем только less значения
                    self.totalGoalsBackView.isHidden = false
                    self.totalGoalsLessBackView.isHidden = true
                    self.totalGoalsGrossBackView.isHidden = true

                    changeStateCell(isSelected: totalGoalCell.isSelectBackView,
                                    cell: self.totalGoalsBackView,
                                    titleLabel: self.totalGoals,
                                    coefficient: self.coefficient)

                    self.totalGoals.text = totalGoalCell.lessTitle
                    self.coefficient.text = String(totalGoalCell.lessValue)
                }
            }
            if totalGoalCell.animateValueChangeGross != .none {
                startAnimateCoefficientChange(valueChangeType: totalGoalCell.animateValueChangeGross,
                                              backView: self.totalGoalsGrossBackView,
                                              label: self.coefficientGross,
                                              type: .gross)
            }

            if totalGoalCell.animateValueChangeLess != .none {
                startAnimateCoefficientChange(valueChangeType: totalGoalCell.animateValueChangeLess,
                                              backView: self.totalGoalsLessBackView,
                                              label: self.coefficientLess,
                                              type: .less)
            }

            if totalGoalCell.animateValueChangeOther != .none {
                startAnimateCoefficientChange(valueChangeType: totalGoalCell.animateValueChangeOther,
                                              backView: self.totalGoalsBackView,
                                              label: self.coefficient,
                                              type: .other)
            }
        } else {
            self.totalGoalsGrossBackView.isHidden = false
            self.totalGoalsLessBackView.isHidden = false
            self.totalGoalsBackView.isHidden = true

            self.totalGoalsGrossBackView.backgroundColor = UIColor.white
            self.totalGoalsLessBackView.backgroundColor = UIColor.white

            self.totalGoalsLess.text = .empty
            self.totalGoalsGross.text = .empty

            self.coefficientLess.text = .empty
            self.coefficientGross.text = .empty
        }

        //self.totalGoalsCollection.layoutIfNeeded()
        //self.totalGoalsCollection.contentSize.height
    }
    
    /// Назначить делегат и источник данных для коллекции
    func setCollectionViewdelegate<D: UICollectionViewDelegate & UICollectionViewDataSource> (delegate: D, forId idMarket: Int) {
        self.totalGoalsCollection.delegate = delegate
        self.totalGoalsCollection.dataSource = delegate
        self.totalGoalsCollection.tag = idMarket
        
        self.totalGoalsCollection.reloadData()
    }

    @objc
    func tapOnGrossCell() {
        self.delegate?.tapOnGrossCell(cell: self)
    }

    @objc
    func tapOnLessCell() {
        self.delegate?.tapOnLessCell(cell: self)
    }

    @objc
    func tapOnCell() {
        self.delegate?.tapOnCell(cell: self)
    }
}


// MARK: Приватные методы

private extension TotalGoalsTableViewCell {

    /// Изменение состояния ячейки
    func changeStateCell(isSelected: Bool, cell: UIView, titleLabel: UILabel, coefficient: UILabel) {
        if isSelected {
            cell.backgroundColor = #colorLiteral(red: 0, green: 0.5960784314, blue: 0.4235294118, alpha: 1)
            titleLabel.textColor = .white
            coefficient.textColor = .white
        } else {
            cell.backgroundColor = .white
            titleLabel.textColor = .black
            coefficient.textColor = .black
        }
    }

    /// Анимация изменения
    func startAnimateCoefficientChange(valueChangeType: BetValueChangeType, backView: UIView, label: UILabel, type: TotalGoalsTypeAnimation) {
        var timeInterval: TimeInterval = 3

        switch valueChangeType {
        case .ascending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)

            if backView.backgroundColor == .white && timeInterval > 0 {
                label.textColor = #colorLiteral(red: 0, green: 0.831372549, blue: 0.6431372549, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .descending(let lastUpdate):
            calculateAnimatingStopDelay(&timeInterval, withLastUpdate: lastUpdate)

            if backView.backgroundColor == .white && timeInterval > 0 {
                label.textColor = #colorLiteral(red: 0.8745098039, green: 0.1960784314, blue: 0.1411764706, alpha: 1)
                setNeedsLayout()
            } else {
                return
            }
        case .none:
            return
        }

        var timer = Timer.init()
        switch type {
        case .gross:
            timer = Timer(timeInterval: timeInterval,
                          target: self,
                          selector: #selector(stopHighlightingValueGross(_:)),
                          userInfo: nil,
                          repeats: false)
        case .less:
            timer = Timer(timeInterval: timeInterval,
                          target: self,
                          selector: #selector(stopHighlightingValueLess(_:)),
                          userInfo: nil,
                          repeats: false)
        case .other:
            timer = Timer(timeInterval: timeInterval,
                          target: self,
                          selector: #selector(stopHighlightingValueOther(_:)),
                          userInfo: nil,
                          repeats: false)
        }


        RunLoop.main.add(timer, forMode: .defaultRunLoopMode)
    }

    /// Остановить анимацию less
    @objc
    func stopHighlightingValueLess(_ timer: Timer) {
        guard self.totalGoalsLessBackView.backgroundColor == .white else { return }

        self.coefficientLess.textColor = .black
        setNeedsLayout()
    }

    /// Остановить анимацию gross
    @objc
    func stopHighlightingValueGross(_ timer: Timer) {
        guard self.totalGoalsGrossBackView.backgroundColor == .white else { return }

        self.coefficientGross.textColor = .black
        setNeedsLayout()
    }

    /// Остановить анимацию other
    @objc
    func stopHighlightingValueOther(_ timer: Timer) {
        guard self.totalGoalsBackView.backgroundColor == .white else { return }

        self.coefficient.textColor = .black
        setNeedsLayout()
    }

    /// Вычисление времени анимации
    func calculateAnimatingStopDelay(_ stopDelay: inout TimeInterval, withLastUpdate lastUpdate: TimeInterval) {
        let now = Date.timeIntervalSinceReferenceDate
        stopDelay += lastUpdate - now

        guard stopDelay > 0 else { return }
        stopDelay = TimeInterval(ceil(stopDelay))
    }
}
