//
//  EventViewInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol EventViewInput: class {

    /// Подготовка
    func setupInitialState()
    
    /// Инициализация шапки таблицы
    func reloadDataHeaderView(event: EventObjectWithJSON)

    ///
    func initData(markets: [MarketViewModel])
    
    ///
    func reloadData(markets: [MarketViewModel])
    
    /// Вернуть все маркеты
    func getMarkets() -> [MarketViewModel]
}
