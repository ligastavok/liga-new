//
//  TotalGoalsViewModel.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 17.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct GoalCollectionCellViewModel {
    
    // MARK: - Публичные свойства
    
    /// Заголовок
    let title: String
    /// Отображение состояние ячейки
    var isActive: Bool
    /// Значение ячейкт (как правило кэф)
    var valueCoef: Double
    
}
