//
//  AccurateAccountModelView.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

class AccurateAccountModelView {
    
    // MARK: - Публичные свойства
    
    /// Счет первой команды
    var firstTeamCountGoals: Int
    /// Счет второй команды
    var secondTeamCountGoals: Int
    /// Ауткамы точного счета
    var outcomes: [Outcome]
    /// Строка отображения счета
    private var accurateAccount: String = .empty
    /// Коэффициент точного счета
    private var coefficient: Double = -1
    /// Анимация
    var animateValueChange: BetValueChangeType = .none
    /// Выделена ли ячейка
    var isSelected: Bool = false
    
    // MARK: - Инициализация
    
    init(firstTeamGoals: Int, secondTeamGoals: Int, outcomes: [Outcome]) {
        self.firstTeamCountGoals = firstTeamGoals
        self.secondTeamCountGoals = secondTeamGoals
        self.outcomes = outcomes
        self.generateAccurateAccountString()
    }
}


// MARK: - Публичные методы

extension AccurateAccountModelView {
    
    /// Генерация строки "точный счет матча"
    func generateAccurateAccountString() {
        self.accurateAccount = calculateCoefficient()
    }
    
    /// Вернуть строку отображения счета
    func getAccurateAccountString() -> String {
        return self.accurateAccount
    }
    
    /// Вернуть кэф точного счета
    func getCoefficient() -> Double {
        return self.coefficient
    }
    
    /// Присвоение отрицания свойства isSelected
    func denialSelected() {
        self.isSelected = !self.isSelected
    }
    
    /// Изменился ли кэф у текущего счета
    func isChangeCoef(newOutcomes: [Outcome]) {
        let accurateAccount = "\(self.firstTeamCountGoals):\(self.secondTeamCountGoals)"
        for outcome in newOutcomes where accurateAccount == outcome.title {
            guard outcome.value != self.coefficient else { return }
            let lastUpdate = Date.timeIntervalSinceReferenceDate
            if outcome.value > self.coefficient {
                self.animateValueChange = BetValueChangeType.ascending(lastUpdate: lastUpdate)
            } else {
                self.animateValueChange = BetValueChangeType.descending(lastUpdate: lastUpdate)
            }
            return
        }
    }
}


// MARK: - Приватные методы

extension AccurateAccountModelView {
    
    /// Вычисление коэффициента точного счета
    func calculateCoefficient() -> String {
        let accurateAccount = "\(self.firstTeamCountGoals):\(self.secondTeamCountGoals)"

        for outcome in outcomes where accurateAccount == outcome.title {
            self.coefficient = outcome.value
            return "Точный счет матча \(accurateAccount)"
        }
        self.coefficient = -1
        return "Точный счет матча \(accurateAccount)"
    }
}
