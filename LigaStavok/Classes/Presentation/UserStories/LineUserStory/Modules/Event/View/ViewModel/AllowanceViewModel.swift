//
//  AllowanceViewModel.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 15.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct AllowanceViewModel: Hashable {
    
    // MARK: - Типы данных
    
    private enum TypeCell: Int, Hashable {
        
        /// Ячейка домашней команды
        case homeTeamCell
        /// Ячейка гостевой команды
        case guestTeamCell
        
    }

    
    // MARK: - Публичные свойства

    /// Идентификатор домашней ячейки
    var idHomeTeam: Int = 0
    /// Идентификатор гостевой ячейки
    var idGuestTeam: Int = 0
    /// Домашняя команда значение 1
    var homeTeamF1Value: String = .empty
    /// Домашняя команда значение 2
    var homeTeamF2Value: Double = 0
    /// Команда на выезде значение 1
    var guestTeamF1Value: String = .empty
    /// Команда на выезде значение 2
    var guestTeamF2Value: Double = 0
    /// Шапка
    var isHeader = false
    /// Имя домашней команды
    var homeTeamName: String = .empty
    /// Имя команды на выезде
    var guestTeamName: String = .empty
    /// Состояние ячейки домашней команды
    var isSelectedHomeTeamCell: Bool = false
    /// Состояние ячейки гостевой команды
    var isSelectedGuestTeamCell: Bool = false
    /// Анимация
    var animateValueChangeHomeTeam: BetValueChangeType = .none
    var animateValueChangeGuestTeam: BetValueChangeType = .none

    // TODO: анимация для след ячеек
    
    
    // MARK: - Инициализация

    init(homeTeamF1Value: String? = .empty,
         homeTeamF2Value: Double? = -1,
         guestTeamF1Value: String? = .empty,
         guestTeamF2Value: Double? = -1,
         isHeader: Bool = false,
         homeTeamName: String = .empty,
         guestTeamName: String = .empty,
         idHomeCell: Int?,
         idGuestCell: Int?) {
        
        self.homeTeamF1Value = homeTeamF1Value ?? .empty
        self.homeTeamF2Value = homeTeamF2Value ?? -1
        self.guestTeamF1Value = guestTeamF1Value ?? .empty
        self.guestTeamF2Value = guestTeamF2Value ?? -1
        self.isHeader = isHeader
        self.homeTeamName = homeTeamName
        self.guestTeamName = guestTeamName
        self.idHomeTeam = idHomeCell ?? -1
        self.idGuestTeam = idGuestCell ?? -1
    }
    
    init(homeTeamName: String = .empty,
         guestTeamName: String = .empty) {
        
        self.homeTeamName = homeTeamName
        self.guestTeamName = guestTeamName
        self.isHeader = true
    }

    // TODO: логика анимации 
}


// MARK: - Публичные методы

extension AllowanceViewModel {
    
    /// Присвоение отрицания свойства домашней команды
    mutating func denialHomeSelected() {
        self.isSelectedHomeTeamCell = !self.isSelectedHomeTeamCell
    }
    
    /// Присвоение отрицания свойства гостевой команды
    mutating func denialGuestSelected() {
        self.isSelectedGuestTeamCell = !self.isSelectedGuestTeamCell
    }
    
}
