//
//  NumberOfHostsCollectionViewCell.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 18.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class NumberOfHostsCollectionViewCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Задний план ячейки колекции "Количество голов хозяев"
    @IBOutlet
    private weak var cellBackground: UIView!
    /// Заголовок ячейки колекции "Количество голов хозяев"
    @IBOutlet
    private weak var cellTitle: UILabel!
    
}


// MARK: - Публичные методы

extension NumberOfHostsCollectionViewCell {
    
    /// Настройка
    func configure(with viewModel: GoalCollectionCellViewModel) {
        self.cellBackground.backgroundColor = viewModel.isActive ? #colorLiteral(red: 0, green: 0.5960784314, blue: 0.4235294118, alpha: 1) : .white
        
        self.cellTitle.textColor = viewModel.isActive ? .white : .black
        self.cellTitle.text = viewModel.title
    }
    
}
