//
//  LiveTranslationsViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LiveTranslationsViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet
    private weak var collectionView: UICollectionView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            
            newValue.register(LiveTranslationCollectionViewCell.self)
        }
    }
    
    
    // MARK: - Публичные свойства
    
    /// Presenter
    var output: LiveTranslationsViewOutput!
    
}


// MARK: - UIViewController

extension LiveTranslationsViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.output.didTriggerViewReadyEvent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navigationController = self.navigationController as? NavigationController {
            navigationController.stopFollowingScrollView(showingNavigationBar: true)
        }
    }
    
}


// MARK: - LiveTranslationsViewInput

extension LiveTranslationsViewController: LiveTranslationsViewInput { }


// MARK: - UICollectionViewDataSource

extension LiveTranslationsViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(for: indexPath) as LiveTranslationCollectionViewCell
        cell.configure(icon: UIImage(asset: .placeholder),
                       isActive: indexPath.item < 3,
                       title: "Запись \(indexPath.item + 1)")
        
        return cell
    }
    
}


// MARK: - UICollectionViewDelegate

extension LiveTranslationsViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
}
