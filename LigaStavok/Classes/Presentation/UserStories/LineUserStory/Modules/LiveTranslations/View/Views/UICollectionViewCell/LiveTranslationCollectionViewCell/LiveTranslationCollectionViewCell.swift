//
//  LiveTranslationCollectionViewCell.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LiveTranslationCollectionViewCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Live-трансляция
    @IBOutlet
    private weak var liveTranslationView: LiveTranslationView!
    /// Название
    @IBOutlet
    private weak var titleLabel: UILabel!
    
}


// MARK: - UICollectionViewCell

extension LiveTranslationCollectionViewCell {
    
    override var isHighlighted: Bool {
        didSet {
            if self.isHighlighted {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    self.liveTranslationView.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                })
            } else {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseOut, animations: {
                    self.liveTranslationView.transform = CGAffineTransform(scaleX: 1, y: 1)
                })
            }
        }
    }
    
}


// MARK: - Публичные методы

extension LiveTranslationCollectionViewCell {
    
    /// Настройка
    func configure(icon: UIImage?, isActive: Bool, title: String) {
        self.liveTranslationView.icon = icon
        self.liveTranslationView.isActive = isActive
        
        self.titleLabel.text = title
        self.titleLabel.textColor = isActive ? .black : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
    }
    
}
