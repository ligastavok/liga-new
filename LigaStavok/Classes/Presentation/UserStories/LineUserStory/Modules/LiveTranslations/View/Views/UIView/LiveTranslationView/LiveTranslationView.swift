//
//  LiveTranslationView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class LiveTranslationView: UIView, NibLoadableSelfOwnerView {
    
    // MARK: - Outlet
    
    /// Иконка
    @IBOutlet
    private weak var iconImageView: UIImageView!
    
    
    // MARK: - Публичные свойства
    
    /// Состояние
    @IBInspectable
    var isActive: Bool = false {
        willSet {
            self.circleLayer.strokeColor = newValue ? #colorLiteral(red: 0, green: 0.8078431373, blue: 0.5764705882, alpha: 1) : #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1)
        }
    }
    
    
    // MARK: - Приватные свойства
    
    /// Круг
    private var circleLayer: CAShapeLayer!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
        // Настройка круга
        self.circleLayer = CAShapeLayer()
        self.circleLayer.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.circleLayer.lineWidth = 2
        
        self.layer.addSublayer(self.circleLayer)
        
        // Другие настройки
        self.isActive = false
    }
    
}


// MARK: - Публичные свойства

extension LiveTranslationView {
    
    /// Иконка
    @IBInspectable
    var icon: UIImage? {
        get {
            return self.iconImageView.image
        } set {
            self.iconImageView.image = newValue?.roundedCorners(with: self.bounds.size)
        }
    }
    
}


// MARK: - UIView

extension LiveTranslationView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Настройка круга
        let rect = self.bounds.insetBy(dx: 1, dy: 1)
        let circlePath = UIBezierPath(ovalIn: rect)
        self.circleLayer.path = circlePath.cgPath
    }
    
}
