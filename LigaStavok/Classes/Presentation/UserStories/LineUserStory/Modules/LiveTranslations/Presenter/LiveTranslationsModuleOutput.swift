//
//  LiveTranslationsModuleOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LiveTranslationsModuleOutput: class {
    
    /// Модуль "Live-трансляции" загружен
    func didLoadLiveTranslationsModule(_ liveTranslationsModule: LiveTranslationsModuleInput)
    
}
