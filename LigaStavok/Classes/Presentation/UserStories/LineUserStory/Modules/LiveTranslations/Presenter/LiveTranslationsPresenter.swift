//
//  LiveTranslationsPresenter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class LiveTranslationsPresenter {
    
    // MARK: - Публичные свойства
    
    /// View
    weak var view: LiveTranslationsViewInput?
    /// Interactor
    var interactor: LiveTranslationsInteractorInput!
    /// Router
    var router: LiveTranslationsRouterInput!
    /// Контейнер
    weak var moduleOutput: LiveTranslationsModuleOutput?
    
}


// MARK: - LiveTranslationsViewOutput

extension LiveTranslationsPresenter: LiveTranslationsViewOutput {
    
    func didTriggerViewReadyEvent() {
        self.moduleOutput?.didLoadLiveTranslationsModule(self)
    }
    
}


// MARK: - LiveTranslationsInteractorOutput

extension LiveTranslationsPresenter: LiveTranslationsInteractorOutput { }


// MARK: - LiveTranslationsModuleInput

extension LiveTranslationsPresenter: LiveTranslationsModuleInput { }
