//
//  LiveTranslationsModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LiveTranslationsModuleAssembly {
    
    /// Собрать модуль
    static func assembleModule(with output: LiveTranslationsModuleOutput? = nil) -> UIViewController {
        let storyboard = UIStoryboard(storyboard: .liveTranslations)
        let view: LiveTranslationsViewController = storyboard.instantiateViewController()
        
        let presenter = LiveTranslationsPresenter()
        let interactor = LiveTranslationsInteractor()
        let router = LiveTranslationsRouter()
        
        view.output = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        presenter.moduleOutput = output
        
        interactor.output = presenter
        
        router.viewController = view
        
        return view
    }
    
}
