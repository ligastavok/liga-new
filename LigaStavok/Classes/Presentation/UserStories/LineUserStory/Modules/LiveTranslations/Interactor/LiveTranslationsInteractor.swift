//
//  LiveTranslationsInteractor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class LiveTranslationsInteractor {
    
    // MARK: - Публичные свойства
    
    /// Presenter
    weak var output: LiveTranslationsInteractorOutput!
    
}


// MARK: - LineInteractorInput

extension LiveTranslationsInteractor: LiveTranslationsInteractorInput { }
