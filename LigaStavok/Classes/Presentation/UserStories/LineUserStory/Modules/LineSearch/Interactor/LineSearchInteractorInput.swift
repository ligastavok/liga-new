//
//  LineSearchInteractorInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LineSearchInteractorInput: class {
    
    /// Поиск событий
    func searchEventsFromNetwork(with searchRequest: String?)
    /// Отмена поиска событий
    func cancelSearchingEventsFromNetwork()
    
}
