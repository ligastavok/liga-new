//
//  LineSearchInteractor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class LineSearchInteractor {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Операция поиска
        weak var searchOperation: Operation?
        
    }
    

    // MARK: - Публичные свойства

    /// Presenter
    weak var output: LineSearchInteractorOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Сервис для поиска
    private let searchService: SearchService
    
    /// Локальные данные
    private var data = LocalData()


    // MARK: - Инициализация

    init(searchService: SearchService) {
        self.searchService = searchService
    }

}


// MARK: - LineSearchInteractorInput

extension LineSearchInteractor: LineSearchInteractorInput {
    
    func searchEventsFromNetwork(with searchRequest: String?) {
        cancelSearchingEventsFromNetwork()
        
        let operation = self.searchService.searchEvents(search: searchRequest,
                                                        gameId: nil,
                                                        skip: nil,
                                                        limit: nil) { [weak self] result in
            switch result {
            case .success(let events):
                self?.output.didSearchEventsFromNetwork(events)
            case .failure(let error):
                self?.output.didFailToSearchEventsFromNetwork(with: error)
            }
        }
        self.data.searchOperation = operation
    }
    
    func cancelSearchingEventsFromNetwork() {
        self.data.searchOperation?.cancel()
    }
    
}
