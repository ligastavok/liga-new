//
//  LineSearchInteractorOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LineSearchInteractorOutput: class {
    
    /// Выполнен поиск событий
    func didSearchEventsFromNetwork(_ events: [EventObject])
    /// При поиске событий произошла ошибка
    func didFailToSearchEventsFromNetwork(with error: Error)
    
}
