//
//  LineSearchViewInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol LineSearchViewInput: class {
    
    /// Установка начальных данных
    func setInitialData(sections: [HeaderSectionViewModel])

    /// Подготовка
    func setupInitialState()
    
    /// Подготовить view к отображению
    func prepareWillAppear()
    
    /// Подготовить view к прекращению отображения
    func prepareWillDisappear()
    
    /// Установить статус
    func setStatus(_ status: ModuleStatus)
    
    /// Отобразить данные
    func showData(events: [EventViewModel])
    
    /// Установить активность поиска
    func setSearchActive(_ active: Bool)

}
