//
//  LineSearchViewDelegate.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 06.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LineSearchViewDelegate: class {
    
    /// Начало редактирования
    func lineSearchViewDidBeginEditing(_ lineSearchView: LineSearchView)
    
    /// Конец редактирования
    func lineSearchViewDidEndEditing(_ lineSearchView: LineSearchView)
    
    /// Начало редактирования
    func lineSearchViewEditingChanged(_ lineSearchView: LineSearchView, value: String?)
    
    /// Конец редактирования
    func lineSearchViewDidTapSearchButton(_ lineSearchView: LineSearchView)
    
    /// Нажата кнопка "Отменить"
    func lineSearchViewDidTapCancelButton(_ lineSearchView: LineSearchView)
    
}
