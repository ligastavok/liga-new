//
//  LineSearchViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

final class LineSearchViewController: UIViewController {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Статус
        var status: ModuleStatus = .unknown
        
        /// Заголовки секциий
        var sectionsHeaders: [HeaderSectionViewModel] = []
        
        /// События
        var events: [EventViewModel] = []
        
    }
    
    
    // MARK: - Outlet
    
    @IBOutlet
    private weak var tableView: UITableView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            
            // Регистрация верхнего колонтитула
            newValue.register(HeaderSectionView.self)
            
            // Регистрация ячеек
            newValue.register(EventTableViewCell.self)
        }
    }
    
    
    // MARK: - Публичные свойства

    /// Presenter
    var output: LineSearchViewOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Поисковая строка
    private weak var searchView: LineSearchView! {
        willSet {
            newValue?.delegate = self
        }
    }
    
    /// Локальные данные
    private var data = LocalData()

}


// MARK: - UIViewController

extension LineSearchViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.output.didTriggerViewReadyEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.output.didTriggerViewWillAppearEvent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.output.didTriggerViewWillDisappearEvent()
    }

}


// MARK: - Приватные методы

private extension LineSearchViewController {
    
    /// Скрыть клавиатуру
    @objc
    func dismissKeyboardInSearchView() {
        self.searchView.dismissKeyboard()
    }
    
}


// MARK: - LineSearchViewInput

extension LineSearchViewController: LineSearchViewInput {
    
    func setInitialData(sections: [HeaderSectionViewModel]) {
        self.data.sectionsHeaders = sections
    }

    func setupInitialState() {
        
        // Строка поиска
        let searchView = LineSearchView.loadFromNib()
        self.navigationItem.titleView = searchView
        self.searchView = searchView
        
        // Отмена редактирования по тапу
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboardInSearchView))
        tapGestureRecognizer.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGestureRecognizer)
        
        // Настройка таблицы
        self.tableView.contentInset = UIEdgeInsets(top: -35, left: 0, bottom: -20, right: 0)
        self.tableView.tableFooterView = UIView()
        
        // Настройка фреймворка DZNEmptyDataSet
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
    }
    
    func prepareWillAppear() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        self.navigationItem.hidesBackButton = true
    }
    
    func prepareWillDisappear() {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func setStatus(_ status: ModuleStatus) {
        guard self.data.status != status else { return }
        self.data.status = status
        
        switch status {
        case .unknown, .loading, .error:
            self.tableView.reloadData()
            
        default:
            return
        }
    }
    
    func showData(events: [EventViewModel]) {
        self.data.events = events
        self.tableView.reloadData()
    }
    
    func setSearchActive(_ active: Bool) {
        if active {
            self.searchView.showKeyboard()
        } else {
            self.searchView.dismissKeyboard()
        }
    }

}


// MARK: - UITableViewDataSource

extension LineSearchViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = self.data.events[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(for: indexPath) as EventTableViewCell
        cell.configure(with: event)
        cell.delegate = self
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.sectionsHeaders.count
    }
    
}


// MARK: - UITableViewDelegate

extension LineSearchViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 34 + (section == 0 ? 10 : 0)
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 10
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            let sectionHeader = self.data.sectionsHeaders[section]
            
            let lineHeaderSectionView = tableView.dequeueReusableHeaderFooterView() as HeaderSectionView
            lineHeaderSectionView.configure(with: sectionHeader)
            
            return lineHeaderSectionView
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return UIView()
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let event = self.data.events[indexPath.row]
        self.output.didSelectEvent(event)
    }
    
}


// MARK: - DZNEmptyDataSetSource

extension LineSearchViewController: DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text: String
        switch self.data.status {
        case .unknown, .loading:
            return nil
        case .loaded:
            text = "Ничего не найдено"
        case .error(let error):
            text = error.localizedDescription
        }
        
        let helper = EmptyDataSetSourceHelper()
        return helper.generateTitle(withText: text)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let text: String
        switch self.data.status {
        case .unknown, .loading, .loaded:
            return nil
        case .error:
            text = "Повторить"
        }
        
        let helper = EmptyDataSetSourceHelper()
        return helper.generateButtonTitle(withText: text)
    }
    
    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
        switch self.data.status {
        case .loading:
            let helper = EmptyDataSetSourceHelper()
            return helper.generateLoadingView(for: scrollView)
        case .unknown, .loaded, .error:
            return nil
        }
    }
    
}


// MARK: - DZNEmptyDataSetDelegate

extension LineSearchViewController: DZNEmptyDataSetDelegate {
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.output.didTapTryAgainButton()
    }
    
}


// MARK: - LineSearchViewDelegate

extension LineSearchViewController: LineSearchViewDelegate {
    
    func lineSearchViewDidBeginEditing(_ lineSearchView: LineSearchView) { }
    
    func lineSearchViewDidEndEditing(_ lineSearchView: LineSearchView) { }
    
    func lineSearchViewEditingChanged(_ lineSearchView: LineSearchView, value: String?) {
        guard let searchRequest = value else { return }
        
        self.output.didEditSearchRequest(searchRequest)
    }
    
    func lineSearchViewDidTapSearchButton(_ lineSearchView: LineSearchView) {
        guard let searchRequest = lineSearchView.text else { return }
        
        self.output.didTapSearchButton(with: searchRequest)
    }
    
    func lineSearchViewDidTapCancelButton(_ lineSearchView: LineSearchView) {
        self.output.didTapCancelButton()
    }
    
}


// MARK: - EventTableViewCellDelegate

extension LineSearchViewController: EventTableViewCellDelegate {
    
    func eventTableViewCellDidSelectRow(_ eventTableViewCell: EventTableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: eventTableViewCell),
            eventTableViewCell.isAvailable else { return }
        
        self.tableView(self.tableView, didSelectRowAt: indexPath)
    }
    
    func eventTableViewCellDidTapFavoritesButton(_ eventTableViewCell: EventTableViewCell) { }
    
}
