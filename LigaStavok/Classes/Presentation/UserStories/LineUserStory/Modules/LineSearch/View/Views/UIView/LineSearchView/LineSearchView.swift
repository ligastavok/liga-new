//
//  LineSearchView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 06.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class LineSearchView: UIView, NibLoadableView {
    
    // MARK: - Outlet
    
    @IBOutlet
    private weak var textField: UITextField! {
        willSet {
            newValue.delegate = self
        }
    }
    
    /// Кнопка "Отменить"
    @IBOutlet
    private weak var cancelButton: UIButton!
    
    
    // MARK: - Публичные свойства
    
    weak var delegate: LineSearchViewDelegate?
    
}


// MARK: - Публичные свойства

extension LineSearchView {
    
    /// Состояние
    var isEditing: Bool {
        return self.textField.isFirstResponder
    }
    
    /// Текущее значение
    var text: String? {
        get {
            return self.textField.text
        }
        set {
            self.textField.text = newValue
        }
    }
    
}


// MARK: - UIView

extension LineSearchView {
    
    override var intrinsicContentSize: CGSize {
        return UILayoutFittingExpandedSize
    }
    
}


// MARK: - Action

private extension LineSearchView {
    
    /// Изменен текст
    @IBAction
    func textFieldEditingChanged() {
        self.delegate?.lineSearchViewEditingChanged(self, value: self.textField.text)
    }
    
    /// Нажата кнопка "Отменить"
    @IBAction
    func cancelButtonTapped() {
        self.delegate?.lineSearchViewDidTapCancelButton(self)
        dismissKeyboard()
    }
    
}


// MARK: - Публичные методы

extension LineSearchView {
    
    /// Отобразить клавиатуру
    func showKeyboard() {
        self.textField.becomeFirstResponder()
    }
    
    /// Скрыть клавиатуру
    func dismissKeyboard() {
        self.textField.resignFirstResponder()
    }
    
}


// MARK: - UITextViewDelegate

extension LineSearchView: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.delegate?.lineSearchViewDidBeginEditing(self)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.delegate?.lineSearchViewDidEndEditing(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.delegate?.lineSearchViewDidTapSearchButton(self)
        dismissKeyboard()
        
        return true
    }
    
}
