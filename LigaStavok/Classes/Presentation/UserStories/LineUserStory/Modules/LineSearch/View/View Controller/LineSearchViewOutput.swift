//
//  LineSearchViewOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LineSearchViewOutput: class {

    /// View загружено в память
    func didTriggerViewReadyEvent()
    
    /// View собирается быть отображен
    func didTriggerViewWillAppearEvent()
    
    /// View собирается прекратить быть отображенным
    func didTriggerViewWillDisappearEvent()
    
    /// Нажата кнопка "Отмена"
    func didTapCancelButton()
    
    /// Изменен поисковый запрос
    func didEditSearchRequest(_ searchRequest: String)
    
    /// Нажата кнопка "Поиск"
    func didTapSearchButton(with searchRequest: String)
    
    /// Нажата кнопка "Повторить"
    func didTapTryAgainButton()
    
    /// Выбрано событие
    func didSelectEvent(_ viewModel: EventViewModel)

}
