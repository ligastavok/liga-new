//
//  LineSearchTextFiled.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 06.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class LineSearchTextFiled: UITextField {
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Настройка скругления углов
        self.layer.cornerRadius = 4
        self.layer.masksToBounds = true
    }
    
}


// MARK: - UIView

extension LineSearchTextFiled {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        runtimeSetup(forInterfaceBuilder: true)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        runtimeSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        for view in self.subviews {
            if let button = view as? UIButton, let icon = button.image(for: .normal) {
                button.setImage(icon.withRenderingMode(.alwaysTemplate), for: .normal)
                button.tintColor = .white
            }
        }
    }

}


// MARK: - UITextField

extension LineSearchTextFiled {
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var leftViewRect = super.leftViewRect(forBounds: bounds)
        leftViewRect.origin.x += 10
        
        return leftViewRect
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        let paddingLeft: CGFloat = 29
        let paddingRight: CGFloat = 19
        return CGRect(x: bounds.origin.x + paddingLeft,
                      y: bounds.origin.y,
                      width: bounds.size.width - paddingLeft - paddingRight,
                      height: bounds.size.height)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
}


// MARK: - Приватные методы

private extension LineSearchTextFiled {
    
    /// Подготовить иконку
    func setupIcon(forInterfaceBuilder: Bool) {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(square: 13)))
        imageView.contentMode = .scaleAspectFit
        if forInterfaceBuilder {
            let bundle = Bundle(for: self.classForCoder)
            imageView.image = UIImage(asset: .iconSearch,
                                      in: bundle,
                                      compatibleWith: self.traitCollection)
        } else {
            imageView.image = UIImage(asset: .iconSearch)
        }
        
        self.leftView = imageView
        self.leftViewMode = .always
    }
    
    /// Настройка при выполнении
    func runtimeSetup(forInterfaceBuilder: Bool = false) {
        self.backgroundColor = #colorLiteral(red: 0, green: 0.4156862745, blue: 0.2901960784, alpha: 1)
        self.tintColor = .white
        
        let font: UIFont = .systemFont(ofSize: 14)
        
        self.font = font
        self.textColor = .white
        
        let placeholder = self.placeholder ?? .empty
        let placeholderTextAttributes: [NSAttributedStringKey: Any] = [
            .font: font,
            .foregroundColor: #colorLiteral(red: 0.4980392157, green: 0.7058823529, blue: 0.6431372549, alpha: 1)
        ]
        let attributedPlaceholder = NSAttributedString(string: placeholder,
                                                       attributes: placeholderTextAttributes)
        self.attributedPlaceholder = attributedPlaceholder
        
        setupIcon(forInterfaceBuilder: forInterfaceBuilder)
    }
    
}
