//
//  LineSearchPresenter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class LineSearchPresenter {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Статус
        var status: ModuleStatus = .unknown
        
        /// Минимальная длина поискового запроса
        let minimumSearchRequestLength = 3
        /// Поисковый запрос
        var searchRequest: String?
        
        /// События
        var events: [EventObject] = []
        
    }
    
    private struct LocalViewData {
        
        /// Секции
        var sections = [HeaderSectionViewModel(title: Localization.LineSearch.eventsSectionTitle, buttonTitle: nil)]
        
        /// События
        var eventsViewModels: [EventViewModel] = []
        
    }
    
    
    // MARK: - Публичные свойства

    /// View
    weak var view: LineSearchViewInput?
    /// Interactor
    var interactor: LineSearchInteractorInput!
    /// Router
    var router: LineSearchRouterInput!
    
    
    // MARK: - Приватные свойства
    
    /// Локальные данные
    private var data = LocalData()
    /// Данные представления
    private var viewData = LocalViewData()
    
    /// Таймер на отложенный поиск
    private weak var searchTimer: Timer?
    
    
    // MARK: - Деинициализация
    
    deinit {
        self.searchTimer?.invalidate()
    }

}


// MARK: - Приватные методы

private extension LineSearchPresenter {
    
    /// Подготовка поискового запроса
    @objc
    func prepareSearchRequest(_ timer: Timer) {
        guard let searchRequest = timer.userInfo as? String else { return }
        processSearchRequest(searchRequest, postponed: true)
    }
    
    /// Обработать поисковый запрос
    func processSearchRequest(_ searchRequest: String, postponed: Bool) {
        if case .loading = self.data.status, self.data.searchRequest == searchRequest {
            return
        }
        
        self.data.searchRequest = searchRequest
        self.data.events = []
        
        prepareEvents()
        showData()
        
        guard searchRequest.count >= self.data.minimumSearchRequestLength || !postponed else {
            cancelSearch()
            return
        }
        
        performSearch(withRequest: searchRequest)
    }
    
    /// Выполнить поиск по событиям
    func performSearch(withRequest searchRequest: String) {
        self.data.status = .loading(type: .network)
        self.view?.setStatus(self.data.status)
        
        self.interactor.searchEventsFromNetwork(with: searchRequest)
    }
    
    /// Отменить поиск
    func cancelSearch() {
        self.data.status = .unknown
        self.view?.setStatus(self.data.status)
        
        self.interactor.cancelSearchingEventsFromNetwork()
    }
    
    /// Подготовка событий
    func prepareEvents() {
        let viewModels = self.data.events.map { EventViewModel(eventObject: $0) }
        self.viewData.eventsViewModels = viewModels
    }
    
    /// Отобразить данные
    func showData() {
        let eventsViewModels = self.viewData.eventsViewModels
        self.view?.showData(events: eventsViewModels)
    }
    
    /// Обработка ошибки
    func processError(_ error: Error) {
        self.data.status = .error(value: error)
        self.view?.setStatus(self.data.status)
    }
    
    /// Отсортировать события
    func sortEvents(_ events: [EventObject]) -> [EventObject] {
        return events//.sorted(by: { $0.sort < $1.sort })
    }
    
}


// MARK: - LineSearchViewOutput

extension LineSearchPresenter: LineSearchViewOutput {

    func didTriggerViewReadyEvent() {
        self.view?.setInitialData(sections: self.viewData.sections)
        self.view?.setupInitialState()
        
        showData()
    }
    
    func didTriggerViewWillAppearEvent() {
        self.view?.setSearchActive(true)
        self.view?.prepareWillAppear()
    }
    
    func didTriggerViewWillDisappearEvent() {
        self.view?.setSearchActive(false)
        self.view?.prepareWillDisappear()
    }
    
    func didTapCancelButton() {
        self.router.close()
    }
    
    func didEditSearchRequest(_ searchRequest: String) {
        self.searchTimer?.invalidate()
        self.searchTimer = Timer.scheduledTimer(timeInterval: 0.5,
                                                target: self,
                                                selector: #selector(prepareSearchRequest(_:)),
                                                userInfo: searchRequest,
                                                repeats: false)
    }
    
    func didTapSearchButton(with searchRequest: String) {
        self.searchTimer?.invalidate()
        processSearchRequest(searchRequest, postponed: false)
    }
    
    func didTapTryAgainButton() {
        guard let searchRequest = self.data.searchRequest else { return }
        performSearch(withRequest: searchRequest)
    }
    
    func didSelectEvent(_ viewModel: EventViewModel) {
        guard let index = self.data.events.index(where: { $0.id == viewModel.id }) else { return }
        
        let event = self.data.events[index]
        self.router.showEvent(event)
    }

}


// MARK: - LineSearchInteractorOutput

extension LineSearchPresenter: LineSearchInteractorOutput {
    
    func didSearchEventsFromNetwork(_ events: [EventObject]) {
        self.data.events = sortEvents(events)
        
        self.data.status = .loaded
        self.view?.setStatus(self.data.status)
        
        prepareEvents()
        showData()
    }
    
    func didFailToSearchEventsFromNetwork(with error: Error) {
        processError(error)
    }
    
}
