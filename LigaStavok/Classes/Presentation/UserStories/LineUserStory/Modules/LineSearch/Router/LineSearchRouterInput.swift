//
//  LineSearchRouterInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol LineSearchRouterInput: class {
    
    /// Закрыть
    func close()
    
    /// Отобразить экран "Роспись события"
    func showEvent(_ event: EventObject)
    
}
