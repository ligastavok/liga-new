//
//  LineSearchRouter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LineSearchRouter: Router {

    // MARK: - Публичные свойства

    weak var viewController: UIViewController?


    // MARK: - Инициализация

    init(viewController: UIViewController? = nil) {
        self.viewController = viewController
    }

}


// MARK: - LineSearchRouterInput

extension LineSearchRouter: LineSearchRouterInput {
    
    func close() {
        pop(animated: false)
    }
    
    func showEvent(_ event: EventObject) {
        let eventViewController = EventModuleAssembly.assembleModule(event: event)
        show(eventViewController)
    }
    
}
