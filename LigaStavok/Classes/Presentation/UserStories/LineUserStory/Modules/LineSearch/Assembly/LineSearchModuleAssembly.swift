//
//  LineSearchModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LineSearchModuleAssembly {

    /// Собрать модуль
    static func assembleModule() -> UIViewController {
        let storyboard = UIStoryboard(storyboard: .lineSearch)
        let view: LineSearchViewController = storyboard.instantiateViewController()
        
        let resolver = ApplicationAssembly.assembler.resolver
        guard let searchService = resolver.resolve(SearchService.self) else { preconditionFailure() }

        let presenter = LineSearchPresenter()
        let interactor = LineSearchInteractor(searchService: searchService)
        let router = LineSearchRouter()

        view.output = presenter

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        interactor.output = presenter

        router.viewController = view

        return view
    }

}
