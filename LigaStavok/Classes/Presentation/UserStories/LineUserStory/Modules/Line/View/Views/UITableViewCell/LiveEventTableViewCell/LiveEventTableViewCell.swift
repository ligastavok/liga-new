//
//  LiveEventTableViewCell.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LiveEventTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Название
    @IBOutlet
    private weak var titleLabel: UILabel!
    /// Счетчик событий
    @IBOutlet
    private weak var eventCounterView: EventCounterView!
    
}


// MARK: - Публичные методы

extension LiveEventTableViewCell {
    
    /// Настройка
    func configure(with viewModel: LiveEventViewModel) {
        self.titleLabel.text = viewModel.title
        
        self.eventCounterView.count = viewModel.count
        self.eventCounterView.isHidden = viewModel.count == 0 // swiftlint:disable:this empty_count
    }
    
}
