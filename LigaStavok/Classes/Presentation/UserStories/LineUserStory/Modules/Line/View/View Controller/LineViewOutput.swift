//
//  LineViewOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LineViewOutput: class {
    
    /// View загружено в память
    func didTriggerViewReadyEvent()
    
    /// Нажата кнопка "Поиск"
    func didTapSearchButton()
    
    /// Выбраны все live-трансляции
    func didSelectLiveEventsAll()
    
    /// Выбраны видеотрансляции
    func didSelectLiveEventsVideo()
    
    /// Нажата кнопка "Редакировать" для секции "Избранные виды спорта"
    func didTapEditFavoritesButton()
    
    /// Выбран вид спорта
    func didSelectSport(_ viewModel: SportViewModel)
    
}
