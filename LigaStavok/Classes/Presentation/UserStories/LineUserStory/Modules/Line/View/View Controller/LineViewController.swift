//
//  LineViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import DeepDiff
import DZNEmptyDataSet

final class LineViewController: UIViewController {
    
    // MARK: - Типы данных
    
    private enum SectionType: Int {
        
        /// Live-события
        case liveEvents
        /// Избранные виды спорта
        case favorites
        /// Виды спорта
        case sports
        
    }
    
    private struct LocalData {
        
        /// Статус
        var status: ModuleStatus = .unknown
        
        /// Секции
        let sections: [SectionType] = [
            .liveEvents,
            .favorites,
            .sports
        ]
        /// Заголовки секциий
        var sectionsHeaders: [HeaderSectionViewModel] = []
        
        /// Live-события
        var liveEvents: [LiveEventViewModel] = []
        /// Избранные виды спорта
        var favorites: [SportViewModel] = []
        /// Все виды спорта
        var sports: [SportViewModel] = []
        
        /// Состояние
        var isEmpty: Bool {
            return self.favorites.isEmpty && self.sports.isEmpty
        }
        
    }
    
    
    // MARK: - Outlet
    
    @IBOutlet
    private weak var tableView: UITableView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            
            // Регистрация верхнего колонтитула
            newValue.register(HeaderSectionView.self)
            
            // Регистрация ячеек
            newValue.register(LiveEventTableViewCell.self)
            newValue.register(SportTableViewCell.self)
        }
    }
    
    /// Контейнер для модуля "Live-трансляции"
    @IBOutlet
    private weak var liveTranslationsContainerView: UIView!
    
    
    // MARK: - Публичные свойства
    
    /// Presenter
    var output: LineViewOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Live-трансляции
    private weak var liveTranslationsViewController: UIViewController!
    
    /// Локальные данные
    private var data = LocalData()
    
}


// MARK: - UIViewController

extension LineViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.output.didTriggerViewReadyEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navigationController = self.navigationController as? NavigationController {
            if let tabBarController = tabBarController as? TabBarController {
                navigationController.followScrollView(self.tableView, delay: 40, scrollSpeedFactor: 2,
                                                      followers: [NavigationBarFollower(view: tabBarController.tabBar, direction: .scrollDown)])
            } else {
                navigationController.followScrollView(self.tableView, delay: 40, scrollSpeedFactor: 2)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navigationController = self.navigationController as? NavigationController {
            navigationController.stopFollowingScrollView(showingNavigationBar: true)
        }
    }
    
}


// MARK: - Приватные методы

private extension LineViewController {
    
    /// Нажата кнопка "Поиск"
    @objc
    func searchButtonTapped() {
        self.output.didTapSearchButton()
    }
    
}


// MARK: - LineViewInput

extension LineViewController: LineViewInput {
    
    func showLiveTranslationsViewController(_ liveTranslationsViewController: UIViewController) {
        add(child: liveTranslationsViewController, in: self.liveTranslationsContainerView, pinToEdges: true)
    }
    
    func setInitialData(sections: [HeaderSectionViewModel]) {
        self.data.sectionsHeaders = sections
    }
    
    func setupInitialState() {
        
        // Настройка заголовка
        self.navigationItem.title = Localization.Line.navigationBarTitle
        
        // Кнопка "Поиск"
        let searchButtonIcon = UIImage(asset: .navigationBarSearch)
        let searchButton = UIBarButtonItem(image: searchButtonIcon,
                                           style: .plain,
                                           target: self,
                                           action: #selector(searchButtonTapped))
        self.navigationItem.rightBarButtonItem = searchButton
        
        // Настройка таблицы
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -20, right: 0)
        self.tableView.tableFooterView = UIView()
        
        // Настройка фреймворка DZNEmptyDataSet
        self.tableView.emptyDataSetSource = self
    }
    
    func setStatus(_ status: ModuleStatus) {
        guard self.data.status != status else { return }
        self.data.status = status
        
        switch status {
        case .loading, .error:
            guard self.data.isEmpty else { return }
            self.tableView.reloadData()
            
        default:
            return
        }
    }
    
    func setTableViewEditingStatus(_ editing: Bool) {
        self.tableView.setEditing(editing, animated: true)
    }
    
    func updateHeaderSection(_ section: HeaderSectionViewModel, atIndex index: Int) {
        self.data.sectionsHeaders[index] = section
        
        guard let type = SectionType(rawValue: index),
            let sectionIndex = self.data.sections.index(of: type),
            let headerSection = self.tableView.headerView(forSection: sectionIndex) as? HeaderSectionView else { return }
        headerSection.configure(with: section)
    }
    
    func showData(liveEvents: [LiveEventViewModel], favorites: [SportViewModel], sports: [SportViewModel]) {
        if self.data.isEmpty {
            self.data.liveEvents = liveEvents
            self.data.favorites = favorites
            self.data.sports = sports
            
            self.tableView.reloadData()
        } else {
            let liveEventsChanges = diff(old: self.data.liveEvents, new: liveEvents)
            self.data.liveEvents = liveEvents
            
            if !liveEventsChanges.isEmpty {
                self.tableView.reload(changes: liveEventsChanges,
                                      section: SectionType.liveEvents.rawValue,
                                      insertionAnimation: .none,
                                      deletionAnimation: .none,
                                      replacementAnimation: .none) { _ in }
            }
            
            let favoritesChanges = diff(old: self.data.favorites, new: favorites)
            self.data.favorites = favorites
            
            if !favoritesChanges.isEmpty {
                self.tableView.reload(changes: favoritesChanges,
                                      section: SectionType.favorites.rawValue,
                                      insertionAnimation: .none,
                                      deletionAnimation: .none,
                                      replacementAnimation: .none) { _ in }
            }
            
            let sportsChanges = diff(old: self.data.sports, new: sports)
            self.data.sports = sports
        
            if !sportsChanges.isEmpty {
                self.tableView.reload(changes: sportsChanges,
                                      section: SectionType.sports.rawValue,
                                      insertionAnimation: .none,
                                      deletionAnimation: .none,
                                      replacementAnimation: .none) { _ in }
            }
        }
    }
    
}


// MARK: - UITableViewDataSource

extension LineViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = self.data.sections[section]
        
        switch section {
        case .liveEvents:
            return self.data.liveEvents.count
        case .favorites:
            return self.data.favorites.count
        case .sports:
            return self.data.sports.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = self.data.sections[indexPath.section]
        
        switch section {
        case .liveEvents:
            let liveEvent = self.data.liveEvents[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(for: indexPath) as LiveEventTableViewCell
            cell.configure(with: liveEvent)
            
            return cell
        case .favorites:
            let sport = self.data.favorites[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(for: indexPath) as SportTableViewCell
            cell.configure(with: sport)
            
            return cell
        case .sports:
            let sport = self.data.sports[indexPath.row]
            
            let cell = tableView.dequeueReusableCell(for: indexPath) as SportTableViewCell
            cell.configure(with: sport)
            
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.data.sections.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        let section = self.data.sections[indexPath.section]
        return section == .favorites
    }
    
}


// MARK: - UITableViewDelegate

extension LineViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 26 + (section == 0 ? 10 : 0)
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 10
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            let section = self.data.sections[section]
            let sectionHeader = self.data.sectionsHeaders[section.rawValue]
            
            let lineHeaderSectionView = tableView.dequeueReusableHeaderFooterView() as HeaderSectionView
            lineHeaderSectionView.configure(with: sectionHeader)
            lineHeaderSectionView.delegate = self
            
            return lineHeaderSectionView
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return UIView()
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let section = self.data.sections[indexPath.section]
        switch section {
        case .liveEvents:
            if indexPath.row == 0 {
                self.output.didSelectLiveEventsAll()
            } else {
                self.output.didSelectLiveEventsVideo()
            }
        case .favorites:
            let sport = self.data.favorites[indexPath.row]
            self.output.didSelectSport(sport)
        case .sports:
            let sport = self.data.sports[indexPath.row]
            self.output.didSelectSport(sport)
        }
    }
    
}


// MARK: - DZNEmptyDataSetSource

extension LineViewController: DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text: String
        switch self.data.status {
        case .unknown, .loading, .loaded:
            return nil
        case .error(let error):
            text = error.localizedDescription
        }
        
        let helper = EmptyDataSetSourceHelper()
        return helper.generateTitle(withText: text)
    }
    
    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
        switch self.data.status {
        case .unknown, .loading:
            let helper = EmptyDataSetSourceHelper()
            return helper.generateLoadingView(for: scrollView)
        case .loaded, .error:
            return nil
        }
    }
    
    func verticalOffset(forEmptyDataSet scrollView: UIScrollView!) -> CGFloat {
        return self.liveTranslationsContainerView.frame.height / 2
    }
    
}


// MARK: - LineHeaderSectionViewDelegate

extension LineViewController: HeaderSectionViewDelegate {
    
    func buttonTapped(in lineHeaderSectionView: HeaderSectionView) {
        self.output.didTapEditFavoritesButton()
    }
    
}
