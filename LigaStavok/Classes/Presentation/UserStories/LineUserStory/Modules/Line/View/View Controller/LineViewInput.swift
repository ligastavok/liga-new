//
//  LineViewInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol LineViewInput: class {
    
    /// Отобразить модуль "Live-трансляции"
    func showLiveTranslationsViewController(_ liveTranslationsViewController: UIViewController)
    
    /// Установка начальных данных
    func setInitialData(sections: [HeaderSectionViewModel])
    
    /// Подготовка
    func setupInitialState()
    
    /// Установить статус
    func setStatus(_ status: ModuleStatus)
    
    /// Изменить состояние таблицы
    func setTableViewEditingStatus(_ editing: Bool)
    
    /// Обновление заголовка секции
    func updateHeaderSection(_ section: HeaderSectionViewModel, atIndex index: Int)
    
    /// Отобразить данные
    func showData(liveEvents: [LiveEventViewModel], favorites: [SportViewModel], sports: [SportViewModel])
    
}
