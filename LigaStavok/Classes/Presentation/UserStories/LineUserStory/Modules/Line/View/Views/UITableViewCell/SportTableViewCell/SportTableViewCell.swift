//
//  SportTableViewCell.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class SportTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Иконка
    @IBOutlet
    private weak var iconImageView: UIImageView!
    
    /// Название
    @IBOutlet
    private weak var titleLabel: UILabel!
    
    /// Счетчик live-событий
    @IBOutlet
    private weak var liveEventsCounterView: EventCounterView!
    /// Счетчик всех событий
    @IBOutlet
    private weak var allEventsCounterView: EventCounterView!
    
    
    // MARK: - Приватные свойства
    
    private var iconImageViewBackgroundColor: UIColor?
    
}


// MARK: - NSObject

extension SportTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.iconImageViewBackgroundColor = self.iconImageView.backgroundColor
    }
    
}


// MARK: - UITableViewCell

extension SportTableViewCell {
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        fixIconImageViewBackgroundColor()
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
        fixIconImageViewBackgroundColor()
    }
    
}


// MARK: - Публичные методы

extension SportTableViewCell {
    
    /// Настройка
    func configure(with viewModel: SportViewModel) {
        let icon = UIImage(asset: viewModel.imageName)
        self.iconImageView.image = icon
        
        self.titleLabel.text = viewModel.title
        
        self.liveEventsCounterView.count = viewModel.liveEventsCount
        self.liveEventsCounterView.isHidden = viewModel.liveEventsCount == 0
        
        self.allEventsCounterView.count = viewModel.allEventsCount
        self.allEventsCounterView.isHidden = viewModel.allEventsCount == 0
    }
    
}


// MARK: - Приватные методы

private extension SportTableViewCell {
    
    /// Зафиксировать цвет заднего фона иконки
    func fixIconImageViewBackgroundColor() {
        self.iconImageView.backgroundColor = self.iconImageViewBackgroundColor
    }
    
}
