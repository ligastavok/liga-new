//
//  SportViewModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 18.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct SportViewModel: Equatable {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор вида спорта
    let gameId: Int
    /// Название картинки
    let imageName: ImageAsset
    /// Название
    let title: String
    /// Счетчик live-событий
    let liveEventsCount: Int
    /// Счетчик всех событий
    let allEventsCount: Int
    
    
    // MARK: - Инициализация
    
    init(gameId: Int, imageName: ImageAsset, title: String, liveEventsCount: Int, allEventsCount: Int) {
        self.gameId = gameId
        self.imageName = imageName
        self.title = title
        self.liveEventsCount = liveEventsCount
        self.allEventsCount = allEventsCount
    }
    
    init(sport: Sport, liveEventsCount: Int) {
        let imageName = SportType(rawValue: sport.gameId)?.imageAsset ?? .sportDefault
        self.init(gameId: sport.gameId,
                  imageName: imageName,
                  title: sport.title,
                  liveEventsCount: liveEventsCount,
                  allEventsCount: sport.quantity)
    }
    
}


// MARK: - Hashable

extension SportViewModel: Hashable {
    
    var hashValue: Int {
        return HashManager.combineHashes([self.gameId.hashValue,
                                          self.imageName.hashValue,
                                          self.title.hashValue,
                                          self.liveEventsCount.hashValue,
                                          self.allEventsCount.hashValue])
    }
    
}
