//
//  LiveEventViewModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 19.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct LiveEventViewModel: Equatable {
    
    // MARK: - Публичные свойства
    
    /// Название
    let title: String
    /// Количество
    let count: Int
    
}


// MARK: - Hashable

extension LiveEventViewModel: Hashable {
    
    var hashValue: Int {
        return HashManager.combineHashes([self.title.hashValue,
                                          self.count.hashValue])
    }
    
}
