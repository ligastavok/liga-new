//
//  LinePresenter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class LinePresenter {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Статус
        var status: ModuleStatus = .unknown
        /// Последний статус состояния интернет-соединения
        var lastNetworkStatus: NetworkReachabilityStatus = .unknown
        
        /// Загружены избранные виды спорта из кэша
        var isFavoritesCachedLoaded = false
        /// Загружены виды спорта из кэша
        var isSportsCachedLoaded = false
        
        /// Загружены избранные виды спорта
        var isFavoritesLoaded = false
        /// Загружены виды спорта
        var isSportsLoaded = false
        /// Загружены счетчики
        var isCountersLoaded = false
        
        /// Загружены ли данные из кэша
        var isCachedDataLoaded: Bool {
            return self.isFavoritesCachedLoaded && self.isSportsCachedLoaded
        }
        
        /// Загружены ли данные
        var isDataLoaded: Bool {
            return self.isFavoritesLoaded && self.isSportsLoaded && self.isCountersLoaded
        }
        
        /// Количество всех live-событий
        var liveEventsAllCount = 0
        /// Количество видеотрансляций
        var liveEventsVideoTranslationsCount = 0
        
        /// Словарь с количеством live-событий для вида спорта
        var liveEvents: [Int: Int] = [:]
        
        /// Виды спорта
        var sports: [Sport] = []
        /// Избранные виды спорта
        var favorites: [Int] = []
        
    }
    
    private struct LocalViewData {
        
        /// Индекс секции "Избранные виды спорта"
        let favoritesSectionIndex = 1
        
        /// Секции
        var sectionsViewModels = [
            HeaderSectionViewModel(title: Localization.Line.liveEventsSectionTitle,
                                   buttonTitle: nil),
            HeaderSectionViewModel(title: Localization.Line.favoritesSportsSectionTitle,
                                   buttonTitle: Localization.Line.favoritesSportsSectionEditButtonTitle),
            HeaderSectionViewModel(title: Localization.Line.allSportsSectionTitle,
                                   buttonTitle: nil)
        ]
        
        /// Live-события
        var liveEventsViewModels: [LiveEventViewModel] = []
        /// Избранные виды спорта
        var favoritesViewModels: [SportViewModel] = []
        /// Виды спорта
        var sportsViewModels: [SportViewModel] = []
        
        /// Таблица редактируется
        var isTableViewEditing = false
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// View
    weak var view: LineViewInput?
    /// Interactor
    var interactor: LineInteractorInput!
    /// Router
    var router: LineRouterInput!
    /// Модуль "Live-трансляции"
    weak var liveTranslationsModule: LiveTranslationsModuleInput?
    
    
    // MARK: - Приватные свойства
    
    /// Локальные данные
    private var data = LocalData()
    /// Данные представления
    private var viewData = LocalViewData()
    
}


// MARK: - Приватные методы

private extension LinePresenter {
    
    /// Загрузить данные из кэша
    func loadDataFromCache() {
        self.data.status = .loading(type: .cache)
        self.view?.setStatus(self.data.status)
        
        self.interactor.obtainSportsFromCache()
        self.interactor.obtainFavoritesFromCache()
    }
    
    /// Загрузить данные из сети
    func loadDataFromNetwork() {
        self.data.status = .loading(type: .network)
        self.view?.setStatus(self.data.status)
        
        self.interactor.obtainSportsFromNetwork()
        self.interactor.obtainFavoritesFromNetwork()
        self.interactor.obtainCountersFromNetwork()
    }
    
    /// Подписаться на обновления
    func subscribeToUpdates() {
        self.interactor.subscribeToCountersUpdates()
    }
    
    /// Проверить статус загрузки из кэша
    func checkCacheLoadStatus() {
        guard self.data.isCachedDataLoaded else { return }
        
        showData()
        loadDataFromNetwork()
    }
    
    /// Проверить статус загрузки
    func checkLoadStatus() {
        guard self.data.isDataLoaded else { return }
        
        self.data.status = .loaded
        self.view?.setStatus(self.data.status)
        
        showData()
        subscribeToUpdates()
    }
    
    /// Подготовка данных
    func prepareData() {
        prepareLiveEvent()
		prepareFavorites()
        prepareSports()
    }
    
    /// Подготовка счетчиков
    func prepareLiveEvent() {
        let viewModels = [
            LiveEventViewModel(title: Localization.Line.liveEventsAllTitle,
                               count: self.data.liveEventsAllCount),
            LiveEventViewModel(title: Localization.Line.liveEventsVideoTranslationsTitle,
                               count: self.data.liveEventsVideoTranslationsCount)
        ]
        self.viewData.liveEventsViewModels = viewModels
    }
    
    /// Подготовка избранных видов спорта
    func prepareFavorites() {
        let viewModels = self.data.sports.enumerated()
            .filter { self.data.favorites.contains($0.offset) }
            .map { SportViewModel(sport: $0.element, liveEventsCount: self.data.liveEvents[$0.element.gameId] ?? 0) }
        self.viewData.favoritesViewModels = viewModels
    }
    
    /// Подготовка видов спорта
    func prepareSports() {
        let viewModels = self.data.sports.enumerated()
            .filter { !self.data.favorites.contains($0.offset) }
            .map { SportViewModel(sport: $0.element, liveEventsCount: self.data.liveEvents[$0.element.gameId] ?? 0) }
        self.viewData.sportsViewModels = viewModels
    }
    
    /// Отобразить данные
    func showData() {
        prepareData()
        
        let liveEventsViewModels = self.viewData.liveEventsViewModels
        let favoritesViewModels = self.viewData.favoritesViewModels
        let sportsViewModels = self.viewData.sportsViewModels
        self.view?.showData(liveEvents: liveEventsViewModels, favorites: favoritesViewModels, sports: sportsViewModels)
    }
    
    /// Обработка счетчиков
    func processCounters(_ counters: Counters) {
        self.data.liveEventsAllCount = counters.live
        self.data.liveEventsVideoTranslationsCount = counters.video
        
        counters.sportsScoreboard.forEach { key, value in
            guard let gameId = Int(key) else { return }
            self.data.liveEvents[gameId] = value
        }
    }
    
    /// Обработка ошибки
    func processError(_ error: Error) {
        self.data.status = .error(value: error)
        self.view?.setStatus(self.data.status)
    }
    
}


// MARK: - LineViewOutput

extension LinePresenter: LineViewOutput {
    
    func didTriggerViewReadyEvent() {
        let liveTranslationsViewController = self.router.createLiveTranslationsModule(with: self)
        self.view?.showLiveTranslationsViewController(liveTranslationsViewController)
        
        self.view?.setInitialData(sections: self.viewData.sectionsViewModels)
        self.view?.setupInitialState()
        
        self.interactor.listenNetworkReachability()
        
        loadDataFromCache()
    }
    
    func didTapSearchButton() {
        self.router.showSearch()
    }
    
    func didSelectLiveEventsAll() {
        self.router.showLiveEvents()
    }
    
    func didSelectLiveEventsVideo() {
        self.router.showVideoboard()
    }
    
    func didTapEditFavoritesButton() {
        self.viewData.isTableViewEditing = !self.viewData.isTableViewEditing
        
        let headerSectionTitle = self.viewData.sectionsViewModels[self.viewData.favoritesSectionIndex].title
        let headerSectionButtonTitle = self.viewData.isTableViewEditing ? Localization.Common.cancelButtonTitle : Localization.Line.favoritesSportsSectionEditButtonTitle // swiftlint:disable:this line_length
        let headerSection = HeaderSectionViewModel(title: headerSectionTitle, buttonTitle: headerSectionButtonTitle)
        self.viewData.sectionsViewModels[self.viewData.favoritesSectionIndex] = headerSection
        
        self.view?.updateHeaderSection(headerSection, atIndex: self.viewData.favoritesSectionIndex)
        self.view?.setTableViewEditingStatus(self.viewData.isTableViewEditing)
    }
    
    func didSelectSport(_ viewModel: SportViewModel) {
        guard let sport = self.data.sports.first(where: { $0.gameId == viewModel.gameId }) else { return }
        self.router.showSportTournaments(for: sport)
    }
    
}


// MARK: - LineInteractorOutput

extension LinePresenter: LineInteractorOutput {
    
    func didObtainNetworkReachabilityStatus(_ status: NetworkReachabilityStatus) {
        defer { self.data.lastNetworkStatus = status }
        guard case .reachable = status, case .notReachable = self.data.lastNetworkStatus else { return }
        
        self.loadDataFromNetwork()
    }
    
    func didObtainSportsFromCache(_ sports: [Sport]) {
        self.data.sports = sports
        self.data.isSportsCachedLoaded = true
        
        checkCacheLoadStatus()
    }
    
    func didObtainFavoritesFromCache(_ favorites: [Int]) {
        self.data.favorites = favorites
        self.data.isFavoritesCachedLoaded = true
        
        checkCacheLoadStatus()
    }
    
    func didObtainSportsFromNetwork(_ sports: [Sport]) {
        self.data.sports = sports
        self.data.isSportsLoaded = true
        
        checkLoadStatus()
    }
    
    func didFailToObtainSportsFromNetwork(with error: Error) {
        processError(error)
    }
    
    func didObtainFavoritesFromNetwork(_ favorites: [Int]) {
        self.data.favorites = favorites
        self.data.isFavoritesLoaded = true
        
        checkLoadStatus()
    }
    
    func didFailToObtainFavoritesFromNetwork(with error: Error) {
        processError(error)
    }
    
    func didObtainCountersFromNetwork(_ counters: Counters) {
        processCounters(counters)
        self.data.isCountersLoaded = true
        
        checkLoadStatus()
    }
    
    func didFailToObtainCountersFromNetwork(with error: Error) {
        processError(error)
    }
    
    func didObtainCountersUpdates(_ counters: Counters) {
        processCounters(counters)
        
        showData()
    }
    
}


// MARK: - LiveTranslationsModuleOutput

extension LinePresenter: LiveTranslationsModuleOutput {
    
    func didLoadLiveTranslationsModule(_ liveTranslationsModule: LiveTranslationsModuleInput) {
        self.liveTranslationsModule = liveTranslationsModule
    }
    
}
