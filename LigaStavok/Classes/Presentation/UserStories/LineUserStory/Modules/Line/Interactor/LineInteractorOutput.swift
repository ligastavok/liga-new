//
//  LineInteractorOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LineInteractorOutput: class {
    
    /// Получена информация о доступности интернет-соединения
    func didObtainNetworkReachabilityStatus(_ status: NetworkReachabilityStatus)
    
    /// Получены виды спорта из кэша
    func didObtainSportsFromCache(_ sports: [Sport])
    /// Получены избранные виды спорта из кэша
    func didObtainFavoritesFromCache(_ favorites: [Int])
    
    /// Получены виды спорта из интернета
    func didObtainSportsFromNetwork(_ sports: [Sport])
    /// При получении видов спорта из интернета произошла ошибка
    func didFailToObtainSportsFromNetwork(with error: Error)
    
    /// Получены избранные виды спорта из интернета
    func didObtainFavoritesFromNetwork(_ favorites: [Int])
    /// При получении избранных видов спорта из интернета произошла ошибка
    func didFailToObtainFavoritesFromNetwork(with error: Error)
    
    /// Получены счетчики из интернета
    func didObtainCountersFromNetwork(_ counters: Counters)
    /// При получении счетчиков из интернета произошла ошибка
    func didFailToObtainCountersFromNetwork(with error: Error)
    
    /// Получены обновления счетчиков
    func didObtainCountersUpdates(_ counters: Counters)
    
}
