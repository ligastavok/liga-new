//
//  LineInteractorInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LineInteractorInput: class {
    
    /// Слушать информацию о доступности интернет-соединения
    func listenNetworkReachability()
    
    /// Получить виды спорта из кэша
    func obtainSportsFromCache()
    /// Получить виды спорта из интернета
    func obtainSportsFromNetwork()
    
    /// Получить избранные виды спорта из кэша
    func obtainFavoritesFromCache()
    /// Получить избранные виды спорта из интернета
    func obtainFavoritesFromNetwork()
    
    /// Получить счетчики из интернета
    func obtainCountersFromNetwork()
    
    /// Подписаться на изменения счетчиков
    func subscribeToCountersUpdates()
    
}
