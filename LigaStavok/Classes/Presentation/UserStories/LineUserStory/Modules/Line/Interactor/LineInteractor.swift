//
//  LineInteractor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class LineInteractor {
    
    // MARK: - Публичные свойства
    
    /// Presenter
    weak var output: LineInteractorOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Сервис для отселживания информации о доступности интернет-соединения
    private let networkReachabilityService: NetworkReachabilityService
    /// Сервис для работы с событиями
    private let eventsService: EventsService
    /// Сервис для получения публичной информации
    private let broadcastService: BroadcastService
    
    
    // MARK: - Инициализация
    
    init(networkReachabilityService: NetworkReachabilityService,
         eventsService: EventsService,
         broadcastService: BroadcastService) {
        
        self.networkReachabilityService = networkReachabilityService
        self.eventsService = eventsService
        self.broadcastService = broadcastService
    }
    
}


// MARK: - LineInteractorInput

extension LineInteractor: LineInteractorInput {
    
    func listenNetworkReachability() {
        self.networkReachabilityService.listenNetworkReachability(self, reachable: { [weak self] connectionType in
            self?.output.didObtainNetworkReachabilityStatus(.reachable(connectionType: connectionType))
        }, notReachable: { [weak self] in
            self?.output.didObtainNetworkReachabilityStatus(.notReachable)
        })
    }
    
    func obtainSportsFromCache() {
        self.eventsService.obtainSportsListCached { [weak self] sports in
            self?.output.didObtainSportsFromCache(sports)
        }
    }
    
    func obtainSportsFromNetwork() {
        _ = self.eventsService.obtainSportsList(namespace: nil, period: nil) { [weak self] result in
            switch result {
            case .success(let sports):
                self?.output.didObtainSportsFromNetwork(sports)
            case .failure(let error):
                self?.output.didFailToObtainSportsFromNetwork(with: error)
            }
        }
    }
    
    func obtainFavoritesFromCache() {
        
        // FIXME: - Заглушка
        DispatchQueue.main.async {
            self.output.didObtainFavoritesFromCache([0, 1, 2, 3, 4])
        }
    }
    
    func obtainFavoritesFromNetwork() {
        
        // FIXME: - Заглушка
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
            self?.output.didObtainFavoritesFromNetwork([0, 1, 2, 3, 4])
        }
    }
    
    func obtainCountersFromNetwork() {
        self.broadcastService.obtainCounters { [weak self] result in
            switch result {
            case .success(let counters):
                self?.output.didObtainCountersFromNetwork(counters)
            case .failure(let error):
                self?.output.didFailToObtainCountersFromNetwork(with: error)
            }
        }
    }
    
    func subscribeToCountersUpdates() {
        self.broadcastService.subscribeToCountersUpdates { [weak self] counters in
            self?.output.didObtainCountersUpdates(counters)
        }
    }
    
}
