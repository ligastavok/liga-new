//
//  LineRouter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LineRouter: Router {
    
    // MARK: - Публичные свойства
    
    weak var viewController: UIViewController?
    
    
    // MARK: - Инициализация
    
    init(viewController: UIViewController? = nil) {
        self.viewController = viewController
    }
    
}


// MARK: - LineRouterInput

extension LineRouter: LineRouterInput {
    
    func createLiveTranslationsModule(with output: LiveTranslationsModuleOutput) -> UIViewController {
        return LiveTranslationsModuleAssembly.assembleModule(with: output)
    }
    
    func showSearch() {
        let lineSearchViewContoller = LineSearchModuleAssembly.assembleModule()
        show(lineSearchViewContoller, animated: false)
    }
    
    func showLiveEvents() {
        let liveEventsViewController = LiveEventsModuleAssembly.assembleModule(mode: .all)
        show(liveEventsViewController)
    }
    
    func showVideoboard() {
        let liveEventsViewController = LiveEventsModuleAssembly.assembleModule(mode: .videoTranslations)
        show(liveEventsViewController)
    }

    func showSportTournaments(for sport: Sport) {
        let sportTournamentsViewController = SportTournamentsModuleAssembly.assembleModule(with: sport)
        show(sportTournamentsViewController)
    }
    
}
