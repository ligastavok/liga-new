//
//  LineRouterInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol LineRouterInput: class {
    
    /// Создание модуля "Live-трансляции"
    func createLiveTranslationsModule(with output: LiveTranslationsModuleOutput) -> UIViewController
    
    /// Отобразить поиск
    func showSearch()
    
    /// Отобразить экран "Live-события"
    func showLiveEvents()
    
    /// Отобразить сцену видеотрансляций
    func showVideoboard()
    
    /// Перейти на сцену "Роспись дисциплины"
    func showSportTournaments(for sport: Sport)
    
}
