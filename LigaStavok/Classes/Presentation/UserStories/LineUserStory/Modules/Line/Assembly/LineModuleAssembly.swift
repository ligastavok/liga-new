//
//  LineModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LineModuleAssembly {
    
    /// Собрать модуль
    static func assembleModule() -> UIViewController {
        let storyboard = UIStoryboard(storyboard: .line)
        let view: LineViewController = storyboard.instantiateViewController()
        
        let resolver = ApplicationAssembly.assembler.resolver
        guard let networkReachabilityService = resolver.resolve(NetworkReachabilityService.self) else { preconditionFailure() }
        guard let eventsService = resolver.resolve(EventsService.self) else { preconditionFailure() }
        guard let broadcastService = resolver.resolve(BroadcastService.self) else { preconditionFailure() }
        
        let presenter = LinePresenter()
        let interactor = LineInteractor(networkReachabilityService: networkReachabilityService,
                                        eventsService: eventsService,
                                        broadcastService: broadcastService)
        let router = LineRouter()
        
        view.output = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        
        router.viewController = view
        
        let navigationController = NavigationController(rootViewController: view)
        return navigationController
    }
    
}
