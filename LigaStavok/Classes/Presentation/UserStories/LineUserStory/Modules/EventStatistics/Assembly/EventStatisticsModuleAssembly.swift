//
//  EventStatisticsModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EventStatisticsModuleAssembly {

    /// Собрать модуль
    static func assembleModule(event: EventObject) -> UIViewController {
        let storyboard = UIStoryboard(storyboard: .eventStatistics)
        let view: EventStatisticsViewController = storyboard.instantiateViewController()

        let presenter = EventStatisticsPresenter(event: event)
        let interactor = EventStatisticsInteractor()
        let router = EventStatisticsRouter()

        view.output = presenter

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        interactor.output = presenter

        router.viewController = view

        return view
    }

}
