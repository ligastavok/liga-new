//
//  EventStatisticsViewOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol EventStatisticsViewOutput: class {

    /// View загружено в память
    func didTriggerViewReadyEvent()

}
