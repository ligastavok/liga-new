//
//  EventStatisticsViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EventStatisticsViewController: UIViewController {

    // MARK: - Публичные свойства

    /// Presenter
    var output: EventStatisticsViewOutput!

}


// MARK: - UIViewController

extension EventStatisticsViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.output.didTriggerViewReadyEvent()
    }

}


// MARK: - EventStatisticsViewInput

extension EventStatisticsViewController: EventStatisticsViewInput {

    func setupInitialState() { }

}
