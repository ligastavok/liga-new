//
//  EventStatisticsRouter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EventStatisticsRouter: Router {

    // MARK: - Публичные свойства

    weak var viewController: UIViewController?


    // MARK: - Инициализация

    init(viewController: UIViewController? = nil) {
        self.viewController = viewController
    }

}


// MARK: - EventStatisticsRouterInput

extension EventStatisticsRouter: EventStatisticsRouterInput { }
