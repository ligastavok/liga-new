//
//  EventStatisticsInteractor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class EventStatisticsInteractor {

    // MARK: - Публичные свойства

    /// Presenter
    weak var output: EventStatisticsInteractorOutput!


    // MARK: - Инициализация

    init() { }

}


// MARK: - EventStatisticsInteractorInput

extension EventStatisticsInteractor: EventStatisticsInteractorInput { }
