//
//  EventStatisticsPresenter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class EventStatisticsPresenter {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// События
        var event: EventObject!
        
    }
    

    // MARK: - Публичные свойства

    /// View
    weak var view: EventStatisticsViewInput?
    /// Interactor
    var interactor: EventStatisticsInteractorInput!
    /// Router
    var router: EventStatisticsRouterInput!
    
    
    // MARK: - Приватные свойства
    
    /// Локальные данные
    private var data = LocalData()
    
    
    // MARK: - Инициализация
    
    init(event: EventObject) {
        self.data.event = event
    }

}


// MARK: - EventStatisticsViewOutput

extension EventStatisticsPresenter: EventStatisticsViewOutput {

    func didTriggerViewReadyEvent() {
        self.view?.setupInitialState()
    }

}


// MARK: - EventStatisticsInteractorOutput

extension EventStatisticsPresenter: EventStatisticsInteractorOutput { }
