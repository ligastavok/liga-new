//
//  SportFilterCollectionViewCell.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class SportFilterCollectionViewCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Live-трансляция
    @IBOutlet
    private weak var iconImageView: UIImageView!
    /// Название
    @IBOutlet
    private weak var titleLabel: UILabel!
    
}


// MARK: - Публичные методы

extension SportFilterCollectionViewCell {
    
    /// Настройка
    func configure(with viewModel: SportFilterViewModel) {
        if let imageName = viewModel.imageName {
            let icon = UIImage(asset: imageName)
            self.iconImageView.image = icon
        } else {
            self.iconImageView.image = nil
        }
        self.iconImageView.backgroundColor = viewModel.isActive ? #colorLiteral(red: 0, green: 0.5960784314, blue: 0.4235294118, alpha: 1) : #colorLiteral(red: 0.7803921569, green: 0.7803921569, blue: 0.8, alpha: 1)
        
        self.titleLabel.text = viewModel.title
        self.titleLabel.isHidden = viewModel.title == nil
    }
    
}
