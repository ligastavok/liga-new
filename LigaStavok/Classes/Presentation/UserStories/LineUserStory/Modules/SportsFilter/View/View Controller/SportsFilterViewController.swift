//
//  SportsFilterViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import DeepDiff

final class SportsFilterViewController: UIViewController {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Фильтры по видам спорта
        var filters: [SportFilterViewModel] = []
        
        /// Состояние
        var isEmpty: Bool {
            return self.filters.isEmpty
        }
        
    }
    
    
    // MARK: - Outlet
    
    @IBOutlet
    private weak var collectionView: UICollectionView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            
            newValue.register(SportFilterCollectionViewCell.self)
        }
    }
    

    // MARK: - Публичные свойства

    /// Presenter
    var output: SportsFilterViewOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Локальные данные
    private var data = LocalData()

}


// MARK: - UIViewController

extension SportsFilterViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.output.didTriggerViewReadyEvent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navigationController = self.navigationController as? NavigationController {
            navigationController.stopFollowingScrollView(showingNavigationBar: true)
        }
    }
    
}


// MARK: - SportsFilterViewInput

extension SportsFilterViewController: SportsFilterViewInput {
    
    func showData(filters: [SportFilterViewModel]) {
        if self.data.isEmpty {
            self.data.filters = filters
            
            self.collectionView.reloadData()
        } else {
            let filtersChanges = diff(old: self.data.filters, new: filters)
            self.data.filters = filters
            
            if !filtersChanges.isEmpty {
                self.collectionView.reload(changes: filtersChanges) { _ in }
            }
        }
    }
    
    func update(filters: [SportFilterViewModel]) {
        var indexes: [Int] = []
        for filter in filters {
            guard let index = self.data.filters.index(where: {
                $0.gameId == filter.gameId && $0.imageName == filter.imageName && $0.title == filter.title
            }) else { continue }
            
            indexes.append(index)
            self.data.filters[index] = filter
        }
        
        guard !indexes.isEmpty else { return }
        
        let indexPaths = indexes.map { IndexPath(item: $0, section: 0) }
        self.collectionView.reloadItems(at: indexPaths)
    }
    
}


// MARK: - UICollectionViewDataSource

extension SportsFilterViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data.filters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let filter = self.data.filters[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(for: indexPath) as SportFilterCollectionViewCell
        cell.configure(with: filter)
        
        return cell
    }
    
}


// MARK: - UICollectionViewDelegate

extension SportsFilterViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let filter = self.data.filters[indexPath.row]
        self.output.didSelectSportFilter(filter)
    }
    
}
