//
//  SportsFilterViewOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol SportsFilterViewOutput: class {
    
    /// View загружено в память
    func didTriggerViewReadyEvent()
    
    /// Выбран вид спорта
    func didSelectSportFilter(_ viewModel: SportFilterViewModel)
    
}
