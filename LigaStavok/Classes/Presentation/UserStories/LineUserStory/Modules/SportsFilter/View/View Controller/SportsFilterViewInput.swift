//
//  SportsFilterViewInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol SportsFilterViewInput: class {
    
    /// Отобразить данные
    func showData(filters: [SportFilterViewModel])
    /// Частичное обновление данных
    func update(filters: [SportFilterViewModel])
    
}
