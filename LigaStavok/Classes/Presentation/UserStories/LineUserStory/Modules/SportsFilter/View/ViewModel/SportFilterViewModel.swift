//
//  SportFilterViewModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 20.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct SportFilterViewModel: Equatable {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор вида спорта
    let gameId: Int?
    /// Название картинки
    let imageName: ImageAsset?
    /// Название
    let title: String?
    /// Состояние
    var isActive: Bool
    
    
    // MARK: - Инициализация
    
    init(gameId: Int?, imageName: ImageAsset?, title: String?, isActive: Bool) {
        self.gameId = gameId
        self.imageName = imageName
        self.title = title
        self.isActive = isActive
    }
    
    init(sportId: Int) {
        let imageName = SportType(rawValue: sportId)?.imageAsset ?? .sportDefault
        
        self.init(gameId: sportId, imageName: imageName, title: nil, isActive: false)
    }
    
}


// MARK: - Hashable

extension SportFilterViewModel: Hashable {
    
    var hashValue: Int {
        var hashes: [Int] = []
        if let gameId = self.gameId {
            hashes.append(gameId.hashValue)
        }
        if let imageName = self.imageName {
            hashes.append(imageName.hashValue)
        }
        if let title = self.title {
            hashes.append(title.hashValue)
        }
        hashes.append(self.isActive.hashValue)
        
        return HashManager.combineHashes(hashes)
    }
    
}
