//
//  SportsFilterRouter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class SportsFilterRouter: Router {

    // MARK: - Публичные свойства

    weak var viewController: UIViewController?


    // MARK: - Инициализация

    init(viewController: UIViewController? = nil) {
        self.viewController = viewController
    }

}


// MARK: - SportsFilterRouterInput

extension SportsFilterRouter: SportsFilterRouterInput { }
