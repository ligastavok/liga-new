//
//  SportsFilterPresenter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class SportsFilterPresenter {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Индекс активного фильтра
        var activeFilterIndex = 0
        
        /// Идентификаторы видов спорта
        var sportsIds: [Int] = []
        
        /// Активный фильтр
        var activeFilter: SportsFilter {
            switch self.activeFilterIndex {
            case 0:
                return .all
            case 1:
                return .video
                
            default:
                let index = self.activeFilterIndex - 2
                let sportId = self.sportsIds[index]
                
                return .game(gameId: sportId)
            }
        }
        
    }
    
    private struct LocalViewData {
        
        /// Виды спорта
        var filtersViewModels: [SportFilterViewModel] = []
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// View
    weak var view: SportsFilterViewInput?
    /// Interactor
    var interactor: SportsFilterInteractorInput!
    /// Router
    var router: SportsFilterRouterInput!
    /// Контейнер
    weak var moduleOutput: SportsFilterModuleOutput?
    
    
    // MARK: - Приватные свойства
    
    /// Локальные данные
    private var data = LocalData()
    /// Данные представления
    private var viewData = LocalViewData()
    
    
    // MARK: - Инициализация
    
    init(activeVideoFilter: Bool) {
        self.data.activeFilterIndex = activeVideoFilter ? 1 : 0
    }
    
}


// MARK: - Приватные методы

private extension SportsFilterPresenter {
    
    /// Подготовка видов спорта
    func prepareSports() {
        let allSportsFilter = SportFilterViewModel(gameId: nil,
                                                   imageName: nil,
                                                   title: Localization.SportsFilter.allTitle,
                                                   isActive: false)
        let videoFilter = SportFilterViewModel(gameId: nil,
                                               imageName: .sportsFilterVideo,
                                               title: nil,
                                               isActive: false)
        let sportsFilters = self.data.sportsIds.map { SportFilterViewModel(sportId: $0) }
        
        var viewModels = [allSportsFilter, videoFilter] + sportsFilters
        viewModels[self.data.activeFilterIndex].isActive = true
        
        self.viewData.filtersViewModels = viewModels
    }
    
    /// Отобразить данные
    func showData() {
        let filtersViewModels = self.viewData.filtersViewModels
        self.view?.showData(filters: filtersViewModels)
    }
    
    /// Уведомить об изменении фильтра
    func notifyOutput() {
        let activeFilter = self.data.activeFilter
        self.moduleOutput?.didSelectSportsFilter(activeFilter)
    }
    
}


// MARK: - SportsFilterViewOutput

extension SportsFilterPresenter: SportsFilterViewOutput {
    
    func didTriggerViewReadyEvent() {
        self.moduleOutput?.didLoadSportsFilterModule(self)
        
        prepareSports()
        showData()
    }
    
    func didSelectSportFilter(_ viewModel: SportFilterViewModel) {
        guard let index = self.viewData.filtersViewModels.index(where: { $0 == viewModel }) else { return }
        guard self.data.activeFilterIndex != index else { return }
        
        self.viewData.filtersViewModels[self.data.activeFilterIndex].isActive = false
        self.viewData.filtersViewModels[index].isActive = true
        
        self.view?.update(filters: [self.viewData.filtersViewModels[self.data.activeFilterIndex],
                                    self.viewData.filtersViewModels[index]])
        self.data.activeFilterIndex = index
        
        notifyOutput()
    }
    
}


// MARK: - SportsFilterInteractorOutput

extension SportsFilterPresenter: SportsFilterInteractorOutput { }


// MARK: - SportsFilterModuleInput

extension SportsFilterPresenter: SportsFilterModuleInput {
    
    func setSports(withIds sportsIds: [Int]) {
        self.data.sportsIds = sportsIds
        
        prepareSports()
        showData()
    }
    
}
