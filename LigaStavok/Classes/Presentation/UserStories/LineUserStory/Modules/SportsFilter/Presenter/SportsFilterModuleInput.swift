//
//  SportsFilterModuleInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol SportsFilterModuleInput: class {
    
    /// Отобразить виды спорта
    func setSports(withIds sportsIds: [Int])
    
}
