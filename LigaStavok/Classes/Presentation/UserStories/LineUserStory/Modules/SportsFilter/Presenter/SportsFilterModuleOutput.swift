//
//  SportsFilterModuleOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol SportsFilterModuleOutput: class {
    
    /// Модуль "Фильтр по виду спорта" загружен
    func didLoadSportsFilterModule(_ sportsFilterModule: SportsFilterModuleInput)
    
    /// Выбран фильтр по виду спорта
    func didSelectSportsFilter(_ sportsFilter: SportsFilter)
    
}
