//
//  SportsFilterInteractor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class SportsFilterInteractor {

    // MARK: - Публичные свойства

    /// Presenter
    weak var output: SportsFilterInteractorOutput!

}


// MARK: - SportsFilterInteractorInput

extension SportsFilterInteractor: SportsFilterInteractorInput { }
