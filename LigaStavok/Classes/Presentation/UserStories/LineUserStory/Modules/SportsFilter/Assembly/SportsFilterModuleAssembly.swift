//
//  SportsFilterModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class SportsFilterModuleAssembly {

    /// Собрать модуль
    static func assembleModule(with output: SportsFilterModuleOutput, activeVideoFilter: Bool) -> UIViewController {
        let storyboard = UIStoryboard(storyboard: .sportsFilter)
        let view: SportsFilterViewController = storyboard.instantiateViewController()
        
        let presenter = SportsFilterPresenter(activeVideoFilter: activeVideoFilter)
        let interactor = SportsFilterInteractor()
        let router = SportsFilterRouter()

        view.output = presenter

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        presenter.moduleOutput = output

        interactor.output = presenter

        router.viewController = view

        return view
    }

}
