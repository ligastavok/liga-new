//
//  TournamentEventsInteractorInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol TournamentEventsInteractorInput: class {
    
    /// Слушать информацию о доступности интернет-соединения
    func listenNetworkReachability()
    
    /// Получить события из интернета
    func obtainEventsFromNetwork(topicIds: [Int], period: Int?)
    /// Отмена получения событий из интернета
    func cancelObtainingEventsFromNetwork()
    
    /// Подписаться на изменения событий
    func subscribeToEventsUpdates(withIds ids: [Int])
    /// Отмена подписки на изменения событий
    func cancelSubscribingToEventsUpdates()
    
}
