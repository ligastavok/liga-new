//
//  TournamentEventsInteractor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class TournamentEventsInteractor {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Операция на получение событий
        weak var obtainingEventsFromNetworkOperation: Operation?
        /// Операция на получение обновлений по событиям
        weak var subscribingToEventsUpdatesOperation: Operation?
        
    }
    

    // MARK: - Публичные свойства

    /// Presenter
    weak var output: TournamentEventsInteractorOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Сервис для отселживания информации о доступности интернет-соединения
    private let networkReachabilityService: NetworkReachabilityService
    /// Сервис для работы с событиями
    private let eventsService: EventsService
    /// Сервис для получения обновления по событиям
    private let observerService: ObserverService
    
    /// Локальные данные
    private var data = LocalData()


    // MARK: - Инициализация

    init(networkReachabilityService: NetworkReachabilityService,
         eventsService: EventsService,
         observerService: ObserverService) {
        
        self.networkReachabilityService = networkReachabilityService
        self.eventsService = eventsService
        self.observerService = observerService
    }

}


// MARK: - TournamentEventsInteractorInput

extension TournamentEventsInteractor: TournamentEventsInteractorInput {
    
    func listenNetworkReachability() {
        self.networkReachabilityService.listenNetworkReachability(self, reachable: { [weak self] connectionType in
            self?.output.didObtainNetworkReachabilityStatus(.reachable(connectionType: connectionType))
        }, notReachable: { [weak self] in
            self?.output.didObtainNetworkReachabilityStatus(.notReachable)
        })
    }
    
    func obtainEventsFromNetwork(topicIds: [Int], period: Int?) {
        cancelObtainingEventsFromNetwork()
        
        let operation = self.eventsService.obtainEventsList(gameId: nil,
                                                            eventIds: nil,
                                                            categoryIds: nil,
                                                            tournamentIds: nil,
                                                            topicId: topicIds,
                                                            period: period,
                                                            limit: nil,
                                                            namespace: nil,
                                                            matchDay: nil,
                                                            fullEvents: nil,
                                                            topEvents: nil) { [weak self] result in
            switch result {
            case .success(let events):
                self?.output.didObtainEventsFromNetwork(events)
            case .failure(let error):
                self?.output.didFailToObtainEventsFromNetwork(with: error)
            }
        }
        self.data.obtainingEventsFromNetworkOperation = operation
    }
    
    func cancelObtainingEventsFromNetwork() {
        self.data.obtainingEventsFromNetworkOperation?.cancel()
    }
    
    func subscribeToEventsUpdates(withIds ids: [Int]) {
        cancelSubscribingToEventsUpdates()
        
        let operation = self.observerService.subscribeToEventsUpdates(ids: ids) { [weak self] updates in
            self?.output.didObtainEventsUpdates(updates)
        }
        self.data.subscribingToEventsUpdatesOperation = operation
    }
    
    func cancelSubscribingToEventsUpdates() {
        self.data.subscribingToEventsUpdatesOperation?.cancel()
    }
    
}
