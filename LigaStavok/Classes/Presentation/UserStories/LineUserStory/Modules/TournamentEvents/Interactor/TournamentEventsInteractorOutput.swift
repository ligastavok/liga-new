//
//  TournamentEventsInteractorOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol TournamentEventsInteractorOutput: class {
    
    /// Получена информация о доступности интернет-соединения
    func didObtainNetworkReachabilityStatus(_ status: NetworkReachabilityStatus)

    /// Получены события из интернета
    func didObtainEventsFromNetwork(_ events: [EventObject])
    /// При получении событий из интернета произошла ошибка
    func didFailToObtainEventsFromNetwork(with error: Error)
    
    /// Получены изменения событий
    func didObtainEventsUpdates(_ updates: [DiffMapModel])
    
}
