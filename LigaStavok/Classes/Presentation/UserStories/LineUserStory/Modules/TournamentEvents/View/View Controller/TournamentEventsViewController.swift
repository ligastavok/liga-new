//
//  TournamentEventsViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import DeepDiff
import DZNEmptyDataSet

final class TournamentEventsViewController: UIViewController {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Статус
        var status: ModuleStatus = .unknown
        
        /// Заголовок
        var title: TournamentEventsTitleViewModel!
        
        /// Доступность кнопки "Избранное"
        var isFavoritesButtonHidden: Bool!
        /// Состояние турнира
        var isTournamentFavorites: Bool?
        
        /// События
        var events: [EventViewModel] = []
        
        /// Состояние
        var isEmpty: Bool {
            return self.events.isEmpty
        }
        
    }
    
    
    // MARK: - Outlet
    
    @IBOutlet
    private weak var tableView: UITableView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            
            // Регистрация ячеек
            newValue.register(EventTableViewCell.self)
        }
    }
    

    // MARK: - Публичные свойства

    /// Presenter
    var output: TournamentEventsViewOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Кнопка "Избранное"
    private weak var favoritesButton: UIBarButtonItem?
    
    /// Локальные данные
    private var data = LocalData()
    
}


// MARK: - UIViewController

extension TournamentEventsViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.output.didTriggerViewReadyEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navigationController = self.navigationController as? NavigationController {
            if let tabBarController = tabBarController as? TabBarController {
                navigationController.followScrollView(self.tableView, delay: 40, scrollSpeedFactor: 2,
                                                      followers: [NavigationBarFollower(view: tabBarController.tabBar, direction: .scrollDown)])
            } else {
                navigationController.followScrollView(self.tableView, delay: 40, scrollSpeedFactor: 2)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navigationController = self.navigationController as? NavigationController {
            navigationController.stopFollowingScrollView(showingNavigationBar: true)
        }
    }

}


// MARK: - Action

private extension TournamentEventsViewController {
    
    /// Изменено значение фильтра
    @IBAction
    func segmentedControlValueChanged(_ sender: SegmentedControl) {
        guard let periodFilter = PeriodFilter(rawValue: sender.selectedSegmentIndex) else { return }
        self.output.didSelectPeriodFilter(periodFilter)
    }
    
}


// MARK: - Приватные методы

private extension TournamentEventsViewController {
    
    /// Иконка для кнопки "Избранное"
    func generateIconForFavoritesButton(active: Bool) -> UIImage {
        let iconAsset: ImageAsset = active ? .navigationBarFavorites : .navigationBarUnfavorites
        return UIImage(asset: iconAsset)
    }
    
    /// Нажата кнопка "Избранное"
    @objc
    func favoritesButtonTapped() {
        self.output.didTapFavoritesButton()
    }
    
}


// MARK: - TournamentEventsViewInput

extension TournamentEventsViewController: TournamentEventsViewInput {
    
    func setInitialData(titleViewModel: TournamentEventsTitleViewModel,
                        favoritesButtonHidden: Bool,
                        tournamentFavorites: Bool?) {
        
        self.data.title = titleViewModel
        self.data.isFavoritesButtonHidden = favoritesButtonHidden
        self.data.isTournamentFavorites = tournamentFavorites
    }

    func setupInitialState() {
        
        // Заголовок
        if self.data.title.subtitle == nil {
            self.navigationItem.title = self.data.title.title
        } else {
            let titleView = TournamentEventsTitleView.loadFromNib()
            titleView.configure(with: self.data.title)
            self.navigationItem.titleView = titleView
        }
        
        // Кнопка "Избранное"
        if let isTournamentFavorites = self.data.isTournamentFavorites, !self.data.isFavoritesButtonHidden {
            let icon = generateIconForFavoritesButton(active: isTournamentFavorites)
            let favoritesButton = UIBarButtonItem(image: icon,
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(favoritesButtonTapped))
            self.favoritesButton = favoritesButton
            
            self.navigationItem.rightBarButtonItem = favoritesButton
        }
        
        // Настройка таблицы
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -20, right: 0)
        self.tableView.tableFooterView = UIView()
        
        // Настройка фреймворка DZNEmptyDataSet
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
    }
    
    func setStatus(_ status: ModuleStatus) {
        guard self.data.status != status else { return }
        self.data.status = status
        
        switch status {
        case .loading, .error:
            guard self.data.isEmpty else { return }
            self.tableView.reloadData()
            
        default:
            return
        }
    }
    
    func showData(events: [EventViewModel]) {
        if self.data.isEmpty || events.isEmpty {
            self.data.events = events
            self.tableView.reloadData()
        } else {
            let eventsChanges = diff(old: self.data.events, new: events)
            self.data.events = events
            
            if !eventsChanges.isEmpty {
                self.tableView.reload(changes: eventsChanges,
                                      insertionAnimation: .none,
                                      deletionAnimation: .none,
                                      replacementAnimation: .none) { _ in }
            }
        }
    }
    
    func setTournamentFavorites(_ tournamentFavorites: Bool) {
        self.data.isTournamentFavorites = tournamentFavorites
        
        let icon = generateIconForFavoritesButton(active: tournamentFavorites)
        self.favoritesButton?.image = icon
    }
    
}


// MARK: - UITableViewDataSource

extension TournamentEventsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = self.data.events[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(for: indexPath) as EventTableViewCell
        cell.configure(with: event)
        cell.delegate = self
        
        return cell
    }
    
}


// MARK: - UITableViewDelegate

extension TournamentEventsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 10
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 10
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return UIView()
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return UIView()
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let event = self.data.events[indexPath.row]
        self.output.didSelectEvent(event)
    }
    
}


// MARK: - DZNEmptyDataSetSource

extension TournamentEventsViewController: DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text: String
        switch self.data.status {
        case .unknown, .loading:
            return nil
        case .loaded:
            text = "Нет событий"
        case .error(let error):
            text = error.localizedDescription
        }
        
        let helper = EmptyDataSetSourceHelper()
        return helper.generateTitle(withText: text)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let text: String
        switch self.data.status {
        case .unknown, .loading, .loaded:
            return nil
        case .error:
            text = "Повторить"
        }
        
        let helper = EmptyDataSetSourceHelper()
        return helper.generateButtonTitle(withText: text)
    }
    
    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
        switch self.data.status {
        case .unknown, .loading:
            let helper = EmptyDataSetSourceHelper()
            return helper.generateLoadingView(for: scrollView)
        case .loaded, .error:
            return nil
        }
    }
    
}


// MARK: - DZNEmptyDataSetDelegate

extension TournamentEventsViewController: DZNEmptyDataSetDelegate {
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.output.didTapTryAgainButton()
    }
    
}


// MARK: - EventTableViewCellDelegate

extension TournamentEventsViewController: EventTableViewCellDelegate {
    
    func eventTableViewCellDidSelectRow(_ eventTableViewCell: EventTableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: eventTableViewCell),
            eventTableViewCell.isAvailable else { return }
        
        self.tableView(self.tableView, didSelectRowAt: indexPath)
    }
    
    func eventTableViewCellDidTapFavoritesButton(_ eventTableViewCell: EventTableViewCell) { }
    
}
