//
//  TournamentEventsTitleView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 22.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TournamentEventsTitleView: UIView, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Заголовок
    @IBOutlet
    private weak var titleLabel: UILabel!
    /// Подзаголовок
    @IBOutlet
    private weak var subtitleLabel: UILabel!
    
}


// MARK: - Публичные методы

extension TournamentEventsTitleView {
    
    /// Настройка
    func configure(with viewModel: TournamentEventsTitleViewModel) {
        self.titleLabel.text = viewModel.title
        self.subtitleLabel.text = viewModel.subtitle
    }
    
}
