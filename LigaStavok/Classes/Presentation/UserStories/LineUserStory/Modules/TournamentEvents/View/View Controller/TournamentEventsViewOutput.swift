//
//  TournamentEventsViewOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol TournamentEventsViewOutput: class {

    /// View загружено в память
    func didTriggerViewReadyEvent()
    
    /// Нажата кнопка "Избранное"
    func didTapFavoritesButton()
    
    /// Изменено значение фильтра по периоду отображения
    func didSelectPeriodFilter(_ periodFilter: PeriodFilter)
    
    /// Нажата кнопка "Повторить"
    func didTapTryAgainButton()
    
    /// Выбрано событие
    func didSelectEvent(_ viewModel: EventViewModel)

}
