//
//  TournamentEventsTitleViewModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 22.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct TournamentEventsTitleViewModel {
    
    // MARK: - Публичные свойства
    
    /// Заголовок
    let title: String
    /// Подзаголовок
    let subtitle: String?
    
}
