//
//  TournamentEventsViewInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol TournamentEventsViewInput: class {
    
    /// Установка начальных данных
    func setInitialData(titleViewModel: TournamentEventsTitleViewModel,
                        favoritesButtonHidden: Bool,
                        tournamentFavorites: Bool?)

    /// Подготовка
    func setupInitialState()
    
    /// Установить статус
    func setStatus(_ status: ModuleStatus)
    
    /// Отобразить данные
    func showData(events: [EventViewModel])
    
    /// Установить состояние турнира
    func setTournamentFavorites(_ tournamentFavorites: Bool)

}
