//
//  TournamentEventsPresenter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

final class TournamentEventsPresenter {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Статус
        var status: ModuleStatus = .unknown
        
        /// Последний статус доступности интернет-соединения
        var lastNetworkStatus: NetworkReachabilityStatus = .unknown
        
        /// Фильтр по периоду отображения
        var periodFilter: PeriodFilter = .all
        
        /// Турниры
        var tournaments: [Topic]!
        
        /// События
        var events: SynchronizedValue<[EventObject]> = .init([])
        
    }
    
    private struct LocalViewData {
        
        /// Заголовок
        var titleViewModel: TournamentEventsTitleViewModel!
        
        /// Доступность кнопки "Избранное"
        var isFavoritesButtonHidden: Bool!
        /// Состояние турнира
        var isTournamentFavorites: Bool?
        
        /// События
        var eventsViewModels: [EventViewModel] = []
        
    }
    
    
    // MARK: - Публичные свойства

    /// View
    weak var view: TournamentEventsViewInput?
    /// Interactor
    var interactor: TournamentEventsInteractorInput!
    /// Router
    var router: TournamentEventsRouterInput!
    
    
    // MARK: - Приватные свойства
    
    /// Очередь для патча событий
    private lazy var patchEventsOperationQueue: OperationQueue = {
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        operationQueue.qualityOfService = .utility
        
        return operationQueue
    }()
    
    /// Локальные данные
    private var data = LocalData()
    /// Данные представления
    private var viewData = LocalViewData()
    
    
    // MARK: - Инициализация
    
    init(tournaments: [Topic]) {
        self.data.tournaments = tournaments
        self.viewData.isFavoritesButtonHidden = tournaments.count > 1
        self.viewData.isTournamentFavorites = self.viewData.isFavoritesButtonHidden ? nil : false
    }

}


// MARK: - Приватные методы

private extension TournamentEventsPresenter {
    
    /// Получить данные
    func loadData() {
        self.data.status = .loading(type: .network)
        self.view?.setStatus(self.data.status)
        
        let tournamentsIds = self.data.tournaments.map { $0.id }
        let period = self.data.periodFilter.value
        self.interactor.obtainEventsFromNetwork(topicIds: tournamentsIds, period: period)
    }
    
    /// Подписаться на обновления
    func subscribeToUpdates() {
        let ids = self.data.events.get().map { $0.id }
        self.interactor.subscribeToEventsUpdates(withIds: ids)
    }
    
    /// Подготовка заголовка
    func prepareTitle() {
        let title: String
        if self.data.tournaments.count > 1 {
            let tournamentsCount = self.data.tournaments.count
            title = Localization.TournamentEvents.navigationBarTitle(withTournamentsCount: tournamentsCount)
        } else {
            title = self.data.tournaments.first?.title ?? .empty
        }
        
        self.viewData.titleViewModel = TournamentEventsTitleViewModel(title: title, subtitle: nil)
    }
    
    /// Подготовка событий
    func prepareEvents() {
        let viewModels = self.data.events.get().map { EventViewModel(eventObject: $0) }
        self.viewData.eventsViewModels = viewModels
    }
    
    /// Отобразить данные
    func showData() {
        prepareEvents()
        
        let eventsViewModels = self.viewData.eventsViewModels
        self.view?.showData(events: eventsViewModels)
    }
    
    /// Обработка ошибки
    func processError(_ error: Error) {
        self.data.status = .error(value: error)
        self.view?.setStatus(self.data.status)
    }
    
    /// Отсортировать события
    func sortEvents(_ events: [EventObject]) -> [EventObject] {
        return events//.sorted(by: { $0.sort < $1.sort })
    }
    
}


// MARK: - TournamentEventsViewOutput

extension TournamentEventsPresenter: TournamentEventsViewOutput {
    
    func didTriggerViewReadyEvent() {
        prepareTitle()
        
        let titleViewModel: TournamentEventsTitleViewModel = self.viewData.titleViewModel
        let isFavoritesButtonHidden: Bool = self.viewData.isFavoritesButtonHidden
        let isTournamentFavorites = self.viewData.isTournamentFavorites
        self.view?.setInitialData(titleViewModel: titleViewModel,
                                  favoritesButtonHidden: isFavoritesButtonHidden,
                                  tournamentFavorites: isTournamentFavorites)
        self.view?.setupInitialState()
        
        self.interactor.listenNetworkReachability()
        
        loadData()
    }
    
    func didTapFavoritesButton() {
        guard var isTournamentFavorites = self.viewData.isTournamentFavorites else { return }
        
        isTournamentFavorites = !isTournamentFavorites
        self.viewData.isTournamentFavorites = isTournamentFavorites
        
        self.view?.setTournamentFavorites(isTournamentFavorites)
    }
    
    func didSelectPeriodFilter(_ periodFilter: PeriodFilter) {
        self.data.periodFilter = periodFilter
        
        switch self.data.status {
        case .loading, .loaded, .error:
            self.interactor.cancelObtainingEventsFromNetwork()
            self.interactor.cancelSubscribingToEventsUpdates()
            
            self.data.events.syncSet { $0.removeAll() }
            
            showData()
            
        default:
            break
        }
        
        loadData()
    }
    
    func didTapTryAgainButton() {
        loadData()
    }
    
    func didSelectEvent(_ viewModel: EventViewModel) {
        let events = self.data.events.get()
        guard let index = events.index(where: { $0.id == viewModel.id }) else { return }
        
        let event = events[index]
        self.router.showEvent(event)
    }

}


// MARK: - TournamentEventsInteractorOutput

extension TournamentEventsPresenter: TournamentEventsInteractorOutput {
    
    func didObtainNetworkReachabilityStatus(_ status: NetworkReachabilityStatus) {
        defer { self.data.lastNetworkStatus = status }
        guard case .reachable = status, case .notReachable = self.data.lastNetworkStatus else { return }
        
        loadData()
    }
    
    func didObtainEventsFromNetwork(_ events: [EventObject]) {
        var events = events
        if case .tomorrow = self.data.periodFilter {
            events = events.filter { $0.gameTimestamp.compare(.isTomorrow) }
        }
        
        events = sortEvents(events)
        self.data.events.syncSet { $0 = events }
        
        self.data.status = .loaded
        self.view?.setStatus(self.data.status)
        
        showData()
        subscribeToUpdates()
    }
    
    func didFailToObtainEventsFromNetwork(with error: Error) {
        processError(error)
    }
    
    func didObtainEventsUpdates(_ updates: [DiffMapModel]) { }
    
}
