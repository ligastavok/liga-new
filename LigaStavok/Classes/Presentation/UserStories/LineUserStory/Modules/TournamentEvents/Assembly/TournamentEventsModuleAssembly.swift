//
//  TournamentEventsModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TournamentEventsModuleAssembly {

    /// Собрать модуль
    static func assembleModule(with tournaments: [Topic]) -> UIViewController {
        let storyboard = UIStoryboard(storyboard: .tournamentEvents)
        let view: TournamentEventsViewController = storyboard.instantiateViewController()
        
        let resolver = ApplicationAssembly.assembler.resolver
        guard let networkReachabilityService = resolver.resolve(NetworkReachabilityService.self) else { preconditionFailure() }
        guard let eventsService = resolver.resolve(EventsService.self) else { preconditionFailure() }
        guard let observerService = resolver.resolve(ObserverService.self) else { preconditionFailure() }

        let presenter = TournamentEventsPresenter(tournaments: tournaments)
        let interactor = TournamentEventsInteractor(networkReachabilityService: networkReachabilityService,
                                                    eventsService: eventsService,
                                                    observerService: observerService)
        let router = TournamentEventsRouter()

        view.output = presenter

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        interactor.output = presenter

        router.viewController = view

        return view
    }

}
