//
//  TournamentEventsRouterInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol TournamentEventsRouterInput: class {
    
    /// Отобразить экран "Роспись события"
    func showEvent(_ event: EventObject)
    
}
