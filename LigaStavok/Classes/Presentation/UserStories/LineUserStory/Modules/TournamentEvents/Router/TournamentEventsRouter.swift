//
//  TournamentEventsRouter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TournamentEventsRouter: Router {

    // MARK: - Публичные свойства

    weak var viewController: UIViewController?


    // MARK: - Инициализация

    init(viewController: UIViewController? = nil) {
        self.viewController = viewController
    }

}


// MARK: - TournamentEventsRouterInput

extension TournamentEventsRouter: TournamentEventsRouterInput {
    
    func showEvent(_ event: EventObject) {
        let eventViewController = EventModuleAssembly.assembleModule(event: event)
        show(eventViewController)
    }
    
}
