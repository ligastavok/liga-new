//
//  SportTournamentsPresenter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class SportTournamentsPresenter {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Статус
        var status: ModuleStatus = .unknown
        
        /// Фильтр по периоду отображения
        var periodFilter: PeriodFilter = .all
        
        /// Вид спорта
        var sport: Sport!
        
        /// Турниры
        var tournaments: [Topic] = []
        
        /// Отмеченные топики
        var selectedTournaments: [Topic] = []
        
    }
    
    private struct LocalViewData {
        
        /// Заголовок
        var titleViewModel: SportTournamentsTitleViewModel!
        
        /// Секции
        var sectionsViewModels = [
            HeaderSectionViewModel(title: Localization.SportTournaments.tournamentsSectionTitle,
                                   buttonTitle: nil)
        ]
        
        /// Турниры
        var tournamentsViewModels: [TournamentViewModel] = []
        
        /// Таблица редактируется
        var isTableViewEditing = false
        
    }
    

    // MARK: - Публичные свойства

    /// View
    weak var view: SportTournamentsViewInput?
    /// Interactor
    var interactor: SportTournamentsInteractorInput!
    /// Router
    var router: SportTournamentsRouterInput!
    
    
    // MARK: - Приватные свойства
    
    /// Локальные данные
    private var data = LocalData()
    /// Данные представления
    private var viewData = LocalViewData()
    
    
    // MARK: - Инициализация
    
    init(sport: Sport) {
        self.data.sport = sport
    }

}


// MARK: - Приватные методы

private extension SportTournamentsPresenter {
    
    /// Получить данные
    func loadData() {
        self.data.status = .loading(type: .network)
        self.view?.setStatus(self.data.status)
        
        let gameId = self.data.sport.gameId
        let period = self.data.periodFilter.value
        self.interactor.obtainTopicsFromNetwork(gameId: gameId, period: period)
    }
    
    /// Подготовка заголовка
    func prepareTitle() {
        self.viewData.titleViewModel = SportTournamentsTitleViewModel(sport: self.data.sport)
    }
    
    /// Подготовка турниров
    func prepareTournaments() {
        let tournaments = self.data.tournaments
        
        let viewModels: [TournamentViewModel] = tournaments.map { tournament in
            let isSelected = self.data.selectedTournaments.contains(where: { $0.id == tournament.id })
            let viewModel = TournamentViewModel(id: tournament.id,
                                                title: tournament.title,
                                                isFavorite: false,
                                                isSelected: isSelected,
                                                liveEventsCount: 0,
                                                allEventsCount: tournament.quantity)
            return viewModel
        }
        self.viewData.tournamentsViewModels = viewModels
    }
    
    /// Отобразить данные
    func showData() {
        let tournamentsViewModels = self.viewData.tournamentsViewModels
        self.view?.showData(tournaments: tournamentsViewModels)
    }
    
    /// Обработка ошибки
    func processError(_ error: Error) {
        self.data.status = .error(value: error)
        self.view?.setStatus(self.data.status)
    }
    
}


// MARK: - SportTournamentsViewOutput

extension SportTournamentsPresenter: SportTournamentsViewOutput {
    
    func didTriggerViewReadyEvent() {
        prepareTitle()
        
        let titleViewModel: SportTournamentsTitleViewModel = self.viewData.titleViewModel
        let sectionsViewModels = self.viewData.sectionsViewModels
        self.view?.setInitialData(title: titleViewModel, sections: sectionsViewModels)
        self.view?.setupInitialState()
        
        loadData()
    }
    
    func didTriggerViewWillAppearEvent() {
        self.view?.prepareWillAppear()
    }
    
    func didTriggerViewWillDisappearEvent() {
        self.view?.prepareWillDisappear()
    }
    
    func didSelectPeriodFilter(_ periodFilter: PeriodFilter) {
        self.data.periodFilter = periodFilter
        
        switch self.data.status {
        case .loading, .loaded, .error:
            self.interactor.cancelObtainingTopicsFromNetwork()
            
            self.data.tournaments = []
            self.viewData.tournamentsViewModels = []
            
            showData()
            didTapCancelButton()
            
        default:
            break
        }
        
        loadData()
    }
    
    func didTapTryAgainButton() {
        loadData()
    }
    
    func didTapEditButton() {
        guard !self.viewData.isTableViewEditing else { return }
        
        self.viewData.isTableViewEditing = true
        self.view?.setTableViewEditingStatus(self.viewData.isTableViewEditing)
    }
    
    func didTapCancelButton() {
        guard self.viewData.isTableViewEditing else { return }
        
        let ids = self.data.selectedTournaments.map { $0.id }
        if !ids.isEmpty {
            for id in ids {
                guard let index = self.viewData.tournamentsViewModels.index(where: { $0.id == id }) else { continue }
                
                self.viewData.tournamentsViewModels[index].isSelected = false
                
                let tournament = self.viewData.tournamentsViewModels[index]
                self.view?.updateTournament(tournament)
            }
            
            self.data.selectedTournaments.removeAll()
        }
        
        self.view?.setShowSelectedTournamentsButtonEnabled(false)
        
        self.viewData.isTableViewEditing = false
        self.view?.setTableViewEditingStatus(self.viewData.isTableViewEditing)
    }
    
    func didTapFavoritesButton(for viewModel: TournamentViewModel) {
        guard let index = self.viewData.tournamentsViewModels.index(where: { $0.id == viewModel.id }) else { return }
        
        self.viewData.tournamentsViewModels[index].isFavorite = !viewModel.isFavorite
        
        let tournament = self.viewData.tournamentsViewModels[index]
        self.view?.updateTournament(tournament)
    }
    
    func didTapCheckboxButton(for viewModel: TournamentViewModel) {
        guard self.data.selectedTournaments.contains(where: { $0.id == viewModel.id }) == viewModel.isSelected else { return }
        
        if viewModel.isSelected {
            guard let index = self.data.selectedTournaments.index(where: { $0.id == viewModel.id }) else { return }
            
            self.data.selectedTournaments.remove(at: index)
        } else {
            guard let index = self.data.tournaments.index(where: { $0.id == viewModel.id }) else { return }
            
            let tournament = self.data.tournaments[index]
            self.data.selectedTournaments.append(tournament)
        }
        
        guard let index = self.viewData.tournamentsViewModels.index(where: { $0.id == viewModel.id }) else { return }
        
        self.viewData.tournamentsViewModels[index].isSelected = !viewModel.isSelected
        
        let tournament = self.viewData.tournamentsViewModels[index]
        self.view?.updateTournament(tournament)
        
        let isEnabledButton = self.data.selectedTournaments.count > 1
        self.view?.setShowSelectedTournamentsButtonEnabled(isEnabledButton)
    }
    
    func didSelectTournament(_ viewModel: TournamentViewModel) {
        if self.viewData.isTableViewEditing {
            didTapCheckboxButton(for: viewModel)
        } else {
            guard let index = self.data.tournaments.index(where: { $0.id == viewModel.id }) else { return }
            
            let tournament = self.data.tournaments[index]
            self.router.showTournamentEvents(for: [tournament])
        }
    }
    
    func didTapShowSelectedTournamentsButton() {
        guard !self.data.selectedTournaments.isEmpty else { return }
        
        self.router.showTournamentEvents(for: self.data.selectedTournaments)
    }
    
}


// MARK: - SportTournamentsInteractorOutput

extension SportTournamentsPresenter: SportTournamentsInteractorOutput {
    
    func didObtainTopicsFromNetwork(_ topics: [Topic]) {
//        var topics = topics
        if case .tomorrow = self.data.periodFilter {
            // FIXME: - Фильтр, отсеивающий сегоднящние турниры
        }
        
        self.data.tournaments = topics
        
        self.data.status = .loaded
        self.view?.setStatus(self.data.status)
        
        prepareTournaments()
        showData()
    }
    
    func didFailToObtainTopicsFromNetwork(with error: Error) {
        processError(error)
    }
    
}
