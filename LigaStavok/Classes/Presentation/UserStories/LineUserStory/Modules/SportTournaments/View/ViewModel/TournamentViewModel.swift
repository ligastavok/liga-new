//
//  TournamentViewModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct TournamentViewModel: Equatable {
    
    /// Идентификатор текущего топика
    let id: Int
    /// Название
    let title: String
    /// Избранный
    var isFavorite: Bool
    /// Отмеченный
    var isSelected: Bool
    /// Счетчик live-событий
    let liveEventsCount: Int
    /// Счетчик всех событий
    let allEventsCount: Int
    
}


// MARK: - Hashable

extension TournamentViewModel: Hashable {
    
    var hashValue: Int {
        return HashManager.combineHashes([self.id.hashValue,
                                          self.title.hashValue,
                                          self.isFavorite.hashValue,
                                          self.isSelected.hashValue,
                                          self.liveEventsCount.hashValue,
                                          self.allEventsCount.hashValue])
    }
    
}
