//
//  SportTournamentsTitleViewModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct SportTournamentsTitleViewModel {
    
    // MARK: - Публичные свойства
    
    /// Название
    let text: String
    /// Название картинки
    let imageName: String
    
    
    // MARK: - Инициализация
    
    init(text: String, imageName: String) {
        self.text = text
        self.imageName = imageName
    }
    
    init(sport: Sport) {
        self.init(text: sport.title, imageName: ImageAsset.placeholderSportTournaments.rawValue)
    }
    
}
