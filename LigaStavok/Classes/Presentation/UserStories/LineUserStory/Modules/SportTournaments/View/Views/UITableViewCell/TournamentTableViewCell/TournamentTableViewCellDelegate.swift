//
//  TournamentTableViewCellDelegate.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol TournamentTableViewCellDelegate: class {
    
    /// Нажата кнопка "Избранное"
    func tournamentTableViewCell(_ tournamentTableViewCell: TournamentTableViewCell,
                                 didTapFavoritesButtonFor viewModel: TournamentViewModel)
    /// Нажата кнопка "Флажок"
    func tournamentTableViewCell(_ tournamentTableViewCell: TournamentTableViewCell,
                                 didTapCheckboxButtonFor viewModel: TournamentViewModel)
    
}
