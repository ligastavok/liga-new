//
//  StretchyHeader.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 3/29/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import GSKStretchyHeaderView

final class StretchyHeader: GSKStretchyHeaderView, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Контейнер для изображения
    @IBOutlet
    private weak var imageContainerView: UIView!
    
    /// Эффект размытия изображения
    @IBOutlet
    private weak var imageBlurVisualEffectView: UIVisualEffectView! {
        willSet {
            newValue.isHidden = true
            newValue.alpha = 0
        }
    }
    /// Изображение
    @IBOutlet
    private weak var imageView: UIImageView!
    
    /// Заголовок
    @IBOutlet
    private weak var titleLabel: UILabel!
    
    
    // MARK: - Приватные свойства
    
    /// Минимальный размер шрифта
    private let minFontSize: CGFloat = 17
    /// Максимальный размер шрифта
    private let maxFontSize: CGFloat = 34
    
    /// Минимальная прозрачность изображения
    private let minImageAlpha: CGFloat = 0
    /// Максимальная прозрачность изображения
    private let maxImageAlpha: CGFloat = 1

}


// MARK: - Публичные свойства

extension StretchyHeader {
    
    /// Изображение
    var image: UIImage? {
        set {
            self.imageView.image = newValue
        }
        get {
            return self.imageView.image
        }
    }
    
    /// Заголовок
    var title: String? {
        set {
            self.titleLabel.text = newValue
        }
        get {
            return self.titleLabel.text
        }
    }
    
}


// MARK: - GSKStretchyHeaderView

extension StretchyHeader {
    
    override func didChangeStretchFactor(_ stretchFactor: CGFloat) {
        super.didChangeStretchFactor(stretchFactor)
        
        let alpha = CGFloatTranslateRange(min(1, stretchFactor), 0, 1, self.minImageAlpha, self.maxImageAlpha)
        self.imageContainerView.alpha = alpha
        
        let fontSize = CGFloatTranslateRange(min(1, stretchFactor), 0, 1, self.minFontSize, self.maxFontSize)
        if abs(fontSize - self.titleLabel.font.pointSize) > 0.05 {
            self.titleLabel.font = .boldSystemFont(ofSize: fontSize)
        }
    }
    
}


// MARK: - Публичные методы

extension StretchyHeader {
    
    /// Установить видимость эффекта
    func showBlurHidden(_ hidden: Bool, animated: Bool) {
        
        // Настройка перед отображением
        if !hidden {
            self.imageBlurVisualEffectView.isHidden = false
        }
        
        // Анимация отображения
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.imageBlurVisualEffectView.alpha = hidden ? 0 : 1
        }, completion: { [weak self] finished in
            guard finished else { return }
            
            // Настройка после скрытия
            if hidden {
                self?.imageBlurVisualEffectView.isHidden = true
            }
        })
    }
    
}
