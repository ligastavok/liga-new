//
//  SportTournamentsViewOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol SportTournamentsViewOutput: class {
    
    /// View загружено в память
    func didTriggerViewReadyEvent()
    
    /// View собирается быть отображен
    func didTriggerViewWillAppearEvent()
    
    /// View собирается прекратить быть отображенным
    func didTriggerViewWillDisappearEvent()
    
    /// Изменено значение фильтра по периоду отображения
    func didSelectPeriodFilter(_ periodFilter: PeriodFilter)
    
    /// Нажата кнопка "Повторить"
    func didTapTryAgainButton()
    
    /// Нажата кнопка "Редактировать"
    func didTapEditButton()
    
    /// Нажата кнопка "Отменить"
    func didTapCancelButton()
    
    /// Нажата кнопка "Избранное" в ячейке турнира
    func didTapFavoritesButton(for viewModel: TournamentViewModel)
    
    /// Нажата кнопка "Флажок" в ячейке турнира
    func didTapCheckboxButton(for viewModel: TournamentViewModel)
    
    /// Выбран турнир
    func didSelectTournament(_ viewModel: TournamentViewModel)
    
    /// Нажата кнопка "Показать события"
    func didTapShowSelectedTournamentsButton()
    
}
