//
//  SportTournamentsViewInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol SportTournamentsViewInput: class {
    
    /// Установка начальных данных
    func setInitialData(title: SportTournamentsTitleViewModel, sections: [HeaderSectionViewModel])
    
    /// Подготовка
    func setupInitialState()
    
    /// Подготовить view к отображению
    func prepareWillAppear()
    
    /// Подготовить view к прекращению отображения
    func prepareWillDisappear()
    
    /// Установить статус
    func setStatus(_ status: ModuleStatus)
    
    /// Изменить состояние таблицы
    func setTableViewEditingStatus(_ editing: Bool)
    
    /// Изменить доступность кнопки "Показать события"
    func setShowSelectedTournamentsButtonEnabled(_ enabled: Bool)
    
    /// Отобразить данные
    func showData(tournaments: [TournamentViewModel])
    
    /// Обновить запись
    func updateTournament(_ tournament: TournamentViewModel)
    
}
