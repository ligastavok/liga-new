//
//  TournamentTableViewCell.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TournamentTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Кнопка
    @IBOutlet
    private weak var button: UIButton!
    
    /// Название
    @IBOutlet
    private weak var titleLabel: UILabel!
    
    /// Счетчик live-событий
    @IBOutlet
    private weak var liveEventsCounterView: EventCounterView!
    /// Счетчик всех событий
    @IBOutlet
    private weak var allEventsCounterView: EventCounterView!
    
    /// Правый отступ счетчиков
    @IBOutlet
    private weak var countersTrailingLayoutConstraint: NSLayoutConstraint!
    
    /// Контейнер индикатора перехода
    @IBOutlet
    private weak var disclosureIndicatorContainerView: UIView!
    /// Ширина контейнера индикатора перехода
    @IBOutlet
    private weak var disclosureIndicatorContainerViewWidthLayoutConstraint: NSLayoutConstraint!
    
    
    // MARK: - Публичные свойства
    
    weak var delegate: TournamentTableViewCellDelegate?
    
    
    // MARK: - Приватные свойства
    
    private var viewModel: TournamentViewModel?
    
}


// MARK: - UITableViewCell

extension TournamentTableViewCell {
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        
        // Настройка стиля выделения
        self.selectionStyle = editing ? .none : .default
        
        // Настройка иконки
        setupIcon()
        
        // Настройка перед отображением
        if !editing {
            self.disclosureIndicatorContainerView.isHidden = false
        }
        
        // Анимация отображения
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            
            // Отступ для счетчиков
            self?.countersTrailingLayoutConstraint.constant = editing ? 10 : 0
            
            // Видимость индикатора перехода
            self?.disclosureIndicatorContainerView.alpha = editing ? 0 : 1
            self?.disclosureIndicatorContainerViewWidthLayoutConstraint.constant = editing ? 0 : 38
            
            // Отображение изменений
            self?.layoutIfNeeded()
        }, completion: { [weak self] finished in
            guard finished else { return }
            
            // Настройка после скрытия
            if editing {
                self?.disclosureIndicatorContainerView.isHidden = true
            }
        })
    }
    
}


// MARK: - Action

private extension TournamentTableViewCell {
    
    /// Нажата кнопка
    @IBAction
    func buttonTapped() {
        guard let viewModel = self.viewModel else { return }
        
        if self.isEditing {
            self.delegate?.tournamentTableViewCell(self, didTapCheckboxButtonFor: viewModel)
        } else {
            self.delegate?.tournamentTableViewCell(self, didTapFavoritesButtonFor: viewModel)
        }
    }
    
}


// MARK: - Публичные методы

extension TournamentTableViewCell {
    
    /// Настройка
    func configure(with viewModel: TournamentViewModel) {
        self.viewModel = viewModel
        
        setupIcon()
        
        self.titleLabel.text = viewModel.title
        
        self.liveEventsCounterView.count = viewModel.liveEventsCount
        self.liveEventsCounterView.isHidden = viewModel.liveEventsCount == 0
        
        self.allEventsCounterView.count = viewModel.allEventsCount
        self.allEventsCounterView.isHidden = viewModel.allEventsCount == 0
    }
    
}


// MARK: - Приватные методы

private extension TournamentTableViewCell {
    
    /// Настройка иконки
    func setupIcon() {
        guard let viewModel = self.viewModel else { return }
        
        let imageAsset: ImageAsset
        if self.isEditing {
            imageAsset = viewModel.isSelected ? .iconCheck : .iconUncheck
        } else {
            imageAsset = viewModel.isFavorite ? .iconStar : .iconUnstar
        }
        
        let icon = UIImage(asset: imageAsset)
        self.button.setImage(icon, for: .normal)
    }
    
}
