//
//  TournamentsButton.swift
//  LigaStavok
//
//  Created by Artem on 24/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TournamentsButton: Button { }


// MARK: - Button

extension TournamentsButton {
    
    override func willUpdateHighlighted(_ highlighted: Bool) {
        self.titleLabel?.alpha = highlighted ? 0.5 : 1
    }
    
}
