//
//  SportTournamentsViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

final class SportTournamentsViewController: UIViewController {
    
    // MARK: - Типы данных
    
    private enum SectionType: Int {
        
        /// Турниры
        case tournaments
        
    }
    
    private struct LocalData {
        
        /// Статус
        var status: ModuleStatus = .unknown
        
        /// Заголовок
        var title: SportTournamentsTitleViewModel!
        
        /// Секции
        let sections: [SectionType] = [
            .tournaments
        ]
        /// Заголовки секциий
        var sectionsHeaders: [HeaderSectionViewModel] = []
        
        /// Турниры
        var tournaments: [TournamentViewModel] = []
        
        /// Состояние
        var isEmpty: Bool {
            return self.tournaments.isEmpty
        }
        
    }
    
    
    // MARK: - Outlet
    
    @IBOutlet
    private weak var tableView: UITableView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            
            // Регистрация верхнего колонтитула
            newValue.register(HeaderSectionView.self)
            
            // Регистрация ячеек
            newValue.register(TournamentTableViewCell.self)
        }
    }
    
    /// Кнопка "Показать события"
    @IBOutlet
    private weak var showSelectedTournamentsButton: UIButton!
    

    // MARK: - Публичные свойства

    /// Presenter
    var output: SportTournamentsViewOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Эластичный заголовок
    private weak var stretchyHeader: StretchyHeader!
    
    /// Локальные данные
    private var data = LocalData()

}


// MARK: - UIViewController

extension SportTournamentsViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.output.didTriggerViewReadyEvent()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.output.didTriggerViewWillAppearEvent()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.output.didTriggerViewWillDisappearEvent()
        
        if let navigationController = self.navigationController as? NavigationController {
            navigationController.stopFollowingScrollView(showingNavigationBar: true)
        }
    }
    
}


// MARK: - Action

private extension SportTournamentsViewController {
    
    /// Изменено значение фильтра
    @IBAction
    func segmentedControlValueChanged(_ sender: SegmentedControl) {
        guard let periodFilter = PeriodFilter(rawValue: sender.selectedSegmentIndex) else { return }
        self.output.didSelectPeriodFilter(periodFilter)
    }
    
    /// Нажата кнопка "Показать события"
    @IBAction
    func showSelectedTournamentsButtonTapped() {
        self.output.didTapShowSelectedTournamentsButton()
    }
    
}


// MARK: - Приватные методы

private extension SportTournamentsViewController {
    
    /// Настройка эластичного заголовка
    func setupStretchyHeader() {
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        }
        
        let stretchyHeader = StretchyHeader.loadFromNib()
        self.tableView.addSubview(stretchyHeader)
        self.stretchyHeader = stretchyHeader
        
        // Настройка текста
        stretchyHeader.title = self.data.title.text
        
        // Настройка иконки
        let image = UIImage(named: self.data.title.imageName)
        stretchyHeader.image = image
    }
    
    /// Настроить видимость кнопки "Отмена"
    func setCancelButtonHidded(_ hidden: Bool) {
        if hidden {
            self.navigationItem.setRightBarButton(nil, animated: true)
        } else {
            let cancelButton = UIBarButtonItem(title: "Отменить", style: .plain, target: self, action: #selector(cancelButtonTapped))
            
            let font: UIFont = .systemFont(ofSize: 15)
            let textColor: UIColor = .white
            
            let attributes: [NSAttributedStringKey: Any] = [
                .font: font,
                .foregroundColor: textColor
            ]
            cancelButton.setTitleTextAttributes(attributes, for: .normal)
            cancelButton.setTitleTextAttributes(attributes, for: .highlighted)
            
            self.navigationItem.setRightBarButton(cancelButton, animated: true)
        }
    }
    
    /// Настроить видимость кнопки "Показать события"
    func setShowSelectedTournamentsButtonHidded(_ hidden: Bool) {
        var contentInset = self.tableView.contentInset
        contentInset.bottom = (!hidden ? 21 + self.showSelectedTournamentsButton.frame.height : 0) - 20
        
        self.tableView.contentInset = contentInset
        
        // Настройка перед отображением
        if !hidden {
            self.showSelectedTournamentsButton.isHidden = false
        }
        
        // Анимация отображения
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.showSelectedTournamentsButton.alpha = hidden ? 0 : 1
        }, completion: { [weak self] finished in
            guard finished else { return }
            
            // Настройка после скрытия
            if hidden {
                self?.showSelectedTournamentsButton.isHidden = true
            }
        })
    }
    
    /// Нажата кнопка "Отменить"
    @objc
    func cancelButtonTapped() {
        self.output.didTapCancelButton()
    }
    
    /// Долгое касание по таблице
    @objc
    func handleLongPress(gesture: UILongPressGestureRecognizer) {
        guard gesture.state == .began else { return }
        self.output.didTapEditButton()
    }
    
}


// MARK: - SportTournamentsViewInput

extension SportTournamentsViewController: SportTournamentsViewInput {
    
    func setInitialData(title: SportTournamentsTitleViewModel, sections: [HeaderSectionViewModel]) {
        self.data.title = title
        self.data.sectionsHeaders = sections
    }
    
    func setupInitialState() {
        
        // Настройка эластичного заголовка
        setupStretchyHeader()
        
        // Настройка таблицы
        var contentInset = self.tableView.contentInset
        contentInset.bottom = -20
        
        self.tableView.contentInset = contentInset
        self.tableView.tableFooterView = UIView()
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gesture:)))
        self.tableView.addGestureRecognizer(longPress)
        
        // Настройка фреймворка DZNEmptyDataSet
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        
        // Настройка кнопки "Показать события"
        self.showSelectedTournamentsButton.isHidden = true
        self.showSelectedTournamentsButton.alpha = 0
    }
    
    func prepareWillAppear() {
        self.navigationController?.setNavigationBarTransparent(true, animated: true)
    }
    
    func prepareWillDisappear() {
        self.navigationController?.setNavigationBarTransparent(false, animated: true)
    }
    
    func setStatus(_ status: ModuleStatus) {
        guard self.data.status != status else { return }
        self.data.status = status
        
        switch status {
        case .loading, .error:
            guard self.data.isEmpty else { return }
            self.tableView.reloadData()
            
        default:
            return
        }
    }
    
    func setTableViewEditingStatus(_ editing: Bool) {
        
        // Тактильный отклик
        if editing, #available(iOS 10.0, *) {
            let feedbackGenerator = UIImpactFeedbackGenerator(style: .light)
            feedbackGenerator.impactOccurred()
        }
        
        // Настройка навигационной панели
        self.stretchyHeader.showBlurHidden(!editing, animated: true)
        setCancelButtonHidded(!editing)
        
        // Настройка таблицы
        self.tableView.setEditing(editing, animated: true)
    }
    
    func setShowSelectedTournamentsButtonEnabled(_ enabled: Bool) {
        setShowSelectedTournamentsButtonHidded(!enabled)
    }
    
    func showData(tournaments: [TournamentViewModel]) {
        self.data.tournaments = tournaments
        self.tableView.reloadData()
    }
    
    func updateTournament(_ tournament: TournamentViewModel) {
        guard let index = self.data.tournaments.index(where: { $0.id == tournament.id }) else { return }
        
        self.data.tournaments[index] = tournament
        
        let indexPath = IndexPath(row: index, section: 0)
        guard let cell = self.tableView.cellForRow(at: indexPath) as? TournamentTableViewCell else { return }
        
        cell.configure(with: tournament)
    }
    
}


// MARK: - TournamentTableViewCellDelegate

extension SportTournamentsViewController: TournamentTableViewCellDelegate {
    
    func tournamentTableViewCell(_ tournamentTableViewCell: TournamentTableViewCell,
                                 didTapFavoritesButtonFor viewModel: TournamentViewModel) {
        
        self.output.didTapFavoritesButton(for: viewModel)
    }
    
    func tournamentTableViewCell(_ tournamentTableViewCell: TournamentTableViewCell,
                                 didTapCheckboxButtonFor viewModel: TournamentViewModel) {
        
        self.output.didTapCheckboxButton(for: viewModel)
    }
    
}


// MARK: - UITableViewDataSource

extension SportTournamentsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.tournaments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tournament = self.data.tournaments[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(for: indexPath) as TournamentTableViewCell
        cell.configure(with: tournament)
        cell.delegate = self
        
        return cell
    }
    
}


// MARK: - UITableViewDelegate

extension SportTournamentsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 34 + (section == 0 ? 10 : 0)
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 10
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            let sectionHeader = self.data.sectionsHeaders[section]
            
            let lineHeaderSectionView = tableView.dequeueReusableHeaderFooterView() as HeaderSectionView
            lineHeaderSectionView.configure(with: sectionHeader)
            
            return lineHeaderSectionView
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return UIView()
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let tournament = self.data.tournaments[indexPath.row]
        self.output.didSelectTournament(tournament)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
}


// MARK: - DZNEmptyDataSetSource

extension SportTournamentsViewController: DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text: String
        switch self.data.status {
        case .unknown, .loading:
            return nil
        case .loaded:
            text = "Нет турниров"
        case .error(let error):
            text = error.localizedDescription
        }
        
        let helper = EmptyDataSetSourceHelper()
        return helper.generateTitle(withText: text)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let text: String
        switch self.data.status {
        case .unknown, .loading, .loaded:
            return nil
        case .error:
            text = "Повторить"
        }
        
        let helper = EmptyDataSetSourceHelper()
        return helper.generateButtonTitle(withText: text)
    }
    
    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
        switch self.data.status {
        case .unknown, .loading:
            let helper = EmptyDataSetSourceHelper()
            return helper.generateLoadingView(for: scrollView)
        case .loaded, .error:
            return nil
        }
    }
    
}


// MARK: - DZNEmptyDataSetDelegate

extension SportTournamentsViewController: DZNEmptyDataSetDelegate {
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.output.didTapTryAgainButton()
    }
    
}
