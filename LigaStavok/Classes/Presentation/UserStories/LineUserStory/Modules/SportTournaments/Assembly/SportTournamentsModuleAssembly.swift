//
//  SportTournamentsModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class SportTournamentsModuleAssembly {

    /// Собрать модуль
    static func assembleModule(with sport: Sport) -> UIViewController {
        let storyboard = UIStoryboard(storyboard: .sportTournaments)
        let view: SportTournamentsViewController = storyboard.instantiateViewController()
        
        let resolver = ApplicationAssembly.assembler.resolver
        guard let eventsService = resolver.resolve(EventsService.self) else { preconditionFailure() }

        let presenter = SportTournamentsPresenter(sport: sport)
        let interactor = SportTournamentsInteractor(eventsService: eventsService)
        let router = SportTournamentsRouter()

        view.output = presenter

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        interactor.output = presenter

        router.viewController = view

        return view
    }

}
