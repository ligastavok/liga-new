//
//  SportTournamentsInteractor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class SportTournamentsInteractor {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Операция на получение турниров
        weak var obtainingTopicsFromNetworkOperation: Operation?
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Presenter
    weak var output: SportTournamentsInteractorOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Сервис для работы с событиями
    private let eventsService: EventsService
    
    /// Локальные данные
    private var data = LocalData()
    
    
    // MARK: - Инициализация
    
    init(eventsService: EventsService) {
        self.eventsService = eventsService
    }
    
}


// MARK: - SportTournamentsInteractorInput

extension SportTournamentsInteractor: SportTournamentsInteractorInput {
    
    func obtainTopicsFromNetwork(gameId: Int, period: Int?) {
        cancelObtainingTopicsFromNetwork()
        
        let operation = self.eventsService.obtainTopicsList(gameId: gameId, period: period, namespace: nil) { [weak self] result in
            switch result {
            case .success(let topics):
                self?.output.didObtainTopicsFromNetwork(topics)
            case .failure(let error):
                self?.output.didFailToObtainTopicsFromNetwork(with: error)
            }
        }
        self.data.obtainingTopicsFromNetworkOperation = operation
    }
    
    func cancelObtainingTopicsFromNetwork() {
        self.data.obtainingTopicsFromNetworkOperation?.cancel()
    }
    
}
