//
//  SportTournamentsInteractorOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol SportTournamentsInteractorOutput: class {
    
    /// Получены топики из сети
    func didObtainTopicsFromNetwork(_ topics: [Topic])
    /// При получении топиков из интернета произошла ошибка
    func didFailToObtainTopicsFromNetwork(with error: Error)
    
}
