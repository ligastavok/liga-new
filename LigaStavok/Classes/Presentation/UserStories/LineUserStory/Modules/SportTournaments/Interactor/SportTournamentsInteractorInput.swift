//
//  SportTournamentsInteractorInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol SportTournamentsInteractorInput: class {
    
    /// Получить турниры из интернета
    func obtainTopicsFromNetwork(gameId: Int, period: Int?)
    /// Отмена получения турниров из интернета
    func cancelObtainingTopicsFromNetwork()
    
}
