//
//  SportTournamentsRouter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class SportTournamentsRouter: Router {

    // MARK: - Публичные свойства

    weak var viewController: UIViewController?


    // MARK: - Инициализация

    init(viewController: UIViewController? = nil) {
        self.viewController = viewController
    }

}


// MARK: - SportTournamentsRouterInput

extension SportTournamentsRouter: SportTournamentsRouterInput {
    
    func showTournamentEvents(for tournaments: [Topic]) {
        let tournamentEventsViewController = TournamentEventsModuleAssembly.assembleModule(with: tournaments)
        show(tournamentEventsViewController)
    }
    
}
