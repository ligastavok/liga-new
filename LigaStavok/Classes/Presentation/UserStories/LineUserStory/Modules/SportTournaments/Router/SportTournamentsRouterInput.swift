//
//  SportTournamentsRouterInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol SportTournamentsRouterInput: class {
    
    /// Отобразить экран "Роспись турнира"
    func showTournamentEvents(for tournaments: [Topic])
    
}
