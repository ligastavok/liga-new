//
//  LiveEventsViewInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol LiveEventsViewInput: class {
    
    /// Отобразить модуль "Фильтр по виду спорта"
    func showSportsFilterViewController(_ sportsFilterViewController: UIViewController)

    /// Подготовка
    func setupInitialState(title: String)
    
    /// Установить статус
    func setStatus(_ status: ModuleStatus)
    
    /// Отобразить данные
    func showData(events: [EventViewModel])
    
}
