//
//  LiveEventsViewOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LiveEventsViewOutput: class {

    /// View загружено в память
    func didTriggerViewReadyEvent()
    
    /// Нажата кнопка "Повторить"
    func didTapTryAgainButton()
    
    /// Выбрано событие
    func didSelectEvent(_ viewModel: EventViewModel)

}
