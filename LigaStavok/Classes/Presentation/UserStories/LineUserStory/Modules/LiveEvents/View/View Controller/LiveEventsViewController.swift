//
//  LiveEventsViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import DeepDiff
import DZNEmptyDataSet

final class LiveEventsViewController: UIViewController {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Статус
        var status: ModuleStatus = .unknown
        
        /// События
        var events: [EventViewModel] = []
        
        /// Состояние
        var isEmpty: Bool {
            return self.events.isEmpty
        }
        
    }
    
    
    // MARK: - Outlet
    
    @IBOutlet
    private weak var tableView: UITableView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            
            // Регистрация ячеек
            newValue.register(EventTableViewCell.self)
        }
    }
    
    /// Контейнер для модуля "Фильтр по виду спорта"
    @IBOutlet
    private weak var sportsFilterContainerView: UIView!
    
    
    // MARK: - Публичные свойства

    /// Presenter
    var output: LiveEventsViewOutput!
    
    
    // MARK: - Приватные свойства
    
    /// Локальные данные
    private var data = LocalData()

}


// MARK: - UIViewController

extension LiveEventsViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.output.didTriggerViewReadyEvent()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let navigationController = self.navigationController as? NavigationController {
            if let tabBarController = tabBarController as? TabBarController {
                navigationController.followScrollView(self.tableView, delay: 40, scrollSpeedFactor: 2,
                                                      followers: [NavigationBarFollower(view: tabBarController.tabBar, direction: .scrollDown)])
            } else {
                navigationController.followScrollView(self.tableView, delay: 40, scrollSpeedFactor: 2)
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let navigationController = self.navigationController as? NavigationController {
            navigationController.stopFollowingScrollView(showingNavigationBar: true)
        }
    }
}


// MARK: - LiveEventsViewInput

extension LiveEventsViewController: LiveEventsViewInput {
    
    func showSportsFilterViewController(_ sportsFilterViewController: UIViewController) {
        add(child: sportsFilterViewController, in: self.sportsFilterContainerView, pinToEdges: true)
    }

    func setupInitialState(title: String) {
        
        // Настройка заголовка
        self.navigationItem.title = title
        
        // Настройка таблицы
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: -20, right: 0)
        self.tableView.tableFooterView = UIView()
        
        // Настройка фреймворка DZNEmptyDataSet
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
    }
    
    func setStatus(_ status: ModuleStatus) {
        guard self.data.status != status else { return }
        self.data.status = status
        
        switch status {
        case .loading, .error:
            guard self.data.isEmpty else { return }
            self.tableView.reloadData()
            
        default:
            return
        }
    }
    
    func showData(events: [EventViewModel]) {
        if self.data.isEmpty || events.isEmpty {
            self.data.events = events
            self.tableView.reloadData()
        } else {
            let eventsChanges = diff(old: self.data.events, new: events)
            self.data.events = events
            
            if !eventsChanges.isEmpty {
                self.tableView.reload(changes: eventsChanges,
                                      insertionAnimation: .none,
                                      deletionAnimation: .none,
                                      replacementAnimation: .none) { _ in }
            }
        }
    }

}


// MARK: - UITableViewDataSource

extension LiveEventsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let event = self.data.events[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(for: indexPath) as EventTableViewCell
        cell.configure(with: event)
        cell.delegate = self
        
        return cell
    }
    
}


// MARK: - UITableViewDelegate

extension LiveEventsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 10
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return 10
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return UIView()
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let numberOfRows = self.tableView(tableView, numberOfRowsInSection: section)
        if numberOfRows > 0 {
            return UIView()
        } else {
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let event = self.data.events[indexPath.row]
        self.output.didSelectEvent(event)
    }
    
}


// MARK: - DZNEmptyDataSetSource

extension LiveEventsViewController: DZNEmptyDataSetSource {
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        let text: String
        switch self.data.status {
        case .unknown, .loading:
            return nil
        case .loaded:
            text = "Нет событий"
        case .error(let error):
            text = error.localizedDescription
        }
        
        let helper = EmptyDataSetSourceHelper()
        return helper.generateTitle(withText: text)
    }
    
    func buttonTitle(forEmptyDataSet scrollView: UIScrollView!, for state: UIControlState) -> NSAttributedString! {
        let text: String
        switch self.data.status {
        case .unknown, .loading, .loaded:
            return nil
        case .error:
            text = "Повторить"
        }
        
        let helper = EmptyDataSetSourceHelper()
        return helper.generateButtonTitle(withText: text)
    }
    
    func customView(forEmptyDataSet scrollView: UIScrollView!) -> UIView! {
        switch self.data.status {
        case .unknown, .loading:
            let helper = EmptyDataSetSourceHelper()
            return helper.generateLoadingView(for: scrollView)
        case .loaded, .error:
            return nil
        }
    }
    
}


// MARK: - DZNEmptyDataSetDelegate

extension LiveEventsViewController: DZNEmptyDataSetDelegate {
    
    func emptyDataSet(_ scrollView: UIScrollView!, didTap button: UIButton!) {
        self.output.didTapTryAgainButton()
    }
    
}


// MARK: - EventTableViewCellDelegate

extension LiveEventsViewController: EventTableViewCellDelegate {
    
    func eventTableViewCellDidSelectRow(_ eventTableViewCell: EventTableViewCell) {
        guard let indexPath = self.tableView.indexPath(for: eventTableViewCell),
            eventTableViewCell.isAvailable else { return }
        
        self.tableView(self.tableView, didSelectRowAt: indexPath)
    }
    
    func eventTableViewCellDidTapFavoritesButton(_ eventTableViewCell: EventTableViewCell) { }
    
}
