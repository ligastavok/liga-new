//
//  LiveEventsInteractorInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LiveEventsInteractorInput: class {
    
    /// Получить события из интернета
    func obtainEventsFromNetwork()
    /// Отмена получения событий из интернета
    func cancelObtainingEventsFromNetwork()
    
    /// Подписаться на изменения событий
    func subscribeToEventsUpdates(withIds ids: [Int])
    /// Отмена подписки на изменения событий
    func cancelSubscribingToEventsUpdates()
    
    /// Добавить подписку на изменения события
    func addSubscriptionToEventUpdates(withId id: Int)
    /// Удалить подписку на изменения события
    func removeSubscriptionToEventUpdates(withId id: Int)
    
    /// Подписаться на сообщения о новых событиях
    func subscribeToNewEvents()
    /// Отмена подписки на сообщения о новых событиях
    func cancelSubscribingToNewEvents()
    
    /// Подписаться на сообщения об удалении событий
    func subscribeToRemoveEvents()
    /// Подписаться на сообщения об удалении событий
    func cancelSubscribingToRemoveEvents()
    
}
