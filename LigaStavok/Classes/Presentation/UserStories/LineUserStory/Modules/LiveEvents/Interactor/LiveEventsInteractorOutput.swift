//
//  LiveEventsInteractorOutput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol LiveEventsInteractorOutput: class {
    
    /// Получены события из интернета
    func didObtainEventsFromNetwork(_ events: [EventObject])
    /// При получении событий из интернета произошла ошибка
    func didFailToObtainEventsFromNetwork(with error: Error)
    
    /// Получены изменения событий
    func didObtainEventsUpdates(_ updates: [DiffMapModel])
    
    /// Получено новое событие
    func didObtainNewEvent(_ event: EventObject)
    
    /// Получено удаленное событие
    func didObtainRemovedEvent(_ removedEvent: RemovedEvent)
    
}
