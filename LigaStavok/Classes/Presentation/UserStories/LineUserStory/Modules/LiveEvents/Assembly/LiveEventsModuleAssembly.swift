//
//  LiveEventsModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LiveEventsModuleAssembly {

    /// Собрать модуль
    static func assembleModule(mode: LiveEventsMode) -> UIViewController {
        let storyboard = UIStoryboard(storyboard: .liveEvents)
        let view: LiveEventsViewController = storyboard.instantiateViewController()
        
        let resolver = ApplicationAssembly.assembler.resolver
        guard let eventsService = resolver.resolve(EventsService.self) else { preconditionFailure() }
        guard let broadcastService = resolver.resolve(BroadcastService.self) else { preconditionFailure() }
        guard let observerService = resolver.resolve(ObserverService.self) else { preconditionFailure() }

        let presenter = LiveEventsPresenter(mode: mode)
        let interactor = LiveEventsInteractor(eventsService: eventsService,
                                              broadcastService: broadcastService,
                                              observerService: observerService)
        let router = LiveEventsRouter()

        view.output = presenter

        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router

        interactor.output = presenter

        router.viewController = view

        return view
    }

}
