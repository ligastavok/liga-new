//
//  LiveEventsRouterInput.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol LiveEventsRouterInput: class {
    
    /// Создание модуля "Фильтр по виду спорта"
    func createSportsFilterModule(with output: SportsFilterModuleOutput, activeVideoFilter: Bool) -> UIViewController
    
    /// Отобразить экран "Роспись события"
    func showEvent(_ event: EventObject)
    
}
