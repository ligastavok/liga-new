//
//  LiveEventsRouter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LiveEventsRouter: Router {

    // MARK: - Публичные свойства

    weak var viewController: UIViewController?


    // MARK: - Инициализация

    init(viewController: UIViewController? = nil) {
        self.viewController = viewController
    }

}


// MARK: - LiveEventsRouterInput

extension LiveEventsRouter: LiveEventsRouterInput {
    
    func createSportsFilterModule(with output: SportsFilterModuleOutput, activeVideoFilter: Bool) -> UIViewController {
        return SportsFilterModuleAssembly.assembleModule(with: output, activeVideoFilter: activeVideoFilter)
    }
    
    func showEvent(_ event: EventObject) {
        let eventViewController = EventModuleAssembly.assembleModule(event: event)
        show(eventViewController)
    }
    
}
