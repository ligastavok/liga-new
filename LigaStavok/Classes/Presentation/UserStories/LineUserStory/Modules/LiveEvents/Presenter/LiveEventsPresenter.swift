//
//  LiveEventsPresenter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

final class LiveEventsPresenter {
    
    // MARK: - Типы данных
    
    private struct LocalData {
        
        /// Режим работы
        var mode: LiveEventsMode = .all
        
        /// Статус
        var status: ModuleStatus = .unknown
        
        /// Фильтр по виду спорта
        var sportsFilter: SportsFilter = .all
        
        /// События
        var events: SynchronizedValue<[EventObject]> = .init([])
        
    }
    
    private struct LocalViewData {
        
        /// События
        var eventsViewModels: SynchronizedValue<[EventViewModel]> = .init([])
        /// Отфильтрованные события
        var filteredEventsViewModels: SynchronizedValue<[EventViewModel]> = .init([])
        
    }
    
    
    // MARK: - Публичные свойства

    /// View
    weak var view: LiveEventsViewInput?
    /// Interactor
    var interactor: LiveEventsInteractorInput!
    /// Router
    var router: LiveEventsRouterInput!
    /// Модуль "Фильтр по виду спорта"
    weak var sportsFilterModule: SportsFilterModuleInput?
    
    
    // MARK: - Приватные свойства
    
    /// Очередь для патча событий
    private lazy var patchEventsOperationQueue: OperationQueue = {
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        operationQueue.qualityOfService = .utility
        
        return operationQueue
    }()
    
    /// Локальные данные
    private var data = LocalData()
    /// Данные представления
    private var viewData = LocalViewData()
    
    
    // MARK: - Инициалиазация
    
    init(mode: LiveEventsMode) {
        self.data.mode = mode
        self.data.sportsFilter = mode.defaultSportsFilter
    }
    
    
    // MARK: - Деинициализация
    
    deinit {
        self.patchEventsOperationQueue.cancelAllOperations()
    }

}


// MARK: - Приватные методы

private extension LiveEventsPresenter {
    
    /// Получить данные
    func loadData() {
        self.data.status = .loading(type: .network)
        self.view?.setStatus(self.data.status)
        
        self.interactor.obtainEventsFromNetwork()
    }
    
    /// Подписаться на обновления
    func subscribeToUpdates(for events: [EventObject]) {
        let ids = events.map { $0.id }
        self.interactor.subscribeToEventsUpdates(withIds: ids)
        
        self.interactor.subscribeToNewEvents()
        self.interactor.subscribeToRemoveEvents()
    }
    
    /// Подготовка событий
    func createViewModels(from events: [EventObject]) -> [EventViewModel] {
        let viewModels = events.map { EventViewModel(eventObject: $0) }
        return viewModels
    }
    
    /// Отобразить данные
    func showData() {
        let eventsViewModels = self.viewData.filteredEventsViewModels.get()
        self.view?.showData(events: eventsViewModels)
    }
    
    /// Обработка ошибки
    func processError(_ error: Error) {
        self.data.status = .error(value: error)
        self.view?.setStatus(self.data.status)
    }
    
    /// Отсортировать события
    func sortEvents(_ events: [EventObject]) -> [EventObject] {
        return events//.sorted(by: { $0.sort < $1.sort })
    }
    
    /// Отсортировать модели событий
    func sortEventsViewModels(_ viewModels: [EventViewModel]) -> [EventViewModel] {
        return viewModels.sorted(by: { $0.sort < $1.sort })
    }
    
    /// Фильтрация событий
    func filterEventsViewModels(_ viewModels: [EventViewModel]) -> [EventViewModel] {
        switch self.data.sportsFilter {
        case .all:
            return viewModels
        case .video:
            return viewModels.filter { $0.isVideoIconVisible }
        case .game(let gameId):
            return viewModels.filter { $0.gameId == gameId }
        }
    }
    
    /// Проверить событие на соответствие фильтру
    func checkEventViewModel(_ viewModel: EventViewModel) -> Bool {
        switch self.data.sportsFilter {
        case .all:
            return true
        case .video:
            return viewModel.isVideoIconVisible
        case .game(let gameId):
            return viewModel.gameId == gameId
        }
    }
    
    /// Проверить событие на соответствие фильтру
    func checkEventViewModel(_ eventId: Int) -> Bool {
        switch self.data.sportsFilter {
        case .all:
            return true
        case .video:
            var result = false
            self.viewData.filteredEventsViewModels.syncSet { viewModels in
                guard let index = viewModels.index(where: {$0.id == eventId }) else { return }
                result = viewModels[index].isVideoIconVisible
            }
            
            return result
        case .game(let gameId):
            var result = false
            self.viewData.filteredEventsViewModels.syncSet { viewModels in
                guard let index = viewModels.index(where: {$0.id == eventId }) else { return }
                result = viewModels[index].gameId == gameId
            }
            
            return result
        }
    }
    
}


// MARK: - LiveEventsViewOutput

extension LiveEventsPresenter: LiveEventsViewOutput {

    func didTriggerViewReadyEvent() {
        let activeVideoFilter = self.data.mode.defaultSportsFilter == .video
        
        let sportsFilterViewController = self.router.createSportsFilterModule(with: self, activeVideoFilter: activeVideoFilter)
        self.view?.showSportsFilterViewController(sportsFilterViewController)
        
        self.view?.setupInitialState(title: self.data.mode.navigationBarTitle)
        
        loadData()
    }
    
    func didTapTryAgainButton() {
        loadData()
    }
    
    func didSelectEvent(_ viewModel: EventViewModel) {
        let events = self.data.events.get()
        guard let index = events.index(where: { $0.id == viewModel.id }) else { return }
        
        let event = events[index]
        self.router.showEvent(event)
    }
    
}


// MARK: - LiveEventsInteractorOutput

extension LiveEventsPresenter: LiveEventsInteractorOutput {
    
    func didObtainEventsFromNetwork(_ events: [EventObject]) {
        subscribeToUpdates(for: events)
        
        let events = sortEvents(events)
        self.data.events.syncSet { $0 = events }
        
        self.data.status = .loaded
        self.view?.setStatus(self.data.status)
        
        let gameIds = Array(Set(events.map { $0.gameId }))
        self.sportsFilterModule?.setSports(withIds: gameIds)
        
        let eventsViewModels = createViewModels(from: events)
        self.viewData.eventsViewModels.syncSet { $0 = eventsViewModels }
        
        let filteredEventsViewModels = filterEventsViewModels(eventsViewModels)
        self.viewData.filteredEventsViewModels.syncSet { $0 = filteredEventsViewModels }
        
        showData()
    }
    
    func didFailToObtainEventsFromNetwork(with error: Error) {
        processError(error)
    }
    
    func didObtainEventsUpdates(_ updates: [DiffMapModel]) {
        let patchEventsViewModelsOperation = PatchEventsViewModelsOperation(updates: updates,
                                                                            viewModels: self.viewData.eventsViewModels)
        
        let patchFilteredEventsViewModelsOperation = PatchEventsViewModelsOperation(updates: updates,
                                                                            viewModels: self.viewData.filteredEventsViewModels)
        patchFilteredEventsViewModelsOperation.completionBlock = { [weak self, weak patchFilteredEventsViewModelsOperation] in
            guard let patchFilteredEventsViewModelsOperation = patchFilteredEventsViewModelsOperation,
                !patchFilteredEventsViewModelsOperation.isCancelled,
                patchFilteredEventsViewModelsOperation.isViewModelsChanged else { return }
            
            DispatchQueue.main.async {
                self?.showData()
            }
        }
        
        let operations = [patchEventsViewModelsOperation, patchFilteredEventsViewModelsOperation]
        self.patchEventsOperationQueue.addOperations(operations, waitUntilFinished: false)
    }
    
    func didObtainNewEvent(_ event: EventObject) {
        guard let namespace = event.namespace, case .live = namespace else { return }
        
        self.interactor.addSubscriptionToEventUpdates(withId: event.id)
        
        self.data.events.syncSet { events in
            events.append(event)
            events = self.sortEvents(events)
        }
        
        let viewModel = EventViewModel(eventObject: event)
        self.viewData.eventsViewModels.syncSet { viewModels in
            if let index = viewModels.index(where: { $0.id == viewModel.id }) {
                viewModels[index] = viewModel
            } else {
                viewModels.append(viewModel)
            }
            
            viewModels = self.sortEventsViewModels(viewModels)
        }
        
        if checkEventViewModel(viewModel) {
            self.viewData.filteredEventsViewModels.syncSet { viewModels in
                if let index = viewModels.index(where: { $0.id == viewModel.id }) {
                    viewModels[index] = viewModel
                } else {
                    viewModels.append(viewModel)
                }
                
                viewModels = self.sortEventsViewModels(viewModels)
            }
        }
        
        DispatchQueue.main.async {
            self.showData()
        }
    }
    
    func didObtainRemovedEvent(_ removedEvent: RemovedEvent) {
        guard let namespace = removedEvent.namespace, case .live = namespace else { return }
        
        self.interactor.removeSubscriptionToEventUpdates(withId: removedEvent.id)
        
        self.data.events.syncSet { events in
            guard let index = events.index(where: { $0.id == removedEvent.id }) else { return }
            events.remove(at: index)
        }
        
        self.viewData.eventsViewModels.syncSet { viewModels in
            guard let index = viewModels.index(where: {$0.id == removedEvent.id }) else { return }
            viewModels[index].blockMode = .removed // viewModels.remove(at: index)
        }
        
        if checkEventViewModel(removedEvent.id) {
            self.viewData.filteredEventsViewModels.syncSet { viewModels in
                guard let index = viewModels.index(where: {$0.id == removedEvent.id }) else { return }
                viewModels[index].blockMode = .removed // viewModels.remove(at: index)
            }
        }
        
        DispatchQueue.main.async {
            self.showData()
        }
    }
    
}


// MARK: - SportsFilterModuleOutput

extension LiveEventsPresenter: SportsFilterModuleOutput {
    
    func didLoadSportsFilterModule(_ sportsFilterModule: SportsFilterModuleInput) {
        self.sportsFilterModule = sportsFilterModule
    }
    
    func didSelectSportsFilter(_ sportsFilter: SportsFilter) {
        self.data.sportsFilter = sportsFilter
        
        let eventsViewModels = self.viewData.eventsViewModels.get()
        
        let filteredEventsViewModels = filterEventsViewModels(eventsViewModels)
        self.viewData.filteredEventsViewModels.syncSet { $0 = filteredEventsViewModels }
        
        showData()
    }
    
}
