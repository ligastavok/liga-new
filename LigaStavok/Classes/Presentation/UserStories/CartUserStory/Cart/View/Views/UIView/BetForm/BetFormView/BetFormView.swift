//
//  BetFormView.swift
//  LigaStavok
//
//  Created by A on 12/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class BetFormView: UIView, NibLoadableSelfOwnerView {

    // MARK: - Outlet
    
    /// Заголовок
    @IBOutlet
    weak var titleLabel: UILabel!
    
    /// Подзаголовок
    @IBOutlet
    weak var subTitleLabel: UILabel!
    
    /// Коэффициент
    @IBOutlet
    weak var coefficientView: CoefficientView!
    
    /// Баланс
    @IBOutlet
    weak var balanceLabel: UILabel!
    
    /// Ставка
    @IBOutlet
    weak var betValueTextField: BetTextField!
    
    /// Кнопка отмены
    @IBOutlet
    weak var cancelButton: Button!
    
    /// Фрибеты
    @IBOutlet
    weak var freeBetView: UIView!
    
    /// Выбор фрибета
    @IBOutlet
    weak var collectionView: UICollectionView!
    
    /// Сохранить
    @IBOutlet
    weak var saveButton: Button!
    
    /// Заключить пари
    @IBOutlet
    weak var confirmButton: Button!
    
    /// Принять изменения
    @IBOutlet
    weak var confirmChangeButton: Button!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
    }

}
