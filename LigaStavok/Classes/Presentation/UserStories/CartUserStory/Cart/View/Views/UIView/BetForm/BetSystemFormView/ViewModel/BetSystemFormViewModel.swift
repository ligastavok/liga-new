//
//  BetSystemFormViewModel.swift
//  LigaStavok
//
//  Created by A on 16/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

struct BetSystemFormViewModel {
    
    // MARK: - Публичные свойства
    
    /// Количество исходов
    let outcomeCount: String
    
    /// Баланс
    let balance: String
    
    /// Ставка
    let betValue: String
    
    /// Количество комбинаций
    let combinationCount: String

    /// Максимальный выигрыш
    let maxWinValue: String
    
    
    // MARK: - Инициализация
    
    init(outcomeCount: String,
         balance: String,
         betValue: String,
         combinationCount: String,
         maxWinValue: String) {
        self.outcomeCount = outcomeCount
        self.balance = balance
        self.betValue = betValue
        self.combinationCount = combinationCount
        self.maxWinValue = maxWinValue
    }
    
}
