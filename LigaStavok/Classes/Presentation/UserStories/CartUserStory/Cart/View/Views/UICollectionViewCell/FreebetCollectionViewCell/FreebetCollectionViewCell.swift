//
//  FreebetCollectionViewCell.swift
//  LigaStavok
//
//  Created by A on 06/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class FreebetCollectionViewCell: UICollectionViewCell, NibLoadableView {

   // MARK: - Outlet
    
    /// Сумма
    @IBOutlet
    weak var valueLabel: UILabel!
    
    /// Дата окончания
    @IBOutlet
    weak var endLabel: UILabel!
    
    /// Дата окончания
    @IBOutlet
    weak var endDateLabel: UILabel!
    
    /// Прогресс бар
    @IBOutlet
    weak var progressView: ColoredProgressView!
    
    /// Внутренний слой
    @IBOutlet
    weak var containerView: UIView!
    
    
}

// MARK: - NSObject

extension FreebetCollectionViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /// Округление углов
        let cornerRadius: CGFloat = 6
        self.containerView.layer.cornerRadius = cornerRadius
    }
    
}


// MARK: - Публичные методы

extension FreebetCollectionViewCell {
    
    /// Настройка
    func configure(with viewModel: FreebetViewModel) {
        if viewModel.isSelected {
            self.containerView.backgroundColor = #colorLiteral(red: 0, green: 0.6039215686, blue: 0.431372549, alpha: 1)
            self.valueLabel.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            self.endLabel.textColor = #colorLiteral(red: 0.7019607843, green: 0.8784313725, blue: 0.8274509804, alpha: 1)
            self.endDateLabel.textColor = #colorLiteral(red: 0.7019607843, green: 0.8784313725, blue: 0.8274509804, alpha: 1)
        } else {
            self.containerView.backgroundColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
            self.valueLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            self.endLabel.textColor = #colorLiteral(red: 0.4823529412, green: 0.4823529412, blue: 0.4823529412, alpha: 1)
            self.endDateLabel.textColor = #colorLiteral(red: 0.4823529412, green: 0.4823529412, blue: 0.4823529412, alpha: 1)
        }
        
        self.valueLabel.text = "\(viewModel.value)₽"
        self.endDateLabel.text = viewModel.endDate
        
        self.progressView.isSelected = viewModel.isSelected
        self.progressView.progress = viewModel.progressValue
        
    }
}
