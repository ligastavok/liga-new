//
//  CoefficientChangeAlertView.swift
//  LigaStavok
//
//  Created by A on 12/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class CoefficientChangeAlertView: UIView, NibLoadableSelfOwnerView {

    // MARK: - Outlet
    
    @IBOutlet
    weak var titleLabel: UILabel!
    
    @IBOutlet
    weak var messageLabel: UILabel!
    
    
    // MARK: - Приватные свойства
    
    /// Маска
    private weak var maskLayer: CAShapeLayer!
    

    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
        // Настройка слоя
        self.shapeLayer?.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        // Маска
        let maskLayer = CAShapeLayer()
        self.layer.mask = maskLayer
        self.maskLayer = maskLayer
    }
    
}


// MARK: - Приватные свойства

private extension CoefficientChangeAlertView {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - NSObject

extension CoefficientChangeAlertView {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        runtimeSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        runtimeSetup()
    }
    
}


// MARK: - UIView

extension CoefficientChangeAlertView {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Настройка границы
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 6)
        self.shapeLayer?.path = path.cgPath
        
        self.maskLayer.path = path.cgPath
    }
    
}


// MARK: - Приватные методы

private extension CoefficientChangeAlertView {
    
    /// Настройка при выполнении
    func runtimeSetup() {
        
    }
    
}
