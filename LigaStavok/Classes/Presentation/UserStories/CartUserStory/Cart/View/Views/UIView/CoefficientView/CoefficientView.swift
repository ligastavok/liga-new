//
//  CoefficientView.swift
//  LigaStavok
//
//  Created by A on 12/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class CoefficientView: UIView, NibLoadableSelfOwnerView {

    // MARK: - Outlet
    
    /// Коэффициент
    @IBOutlet
    weak var coefficientLabel: UILabel!
    
    
    // MARK: - Публичные свойства
    
    /// Коэффициент
    var coefficient: String? {
        didSet {
            self.coefficientLabel.text = coefficient
        }
    }
    

    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
        // Настройка слоя
        self.shapeLayer?.fillColor = #colorLiteral(red: 0.9490196078, green: 0.9490196078, blue: 0.9490196078, alpha: 1)
    }
    
}


// MARK: - Приватные свойства

private extension CoefficientView {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - NSObject

extension CoefficientView {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        runtimeSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        runtimeSetup()
    }
    
}


// MARK: - UIView

extension CoefficientView {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Настройка границы
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 6)
        self.shapeLayer?.path = path.cgPath
    }
    
}


// MARK: - Приватные методы

private extension CoefficientView {
    
    /// Настройка при выполнении
    func runtimeSetup() {
        self.isHidden = true
    }
    
}
