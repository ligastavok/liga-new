//
//  BetTextField.swift
//  LigaStavok
//
//  Created by A on 05/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class BetTextField: UITextField { }

// MARK: - UIView

extension BetTextField {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Настройка скругления углов
        self.layer.cornerRadius = 6
        self.layer.masksToBounds = true
        
        let font: UIFont = .systemFont(ofSize: 15)
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        label.text = "₽"
        label.textColor = #colorLiteral(red: 0.7960784314, green: 0.7960784314, blue: 0.7960784314, alpha: 1)
        label.font = font
        
        
        self.rightView = label
        self.rightViewMode = .always
        
    }
    
}
