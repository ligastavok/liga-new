//
//  CartOutcomeTableViewCell.swift
//  LigaStavok
//
//  Created by A on 02/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class CartOutcomeTableViewCell: UITableViewCell, NibLoadableView {

    // MARK: - Outlet
    
    /// кнопка выделения
    @IBOutlet weak var checkbox: UIButton!
    /// Иконка вида спорта
    @IBOutlet
    private weak var iconImageView: UIImageView!
    /// Счет первой команды
    @IBOutlet
    private weak var firstScoreLabel: UILabel!
    /// Счет второй команды
    @IBOutlet
    private weak var secondScoreLabel: UILabel!
    /// Название первой команды
    @IBOutlet
    private weak var firstNameLabel: UILabel!
    /// Победитель
    @IBOutlet
    weak var winnerLabel: UILabel!
    /// Название второй команды
    @IBOutlet
    private weak var secondNameLabel: UILabel!
    /// Кэф
    @IBOutlet
    private weak var coefficientLabel: UILabel!

    /// Комментарий
    @IBOutlet
    private weak var commentLabel: UILabel!
    /// индикатор live
    @IBOutlet
    private weak var liveIndicatorView: UIView!
    /// время
    @IBOutlet
    private weak var timeLabel: UILabel!
    
    /// Внутренний слой ячейки
    @IBOutlet
    private weak var containerView: UIView!
    
    /// слой под счетом
    @IBOutlet
    private weak var scoreBackgroundView: UIView!
    /// Слой кэфа

    @IBOutlet
    weak var coefficientView: UIView!
    
    /// ширина
    @IBOutlet weak var iconsContainerViewWidthLayoutConstraint: NSLayoutConstraint!
    
    /// Блокировка исхода
    @IBOutlet weak var blockView: CartTableViewCellContentBlockView!
    
    
    // MARK: - Приватные свойства
    
    /// Модель данных
    private var viewModel: CartOutcomeViewModel!
    
}

// MARK: - NSObject

extension CartOutcomeTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /// Округление углов
        let cornerRadius: CGFloat = 6
        
        self.scoreBackgroundView.layer.cornerRadius = cornerRadius
        self.coefficientView.layer.cornerRadius = cornerRadius
    }
    
}


// MARK: - Публичные методы

extension CartOutcomeTableViewCell {
    /// Настройка
    func configure(with viewModel: CartOutcomeViewModel) {
        self.viewModel = viewModel
        
        self.iconsContainerViewWidthLayoutConstraint.constant = viewModel.isCheckboxVisible ? 62 : 28
        
        self.checkbox.isHidden = !viewModel.isCheckboxVisible
        self.iconImageView.image = viewModel.iconName.image
        
        self.firstScoreLabel.isHidden = !viewModel.isVisibleScore
        self.secondScoreLabel.isHidden = !viewModel.isVisibleScore
        self.firstScoreLabel.isHidden = !viewModel.isVisibleScore
        self.secondNameLabel.isHidden = !viewModel.isVisibleScore
        
        self.winnerLabel.isHidden = viewModel.isVisibleScore
        
        self.firstScoreLabel.text = viewModel.firstScore
        self.secondScoreLabel.text = viewModel.secondScore
        self.firstNameLabel.text = viewModel.firstName
        self.secondNameLabel.text = viewModel.secondName
        self.winnerLabel.text = viewModel.winnerName
        self.coefficientLabel.text = String(viewModel.coefficient)
        self.commentLabel.text = viewModel.title
        self.timeLabel.text = viewModel.timeValue
        
        self.blockView.mode = viewModel.blockMode
    }
    
}


// MARK: - Приватные методы

extension CartOutcomeTableViewCell {
    
    /// Настройка чекбокса
    func setupCheckbox() {
        let imageAsset: ImageAsset = self.viewModel.isSelected ? .iconCheck : .iconUncheck
        
        let icon = UIImage(asset: imageAsset)
        self.checkbox.setImage(icon, for: .normal)
    }
    
}
