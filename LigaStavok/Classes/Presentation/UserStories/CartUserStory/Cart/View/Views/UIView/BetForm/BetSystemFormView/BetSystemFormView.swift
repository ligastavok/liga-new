//
//  BetFormView.swift
//  LigaStavok
//
//  Created by A on 12/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class BetSystemFormView: UIView, NibLoadableSelfOwnerView {

    // MARK: - Outlet
    
    /// Количество исходов
    @IBOutlet
    weak var outcomeCountLabel: UILabel!
    
    /// Баланс
    @IBOutlet
    weak var balanceLabel: UILabel!
    
    /// Ставка
    @IBOutlet
    weak var betValueTextField: BetTextField!
    
    /// Кнопка отмены
    @IBOutlet
    weak var cancelButton: Button!
    
    /// Выбор разрядности
    @IBOutlet
    weak var collectionView: UICollectionView!
    
    /// Количество комбинаций
    @IBOutlet
    weak var combinationCountViewLabel: UILabel!
    
    /// Максимальный выигрыш
    @IBOutlet
    weak var maxWinValueLabel: UILabel!
    
    /// Сохранить
    @IBOutlet
    weak var saveButton: Button!
    
    /// Заключить пари
    @IBOutlet
    weak var confirmButton: Button!
    
    /// Принять изменения
    @IBOutlet
    weak var confirmChangeButton: Button!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
    }

}
