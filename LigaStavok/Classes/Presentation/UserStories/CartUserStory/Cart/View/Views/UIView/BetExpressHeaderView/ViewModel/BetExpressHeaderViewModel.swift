//
//  BetExpressHeaderViewModel.swift
//  LigaStavok
//
//  Created by A on 16/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

struct BetExpressHeaderViewModel {
    
    // MARK: - Публичные свойства
    
    /// Сумма
    let value: String
    
    /// Возможный выигрыш
    let winValue: String
    
    
    // MARK: - Инициализация
    
    init(value: String,
         winValue: String) {
        self.value = value
        self.winValue = value
    }
    
}
