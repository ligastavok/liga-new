//
//  CartTabBarItem.swift
//  LigaStavok
//
//  Created by A on 10/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
class CartTabBarItem: UIControl, NibLoadableSelfOwnerView {

    // MARK: - UIControl
    
    
     // MARK: - Outlet
    
    /// Изображение
    @IBOutlet
    weak var imageView: UIImageView!
    
    /// Подпись
    @IBOutlet
    weak var titleLabel: UILabel!
    
    // MARK: - Публичные свойства
    
    
    // MARK: - Приватные свойства
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
    }

}


// MARK: - Приватные методы

private extension CartTabBarItem {
    
    /// Настройка
    func configure() {
        
    }
    
}
