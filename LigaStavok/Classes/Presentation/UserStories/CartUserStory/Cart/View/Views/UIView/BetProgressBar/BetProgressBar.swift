//
//  BetProgressBar.swift
//  LigaStavok
//
//  Created by A on 05/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class BetProgressBar: UIView {
    
    // MARK: - Публичные свойства
    
    var segments: [BetProgressSectionType] = [] {
        didSet {
            setNeedsLayout()
        }
    }

    
    // MARK: - Приватные свойства
    
    private let segmentSpace: CGFloat = 2
    
    private var segmentWidth: CGFloat {
        return (self.bounds.width - ((CGFloat(numberOfSegments) - 1) * segmentSpace)) / CGFloat(numberOfSegments)
    }
    
    private var numberOfSegments: Int {
        return segments.count
    }
    
    private var sortedSegments: [BetProgressSectionType] {
        return segments.sorted {
            $0.rawValue > $1.rawValue
        }
    }
    
    private var segmentHeight: CGFloat = 7
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
    }
    
}

// MARK: - Приватные свойства

private extension BetProgressBar {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension BetProgressBar {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
       configure()
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 0, height: 7)
    }
    
}

// MARK: - Приватные методы

private extension BetProgressBar {
    
    /// Настройка
    func configure() {
        for index in 0 ..< numberOfSegments {
            let layer = CAShapeLayer()
            layer.fillColor = sortedSegments[index].color
            
            let x = (segmentWidth + segmentSpace) * CGFloat(index)
            
            let path = UIBezierPath(roundedRect: CGRect(x: x, y: 0, width: segmentWidth, height: 7), cornerRadius: 0)
            layer.path = path.cgPath
            
            self.shapeLayer?.addSublayer(layer)
        }
        
        let layer = CAShapeLayer()
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 3)
        layer.path = path.cgPath
        self.shapeLayer?.mask = layer
    }
    
}
