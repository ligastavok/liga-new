//
//  ColoredProgressView.swift
//  LigaStavok
//
//  Created by A on 10/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class ColoredProgressView: UIProgressView {
    
    // MARK: - Публичные свойства
    
    /// Выделение в контейнере, содержащем прогресс
    var isSelected: Bool = false {
        didSet {
            configure()
        }
    }
   
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 3)
        self.shapeLayer?.path = path.cgPath
    }
    
}

// MARK: - Приватные свойства

private extension ColoredProgressView {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension ColoredProgressView {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        configure()
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 0, height: 7)
    }
    
}

// MARK: - Приватные методы

private extension ColoredProgressView {
    
    /// Настройка
    func configure() {
        if isSelected {
            self.progressTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.trackTintColor = #colorLiteral(red: 0.2588235294, green: 0.6745098039, blue: 0.5529411765, alpha: 1)
        } else {
            self.progressTintColor = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
            
            if self.progress < 0.5 {
                self.progressTintColor = #colorLiteral(red: 0.4745098039, green: 0.9058823529, blue: 0.4588235294, alpha: 1)
            } else if self.progress < 0.7 {
                self.progressTintColor = #colorLiteral(red: 0.9843137255, green: 0.8039215686, blue: 0.4588235294, alpha: 1)
            } else {
                self.progressTintColor = #colorLiteral(red: 0.9843137255, green: 0.6549019608, blue: 0.6549019608, alpha: 1)
            }
        }
    }
    
}
