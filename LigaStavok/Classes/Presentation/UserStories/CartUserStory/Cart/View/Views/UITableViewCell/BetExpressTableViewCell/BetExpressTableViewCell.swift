//
//  BetExpressTableViewCell.swift
//  LigaStavok
//
//  Created by A on 05/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class BetExpressTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Заголовок экспресса
    @IBOutlet
    weak var titleLabel: UILabel!
    /// Коэффициент
    @IBOutlet
    weak var coefficientLabel: UILabel!
    
    /// Верхний слой
    @IBOutlet
    weak var topView: UIView!
    /// Нижний слой
    @IBOutlet
    weak var bottomView: UIView!
    /// Слой коэффициента
    @IBOutlet
    weak var coefficientView: UIView!
    /// Прогресс бар
    @IBOutlet
    weak var betProgressBar: BetProgressBar!
    
    
    // MARK: - Публичные свойства
    
    
    // MARK: - Приватные свойства
    
    /// Модель данных
    private var viewModel: BetExpressViewModel!
}


// MARK: - NSObject

extension BetExpressTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /// Округление углов
        let cornerRadius: CGFloat = 6
        self.topView.layer.cornerRadius = cornerRadius
        self.bottomView.layer.cornerRadius = cornerRadius
        self.coefficientView.layer.cornerRadius = cornerRadius
    }
    
}


// MARK: - Публичные методы

extension BetExpressTableViewCell {
    
    /// Настройка
    func configure(with viewModel: BetExpressViewModel) {
        self.viewModel = viewModel
        
        self.titleLabel.text = "Экспресс \(viewModel.outcomeCount)"
        self.coefficientLabel.text = String(viewModel.coefficient)
        
        self.betProgressBar.segments = [.grey, .green, .red, .green, .yellow, .red, .grey, .green]
    }
}


// MARK: - Приватные методы

private extension BetExpressTableViewCell {
    
}
