//
//  BetExpressHeaderView.swift
//  LigaStavok
//
//  Created by A on 13/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class BetSystemHeaderView: UIView, NibLoadableSelfOwnerView {
    
    // MARK: - Outlet
    
    /// Разрядность
    @IBOutlet
    weak var sizeLabel: UILabel!
    
    /// Сумма
    @IBOutlet
    weak var valueLabel: UILabel!
    
    /// Максимальный выигрыш
    @IBOutlet
    weak var maxWinValue: UILabel!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
    }
}
