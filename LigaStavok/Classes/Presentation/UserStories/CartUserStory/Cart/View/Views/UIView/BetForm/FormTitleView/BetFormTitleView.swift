//
//  BetFormTitleView.swift
//  LigaStavok
//
//  Created by A on 16/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class BetFormTitleView: UIView, NibLoadableSelfOwnerView {

    // MARK: - Outlet
    
    /// Заголовок
    @IBOutlet
    weak var titleLabel: UILabel!
    
    /// Исходы
    @IBOutlet
    weak var subtitleLabel: UILabel!
    
    /// Коэффициент
    @IBOutlet
    weak var coefficientView: CoefficientView!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
    }

}


// MARK: - Публичные методы

extension BetFormTitleView {
    
    /// Настройка
    func configure(with viewModel: BetFormTitleViewModel) {
        self.titleLabel.text = viewModel.title
        self.subtitleLabel.text = viewModel.subtitle
    }
    
}
