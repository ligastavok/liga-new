//
//  BetSystemHeaderViewModel.swift
//  LigaStavok
//
//  Created by A on 16/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

struct BetSystemHeaderViewModel {
    
    // MARK: - Публичные свойства
    
    /// Разрядность
    let size: String
    
    /// Сумма
    let value: String
    
    ///Максимальный выигрыш
    let maxWinValue: String
    
    
    // MARK: - Инициализация
    
    init(size: String,
         value: String,
         maxWinValue: String) {
        self.size = size
        self.value = value
        self.maxWinValue = maxWinValue
    }
    
}
