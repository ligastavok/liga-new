//
//  FreebetTableViewCell.swift
//  LigaStavok
//
//  Created by A on 06/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class FreebetTableViewCell: UITableViewCell, NibLoadableView {

    // MARK: - Outlet
    
    /// Дата начала
    @IBOutlet
    weak var startDateLabel: UILabel!
    /// Дата окончания
    @IBOutlet
    weak var endDateLabel: UILabel!
    /// Сумма
    @IBOutlet
    weak var valueLabel: UILabel!
    /// Прогресс бар
    @IBOutlet
    weak var progressBar: UIProgressView!
    
    /// Внутренний слой ячейки
    @IBOutlet weak var containerView: UIView!
    
    // MARK: - Публичные свойства
    
    
    // MARK: - Приватные свойства
    
    /// Модель данных
    private var viewModel: FreebetViewModel!
    
}


// MARK: - NSObject

extension FreebetTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /// Округление углов
        let cornerRadius: CGFloat = 6
        self.containerView.layer.cornerRadius = cornerRadius
    }
    
}


// MARK: - Публичные методы

extension FreebetTableViewCell {
    
    /// Настройка
    func configure(with viewModel: FreebetViewModel) {
        self.viewModel = viewModel
        
        self.startDateLabel.text = viewModel.startDate
        self.endDateLabel.text = viewModel.endDate
        self.valueLabel.text = String(viewModel.value)
        self.progressBar.progress = viewModel.progressValue
        
        if viewModel.progressValue < 0.5 {
            self.progressBar.progressTintColor = #colorLiteral(red: 0.4745098039, green: 0.9058823529, blue: 0.4588235294, alpha: 1)
        } else if viewModel.progressValue < 0.7 {
            self.progressBar.progressTintColor = #colorLiteral(red: 0.9843137255, green: 0.8039215686, blue: 0.4588235294, alpha: 1)
        } else {
            self.progressBar.progressTintColor = #colorLiteral(red: 0.9843137255, green: 0.6549019608, blue: 0.6549019608, alpha: 1)
        }
        
    }
    
}


// MARK: - Приватные методы

private extension FreebetTableViewCell {
    
}
