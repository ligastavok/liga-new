//
//  BetFormTitleViewModel.swift
//  LigaStavok
//
//  Created by A on 16/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

struct BetFormTitleViewModel {
    
    // MARK: - Публичные свойства
    
    /// Заголовок
    let title: String
    
    /// Исходы
    let subtitle: String
    
    /// Коэффициент
    let coefficient: String
    
    /// Видимость коэффициента
    let isCoefficientVisible: Bool
    
    
    // MARK: - Инициализация
    
    init(title: String,
         subtitle: String,
         coefficient: String,
         isCoefficientVisible: Bool
        ) {
        self.title = title
        self.subtitle = subtitle
        self.coefficient = coefficient
        self.isCoefficientVisible = isCoefficientVisible
    }
    
}
