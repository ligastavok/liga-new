//
//  CartOutcomeTableViewCell.swift
//  LigaStavok
//
//  Created by A on 02/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class CartMultiOutcomeTableViewCell: UITableViewCell, NibLoadableView {

    // MARK: - Outlet
    
    /// кнопка выделения
    @IBOutlet
    weak var checkbox: UIButton!
    /// Иконка вида спорта
    @IBOutlet
    weak var iconImageView: UIImageView!
    /// Счет первой команды
    @IBOutlet
    weak var firstScoreLabel: UILabel!
    /// Счет второй команды
    @IBOutlet
    weak var secondScoreLabel: UILabel!
    /// Название первой команды
    @IBOutlet
    weak var firstNameLabel: UILabel!
    /// Название второй команды
    @IBOutlet
    weak var secondNameLabel: UILabel!
    /// Победитель
    @IBOutlet
    weak var winnerLabel: UILabel!
    /// Кэф
    @IBOutlet
    weak var coefficientLabel: UILabel!
    /// Комментарий
    @IBOutlet
    weak var commentLabel: UILabel!
    /// индикатор live
    @IBOutlet
    weak var liveIndicatorView: UIView!
    /// время
    @IBOutlet
    weak var timeLabel: UILabel!
    
    /// Внутренний слой ячейки
    @IBOutlet
    weak var containerView: UIView!
    /// Верхний слой
    @IBOutlet
    weak var topView: UIView!
    /// слой под счетом
    @IBOutlet
    weak var scoreBackgroundView: UIView!
    /// Слой кэфа
    @IBOutlet
    weak var coefficientView: UIView!
    /// Нижний слой исхода
    @IBOutlet
    weak var bottomView: UIView!
    
    /// Поле ввода ставки
    @IBOutlet
    weak var betValueTextField: UITextField!
    
    /// Фрибет
    @IBOutlet
    weak var freeBetButton: UIButton!
    /// Для всех
    @IBOutlet
    weak var allBetButton: UIButton!
    /// Отмена фрибета
    @IBOutlet weak var cancelFreebetButton: UIButton!
    /// Возможный выигрыш
    @IBOutlet weak var winValueLabel: UILabel!
    
    /// ширина поля для иконок
    @IBOutlet
    weak var iconsContainerViewWidthLayoutConstraint: NSLayoutConstraint!
    /// Ширина поля кнопок ввода ставки
    @IBOutlet
    weak var betButtonViewWidthLayoutConstraint: NSLayoutConstraint!
    
    
    // MARK: - Приватные свойства
    
    /// Модель данных
    private var viewModel: CartOutcomeViewModel!
    
}

// MARK: - NSObject

extension CartMultiOutcomeTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /// Округление углов
        let cornerRadius: CGFloat = 6
        self.topView.layer.cornerRadius = cornerRadius
        self.scoreBackgroundView.layer.cornerRadius = cornerRadius
        self.coefficientView.layer.cornerRadius = cornerRadius
        
        self.betValueTextField.layer.cornerRadius = cornerRadius
        self.betValueTextField.layer.borderColor = #colorLiteral(red: 0.7960784314, green: 0.7960784314, blue: 0.7960784314, alpha: 1)
        
        self.freeBetButton.layer.cornerRadius = cornerRadius
        self.allBetButton.layer.cornerRadius = cornerRadius
        self.cancelFreebetButton.layer.cornerRadius = cornerRadius
        
        /// Тени
        self.topView.dropShadow()
    }
    
}


// MARK: - Публичные методы

extension CartMultiOutcomeTableViewCell {
    /// Настройка
    func configure(with viewModel: CartOutcomeViewModel) {
        self.viewModel = viewModel
        
        self.iconsContainerViewWidthLayoutConstraint.constant = viewModel.isCheckboxVisible ? 62 : 28
        
        self.checkbox.isHidden = !viewModel.isCheckboxVisible
        
        self.firstScoreLabel.isHidden = !viewModel.isVisibleScore
        self.secondScoreLabel.isHidden = !viewModel.isVisibleScore
        self.firstScoreLabel.isHidden = !viewModel.isVisibleScore
        self.secondNameLabel.isHidden = !viewModel.isVisibleScore
        
        self.winnerLabel.isHidden = viewModel.isVisibleScore
        
        self.firstScoreLabel.text = viewModel.firstScore
        self.secondScoreLabel.text = viewModel.secondScore
        self.firstNameLabel.text = viewModel.firstName
        self.secondNameLabel.text = viewModel.secondName
        self.winnerLabel.text = viewModel.winnerName
        self.coefficientLabel.text = String(viewModel.coefficient)
        self.commentLabel.text = viewModel.title
        self.timeLabel.text = viewModel.timeValue
        
        self.betValueTextField.text = String(viewModel.betValue)
        
        self.betButtonViewWidthLayoutConstraint.constant = viewModel.isVisibleCancelButton ? 65 : 127
        
        self.freeBetButton.isHidden = viewModel.isVisibleCancelButton
        self.allBetButton.isHidden = viewModel.isVisibleCancelButton
        self.cancelFreebetButton.isHidden = !viewModel.isVisibleCancelButton
        
        self.winValueLabel.text = "Возможный выигрыш: \(viewModel.winValue)"
    }
    
}

// MARK: - Приватные методы

extension CartMultiOutcomeTableViewCell {
    
    /// Настройка чекбокса
    func setupCheckbox() {
        let imageAsset: ImageAsset = self.viewModel.isSelected ? .iconCheck : .iconUncheck
        
        let icon = UIImage(asset: imageAsset)
        self.checkbox.setImage(icon, for: .normal)
    }
    
}
