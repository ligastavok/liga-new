//
//  CartTabBarButton.swift
//  LigaStavok
//
//  Created by A on 10/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class CartTabBar: UIView, NibLoadableSelfOwnerView {

    // MARK: - Outlet
    
    /// Кнопка назад
    @IBOutlet
    weak var backButtonView: CartBackView!
    
    /// Одинар
    @IBOutlet
    weak var ordinarButton: CartTabBarItem!
    
    /// Экспресс
    @IBOutlet
    weak var expressButton: CartTabBarItem!
    
    /// Система
    @IBOutlet
    weak var systemButton: CartTabBarItem!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
    }

}
