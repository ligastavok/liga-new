//
//  BetCompleteView.swift
//  LigaStavok
//
//  Created by A on 12/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class BetCompleteView: UIView, NibLoadableSelfOwnerView {

    // MARK: - Outlet
    
    /// Сумма пари
    @IBOutlet
    weak var betValueLabel: UILabel!
    
    /// Старый баланс
    @IBOutlet
    weak var oldBalanceLabel: UILabel!
    
    /// Новый баланс
    @IBOutlet
    weak var newBalanceLabel: UILabel!
    
    /// Коэффициент
    @IBOutlet
    weak var coefficientLabel: UILabel!
    
    /// Возможный выигрыш
    @IBOutlet
    weak var winValueLabel: UILabel!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
    }
    
}
