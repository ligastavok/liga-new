//
//  BetExpressTableViewCell.swift
//  LigaStavok
//
//  Created by A on 05/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class BetSystemTableViewCell: UITableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Заголовок экспресса
    @IBOutlet
    weak var titleLabel: UILabel!
    /// Верхний слой
    @IBOutlet
    weak var topView: UIView!
    /// Нижний слой
    @IBOutlet
    weak var bottomView: UIView!
    /// Прогресс бар
    @IBOutlet
    weak var betProgressBar: BetProgressBar!
    
    
    // MARK: - Публичные свойства
    
    
    // MARK: - Приватные свойства
    
    /// Модель данных
    private var viewModel: BetSystemViewModel!
}


// MARK: - NSObject

extension BetSystemTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        /// Округление углов
        let cornerRadius: CGFloat = 6
        self.topView.layer.cornerRadius = cornerRadius
        self.bottomView.layer.cornerRadius = cornerRadius
    }
    
}


// MARK: - Публичные методы

extension BetSystemTableViewCell {
    
    /// Настройка
    func configure(with viewModel: BetSystemViewModel) {
        self.viewModel = viewModel
        
        self.titleLabel.text = "Система \(viewModel.outcomeCount)"
        
        self.betProgressBar.segments = [.grey, .green, .red, .green, .yellow, .red, .grey, .green]
    }
}


// MARK: - Приватные методы

private extension BetSystemTableViewCell {
    
}
