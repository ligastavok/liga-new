//
//  BetCompleteViewModel.swift
//  LigaStavok
//
//  Created by A on 16/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

struct BetCompleteViewModel {
    
    // MARK: - Публичные свойства
    
    /// Сумма пари
    let betValue: String
    
    /// Старый баланс
    let oldBalance: String
    
    /// Новый баланс
    let newBalance: String
    
    /// Коэффициент
    let coefficient: String
    
    /// Возможный выигрыш
    let winValue: String
    
    
    // MARK: - Инициализация
    
    init(betValue: String,
         oldBalance: String,
         newBalance: String,
         coefficient: String,
         winValue: String) {
        self.betValue = betValue
        self.oldBalance = oldBalance
        self.newBalance = newBalance
        self.coefficient = coefficient
        self.winValue = winValue
    }
    
}
