//
//  CartTableHeaderView.swift
//  LigaStavok
//
//  Created by A on 11/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class CartTableHeaderView: UIView, NibLoadableSelfOwnerView {
    
    // MARK: - Outlet
    
    ///Сумма
    @IBOutlet
    weak var valueLabel: UILabel!
    
    /// Кнопка выделения
    @IBOutlet weak var selectButton: UIButton!
    

    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
    }
}
