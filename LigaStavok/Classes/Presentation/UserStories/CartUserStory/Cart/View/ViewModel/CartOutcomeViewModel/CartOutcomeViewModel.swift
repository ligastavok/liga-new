//
//  CartOutcomeViewModel.swift
//  LigaStavok
//
//  Created by A on 02/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

struct CartOutcomeViewModel {
    
    // MARK: - Публичные свойства
    
    /// Видимость чекбокса
    let isCheckboxVisible: Bool
    /// Исход выбран
    let isSelected: Bool
    /// Иконка
    private(set) var iconName: ImageAsset
    /// Видимость счета
    let isVisibleScore: Bool
    /// Название первой команды
    let firstName: String
    /// Название второй команды
    let secondName: String
    /// Победитель
    let winnerName: String
    /// Счет первой команды
    var firstScore: String
    /// Счет второй команды
    var secondScore: String
    /// Доступность для отображения счетов
    let isScoresHidden: Bool
    /// Коэффициент
    let coefficient: Double
    /// Название
    let title: String
    /// Индикатор live-состояния
    let isLiveIndicatorVisible: Bool
    /// Время
    let timeValue: String
    /// Сумма ставки
    let betValue: Double
    /// Кнопка отмены фрибета
    let isVisibleCancelButton: Bool
    /// Возможный выигрыш
    let winValue: Double
    
    /// Вид блокировки события
    var blockMode: BlockMode = .unblocked
    
    // MARK: - Инициализация
    
    init(isCheckboxVisible: Bool,
         isSelected: Bool,
         iconName: ImageAsset,
         isVisibleScore: Bool,
         firstName: String,
         secondName: String,
         winnerName: String,
         firstScore: String,
         secondScore: String,
         isScoresHidden: Bool,
         coefficient: Double,
         title: String,
         isLiveIndicatorVisible: Bool,
         timeValue: String,
         betValue: Double,
         isVisibleCancelButton: Bool,
         winValue: Double) {
        self.isCheckboxVisible = isCheckboxVisible
        self.isSelected = isSelected
        self.iconName = iconName
        self.isVisibleScore = isVisibleScore
        self.firstName = firstName
        self.secondName = secondName
        self.winnerName = winnerName
        self.firstScore = firstScore
        self.secondScore = secondScore
        self.isScoresHidden = isScoresHidden
        self.coefficient = coefficient
        self.title = title
        self.isLiveIndicatorVisible = isLiveIndicatorVisible
        self.timeValue = timeValue
        self.betValue = betValue
        self.isVisibleCancelButton = isVisibleCancelButton
        self.winValue = winValue
    }
    
}
