//
//  SectionType.swift
//  LigaStavok
//
//  Created by A on 05/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

enum BetProgressSectionType: Int {
    case grey
    case red
    case yellow
    case green
    
    var color: CGColor {
        switch self {
        case .grey:
            return UIColor(red: 237 / 255, green: 237 / 255, blue: 237 / 255, alpha: 1).cgColor
        case .red:
            return UIColor(red: 213 / 255, green: 28 / 255, blue: 28 / 255, alpha: 1).cgColor
        case .yellow:
            return UIColor(red: 1, green: 161 / 255, blue: 0, alpha: 1).cgColor
        case .green:
            return UIColor(red: 0, green: 154 / 255, blue: 110 / 255, alpha: 1).cgColor
        }
    }
}
