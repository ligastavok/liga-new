//
//  CartOutcomeViewModel+BlockMode.swift
//  LigaStavok
//
//  Created by A on 09/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension CartOutcomeViewModel {
    
    /// Вид блокировки события
    enum BlockMode: Hashable {
        
        /// Событие разблокировано
        case unblocked
        /// Событие завершено
        case completed
        /// Событие удалено из линии
        case removed
        
    }
    
}
