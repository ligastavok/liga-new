//
//  BetExpressViewModel.swift
//  LigaStavok
//
//  Created by A on 05/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

struct BetSystemViewModel {
    
    // MARK: - Публичные свойства
    
    /// Количество исходов в экспрессе
    let outcomeCount: Int

    
    // MARK: - Инициализация
    
    init(outcomeCount: Int) {
        self.outcomeCount = outcomeCount
       
    }
    
}
