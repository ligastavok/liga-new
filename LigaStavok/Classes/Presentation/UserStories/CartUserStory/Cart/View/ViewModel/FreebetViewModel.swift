//
//  FreebetViewModel.swift
//  LigaStavok
//
//  Created by A on 06/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

struct FreebetViewModel {
    
    // MARK: - Публичные свойства
    
    /// Дата начала
    let startDate: String
    /// Дата окончания
    let endDate: String
    /// Сумма
    let value: Double
    /// Прогресс
    let progressValue: Float
    /// Выделение
    let isSelected: Bool = false
    
    // MARK: - Инициализация
    
    init(startDate: String,
         endDate: String,
         value: Double,
         progressValue: Float) {
        self.startDate = startDate
        self.endDate = endDate
        self.value = value
        self.progressValue = progressValue
    }
    
}
