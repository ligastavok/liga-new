//
//  BetExpressViewModel.swift
//  LigaStavok
//
//  Created by A on 05/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

struct BetExpressViewModel {
    
    // MARK: - Публичные свойства
    
    /// Количество исходов в экспрессе
    let outcomeCount: Int
    /// Коэффициент
    let coefficient: Double
    
    
    // MARK: - Инициализация
    init(outcomeCount: Int, coefficient: Double) {
        self.outcomeCount = outcomeCount
        self.coefficient = coefficient
    }
    
}
