//
//  CartViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class CartViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet weak var tableView: UITableView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
            
            // Регистрация ячеек
            newValue.register(CartOutcomeTableViewCell.self)
            newValue.register(CartMultiOutcomeTableViewCell.self)
            newValue.register(BetExpressTableViewCell.self)
            newValue.register(BetSystemTableViewCell.self)
            newValue.register(FreebetTableViewCell.self)
        }
    }
    
    
    // MARK: - Приватные свойства
    
    /// Временнное значение
    private let outcome = CartOutcomeViewModel(isCheckboxVisible: true,
                                               isSelected: true,
                                               iconName: .iconCheck,
                                               isVisibleScore: true,
                                               firstName: "Ким Чонг Юи/ Лим Хью Чан",
                                               secondName: "Джонг Юнг Хун/Со Джи Хун",
                                               winnerName: "Ким Чонг Юи/ Лим Хью Чан",
                                               firstScore: "2",
                                               secondScore: "1",
                                               isScoresHidden: false,
                                               coefficient: 44.24,
                                               title: "1 - сет. Кто выиграет 4-й розыгрыш 11-го гейма - 2",
                                               isLiveIndicatorVisible: true,
                                               timeValue: "Live 12:43", betValue: 0, isVisibleCancelButton: false, winValue: 15000)
    
    private let betExpress = BetExpressViewModel(outcomeCount: 10, coefficient: 44.22)
    private let betSystem = BetSystemViewModel(outcomeCount: 10)
    private let freebet = FreebetViewModel(startDate: "23 октября 2017", endDate: "23 октября 2017", value: 10000, progressValue: 0.7)
    
}


// MARK: - UIViewController

extension CartViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Подготовка экрана
        setup()
    }
    
}


// MARK: - Приватные методы

private extension CartViewController {
    
    /// Подготовка экрана
    func setup() {
        //self.navigationItem.title = Localization.Cart.navigationBarTitle
        
        // Кнопка "Корзина"
        let trashButtonIcon = UIImage(asset: .navigationBarTrash)
        let trashButton = UIBarButtonItem(image: trashButtonIcon, style: .plain, target: self, action: #selector(tap))
        self.navigationItem.leftBarButtonItem = trashButton
        
        // Кнопка "Настройки"
        let settingsButtonIcon = UIImage(asset: .navigationBarSetting)
        let settingsButton = UIBarButtonItem(image: settingsButtonIcon, style: .plain, target: self, action: #selector(tap))
        self.navigationItem.rightBarButtonItem = settingsButton
        
        // прячем TabBar
        //self.tabBarController?.tabBar.isHidden = true

        // SegmentedControl
        let control = CartSegmentedView(frame: CGRect(x: 0, y: 0, width: 500, height: 31))
        
        self.navigationItem.titleView = control
    }
    
    /// Нажатие
    @objc
    func tap() { }
    
}


// MARK: - UITableViewDataSource

extension CartViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(for: indexPath) as CartOutcomeTableViewCell
            cell.configure(with: outcome)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(for: indexPath) as CartMultiOutcomeTableViewCell
            cell.configure(with: outcome)
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(for: indexPath) as BetExpressTableViewCell
            cell.configure(with: betExpress)
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(for: indexPath) as BetSystemTableViewCell
            cell.configure(with: betSystem)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(for: indexPath) as FreebetTableViewCell
            cell.configure(with: freebet)
            return cell
        }
    }
    
}


// MARK: - UITableViewDelegate

extension CartViewController: UITableViewDelegate { }
