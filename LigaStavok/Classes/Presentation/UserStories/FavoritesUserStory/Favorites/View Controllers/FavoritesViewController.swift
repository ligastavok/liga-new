//
//  FavoritesViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class FavoritesViewController: UIViewController { }


// MARK: - UIViewController

extension FavoritesViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Подготовка экрана
        setup()
    }
    
}


// MARK: - Приватные методы

private extension FavoritesViewController {
    
    // Подготовка экрана
    func setup() {
        self.navigationItem.title = Localization.Favorites.navigationBarTitle
    }
    
}
