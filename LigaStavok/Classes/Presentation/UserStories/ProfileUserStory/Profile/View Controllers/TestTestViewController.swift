//
//  TestTestViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 31.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TestTestViewController: UIViewController {
    
    // MARK: - Outlet
    
    @IBOutlet
    weak var handleView: UIView!
    
    @IBOutlet
    private weak var tableView: UITableView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
        }
    }
    
    @IBOutlet
    private weak var bottomContentLayoutConstraint: NSLayoutConstraint! {
        willSet {
            newValue.constant = UIWindow.keyWindowSafeAreaInsets.bottom
        }
    }
    
    
    // MARK: - Приватные свойства
    
    /// Количество строк в таблице
    private let numberOfRows = (Int(arc4random_uniform(3)) + 1) * 5
    
}


// MARK: - UITableViewDataSource

extension TestTestViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
        cell.textLabel?.text = "Row #\(indexPath.row)"
        
        return cell
    }
    
}


// MARK: - UITableViewDelegate

extension TestTestViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}


// MARK: - PullUpPresentable

extension TestTestViewController: PullUpPresentable {
    
    var contentHeight: CGFloat? {
        self.view.layoutIfNeeded()
        return self.tableView.contentSize.height + self.handleView.frame.size.height
    }
    
    var bottomOffset: CGFloat? {
        return self.bottomContentLayoutConstraint.constant
    }
    
    var contentScrollView: UIScrollView? {
        return self.tableView
    }
    
    var translationMultiplier: CGFloat? {
        return nil
    }
    
    var dismissOffset: PullUpDismissOffset {
        return .percentages(value: 0.2)
    }
    
    var dismissMethods: PullUpDismissMethod {
        return [.tap, .swipe]
    }
    
}


// MARK: - PullUpHandleProvider

extension TestTestViewController: PullUpHandleProvider { }


// MARK: - PullUpAppearanceProvider

extension TestTestViewController: PullUpAppearanceProvider {
    
    var dimmingColor: UIColor {
        return .black
    }
    
    var dimmingAlpha: CGFloat {
        return 0.3
    }
    
    var minimalPosition: PullUpMinimalPosition {
        return .percentages(value: 0.25)
    }
    
    var contentCornerRadius: CGFloat? {
        return 12
    }
    
}
