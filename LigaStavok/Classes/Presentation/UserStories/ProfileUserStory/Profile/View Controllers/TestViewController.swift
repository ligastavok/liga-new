//
//  TestViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 19.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TestViewController: UIViewController {
    
    // MARK: - Публичные свойства
    
    /// Информация об идентификаторе
    var indicatorInfo: TabLayoutIndicatorInfo!
    
    /// Менеджер отображения
    lazy var presentationManager: PullUpPresentationManager? = .init()
    
    
    // MARK: - Инициализация
    
    init(indicatorInfo: TabLayoutIndicatorInfo) {
        self.indicatorInfo = indicatorInfo
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


// MARK: - UIViewController

extension TestViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton(type: .system)
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        button.setTitle(self.indicatorInfo.title, for: .normal)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(button)
        
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            button.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        ])
    }
    
}


// MARK: - Приватные методы

private extension TestViewController {
    
    @objc
    func didTapButton() {
        let storyboard = UIStoryboard(storyboard: .profile)
        let viewController: TestTestViewController = storyboard.instantiateViewController()
        
        pullUp(viewController, animated: true)
    }
    
}


// MARK: - TabLayoutIndicatorInfoProvider

extension TestViewController: TabLayoutIndicatorInfoProvider {
    
    func indicatorInfo(for tabLayoutViewController: TabLayoutViewController) -> TabLayoutIndicatorInfo {
        return self.indicatorInfo
    }
    
}


// MARK: - PullUpPresenting

extension TestViewController: PullUpPresenting {
    
    var viewController: UIViewController? {
        return self
    }
    
}
