//
//  ProfileViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class ProfileViewController: TabLayoutDefaultButtonBarViewController {
    
    // MARK: - Инициализация
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        fatalError("init(nibName:bundle:) has not been implemented")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Настройка TabLayout
        self.settings.buttonBarBackgroundColor = #colorLiteral(red: 0, green: 0.4156862745, blue: 0.2901960784, alpha: 1)
        self.settings.buttonBarMinimumInteritemSpacing = 0
        self.settings.buttonBarLeftContentInset = 0
        self.settings.buttonBarRightContentInset = 0
        
        self.settings.selectedIndicatorBackgroundColor = .white
        self.settings.selectedIndicatorHeight = 2
        self.settings.selectedIndicatorVerticalAlignment = .bottom
        
        self.settings.buttonBarItemBackgroundColor = #colorLiteral(red: 0, green: 0.4156862745, blue: 0.2901960784, alpha: 1)
        self.settings.buttonBarItemLeftRightMargin = 8
        self.settings.buttonBarItemFont = .systemFont(ofSize: 15)
        
        self.settings.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.buttonBarHeight = 44
        
        self.configureCell = { cell, isActive in
            if isActive {
                cell.titleColor = .white
            } else {
                cell.titleColor = #colorLiteral(red: 0.4980392157, green: 0.7058823529, blue: 0.6431372549, alpha: 1)
            }
        }
    }
    
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Подготовка экрана
        setup()
    }
    
    override func viewControllers(for tabLayoutViewController: TabLayoutViewController) -> [UIViewController] {
        let first = TestViewController(indicatorInfo: "H2H")
        first.view.backgroundColor = .orange
        
        let second = TestViewController(indicatorInfo: "LMT")
        second.view.backgroundColor = .green
        
        let third = TestViewController(indicatorInfo: "Timeline")
        third.view.backgroundColor = .cyan
        
        let first1 = TestViewController(indicatorInfo: "H2H")
        first1.view.backgroundColor = .orange
        
        let second1 = TestViewController(indicatorInfo: "LMT")
        second1.view.backgroundColor = .green
        
        let third1 = TestViewController(indicatorInfo: "Timeline")
        third1.view.backgroundColor = .cyan
        
        let first2 = TestViewController(indicatorInfo: "H2H")
        first2.view.backgroundColor = .orange
        
        let second2 = TestViewController(indicatorInfo: "LMT")
        second2.view.backgroundColor = .green
        
        let third2 = TestViewController(indicatorInfo: "Timeline")
        third2.view.backgroundColor = .cyan
        
        let first3 = TestViewController(indicatorInfo: "H2H")
        first3.view.backgroundColor = .orange
        
        let second3 = TestViewController(indicatorInfo: "LMT")
        second3.view.backgroundColor = .green
        
        let third3 = TestViewController(indicatorInfo: "Timeline")
        third3.view.backgroundColor = .cyan
        
        return [first, second, third, first1, second1, third1, first2, second2, third2, first3, second3, third3]
    }
    
}


// MARK: - Приватные методы

private extension ProfileViewController {
    
    // Подготовка экрана
    func setup() {
        self.navigationItem.title = Localization.Profile.navigationBarTitle
    }
    
}
