//
//  TabBarModuleAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TabBarModuleAssembly {
    
    /// Собрать модуль
    static func assembleModule() -> UIViewController {
        let tabBarController = TabBarController()
        return tabBarController
    }

}
