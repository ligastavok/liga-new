//
//  TabBar.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TabBar: UITabBar { }


// MARK: - NSObject

extension TabBar {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        TabBar.configure(self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        TabBar.configure(self)
    }
    
}


// MARK: - Публичные методы

extension TabBar {
    
    /// Настроить TabBar
    class func configure(_ tabBar: UITabBar) {
        
        // Задний фон панели вкладок
        tabBar.barTintColor = UIConstants.tabBarBackgroundColor
        tabBar.backgroundImage = UIImage.generate(with: UIConstants.tabBarBackgroundColor)?.removeAlphaChannel()
        
        // Разделитель
        tabBar.shadowImage = UIImage.generate(with: UIConstants.tabBarShadowColor)
        
        // Цвет текста и иконок
        tabBar.tintColor = UIConstants.tabBarItemSelectedTintColor
        
        let tabBarItemTitleTextAttributes: [NSAttributedStringKey: Any] = [
            .foregroundColor: UIConstants.tabBarItemTintColor
        ]
        let selectedTabBarItemTitleTextAttributes: [NSAttributedStringKey: Any] = [
            .foregroundColor: UIConstants.tabBarItemSelectedTintColor
        ]
        tabBar.items?.forEach { item in
            if let image = item.image {
                item.image = image.mask(with: UIConstants.tabBarItemTintColor).withRenderingMode(.alwaysOriginal)
            }
            
            item.setTitleTextAttributes(tabBarItemTitleTextAttributes, for: .normal)
            item.setTitleTextAttributes(selectedTabBarItemTitleTextAttributes, for: .selected)
        }
    }
    
}
