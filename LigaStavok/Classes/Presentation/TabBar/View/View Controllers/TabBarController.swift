//
//  TabBarController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TabBarController: UITabBarController { }


// MARK: - UIViewController

extension TabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Подготовка экрана
        setup()
    }
    
}


// MARK: - Приватные методы

private extension TabBarController {
    
    /// Подготовка экрана
    func setup() {
        
        // Назначение вкладок
        build()
        
        // Настройка панели вкладок
        configureTabBar()
    }
    
    /// Назначение вкладок
    func build() {
        var tabs: [(viewController: UIViewController, tabItem: UITabBarItem)] = []
        
        // Вкладка "Линия"
        let lineViewController = LineModuleAssembly.assembleModule()
        //let lineViewController = EventModuleAssembly.assembleModule()

        let lineItemTitle = Localization.TabBar.lineBarTitle
        let lineItemImage = UIImage(asset: .tabBarCalendar)
        let lineItem = UITabBarItem(title: lineItemTitle, image: lineItemImage, tag: 0)
        
        tabs.append((viewController: lineViewController, tabItem: lineItem))
        
        // Вкладка "Избранное"
        let favoritesStoryboard = UIStoryboard(storyboard: .favorites)
        let favoritesViewController: FavoritesViewController = favoritesStoryboard.instantiateViewController()
        let favoritesNavigationController = NavigationController(rootViewController: favoritesViewController)
        
        let favoritesItemTitle = Localization.TabBar.favoritesBarTitle
        let favoritesItemImage = UIImage(asset: .tabBarFavorites)
        let favoritesItem = UITabBarItem(title: favoritesItemTitle, image: favoritesItemImage, tag: 1)
        
        tabs.append((viewController: favoritesNavigationController, tabItem: favoritesItem))
        
        // Вкладка "Профиль"
        let profileStoryboard = UIStoryboard(storyboard: .profile)
        let profileViewController: ProfileViewController = profileStoryboard.instantiateViewController()
        let profileNavigationController = NavigationController(rootViewController: profileViewController)
        
        let profileItemTitle = Localization.TabBar.profileBarTitle
        let profileItemImage = UIImage(asset: .tabBarUser)
        let profileItem = UITabBarItem(title: profileItemTitle, image: profileItemImage, tag: 2)
        
        tabs.append((viewController: profileNavigationController, tabItem: profileItem))
        
        // Вкладка "Купонник"
        let cartStoryboard = UIStoryboard(storyboard: .cart)
        let cartViewController: CartViewController = cartStoryboard.instantiateViewController()
        let cartNavigationController = NavigationController(rootViewController: cartViewController)
        
        let cartItemTitle = Localization.TabBar.cartBarTitle
        let cartItemImage = UIImage(asset: .tabBarShoppingCart)
        let cartItem = UITabBarItem(title: cartItemTitle, image: cartItemImage, tag: 3)
        
        tabs.append((viewController: cartNavigationController, tabItem: cartItem))
        
        // Настройка TabBar
        let viewControllers: [UIViewController] = tabs.map { tab in
            let viewController = tab.viewController
            viewController.tabBarItem = tab.tabItem
            
            return viewController
        }
        
        self.viewControllers = viewControllers
    }
    
    /// Настройка панели вкладок
    func configureTabBar() {
        
        // Удаляем границы
        self.tabBar.frame.size.width = self.view.frame.width + 4
        self.tabBar.frame.origin.x = -2
        
        // Прочие настройки
        TabBar.configure(self.tabBar)
    }
    
}
