//
//  UIComponentsAssembly.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/10/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Swinject

/// Сорщик UI компонентов
final class UIComponentsAssembly { }


// MARK: - Assembly

extension UIComponentsAssembly: Assembly {
    
    func assemble(container: Container) {
        
        // Status Bar Message View Singlton
        container.register(StatusBarMessageView.self) { _ in
            StatusBarMessageView.loadFromNib()
        }.inObjectScope(.container)

    }
    
}
