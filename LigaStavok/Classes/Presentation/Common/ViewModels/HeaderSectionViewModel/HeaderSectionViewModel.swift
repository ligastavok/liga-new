//
//  HeaderSectionViewModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 19.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct HeaderSectionViewModel {
    
    // MARK: - Публичные свойства
    
    /// Название
    let title: String
    /// Название кнопки
    let buttonTitle: String?
    
}
