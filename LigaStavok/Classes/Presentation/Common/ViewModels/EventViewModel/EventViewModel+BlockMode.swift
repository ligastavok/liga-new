//
//  EventViewModel+BlockMode.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 18.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension EventViewModel {
    
    /// Вид блокировки события
    enum BlockMode: Hashable {
        
        /// Событие разблокировано
        case unblocked
        /// Событие завершено
        case completed
        /// Событие удалено из линии
        case removed
        
    }
    
}
