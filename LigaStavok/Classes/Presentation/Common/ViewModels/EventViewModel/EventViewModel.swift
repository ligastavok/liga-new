//
//  EventViewModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

// FIXME: - Сделать отдельное поле для признака удаленного события

struct EventViewModel: Equatable {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор
    let id: Int
    /// Порядок сортировки
    let sort: Double
    /// Идентификатор вида спорта
    let gameId: Int
    /// Спецпредложение
    let isSpecial: Bool
    /// Вид блокировки события
    var blockMode: BlockMode = .unblocked {
        didSet {
            // FIXME: - Доработать
            
            updateBetEnabled(value: self.firstWinnerBetValue, betEnabled: &self.isFirstWinnerBetEnabled)
            updateBetEnabled(value: self.drawBetValue, betEnabled: &self.isDrawBetEnabled)
            updateBetEnabled(value: self.secondWinnerBetValue, betEnabled: &self.isSecondWinnerBetEnabled)
            
            updateBetsHiddenValue()
            updateBetsPlaceholder()
        }
    }
    /// Признак завершенного события
    var expired: Bool {
        willSet {
            self.blockMode = newValue ? .completed : .unblocked
        }
    }
    
    /// Видимость индикатора "Избранное"
    var isFavoritesIndicatorVisible: Bool
    /// Иконка
    private(set) var iconName: ImageAsset
    /// Задний фон иконки
    private(set) var iconBackgroundColor: UIColor
    
    /// Название
    let title: String
    
    /// Индикатор live-состояния
    let isLiveIndicatorVisible: Bool
    /// Время
    let timeValue: String
    
    /// Видео
    let isVideoIconVisible: Bool
    
    /// Название первой команды
    let firstName: String
    /// Название второй команды
    let secondName: String
    
    /// Счет первой команды
    var firstScore: String
    /// Счет второй команды
    var secondScore: String
    /// Доступность для отображения счетов
    let isScoresHidden: Bool
    
    /// Событие содержит незаблокированные исходы
    var isUnlocked: Bool {
        didSet {
            self.iconName = EventViewModel.selectIconName(isUnlocked: self.isUnlocked, gameId: self.gameId)
            self.iconBackgroundColor = EventViewModel.selectIconBackgroundColor(isUnlocked: self.isUnlocked)
            
            updateBetsHiddenValue()
            updateBetsPlaceholder()
        }
    }
    
    /// Коэффициент на победу первой команды
    private(set) var firstWinnerBetValue: Double
    /// Тип изменения коэффициента на победу первой команды
    private(set) var firstWinnerBetValueChangeType: BetValueChangeType = .none
    /// Доступность пари на победу первой команды
    private(set) var isFirstWinnerBetEnabled: Bool
    /// Выбрано пари на победу первой команды
    var isFirstWinnerBetSelected: Bool
    
    /// Коэффициент на ничью
    private(set) var drawBetValue: Double
    /// Тип изменения коэффициента на ничью
    private(set) var drawBetValueChangeType: BetValueChangeType = .none
    /// Доступность пари на ничью
    private(set) var isDrawBetEnabled: Bool
    /// Выбрано пари на ничью
    var isDrawBetSelected: Bool
    
    /// Коэффициент на победу второй команды
    private(set) var secondWinnerBetValue: Double
    /// Тип изменения коэффициента на победу второй команды
    private(set) var secondWinnerBetValueChangeType: BetValueChangeType = .none
    /// Доступность пари на победу второй команды
    private(set) var isSecondWinnerBetEnabled: Bool
    /// Выбрано пари на победу второй команды
    var isSecondWinnerBetSelected: Bool
    
    /// Доступность для отображения пари
    private(set) var isBetsHidden: Bool
    /// Текст, отображаемые при отсутсвии пари
    private(set) var betsPlaceholder: String?
    
    
    // MARK: Приватные свойства
    
    private static var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "d MMM, HH:mm"
        
        return dateFormatter
    }()
    
    /// Коэффициент на победу первой команды (fulltime)
    private var firstWinnerBetValueMain: Double
    /// Коэффициент на победу первой команды (over all time)
    private var firstWinnerBetValueOverAllTime: Double
    
    /// Коэффициент на ничью (fulltime)
    private var drawBetValueMain: Double
    /// Коэффициент на ничью (over all time)
    private var drawBetValueOverAllTime: Double
    
    /// Коэффициент на победу второй команды (fulltime)
    private var secondWinnerBetValueMain: Double
    /// Коэффициент на победу второй команды (over all time)
    private var secondWinnerBetValueOverAllTime: Double
    
    
    // MARK: - Инициализация
    
    init(id: Int,
         sort: Double,
         gameId: Int,
         isSpecial: Bool,
         isUnlocked: Bool,
         expired: Bool,
         isFavoritesIndicatorVisible: Bool,
         iconName: ImageAsset,
         iconBackgroundColor: UIColor,
         title: String,
         isLiveIndicatorVisible: Bool,
         timeValue: String,
         isVideoIconVisible: Bool,
         firstName: String,
         secondName: String,
         firstScore: String,
         secondScore: String,
         isScoresHidden: Bool,
         firstWinnerBetValue: Double,
         firstWinnerBetValueMain: Double,
         firstWinnerBetValueOverAllTime: Double,
         isFirstWinnerBetEnabled: Bool,
         isFirstWinnerBetSelected: Bool,
         drawBetValue: Double,
         drawBetValueMain: Double,
         drawBetValueOverAllTime: Double,
         isDrawBetEnabled: Bool,
         isDrawBetSelected: Bool,
         secondWinnerBetValue: Double,
         secondWinnerBetValueMain: Double,
         secondWinnerBetValueOverAllTime: Double,
         isSecondWinnerBetEnabled: Bool,
         isSecondWinnerBetSelected: Bool,
         isBetsHidden: Bool,
         betsPlaceholder: String?) {
        
        self.id = id
        self.sort = sort
        self.gameId = gameId
        self.isSpecial = isSpecial
        self.isUnlocked = isUnlocked
        self.expired = expired
        self.isFavoritesIndicatorVisible = isFavoritesIndicatorVisible
        self.iconName = iconName
        self.iconBackgroundColor = iconBackgroundColor
        self.title = title
        self.isLiveIndicatorVisible = isLiveIndicatorVisible
        self.timeValue = timeValue
        self.isVideoIconVisible = isVideoIconVisible
        self.firstName = firstName
        self.secondName = secondName
        self.firstScore = firstScore
        self.secondScore = secondScore
        self.isScoresHidden = isScoresHidden
        self.firstWinnerBetValue = firstWinnerBetValue
        self.firstWinnerBetValueMain = firstWinnerBetValueMain
        self.firstWinnerBetValueOverAllTime = firstWinnerBetValueOverAllTime
        self.isFirstWinnerBetEnabled = isFirstWinnerBetEnabled
        self.isFirstWinnerBetSelected = isFirstWinnerBetSelected
        self.drawBetValue = drawBetValue
        self.drawBetValueMain = drawBetValueMain
        self.drawBetValueOverAllTime = drawBetValueOverAllTime
        self.isDrawBetEnabled = isDrawBetEnabled
        self.isDrawBetSelected = isDrawBetSelected
        self.secondWinnerBetValue = secondWinnerBetValue
        self.secondWinnerBetValueMain = secondWinnerBetValueMain
        self.secondWinnerBetValueOverAllTime = secondWinnerBetValueOverAllTime
        self.isSecondWinnerBetEnabled = isSecondWinnerBetEnabled
        self.isSecondWinnerBetSelected = isSecondWinnerBetSelected
        self.isBetsHidden = isBetsHidden
        self.betsPlaceholder = betsPlaceholder
    }
    
    init(eventObject: EventObject) {
        let id = eventObject.id
        let sort = 0.0//eventObject.sort
        let gameId = eventObject.gameId
        let isSpecial = eventObject.proposedTypes?.contains(.mainOffer) ?? false
        let isUnlocked = eventObject.hasUnlocked
        let expired = eventObject.event.expired
        
        let isFavoritesIndicatorVisible = false
        let iconName = EventViewModel.selectIconName(isUnlocked: isUnlocked, gameId: gameId)
        let iconBackgroundColor = EventViewModel.selectIconBackgroundColor(isUnlocked: isUnlocked)
        
        let title = eventObject.event.topicTitle
        
        let isLiveIndicatorVisible = false
        let timeValue = EventViewModel.dateFormatter.string(from: eventObject.gameTimestamp)
        
        let isVideoIconVisible = !(eventObject.event.videoStream ?? .empty).isEmpty
        
        let firstName = eventObject.event.firstTeamName
        let secondName = eventObject.event.secondTeamName ?? ""
        
        let firstScore = eventObject.scores.current?.firstTeamScore ?? "0"
        let secondScore = eventObject.scores.current?.secondTeamScore ?? "0"
        let isScoresHidden = (eventObject.namespace ?? .prematch) == .prematch || isSpecial
        
        let firstWinnerBetValueMain = eventObject.outcomesWinner?[.main]?.outcomes?[.underscoreFirst]?.value ?? 0
        let firstWinnerBetValueOverAllTime = eventObject.outcomesWinner?[.overAllTime]?.outcomes?[.underscoreFirst]?.value ?? 0
        let firstWinnerBetValue = EventViewModel.selectBetValue(main: firstWinnerBetValueMain,
                                                                overAllTime: firstWinnerBetValueOverAllTime)
        let isFirstWinnerBetEnabled = firstWinnerBetValue != 0
        let isFirstWinnerBetSelected = false
        
        let drawBetValueMain = eventObject.outcomesWinner?[.main]?.outcomes?[.x]?.value ?? 0
        let drawBetValueOverAllTime = eventObject.outcomesWinner?[.overAllTime]?.outcomes?[.x]?.value ?? 0
        let drawBetValue = EventViewModel.selectBetValue(main: drawBetValueMain,
                                                         overAllTime: drawBetValueOverAllTime)
        let isDrawBetEnabled = drawBetValue != 0
        let isDrawBetSelected = false
        
        let secondWinnerBetValueMain = eventObject.outcomesWinner?[.main]?.outcomes?[.underscoreSecond]?.value ?? 0
        let secondWinnerBetValueOverAllTime = eventObject.outcomesWinner?[.overAllTime]?.outcomes?[.underscoreSecond]?.value ?? 0
        let secondWinnerBetValue = EventViewModel.selectBetValue(main: secondWinnerBetValueMain,
                                                                 overAllTime: secondWinnerBetValueOverAllTime)
        let isSecondWinnerBetEnabled = secondWinnerBetValue != 0
        let isSecondWinnerBetSelected = false
        
        let isBetsHidden = EventViewModel.calculateBetsHiddenValue(isUnlocked: isUnlocked,
                                                                   isFirstWinnerBetEnabled: isFirstWinnerBetEnabled,
                                                                   isDrawBetEnabled: isDrawBetEnabled,
                                                                   isSecondWinnerBetEnabled: isSecondWinnerBetEnabled)
        
        
        let betsPlaceholder = EventViewModel.selectBetsPlaceholder(isUnlocked: isUnlocked,
                                                                   isBetsHidden: isBetsHidden,
                                                                   outcomesCount: eventObject.outcomes?.count)
        
        
        self.init(id: id,
                  sort: sort,
                  gameId: gameId,
                  isSpecial: isSpecial,
                  isUnlocked: isUnlocked,
                  expired: expired,
                  isFavoritesIndicatorVisible: isFavoritesIndicatorVisible,
                  iconName: iconName,
                  iconBackgroundColor: iconBackgroundColor,
                  title: title,
                  isLiveIndicatorVisible: isLiveIndicatorVisible,
                  timeValue: timeValue,
                  isVideoIconVisible: isVideoIconVisible,
                  firstName: firstName,
                  secondName: secondName,
                  firstScore: firstScore,
                  secondScore: secondScore,
                  isScoresHidden: isScoresHidden,
                  firstWinnerBetValue: firstWinnerBetValue,
                  firstWinnerBetValueMain: firstWinnerBetValueMain,
                  firstWinnerBetValueOverAllTime: firstWinnerBetValueOverAllTime,
                  isFirstWinnerBetEnabled: isFirstWinnerBetEnabled,
                  isFirstWinnerBetSelected: isFirstWinnerBetSelected,
                  drawBetValue: drawBetValue,
                  drawBetValueMain: drawBetValueMain,
                  drawBetValueOverAllTime: drawBetValueOverAllTime,
                  isDrawBetEnabled: isDrawBetEnabled,
                  isDrawBetSelected: isDrawBetSelected,
                  secondWinnerBetValue: secondWinnerBetValue,
                  secondWinnerBetValueMain: secondWinnerBetValueMain,
                  secondWinnerBetValueOverAllTime: secondWinnerBetValueOverAllTime,
                  isSecondWinnerBetEnabled: isSecondWinnerBetEnabled,
                  isSecondWinnerBetSelected: isSecondWinnerBetSelected,
                  isBetsHidden: isBetsHidden,
                  betsPlaceholder: betsPlaceholder)
    }
    
}


// MARK: - Публичные методы

extension EventViewModel {
    
    /// Повторно вычислить коэффициент на победу первой команды
    mutating func recalculateFirstWinnerBetValue(firstWinnerBetValueMain: Double? = nil,
                                                 firstWinnerBetValueOverAllTime: Double? = nil) {
        
        recalculateBetValue(value: &self.firstWinnerBetValue,
                            changeType: &self.firstWinnerBetValueChangeType,
                            main: &self.firstWinnerBetValueMain,
                            overAllTime: &self.firstWinnerBetValueOverAllTime,
                            newMain: firstWinnerBetValueMain,
                            newOverAllTime: firstWinnerBetValueOverAllTime)
        updateBetEnabled(value: self.firstWinnerBetValue, betEnabled: &self.isFirstWinnerBetEnabled)
        
        updateBetsHiddenValue()
        updateBetsPlaceholder()
    }
    
    /// Повторно вычислить коэффициент на ничью
    mutating func recalculateDrawBetValue(drawBetValueMain: Double? = nil,
                                          drawBetValueOverAllTime: Double? = nil) {
        
        recalculateBetValue(value: &self.drawBetValue,
                            changeType: &self.drawBetValueChangeType,
                            main: &self.drawBetValueMain,
                            overAllTime: &self.drawBetValueOverAllTime,
                            newMain: drawBetValueMain,
                            newOverAllTime: drawBetValueOverAllTime)
        updateBetEnabled(value: self.drawBetValue, betEnabled: &self.isDrawBetEnabled)
        
        updateBetsHiddenValue()
        updateBetsPlaceholder()
    }
    
    /// Повторно вычислить коэффициент на победу второй команды
    mutating func recalculateSecondWinnerBetValue(secondWinnerBetValueMain: Double? = nil,
                                                  secondWinnerBetValueOverAllTime: Double? = nil) {
        
        recalculateBetValue(value: &self.secondWinnerBetValue,
                            changeType: &self.secondWinnerBetValueChangeType,
                            main: &self.secondWinnerBetValueMain,
                            overAllTime: &self.secondWinnerBetValueOverAllTime,
                            newMain: secondWinnerBetValueMain,
                            newOverAllTime: secondWinnerBetValueOverAllTime)
        updateBetEnabled(value: self.secondWinnerBetValue, betEnabled: &self.isSecondWinnerBetEnabled)
        
        updateBetsHiddenValue()
        updateBetsPlaceholder()
    }
    
}


// MARK: - Приватные методы

private extension EventViewModel {
    
    /// Выбрать название изображения
    static func selectIconName(isUnlocked: Bool, gameId: Int) -> ImageAsset {
        return isUnlocked ? SportType(rawValue: gameId)?.imageAsset ?? .sportDefault : .iconLocked
    }
    
    /// Выбрать задний фон изображения
    static func selectIconBackgroundColor(isUnlocked: Bool) -> UIColor {
        return isUnlocked ? #colorLiteral(red: 0, green: 0.6039215686, blue: 0.431372549, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    }
    
    /// Выбор значения коэффициента
    static func selectBetValue(main: Double, overAllTime: Double) -> Double {
        return main != 0 ? main : overAllTime
    }
    
    /// Повторно вычислить коэффициент
    func recalculateBetValue(value: inout Double,
                             changeType: inout BetValueChangeType,
                             main: inout Double,
                             overAllTime: inout Double,
                             newMain: Double?,
                             newOverAllTime: Double?) {
        
        if let newMain = newMain {
            main = newMain
        }
        if let newOverAllTime = newOverAllTime {
            overAllTime = newOverAllTime
        }
        
        let tmpValue = value
        value = EventViewModel.selectBetValue(main: main, overAllTime: overAllTime)
        
        guard tmpValue != value else { return }
        guard tmpValue != 0 else {
            changeType = .none
            return
        }
        
        let lastUpdate = Date.timeIntervalSinceReferenceDate
        changeType = tmpValue > value ? .descending(lastUpdate: lastUpdate) : .ascending(lastUpdate: lastUpdate)
    }
    
    /// Обновить доступность пари
    func updateBetEnabled(value: Double, betEnabled: inout Bool) {
        if self.blockMode == .unblocked {
            betEnabled = value != 0
        } else {
            betEnabled = false
        }
    }
    
    /// Вычислить видимость пари
    static func calculateBetsHiddenValue(isUnlocked: Bool,
                                         isFirstWinnerBetEnabled: Bool,
                                         isDrawBetEnabled: Bool,
                                         isSecondWinnerBetEnabled: Bool) -> Bool {
        return !isUnlocked || !(isFirstWinnerBetEnabled || isDrawBetEnabled || isSecondWinnerBetEnabled)
    }
    
    /// Обновить видимость пари
    mutating func updateBetsHiddenValue() {
        if self.blockMode == .unblocked {
            self.isBetsHidden = EventViewModel.calculateBetsHiddenValue(isUnlocked: self.isUnlocked,
                                                                        isFirstWinnerBetEnabled: self.isFirstWinnerBetEnabled,
                                                                        isDrawBetEnabled: self.isDrawBetEnabled,
                                                                        isSecondWinnerBetEnabled: self.isSecondWinnerBetEnabled)
        } else {
            self.isBetsHidden = true
        }
    }
    
    /// Выбрать текст, отображаемые при отсутсвии пари
    static func selectBetsPlaceholder(isUnlocked: Bool, isBetsHidden: Bool, outcomesCount: Int?) -> String? {
        guard isBetsHidden else { return nil }
        
        if let count = outcomesCount, isUnlocked {
            return "Показать все коэффициенты (\(count))"
        } else {
            return "Прием пари временно приостановлен"
        }
    }
    
    /// Обновить текст, отображаемые при отсутсвии пари
    mutating func updateBetsPlaceholder() {
        self.betsPlaceholder = EventViewModel.selectBetsPlaceholder(isUnlocked: self.isUnlocked,
                                                                    isBetsHidden: self.isBetsHidden,
                                                                    outcomesCount: nil)
    }
    
}


// MARK: - Hashable

extension EventViewModel: Hashable {
    
    var hashValue: Int {
        var hashes = [self.id.hashValue,
                      self.sort.hashValue,
                      self.gameId.hashValue,
                      self.isSpecial.hashValue,
                      self.isUnlocked.hashValue,
                      self.blockMode.hashValue,
                      self.expired.hashValue,
                      self.isFavoritesIndicatorVisible.hashValue,
                      self.iconName.hashValue,
                      self.iconBackgroundColor.hashValue,
                      self.title.hashValue,
                      self.isLiveIndicatorVisible.hashValue,
                      self.timeValue.hashValue,
                      self.isVideoIconVisible.hashValue,
                      self.firstName.hashValue,
                      self.secondName.hashValue,
                      self.firstScore.hashValue,
                      self.secondScore.hashValue,
                      self.isScoresHidden.hashValue,
                      self.firstWinnerBetValue.hashValue,
                      self.firstWinnerBetValueChangeType.hashValue,
                      self.isFirstWinnerBetEnabled.hashValue,
                      self.isFirstWinnerBetSelected.hashValue,
                      self.drawBetValue.hashValue,
                      self.drawBetValueChangeType.hashValue,
                      self.isDrawBetEnabled.hashValue,
                      self.isDrawBetSelected.hashValue,
                      self.secondWinnerBetValue.hashValue,
                      self.secondWinnerBetValueChangeType.hashValue,
                      self.isSecondWinnerBetEnabled.hashValue,
                      self.isSecondWinnerBetSelected.hashValue,
                      self.isBetsHidden.hashValue]
        if let betsPlaceholder = self.betsPlaceholder {
            hashes.append(betsPlaceholder.hashValue)
        }
        
        return HashManager.combineHashes(hashes)
    }
    
}
