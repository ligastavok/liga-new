//
//  NavigationController+UIGestureRecognizerDelegate.swift
//  LigaStavok
//
//  Created by Artem on 26/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension NavigationController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard let gestureRecognizer = gestureRecognizer as? UIPanGestureRecognizer else { return true }
        
        let velocity = gestureRecognizer.velocity(in: gestureRecognizer.view)
        return fabs(velocity.y) > fabs(velocity.x)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return self.scrollingEnabled
    }
    
}
