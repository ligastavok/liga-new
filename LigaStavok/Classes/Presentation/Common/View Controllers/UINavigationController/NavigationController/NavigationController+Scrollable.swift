//
//  NavigationController+Scrollable.swift
//  LigaStavok
//
//  Created by Artem on 26/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Foundation
import UIKit


/// Протокол NavigationController
@objc
protocol NavigationControllerDelegate: NSObjectProtocol {
    
    /// Вызывается при изменении состояния NavigationBar
    @objc
    optional func navigationController(_ controller: NavigationController, didChangeState state: NavigationBarState)
    
    /// Вызывается перед изменением состояния NavigationBar
    @objc
    optional func navigationController(_ controller: NavigationController, willChangeState state: NavigationBarState)
    
}

/// Состояние NavigationBar
@objc
enum NavigationBarState: Int {
    
    ///
    case collapsed
    ///
    case expanded
    ///
    case scrolling
    
}

/// Направление скролла, при котором NavigationBar или его Follower будут скрываться
enum CollapseDirection: Int {
    
    ///
    case scrollUp = -1
    ///
    case scrollDown = 1
    
}


/// Обертка для UIView, которая скрывается вместе с NavigationBar
final class NavigationBarFollower {
    
    // MARK: - Публичные свойства
    
    ///
    weak var view: UIView?
    ///
    var direction: CollapseDirection = .scrollUp
    
    
    // MARK: - Инициализация
    
    init(view: UIView, direction: CollapseDirection = .scrollUp) {
        self.view = view
        self.direction = direction
    }
    
}


extension NavigationController {
    
    /**
     Начало скроллинга
     - parameters:
        - scrollableView: UIView, за скроллингом которой будет наблюдать контроллер
        - delay: задержка скрытия
        - scrollSpeedFactor: коэффициент скорости прокручивания относительно анимации navigation bar
        - collapseDirection: направление скрытия (определяется знаком)
        - followers: массив UIView (NavigationBarFollower), которые будут скрываться вместе с NavigationBar
     */
    func followScrollView(_ scrollableView: UIView,
                          delay: Double = 0,
                          scrollSpeedFactor: Double = 1.0,
                          collapseDirection: CollapseDirection = .scrollDown,
                          followers: [NavigationBarFollower] = []) {
        self.scrollableView = scrollableView
        
        gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(NavigationController.handlePan(_:)))
        gestureRecognizer?.maximumNumberOfTouches = 1
        gestureRecognizer?.delegate = self
        scrollableView.addGestureRecognizer(gestureRecognizer!)
        
        previousOrientation = UIDevice.current.orientation
        NotificationCenter.default.addObserver(self, selector: #selector(NavigationController.willResignActive(_:)),
                                               name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NavigationController.didBecomeActive(_:)),
                                               name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NavigationController.didRotate(_:)),
                                               name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
        
        maxDelay = CGFloat(delay)
        delayDistance = CGFloat(delay)
        scrollingEnabled = true
        
        if let tab = followers.map({ $0.view }).first(where: { $0 is UITabBar }) as? UITabBar {
            self.sourceTabBar = TabBarMock(origin: CGPoint(x: tab.frame.origin.x, y: CGFloat(round(tab.frame.origin.y))),
                                           translucent: tab.isTranslucent)
        }
        
        self.followers = followers
        self.scrollSpeedFactor = CGFloat(scrollSpeedFactor)
        self.collapseDirectionFactor = CGFloat(collapseDirection.rawValue)
    }
    
    /**
     Скрытие NavigationBar
     - parameters:
        - animated: определяет, скрывается ли NavigationBar с анимацией
        - duration: optional, длительность анимации
     */
    func hideNavigationBar(animated: Bool = true, duration: TimeInterval = 0.1) {
        guard self.scrollableView != nil, let visibleViewController = self.visibleViewController else {
            return
        }
        
        guard state == .expanded else {
            updateNavigationBarAlpha()
            return
        }
        
        gestureRecognizer?.isEnabled = false
        
        let animations = {
            self.scrollWithDelta(self.fullNavigationBarHeight, ignoreDelay: true)
            visibleViewController.view.setNeedsLayout()
            
            if self.navigationBar.isTranslucent {
                let currentOffset = self.contentOffset
                self.scrollView()?.contentOffset = CGPoint(x: currentOffset.x, y: currentOffset.y + self.navigationBarHeight)
            }
        }
        
        if animated {
            state = .scrolling
            UIView.animate(withDuration: duration, animations: animations) { _ in
                self.state = .collapsed
                self.gestureRecognizer?.isEnabled = true
            }
        } else {
            animations()
            state = .collapsed
            gestureRecognizer?.isEnabled = true
        }
    }
    /**
     Появление NavigationBar
     - parameters:
        - animated: определяет, появляется ли NavigationBar с анимацией
        - duration: optional, длительность анимации
     */
    public func showNavigationBar(animated: Bool = true, duration: TimeInterval = 0.1) {
        guard self.scrollableView != nil, let visibleViewController = self.visibleViewController else {
            return
        }
        
        guard state == .collapsed else {
            updateNavigationBarAlpha()
            return
        }
        
        gestureRecognizer?.isEnabled = false
        
        let animations = {
            self.lastContentOffset = 0
            self.scrollWithDelta(-self.fullNavigationBarHeight, ignoreDelay: true)
            visibleViewController.view.setNeedsLayout()
            
            if self.navigationBar.isTranslucent {
                let currentOffset = self.contentOffset
                self.scrollView()?.contentOffset = CGPoint(x: currentOffset.x, y: currentOffset.y - self.navigationBarHeight)
            }
        }
        
        if animated {
            state = .scrolling
            UIView.animate(withDuration: duration, animations: animations) { _ in
                self.state = .expanded
                self.gestureRecognizer?.isEnabled = true
            }
        } else {
            animations()
            state = .expanded
            gestureRecognizer?.isEnabled = true
        }
        
    }
    /**
     Прекратить наблюдение за ScrollView
     - parameters:
        - showingNavigationBar: вернуть NavigationBar в изначальное состояние
     */
    public func stopFollowingScrollView(showingNavigationBar: Bool = true) {
        if showingNavigationBar {
            showNavigationBar(animated: true)
        }
        
        if let gesture = gestureRecognizer {
            scrollableView?.removeGestureRecognizer(gesture)
        }
        
        scrollableView = .none
        gestureRecognizer = .none
        navigationBarDelegate = .none
        scrollingEnabled = false
        
        let center = NotificationCenter.default
        center.removeObserver(self, name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        center.removeObserver(self, name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil)
    }
    
    // MARK: - Gesture recognizer
    @objc
    func handlePan(_ gesture: UIPanGestureRecognizer) {
        if let superview = scrollableView?.superview {
            let translation = gesture.translation(in: superview)
            let delta = (lastContentOffset - translation.y) / scrollSpeedFactor
            
            if !checkSearchController(delta) {
                lastContentOffset = translation.y
                return
            }
            
            if gesture.state != .failed {
                lastContentOffset = translation.y
                if shouldScrollWithDelta(delta) {
                    scrollWithDelta(delta)
                }
            }
        }
        if gesture.state == .ended || gesture.state == .cancelled || gesture.state == .failed {
            checkForPartialScroll()
            lastContentOffset = 0
        }
    }
    
    // MARK: - Fullscreen handling
    func windowDidBecomeVisible(_ notification: Notification) {
        self.showNavigationBar()
    }
    
    // MARK: - Rotation handler
    @objc
    func didRotate(_ notification: Notification) {
        let newOrientation = UIDevice.current.orientation
        
        if (previousOrientation == newOrientation) || (previousOrientation.isPortrait && newOrientation.isLandscape)
            || (previousOrientation.isLandscape && newOrientation.isPortrait) {
            showNavigationBar()
        }
        previousOrientation = newOrientation
    }
    /**
     Метод протокола UIContentContainer
     Обрабатывает NavigationBar при повороте экрана
     */
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        showNavigationBar()
    }
    
    // MARK: - Notification handler
    @objc
    func didBecomeActive(_ notification: Notification) {
        if expandOnActive {
            showNavigationBar(animated: false)
        } else {
            if previousState == .collapsed {
                hideNavigationBar(animated: false)
            }
        }
    }
    
    @objc
    func willResignActive(_ notification: Notification) {
        previousState = state
    }
    
    /// Обрабатывает изменения Status Bar
    func willChangeStatusBar() {
        showNavigationBar(animated: true)
    }
    
    // MARK: - Scrolling functions
    private func shouldScrollWithDelta(_ delta: CGFloat) -> Bool {
        let scrollDelta = delta
        
        if scrollDelta < 0 {
            if let scrollableView = scrollableView,
                contentOffset.y + scrollableView.frame.size.height > contentSize.height && scrollableView.frame.size.height < contentSize.height {
                
                return false
            }
        }
        
        return true
    }

    private func scrollWithDelta(_ delta: CGFloat, ignoreDelay: Bool = false) {
        var scrollDelta = delta
        let frame = navigationBar.frame
        
        // UIView скроллится вверх, прячем NavigationBar
        if scrollDelta > 0 {
            // Обновить задержку
            if !ignoreDelay {
                delayDistance -= scrollDelta
                
                // return если задержка не завершена
                if delayDistance > 0 {
                    return
                }
            }
            
            // Не скроллим если content fits
            if !shouldScrollWhenContentFits && state != .collapsed && (scrollableView?.frame.size.height)! >= contentSize.height {
                return
            }
            
            // Вычисление позиции NavigationBar
            if frame.origin.y - scrollDelta < -deltaLimit {
                scrollDelta = frame.origin.y + deltaLimit
            }
            
            // Определение, скрыт ли полностью NavigationBar
            if frame.origin.y <= -deltaLimit {
                state = .collapsed
                delayDistance = maxDelay
            } else {
                state = .scrolling
            }
        }
        
        if scrollDelta < 0 {
            // Обновить задержку
            if !ignoreDelay {
                delayDistance += scrollDelta
                
                // return если задержка не завершена
                if delayDistance > 0 && maxDelay < contentOffset.y {
                    return
                }
            }
            
            // Вычисление позиции NavigationBar
            if frame.origin.y - scrollDelta > statusBarHeight {
                scrollDelta = frame.origin.y - statusBarHeight
            }
            // Определение, развернут ли полностью NavigationBar
            if frame.origin.y >= statusBarHeight {
                state = .expanded
                delayDistance = maxDelay
            } else {
                state = .scrolling
            }
        }
        
        updateSizing(scrollDelta)
        updateNavigationBarAlpha()
        restoreContentOffset(scrollDelta)
        updateFollowers(scrollDelta)
        updateContentInset(scrollDelta)
    }
    
    private func updateContentInset(_ delta: CGFloat) {
        if let contentInset = scrollView()?.contentInset, let scrollInset = scrollView()?.scrollIndicatorInsets {
            scrollView()?.contentInset = UIEdgeInsets(top: contentInset.top - delta, left: contentInset.left,
                                                      bottom: contentInset.bottom, right: contentInset.right)
            scrollView()?.scrollIndicatorInsets = UIEdgeInsets(top: scrollInset.top - delta, left: scrollInset.left,
                                                               bottom: scrollInset.bottom, right: scrollInset.right)
        }
    }
    
    /**
     Обновить состояние всех UIView, что следуют за NavigationBar
     - parameters:
        - delta:
     */
    private func updateFollowers(_ delta: CGFloat) {
        followers.forEach {
            guard let tabBar = $0.view as? UITabBar else {
                $0.view?.transform = $0.view?.transform.translatedBy(x: 0, y: CGFloat($0.direction.rawValue) * delta *
                    (($0.view?.frame.height ?? 0) / navigationBar.frame.height)) ?? .identity
                return
            }
            
            tabBar.isTranslucent = true
            tabBar.frame.origin.y += delta * (tabBar.frame.height / navigationBar.frame.height)
            
            // Вернуть bar в его изначальное состояние
            if let originalTabBar = sourceTabBar, originalTabBar.origin.y == round(tabBar.frame.origin.y) {
                tabBar.isTranslucent = originalTabBar.isTranslucent
            }
        }
    }
    
    /**
     Обновить размеры NavigationBar
     - parameters:
        - delta:
     */
    private func updateSizing(_ delta: CGFloat) {
        guard let topViewController = self.topViewController else { return }
        
        var frame = navigationBar.frame
        
        // Двигать NavigationBar
        frame.origin = CGPoint(x: frame.origin.x, y: frame.origin.y - delta)
        navigationBar.frame = frame
        
        // Ресайз view если NavigationBar не полупрозрачное
        if !navigationBar.isTranslucent {
            let navBarY = navigationBar.frame.origin.y + navigationBar.frame.size.height
            frame = topViewController.view.frame
            frame.origin = CGPoint(x: frame.origin.x, y: navBarY)
            frame.size = CGSize(width: frame.size.width, height: view.frame.size.height - (navBarY) - tabBarOffset)
            topViewController.view.frame = frame
        }
    }
    
    /**
     Восстановить смещение содержимого
     - parameters:
        - delta:
     */
    private func restoreContentOffset(_ delta: CGFloat) {
        if navigationBar.isTranslucent || delta == 0 {
            return
        }
        
        // Держать в смещенном состоянии UIScrollView пока NavigationBar не появится/исчезнет
        if let scrollView = scrollView() {
            scrollView.setContentOffset(CGPoint(x: contentOffset.x, y: contentOffset.y - delta), animated: false)
        }
    }
    
    /**
     Проверка, был ли частичный скролл
     */
    private func checkForPartialScroll() {
        let frame = navigationBar.frame
        var duration = TimeInterval(0)
        var delta = CGFloat(0.0)
        
        // Scroll back down
        let threshold = statusBarHeight - (frame.size.height / 2)
        if navigationBar.frame.origin.y >= threshold {
            delta = frame.origin.y - statusBarHeight
            let distance = delta / (frame.size.height / 2)
            duration = TimeInterval(abs(distance * 0.2))
            state = .expanded
        } else {
            // Scroll up
            delta = frame.origin.y + deltaLimit
            let distance = delta / (frame.size.height / 2)
            duration = TimeInterval(abs(distance * 0.2))
            state = .collapsed
        }
        
        delayDistance = maxDelay
        
        UIView.animate(withDuration: duration, delay: 0, options: UIViewAnimationOptions.beginFromCurrentState, animations: {
            self.updateSizing(delta)
            self.updateFollowers(delta)
            self.updateNavigationBarAlpha()
            self.updateContentInset(delta)
        }, completion: nil)
    }
    
    /**
     Обновить непрозрачность всех элементов в NavigatioBar
     */
    private func updateNavigationBarAlpha() {
        guard let navigationItem = topViewController?.navigationItem else { return }
        
        let frame = navigationBar.frame
        
        // Изменить альфу в каждом subview NavigationBar
        let alpha = (frame.origin.y + deltaLimit) / frame.size.height
        
        // Спрятать все label и title
        navigationItem.titleView?.alpha = alpha
        navigationBar.tintColor = navigationBar.tintColor.withAlphaComponent(alpha)
        navigationItem.leftBarButtonItem?.tintColor = navigationItem.leftBarButtonItem?.tintColor?.withAlphaComponent(alpha)
        navigationItem.rightBarButtonItem?.tintColor = navigationItem.rightBarButtonItem?.tintColor?.withAlphaComponent(alpha)
        navigationItem.leftBarButtonItems?.forEach { $0.tintColor = $0.tintColor?.withAlphaComponent(alpha) }
        navigationItem.rightBarButtonItems?.forEach { $0.tintColor = $0.tintColor?.withAlphaComponent(alpha) }
        if let titleColor = navigationBar.titleTextAttributes?[NSAttributedStringKey.foregroundColor] as? UIColor {
            navigationBar.titleTextAttributes?[NSAttributedStringKey.foregroundColor] = titleColor.withAlphaComponent(alpha)
        } else {
            navigationBar.titleTextAttributes?[NSAttributedStringKey.foregroundColor] = UIColor.black.withAlphaComponent(alpha)
        }
        
        // Спрятать все кнопки и navigation items
        func shouldHideView(_ view: UIView) -> Bool {
            let className = view.classForCoder.description().replacingOccurrences(of: "_", with: "")
            var viewNames = ["UINavigationButton", "UINavigationItemView", "UIImageView", "UISegmentedControl"]
            if #available(iOS 11.0, *) {
                viewNames.append(navigationBar.prefersLargeTitles ? "UINavigationBarLargeTitleView" : "UINavigationBarContentView")
            } else {
                viewNames.append("UINavigationBarContentView")
            }
            return viewNames.contains(className)
        }
        
        func setAlphaOfSubviews(view: UIView, alpha: CGFloat) {
            view.alpha = alpha
            view.subviews.forEach { setAlphaOfSubviews(view: $0, alpha: alpha) }
        }
        
        navigationBar.subviews
            .filter(shouldHideView)
            .forEach { setAlphaOfSubviews(view: $0, alpha: alpha) }
        
        // Спрятать все BarItem слева
        navigationItem.leftBarButtonItem?.customView?.alpha = alpha
        navigationItem.leftBarButtonItems?.forEach { $0.customView?.alpha = alpha }
        
        // Спрятать все BarItem справа
        navigationItem.rightBarButtonItem?.customView?.alpha = alpha
        navigationItem.rightBarButtonItems?.forEach { $0.customView?.alpha = alpha }
    }
    
    /**
     Проверка, присутствует ли элемент поиска
     */
    private func checkSearchController(_ delta: CGFloat) -> Bool {
        if #available(iOS 11.0, *) {
            if let searchController = topViewController?.navigationItem.searchController, delta > 0 {
                if searchController.searchBar.frame.height != 0 {
                    return false
                }
            }
        }
        return true
    }
}
