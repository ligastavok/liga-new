//
//  NavigationController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class NavigationController: UINavigationController {
    
    // MARK: - Приватные свойства
    
    /// Сервис для отселживания информации о доступности интернет-соединения
    private var networkReachabilityService: NetworkReachabilityService!
    
    private(set) weak var statusMessageView: StatusBarMessageView?
    
    /// Состояние (NavigationBarState) navigation bar
    var state: NavigationBarState = .expanded {
        willSet {
            if state != newValue {
                navigationBarDelegate?.navigationController?(self, willChangeState: newValue)
            }
        }
        didSet {
            navigationBar.isUserInteractionEnabled = (state == .expanded)
            if state != oldValue {
                navigationBarDelegate?.navigationController?(self, didChangeState: state)
            }
        }
    }
    
    /// Признак необходимости
    var shouldScrollWhenContentFits = false
    
    ///
    var expandOnActive = true
    
    /// Определялет, скроллится ли NavigationBar
    var scrollingEnabled = true
    
    /// Делегат NavigationController
    weak var navigationBarDelegate: NavigationControllerDelegate?
    
    /// Массив view, которые скрываются вместе с NavigationBar
    var followers: [NavigationBarFollower] = []
    
    /// Структура с метаданными UITabBar
    internal struct TabBarMock {
        var isTranslucent: Bool = false
        var origin: CGPoint = .zero
        
        init(origin: CGPoint, translucent: Bool) {
            self.origin = origin
            self.isTranslucent = translucent
        }
    }
    
    // Gesture recognizer, по которому будет скрываться NavigationBar
    var gestureRecognizer: UIPanGestureRecognizer?
    
    // метаданные TabBar, который будет скрываться вместе с NavigationBar
    var sourceTabBar: TabBarMock?
    
    // Предыдущая ориентация экрана
    var previousOrientation: UIDeviceOrientation = UIDevice.current.orientation
    
    var delayDistance: CGFloat = 0.0
    
    var maxDelay: CGFloat = 0.0
    
    // Прокручиваемая UIView, за которой будет следить контроллер
    var scrollableView: UIView?
    
    // Последнее смещение NavigationBar
    var lastContentOffset: CGFloat = 0.0
    
    // Скорость скрытия
    var scrollSpeedFactor: CGFloat = 1.0
    
    // Направление скрытия (определяется знаком)
    var collapseDirectionFactor: CGFloat = 1.0

    // Предыдушее состояние NavigationBar
    var previousState: NavigationBarState = .expanded
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}


// MARK: - UIViewController

extension NavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Подготовка экрана
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        checkNetworkReachabilityStatus()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        checkNetworkReachabilityStatus()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}


// MARK: - UINavigationController

extension NavigationController {
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        
        // Удаляем текст у кнопки "Назад"
        self.viewControllers.last?.makeBackButtonEmpty()
        
        // Добавляем контроллер в стек
        super.pushViewController(viewController, animated: animated)
    }
    
}


// MARK: - Приватные методы

private extension NavigationController {
    
    /// Подготовка экрана
    func setup() {
        
        // Подключить компоненты
        resolveDependencies()
        
        // Настройка ReachabilityService
        configureReachabilityService()
        
        // Настройка навигационной панели
        configureNavigationBar()
    }
    
    /// Подключить все зависимости
    func resolveDependencies() {
        let resolver = ApplicationAssembly.assembler.resolver
        
        guard let networkReachabilityService = resolver.resolve(NetworkReachabilityService.self) else { preconditionFailure() }
        guard let statusMessageView = resolver.resolve(StatusBarMessageView.self) else { preconditionFailure() }
        
        self.networkReachabilityService = networkReachabilityService
        self.statusMessageView = statusMessageView
    }
    
    /// Настройка ReachabilityService
    func configureReachabilityService() {
        self.networkReachabilityService.listenNetworkReachability(self, reachable: { [weak self] _ in
            self?.statusMessageView?.hide()
        }, notReachable: { [weak self] in
            self?.statusMessageView?.show(message: Localization.StatusBarMessageView.internetNotConnected)
        })
    }
    
    /// Настройка навигационной панели
    func configureNavigationBar() {
        
        // Задний фон
        self.view.backgroundColor = UIConstants.navigationBarBackgroundColor
        self.navigationBar.barTintColor = UIConstants.navigationBarBackgroundColor
        self.navigationBar.isTranslucent = false
        
        // Разделитель
        self.navigationBar.shadowImage = .generate(with: UIConstants.navigationBarShadowColor)
        
        // Цвет кнопок
        self.navigationBar.tintColor = UIConstants.navigationBarItemTintColor
        
        // Настройка заголовка
        let navigationBarTitleTextAttributes: [NSAttributedStringKey: Any] = [
            .foregroundColor: UIConstants.navigationBarTitleTintColor
        ]
        self.navigationBar.titleTextAttributes = navigationBarTitleTextAttributes
    }
    
    func checkNetworkReachabilityStatus() {
        guard let networkStatus = self.networkReachabilityService.currentStatus,
            case .notReachable = networkStatus else { return }
        self.statusMessageView?.show()
    }
    
}
