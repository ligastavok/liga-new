//
//  NavigationController+Sizes.swift
//  LigaStavok
//
//  Created by Artem on 25/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit
import WebKit


extension NavigationController {
    
    // MARK: - View sizing
    
    /// Высота NavigationBar вместе с StatusBar
    var fullNavigationBarHeight: CGFloat {
        return navigationBarHeight + statusBarHeight
    }
    
    /// Высота NavigationBar
    var navigationBarHeight: CGFloat {
        return navigationBar.frame.size.height
    }
    
    /// Высота StatusBar
    var statusBarHeight: CGFloat {
        var statusBarHeight = UIApplication.shared.statusBarFrame.size.height
        if #available(iOS 11.0, *) {
            // Account for the notch when the status bar is hidden
            statusBarHeight = max(UIApplication.shared.statusBarFrame.size.height, UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0)
        }
        return statusBarHeight - extendedStatusBarDifference
    }
    
    
    /// Разница в доступной площади в UIView
    var extendedStatusBarDifference: CGFloat {
        return abs(view.bounds.height - (UIApplication.shared.delegate?.window??.frame.size.height ?? UIScreen.main.bounds.height))
    }
    
    /// Смещение TabBar
    var tabBarOffset: CGFloat {
        // Only account for the tab bar if a tab bar controller is present and the bar is not translucent
        if let tabBarController = tabBarController, !(topViewController?.hidesBottomBarWhenPushed ?? false) {
            return tabBarController.tabBar.isTranslucent ? 0 : tabBarController.tabBar.frame.height
        }
        return 0
    }
    
    /// Возвращает ScrollView, за которой наблюдает контроллер
    func scrollView() -> UIScrollView? {
        if let webView = self.scrollableView as? UIWebView {
            return webView.scrollView
        } else if let wkWebView = self.scrollableView as? WKWebView {
            return wkWebView.scrollView
        } else {
            return scrollableView as? UIScrollView
        }
    }
    
    /// Смещение содержимого
    var contentOffset: CGPoint {
        return scrollView()?.contentOffset ?? CGPoint.zero
    }
    
    /// Размер содержимого
    var contentSize: CGSize {
        guard let scrollView = scrollView() else {
            return CGSize.zero
        }
        
        let verticalInset = scrollView.contentInset.top + scrollView.contentInset.bottom
        return CGSize(width: scrollView.contentSize.width, height: scrollView.contentSize.height + verticalInset)
    }
    
    var deltaLimit: CGFloat {
        return navigationBarHeight - statusBarHeight
    }
}
