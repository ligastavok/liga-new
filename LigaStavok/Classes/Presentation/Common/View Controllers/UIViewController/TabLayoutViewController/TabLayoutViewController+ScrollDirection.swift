//
//  TabLayoutViewController+ScrollDirection.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension TabLayoutViewController {
    
    /// Направление смещения
    enum ScrollDirection {
        
        /// Лево
        case left
        /// Право
        case right
        /// Нет смещения
        case none
        
    }
    
}
