//
//  TabLayoutIndicatorInfo.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

/// Информация об индикаторе
struct TabLayoutIndicatorInfo {
    
    // MARK: - Публичные свойства
    
    /// Заголовок
    let title: String?
    /// Иконка
    let image: UIImage?
    /// Иконка для активного состояния
    let highlightedImage: UIImage?
    
    
    // MARK: - Инициалиазация
    
    init(title: String) {
        self.title = title
        self.image = nil
        self.highlightedImage = nil
    }
    
    init(image: UIImage, highlightedImage: UIImage? = nil) {
        self.title = nil
        self.image = image
        self.highlightedImage = highlightedImage
    }
    
    init(title: String, image: UIImage, highlightedImage: UIImage? = nil) {
        self.title = title
        self.image = image
        self.highlightedImage = highlightedImage
    }
    
}


// MARK: - ExpressibleByStringLiteral

extension TabLayoutIndicatorInfo: ExpressibleByStringLiteral {
    
    init(stringLiteral value: String) {
        self.init(title: value)
    }
    
    init(extendedGraphemeClusterLiteral value: String) {
        self.init(title: value)
    }
    
    init(unicodeScalarLiteral value: String) {
        self.init(title: value)
    }
    
}
