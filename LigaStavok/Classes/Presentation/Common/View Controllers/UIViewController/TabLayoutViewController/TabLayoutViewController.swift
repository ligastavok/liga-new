//
//  TabLayoutViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class TabLayoutViewController: UIViewController {
    
    // MARK: - Outlet
    
    /// Контейнер
    @IBOutlet
    weak var containerView: UIScrollView! {
        willSet {
            newValue.delegate = self
        }
    }
    
    
    // MARK: - Публичные свойства
    
    /// Источник данных
    weak var dataSource: TabLayoutViewControllerDataSource?
    /// Делегат
    weak var delegate: TabLayoutViewControllerDelegate?
    
    /// Поведение
    var behaviour: Behaviour = .progressive(skipIntermediateViewControllers: true)
    
    /// Элементы
    private(set) var viewControllers: [UIViewController] = []
    /// Индекс текущего элемента
    private(set) var currentIndex = 0
    
    /// Признак выполнения отображения на экране
    var isViewAppearing = false
    /// Признак выполнения поворота экрана
    var isViewRotating = false
    
    
    // MARK: - Приватные свойства
    
    /// Индекс, к которому будет выполнен переход при отображении
    private var prepareIndex = 0
    
    /// Последнее смещение контента
    private var lastContentOffset: CGFloat = 0
    /// Последний размер контейнера при отображении
    private var lastContainerSize: CGSize = .zero
    
    /// Элементы для смещения
    private var childViewControllersForScrolling: [UIViewController]?
    
}


// MARK: - Публичные свойства

extension TabLayoutViewController {
    
    /// Количество элементов
    var tabsCount: Int {
        return self.viewControllers.count
    }
    
}


// MARK: - Приватные свойства

private extension TabLayoutViewController {
    
    /// Ширина страницы
    var contentWidth: CGFloat {
        return self.containerView.bounds.size.width
    }
    
    /// Процент отображения
    var scrollPercentage: CGFloat {
        let contentOffset: CGFloat
        if self.containerView.contentOffset.x < 0 {
            contentOffset = self.contentWidth + self.containerView.contentOffset.x
        } else {
            contentOffset = self.containerView.contentOffset.x
        }
        
        if case .right = self.scrollDirection {
            return 1 - fmod(contentOffset, self.contentWidth) / self.contentWidth
        } else {
            let remainder = fmod(contentOffset, self.contentWidth)
            return remainder == 0 ? 1 : remainder / self.contentWidth
        }
    }
    
    /// Направление смещения
    var scrollDirection: ScrollDirection {
        if self.containerView.contentOffset.x > self.lastContentOffset {
            return .left
        } else if self.containerView.contentOffset.x < self.lastContentOffset {
            return .right
        }
        
        return .none
    }
    
    /// Признак изменения размера контейнера
    var isContentSizeChanged: Bool {
        return self.lastContainerSize != self.containerView.bounds.size
    }
    
    /// Признак необходимости обновления контента
    var isContentUpdateNeeded: Bool {
        return self.isViewLoaded && self.isContentSizeChanged
    }
    
}


// MARK: - UIViewController

extension TabLayoutViewController {
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Установка флага
        self.isViewRotating = true
        
        // Поворот
        let indexBeforeRotate = self.currentIndex
        coordinator.animate(alongsideTransition: nil) { [weak self] _ in
            guard let `self` = self else { return }
            
            // Установка флага
            self.isViewRotating = false
            
            // Настройка индекса
            self.currentIndex = indexBeforeRotate
            self.prepareIndex = indexBeforeRotate
            
            // Обновление контента
            self.updateContentIfNeeded()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Настройка контейнера
        let containerView = self.containerView ?? {
            let containerView = UIScrollView(frame: .init(origin: .zero, size: self.view.bounds.size))
            containerView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            
            self.view.addSubview(containerView)
            
            self.containerView = containerView
            return containerView
        }()
        
        containerView.bounces = false
        containerView.showsVerticalScrollIndicator = false
        containerView.showsHorizontalScrollIndicator = false
        containerView.scrollsToTop = false
        containerView.isPagingEnabled = true
        
        // Получение элементов
        reloadViewControllers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Установка флага
        self.isViewAppearing = true
        
        // Уведомление дочерних view controller'ов
        self.childViewControllers.forEach { $0.beginAppearanceTransition(true, animated: animated) }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Проверка отображаемого элемента
        let isNeedToUpdateCurrent = self.prepareIndex != self.currentIndex
        if isNeedToUpdateCurrent {
            moveToViewController(atIndex: self.prepareIndex)
        }
        
        // Установка флага
        self.isViewAppearing = false
        
        // Уведомление дочерних view controller'ов
        self.childViewControllers.forEach { $0.endAppearanceTransition() }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Уведомление дочерних view controller'ов
        self.childViewControllers.forEach { $0.beginAppearanceTransition(false, animated: animated) }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        // Уведомление дочерних view controller'ов
        self.childViewControllers.forEach { $0.endAppearanceTransition() }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateContentIfNeeded()
    }
    
    override var shouldAutomaticallyForwardAppearanceMethods: Bool {
        return false
    }
    
}


// MARK: - Публичные методы

extension TabLayoutViewController {
    
    /// Повторно отобразить данные
    @objc
    func reloadTabLayout() {
        guard self.isViewLoaded else { return }
        
        // Удаление элементов
        for childViewController in self.viewControllers {
            removeViewController(childViewController)
        }
        
        self.lastContainerSize = .zero
        
        // Получение элементов
        reloadViewControllers()
        
        // Вычисление индекса
        if self.currentIndex >= self.tabsCount {
            self.currentIndex = self.tabsCount - 1
        }
        
        self.prepareIndex = self.currentIndex
        
        // Обновление контента
        updateContent()
    }
    
    /// Отобразить элемент
    func moveToViewController(atIndex index: Int, animated: Bool = true) {
        guard self.isOnScreen && self.currentIndex != index else {
            self.prepareIndex = index
            return
        }
        
        if animated && self.behaviour.skipIntermediateViewControllers && abs(self.currentIndex - index) > 1 {
            var viewControllers = self.viewControllers
            
            let currentViewController = viewControllers[self.currentIndex]
            
            let fromIndex = self.currentIndex < index ? index - 1 : index + 1
            let fromViewController = viewControllers[fromIndex]
            
            viewControllers[self.currentIndex] = fromViewController
            viewControllers[fromIndex] = currentViewController
            
            self.childViewControllersForScrolling = viewControllers
            
            self.containerView.setContentOffset(.init(x: contentOffset(forIndex: fromIndex), y: 0), animated: false)
        }
        
        (self.navigationController?.view ?? self.view).isUserInteractionEnabled = !animated
        self.containerView.setContentOffset(.init(x: contentOffset(forIndex: index), y: 0), animated: animated)
    }
    
    /// Отобразить элемент
    func moveToViewController(_ viewController: UIViewController, animated: Bool = true) {
        guard let index = self.viewControllers.index(of: viewController) else { return }
        
        moveToViewController(atIndex: index)
    }
    
}


// MARK: - Приватные методы

private extension TabLayoutViewController {
    
    /// Обновить элементы
    func reloadViewControllers() {
        guard let dataSource = self.dataSource else { preconditionFailure("dataSource must not be nil") }
        self.viewControllers = dataSource.viewControllers(for: self)
        
        guard !self.viewControllers.isEmpty else { preconditionFailure("dataSource should provide at least one child view controller") }
        self.viewControllers.forEach {
            guard $0 is TabLayoutIndicatorInfoProvider else {
                // swiftlint:disable:next line_length
                preconditionFailure("Every view controller, provided by TabLayoutViewControllerDataSource viewControllers(for:) method must conform to TabLayoutIndicatorInfoProvider")
            }
        }
    }
    
    /// Удалить элемент из контейнера
    func removeViewController(_ viewController: UIViewController) {
        guard viewController.parent != nil else { return }
        
        viewController.beginAppearanceTransition(false, animated: false)
        viewController.removeFromParent()
        viewController.endAppearanceTransition()
    }
    
    /// Смещение для контента
    func contentOffset(forIndex index: Int) -> CGFloat {
        return CGFloat(index) * self.contentWidth
    }
    
    /// Прогрессивный индекс, зависящий от состояния перехода
    func progressiveIndex(forContentOffset contentOffset: CGFloat) -> Int {
        let progressiveIndex = Int(round(contentOffset / self.contentWidth))
        
        guard progressiveIndex > 0 else { return 0 }
        guard progressiveIndex < self.tabsCount - 1 else { return self.tabsCount - 1 }
        
        return progressiveIndex
    }
    
    /// Индексы для прогрессивного индикатора
    func progressiveIndicatorIndexes(forIndex index: Int) -> (fromIndex: Int, toIndex: Int) {
        var fromIndex = self.currentIndex
        var toIndex = self.currentIndex
        
        let scrollDirection = self.scrollDirection
        if case .left = scrollDirection {
            if self.scrollPercentage >= 0.5 {
                fromIndex = toIndex - 1
            } else {
                toIndex = fromIndex + 1
            }
        } else if case .right = scrollDirection {
            if self.scrollPercentage > 0.5 {
                fromIndex = toIndex + 1
            } else {
                toIndex = fromIndex - 1
            }
        }
        
        return (fromIndex: fromIndex, toIndex: toIndex)
    }
    
    /// Обновить контент, если необходимо
    func updateContentIfNeeded() {
        guard self.isContentUpdateNeeded else { return }
        
        updateContent()
    }
    
    /// Обновить контент
    func updateContent() {
        
        // Смещение контента, в случае изменения ширины контейнера
        if self.lastContainerSize.width != self.contentWidth {
            self.containerView.contentOffset = .init(x: contentOffset(forIndex: self.currentIndex), y: 0)
        }
        
        self.lastContainerSize = self.containerView.bounds.size
        
        // Вычисление размера контента
        let childViewControllers = self.childViewControllersForScrolling ?? self.viewControllers
        self.containerView.contentSize.width = self.contentWidth * CGFloat(childViewControllers.count)
        
        // Размещение элементов
        for (index, childViewController) in childViewControllers.enumerated() {
            
            // Проверка на вхождение элемента в контейнер
            if fabs(self.containerView.contentOffset.x - contentOffset(forIndex: index)) < self.contentWidth {
                if childViewController.parent == nil {
                    childViewController.beginAppearanceTransition(true, animated: false)
                    
                    add(child: childViewController, in: self.containerView, pinToEdges: true, withConstraints: false)
                    childViewController.view.frame = .init(x: contentOffset(forIndex: index),
                                                           y: 0,
                                                           width: self.containerView.bounds.size.width,
                                                           height: self.containerView.bounds.size.height)
                    
                    childViewController.endAppearanceTransition()
                } else {
                    childViewController.view.frame = .init(x: contentOffset(forIndex: index),
                                                           y: 0,
                                                           width: self.containerView.bounds.size.width,
                                                           height: self.containerView.bounds.size.height)
                    childViewController.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
                }
            } else {
                removeViewController(childViewController)
            }
        }
        
        // Обновление индекса активного элемента
        let currentIndex = progressiveIndex(forContentOffset: self.containerView.contentOffset.x)
        
        let oldCurrentIndex = self.currentIndex
        self.currentIndex = currentIndex
        self.prepareIndex = currentIndex
        
        if let progressiveDelegate = self.delegate as? TabLayoutViewControllerProgressiveDelegate, self.behaviour.isProgressiveIndicator {
            let (fromIndex, toIndex) = progressiveIndicatorIndexes(forIndex: currentIndex)
            let scrollPercentage = self.scrollPercentage
            let changeCurrentIndex = currentIndex != oldCurrentIndex
            
            progressiveDelegate.tabLayoutViewController(self,
                                                        didUpdateIndicatorFromIndex: fromIndex,
                                                        toIndex: toIndex,
                                                        withProgressPercentage: scrollPercentage,
                                                        indexChanged: changeCurrentIndex)
        } else {
            self.delegate?.tabLayoutViewController(self,
                                                   didUpdateIndicatorFromIndex: oldCurrentIndex,
                                                   toIndex: currentIndex)
        }
    }
    
}


// MARK: - UIScrollViewDelegate

extension TabLayoutViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard self.containerView === scrollView else { return }
        
        updateContent()
        
        self.lastContentOffset = scrollView.contentOffset.x
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        guard self.containerView === scrollView else { return }
        
        self.childViewControllersForScrolling = nil
        
        (self.navigationController?.view ?? self.view).isUserInteractionEnabled = true
        
        updateContent()
    }
    
}
