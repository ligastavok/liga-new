//
//  TabLayoutViewControllerDataSource.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol TabLayoutViewControllerDataSource: class {
    
    /// Элементы для отображения
    func viewControllers(for tabLayoutViewController: TabLayoutViewController) -> [UIViewController]
    
}
