//
//  TabLayoutIndicatorInfoProvider.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol TabLayoutIndicatorInfoProvider: class {
    
    /// Получить информацию об индикаторе
    func indicatorInfo(for tabLayoutViewController: TabLayoutViewController) -> TabLayoutIndicatorInfo
    
}
