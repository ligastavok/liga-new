//
//  TabLayoutButtonBarViewController+ItemSpecification.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 18.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension TabLayoutButtonBarViewController {
    
    /// Спецификация элемента на панели вкладок
    enum ItemSpecification<CellType> where CellType: UICollectionViewCell {
        
        /// Создание ячейки из Nib-файла
        case nibFile(nibName: String, bundle: Bundle)
        /// Создание ячейки программнно
        case cellClass
        
    }
    
}
