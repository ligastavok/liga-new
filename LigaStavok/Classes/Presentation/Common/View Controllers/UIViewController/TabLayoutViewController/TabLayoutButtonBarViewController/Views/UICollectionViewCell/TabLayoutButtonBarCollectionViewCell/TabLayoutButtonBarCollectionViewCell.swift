//
//  TabLayoutButtonBarCollectionViewCell.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class TabLayoutButtonBarCollectionViewCell: UICollectionViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Иконка
    @IBOutlet
    private weak var iconImageView: UIImageView!
    /// Название
    @IBOutlet
    private weak var titleLabel: UILabel!
    
}


// MARK: - Публичные свойства

extension TabLayoutButtonBarCollectionViewCell {
    
    /// Фоновый цвет
    var contentBackgroundColor: UIColor? {
        get {
            return self.contentView.backgroundColor
        }
        set {
            self.contentView.backgroundColor = newValue
        }
    }
    
    /// Шрифт для названия
    var titleFont: UIFont {
        get {
            return self.titleLabel.font
        }
        set {
            self.titleLabel.font = newValue
        }
    }
    
    /// Цвет для названия
    var titleColor: UIColor {
        get {
            return self.titleLabel.textColor
        }
        set {
            self.titleLabel.textColor = newValue
        }
    }
    
}


// MARK: - Публичные методы

extension TabLayoutButtonBarCollectionViewCell {
    
    /// Настройка
    func configure(with indicatorInfo: TabLayoutIndicatorInfo) {
        self.iconImageView.image = indicatorInfo.image
        self.iconImageView.highlightedImage = indicatorInfo.highlightedImage
        
        self.titleLabel.text = indicatorInfo.title
    }
    
}
