//
//  TabLayoutDefaultButtonBarViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 18.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

typealias TabLayoutDefaultButtonBarViewController = TabLayoutButtonBarViewController<TabLayoutButtonBarCollectionViewCell>
