//
//  TabLayoutButtonBarViewController+Settings.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension TabLayoutButtonBarViewController {
    
    /// Настройки
    struct Settings {
        
        /// Фоновый цвет панели вкладок
        var buttonBarBackgroundColor: UIColor?
        /// Минимальное расстояние между элементами на панели вкладок
        var buttonBarMinimumInteritemSpacing: CGFloat?
        /// Левый отступ для содержимого на панели вкладок
        var buttonBarLeftContentInset: CGFloat?
        /// Правый отступ для содержимого на панели вкладок
        var buttonBarRightContentInset: CGFloat?
        
        /// Фоновый цвет для индикатора выбранного элемента на панели вкладок
        var selectedIndicatorBackgroundColor: UIColor = .black
        /// Высота индикатора выбранного элемента на панели вкладок
        var selectedIndicatorHeight: CGFloat = 5
        /// Вертикальное выравнивание индикатора выбранного элемента на панели вкладок
        var selectedIndicatorVerticalAlignment: TabLayoutButtonBarView.SelectedIndicatorVerticalAlignment = .bottom
        
        /// Фоновый цвет элемента на панели вкладок
        var buttonBarItemBackgroundColor: UIColor?
        /// Левый и правый отступ элемента на панели вкладок
        var buttonBarItemLeftRightMargin: CGFloat = 8
        /// Шрифт текста элемента на панели вкладок
        var buttonBarItemFont: UIFont = .systemFont(ofSize: 17)
        
        /// Признак необходимости заполнения элементами всего свободного пространства панели вкладок
        var buttonBarItemsShouldFillAvailableWidth = true
        /// Высота панели вкладок
        var buttonBarHeight: CGFloat?
        
    }
    
}
