//
//  TabLayoutButtonBarView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

/// Панель вкладок
final class TabLayoutButtonBarView: UICollectionView {
    
    // MARK: - Публичные свойства
    
    /// Высота индикатора выбранного элемента
    var selectedIndicatorHeight: CGFloat = 5 {
        didSet {
            setNeedsLayout()
        }
    }
    /// Вертикальное выравнивание индикатора выбранного элемента
    var selectedIndicatorVerticalAlignment: SelectedIndicatorVerticalAlignment = .bottom
    
    /// Выравнивание выбранного элемента
    var selectedItemAlignment: SelectedItemAlignment = .center
    
    /// Индекс выбранного элемента
    private(set) var selectedIndex = 0
    
    
    // MARK: - Приватные свойства
    
    /// Индикатор выбранного элемента
    private lazy var selectedIndicator: UIView = {
        let selectedIndicator = UIView()
        selectedIndicator.layer.zPosition = 9999
        
        return selectedIndicator
    }()
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Добавление индикатора выбранного элемента
        addSubview(self.selectedIndicator)
    }
    
}


// MARK: - Публичные свойства

extension TabLayoutButtonBarView {
    
    /// Фоновый цвет для индикатора выбранного элемента
    var selectedIndicatorBackgroundColor: UIColor? {
        get {
            return self.selectedIndicator.backgroundColor
        }
        set {
            self.selectedIndicator.backgroundColor = newValue
        }
    }
    
}


// MARK: - UIView

extension TabLayoutButtonBarView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updateSelectedIndicatorFrame()
    }
    
}


// MARK: - Публичные методы

extension TabLayoutButtonBarView {
    
    /// Изменить индекс выбранного элемента
    func move(toIndex index: Int, barScroll: BarScroll, animated: Bool) {
        self.selectedIndex = index
        
        // Обновление отображения выбранного элемента
        updateSelectedItem(toIndex: index, barScroll: barScroll, animated: animated)
    }
    
    /// Изменить индекс выбранного элемента
    func move(fromIndex: Int, toIndex: Int, progressPercentage: CGFloat, barScroll: BarScroll) {
        self.selectedIndex = progressPercentage > 0.5 ? toIndex : fromIndex
        
        // Обновление отображения выбранного элемента
        updateSelectedItem(fromIndex: fromIndex, toIndex: toIndex, progressPercentage: progressPercentage, barScroll: barScroll)
    }
    
}


// MARK: - Приватные методы

private extension TabLayoutButtonBarView {
    
    /// Обновить границу индикатора выбранного элемента
    func updateSelectedIndicatorFrame() {
        let y: CGFloat
        switch self.selectedIndicatorVerticalAlignment {
        case .top:
            y = 0
        case .middle:
            y = (self.frame.size.height - self.selectedIndicatorHeight) / 2
        case .bottom:
            y = self.frame.size.height - self.selectedIndicatorHeight
        }
        
        self.selectedIndicator.frame.origin.y = y
        self.selectedIndicator.frame.size.height = self.selectedIndicatorHeight
    }
    
    /// Проверка элемента на видимость
    func chechCellFrameVisibility(_ frame: CGRect) -> Bool {
        let containerLeftEdge = self.contentOffset.x
        let containerRightEdge = containerLeftEdge + self.frame.size.width
        
        let frameLeftEdge = frame.origin.x
        let frameRightEdge = frameLeftEdge + frame.size.width
        
        let frameLeftEdgeVisible = frameLeftEdge > containerLeftEdge && frameLeftEdge < containerRightEdge
        let frameRightEdgeVisible = frameRightEdge > containerLeftEdge && frameRightEdge < containerRightEdge
        
        return frameLeftEdgeVisible || frameRightEdgeVisible
    }
    
    /// Вычислить смещение содержимого коллекции
    func contentOffsetForCell(withFrame frame: CGRect) -> CGFloat {
        guard let sectionInset = (collectionViewLayout as? UICollectionViewFlowLayout)?.sectionInset else { return 0 }
        
        let alignmentOffset: CGFloat
        switch self.selectedItemAlignment {
        case .left:
            alignmentOffset = sectionInset.left
        case .center:
            alignmentOffset = (self.frame.size.width - frame.size.width) / 2
        case .right:
            alignmentOffset = self.frame.size.width - sectionInset.right - frame.size.width
        }
        
        var contentOffset = frame.origin.x - alignmentOffset
        contentOffset = min(self.contentSize.width - self.frame.size.width, max(0, contentOffset))
        
        return contentOffset
    }
    
    /// Обновить смещение содержимого коллекции
    func updateContentOffset(toCellFrame cellFrame: CGRect, barScroll: BarScroll, animated: Bool) {
        guard barScroll == .yes || barScroll == .onlyIfOutOfScreen && !chechCellFrameVisibility(cellFrame) else { return }
        
        let contentOffset = self.contentSize.width > self.frame.size.width ? contentOffsetForCell(withFrame: cellFrame) : 0
        setContentOffset(.init(x: contentOffset, y: 0), animated: animated)
    }
    
    /// Обновить смещение содержимого коллекции
    func updateContentOffset(fromCellFrame: CGRect, toCellFrame: CGRect, progressPercentage: CGFloat, barScroll: BarScroll) {
        guard self.contentSize.width > self.frame.size.width else {
            setContentOffset(.zero, animated: false)
            return
        }
        
        if case .yes = barScroll {
            let toContentOffset = contentOffsetForCell(withFrame: toCellFrame)
            let fromContentOffset = contentOffsetForCell(withFrame: fromCellFrame)
            
            let contentOffset = fromContentOffset + (toContentOffset - fromContentOffset) * progressPercentage
            setContentOffset(.init(x: contentOffset, y: 0), animated: false)
        } else if barScroll == .onlyIfOutOfScreen && !chechCellFrameVisibility(toCellFrame) {
            let contentOffset = contentOffsetForCell(withFrame: toCellFrame)
            setContentOffset(.init(x: contentOffset, y: 0), animated: true)
        }
    }
    
    /// Обновить выбранный элемент
    func updateSelectedItem(toIndex index: Int,
                            barScroll: BarScroll,
                            animated: Bool) {
        
        // Вычисление рамок ячейки
        guard let selectedCellFrame = layoutAttributesForItem(at: .init(item: index, section: 0))?.frame else { return }
        
        // Обновление смещения содержимого коллекции
        updateContentOffset(toCellFrame: selectedCellFrame, barScroll: barScroll, animated: animated)
        
        // Изменение размера и позиции индикатора выбранного элемента
        var selectedIndicatorFrame = self.selectedIndicator.frame
        selectedIndicatorFrame.size.width = selectedCellFrame.size.width
        selectedIndicatorFrame.origin.x = selectedCellFrame.origin.x
        
        let animationDuration = animated ? 0.3 : 0
        UIView.animate(withDuration: animationDuration) { [weak self] in
            self?.selectedIndicator.frame = selectedIndicatorFrame
        }
    }
    
    /// Обновить выбранный элемент
    func updateSelectedItem(fromIndex: Int,
                            toIndex: Int,
                            progressPercentage: CGFloat,
                            barScroll: BarScroll) {
        
        // Вычисление рамок ячеек
        guard let toCellFrame = layoutAttributesForItem(at: .init(item: toIndex, section: 0))?.frame,
            let fromCellFrame = layoutAttributesForItem(at: .init(item: fromIndex, section: 0))?.frame else { return }
        
        // Обновление смещения содержимого коллекции
        updateContentOffset(fromCellFrame: fromCellFrame,
                            toCellFrame: toCellFrame,
                            progressPercentage: progressPercentage,
                            barScroll: barScroll)
        
        // Изменение размера и позиции индикатора выбранного элемента
        self.selectedIndicator.frame.size.width = fromCellFrame.size.width + (toCellFrame.size.width - fromCellFrame.size.width) * progressPercentage
        self.selectedIndicator.frame.origin.x = fromCellFrame.origin.x + (toCellFrame.origin.x - fromCellFrame.origin.x) * progressPercentage
    }
    
}
