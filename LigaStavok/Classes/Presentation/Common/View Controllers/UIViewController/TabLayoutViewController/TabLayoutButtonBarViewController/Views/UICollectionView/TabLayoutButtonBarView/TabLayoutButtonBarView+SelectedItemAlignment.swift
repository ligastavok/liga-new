//
//  TabLayoutButtonBarView+SelectedItemAlignment.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension TabLayoutButtonBarView {
    
    /// Выравнивание выбранного элемента
    enum SelectedItemAlignment {
        
        /// По левой стороне
        case left
        /// По центу
        case center
        /// По правой стороне
        case right
        
    }
    
}
