//
//  TabLayoutButtonBarViewController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class TabLayoutButtonBarViewController<ButtonBarCellType>: TabLayoutViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout where ButtonBarCellType: UICollectionViewCell { // swiftlint:disable:this line_length
    
    // MARK: - Outlet
    
    /// Панель вкладок
    @IBOutlet
    weak var buttonBarView: TabLayoutButtonBarView! {
        willSet {
            newValue.dataSource = self
            newValue.delegate = self
        }
    }
    
    
    // MARK: - Публичные свойства
    
    /// Настройки
    var settings = Settings()
    
    /// Спецификация элемента на панели вкладок
    lazy var buttonBarItemSpecification: ItemSpecification<ButtonBarCellType> = {
        let nibName = String(describing: ButtonBarCellType.self)
        let bundle = Bundle(for: ButtonBarCellType.self)
        
        return .nibFile(nibName: nibName, bundle: bundle)
    }()
    
    /// Ширина для элемента на панели вкладок
    lazy var widthForButtonBarItem: ((_ indicatorInfo: TabLayoutIndicatorInfo) -> CGFloat) = { [weak self] indicatorInfo in
        guard let `self` = self else { return 0 }
        
        let titleWidth = indicatorInfo.title?.width(usingFont: self.settings.buttonBarItemFont) ?? 0
        return titleWidth + CGFloat(self.settings.buttonBarItemLeftRightMargin) * 2
    }
    
    /// Настройка ячейки
    var configureCell: ((_ cell: ButtonBarCellType, _ isActive: Bool) -> Void)?
    
    /// Была выбрана новая ячейка на панели вкладок
    var didChangeCurrentIndex: ((_ inactiveCell: ButtonBarCellType?,
                                 _ activeCell: ButtonBarCellType?,
                                 _ animated: Bool) -> Void)?
    /// Была выбрана новая ячейка на панели вкладок
    var didChangeCurrentIndexProgressive: ((_ inactiveCell: ButtonBarCellType?,
                                            _ activeCell: ButtonBarCellType?,
                                            _ progressPercentage: CGFloat,
                                            _ currentIndexChanged: Bool,
                                            _ animated: Bool) -> Void)?
    
    
    // MARK: - Приватные свойства
    
    /// Принудительный индекс
    private var forceIndex: Int?
    
    /// Сохраненная ширина элементов на панели вкладок
    private lazy var cachedButtonBarItemsWidths: [CGFloat]? = { [weak self] in
        return self?.calculateButtonBarItemsWidths()
    }()
    
    /// Признак, указывающий на необходимость обновить панель вкладок
    private var shouldUpdateButtonBarView = true
    
    
    // MARK: - Инициализация
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        initPhase2()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        self.dataSource = self
        self.delegate = self
    }
    
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let buttonBarView = self.buttonBarView ?? {
            let buttonBarViewHeight = self.settings.buttonBarHeight ?? 44
            let buttonBarViewFrame = CGRect(origin: .zero, size: .init(width: self.view.frame.size.width,
                                                                       height: buttonBarViewHeight))
            
            let buttonBarView = TabLayoutButtonBarView(frame: buttonBarViewFrame,
                                                       collectionViewLayout: UICollectionViewFlowLayout())
            buttonBarView.autoresizingMask = .flexibleWidth
            
            self.view.addSubview(buttonBarView)
            
            self.containerView.frame.origin.y = buttonBarViewHeight
            self.containerView.frame.size.height -= buttonBarViewHeight
            
            self.buttonBarView = buttonBarView
            return buttonBarView
        }()
        
        buttonBarView.scrollsToTop = false
        
        if let flowLayout = buttonBarView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = self.settings.buttonBarMinimumInteritemSpacing ?? flowLayout.minimumLineSpacing
            flowLayout.sectionInset = .init(top: flowLayout.sectionInset.top,
                                            left: self.settings.buttonBarLeftContentInset ?? flowLayout.sectionInset.left,
                                            bottom: flowLayout.sectionInset.bottom,
                                            right: self.settings.buttonBarRightContentInset ?? flowLayout.sectionInset.right)
        }
        
        buttonBarView.showsHorizontalScrollIndicator = false
        buttonBarView.backgroundColor = self.settings.buttonBarBackgroundColor ?? buttonBarView.backgroundColor
        
        buttonBarView.selectedIndicatorBackgroundColor = self.settings.selectedIndicatorBackgroundColor
        buttonBarView.selectedIndicatorHeight = self.settings.selectedIndicatorHeight
        buttonBarView.selectedIndicatorVerticalAlignment = self.settings.selectedIndicatorVerticalAlignment
        
        switch self.buttonBarItemSpecification {
        case .nibFile(let nibName, let bundle):
            let nib = UINib(nibName: nibName, bundle: bundle)
            buttonBarView.register(nib, forCellWithReuseIdentifier: ButtonBarCellType.storyboardIdentifier)
        case .cellClass:
            buttonBarView.register(ButtonBarCellType.self, forCellWithReuseIdentifier: ButtonBarCellType.storyboardIdentifier)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard self.isViewAppearing || self.isViewRotating else { return }
        
        self.cachedButtonBarItemsWidths = calculateButtonBarItemsWidths()
        self.buttonBarView.collectionViewLayout.invalidateLayout()
        
        self.buttonBarView.move(toIndex: self.currentIndex, barScroll: .onlyIfOutOfScreen, animated: false)
    }
    
    
    // MARK: - TabLayoutViewController
    
    override func reloadTabLayout() {
        super.reloadTabLayout()
        
        guard self.isViewLoaded else { return }
        
        self.cachedButtonBarItemsWidths = calculateButtonBarItemsWidths()
        self.buttonBarView.collectionViewLayout.invalidateLayout()
        
        self.buttonBarView.reloadData()
        self.buttonBarView.move(toIndex: self.currentIndex, barScroll: .yes, animated: false)
    }
    
    
    // MARK: - UIScrollViewDelegate
    
    override func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        super.scrollViewDidEndScrollingAnimation(scrollView)
        
        guard scrollView == self.containerView else { return }
        
        self.forceIndex = nil
        self.shouldUpdateButtonBarView = true
    }
    
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tabsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let indicatorInfoProvider = self.viewControllers[indexPath.row] as? TabLayoutIndicatorInfoProvider else { preconditionFailure() }
        
        let indicatorInfo = indicatorInfoProvider.indicatorInfo(for: self)
        
        let cell = collectionView.dequeueReusableCell(for: indexPath) as ButtonBarCellType
        configure(cell: cell, for: indicatorInfo)
        
        let currentIndex = self.forceIndex ?? self.currentIndex
        self.configureCell?(cell, currentIndex == indexPath.item)
        
        return cell
    }
    
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        guard indexPath.item != self.currentIndex else { return }
        
        self.forceIndex = indexPath.item
        self.shouldUpdateButtonBarView = false
        
        self.buttonBarView.move(toIndex: indexPath.item, barScroll: .yes, animated: true)
        
        let inactiveCellIndexPath = IndexPath(item: self.currentIndex, section: 0)
        let activeCellIndexPath = IndexPath(item: indexPath.item, section: 0)
        
        self.buttonBarView.reloadItems(at: [inactiveCellIndexPath, activeCellIndexPath].unique)
        
        let inactiveCell = cellForItem(at: inactiveCellIndexPath)
        let activeCell = cellForItem(at: activeCellIndexPath)
        
        if self.behaviour.isProgressiveIndicator {
            self.didChangeCurrentIndexProgressive?(inactiveCell, activeCell, 1, true, true)
        } else {
            self.didChangeCurrentIndex?(inactiveCell, activeCell, true)
        }
        
        moveToViewController(atIndex: indexPath.item)
    }
    
    
    // MARK: - UICollectionViewDelegateFlowLayut
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard let cellWidth = self.cachedButtonBarItemsWidths?[indexPath.row] else { return .zero }
        return .init(width: cellWidth, height: collectionView.frame.size.height)
    }
    
    
    // MARK: - TabLayoutViewControllerDataSource
    
    func viewControllers(for tabLayoutViewController: TabLayoutViewController) -> [UIViewController] {
        assertionFailure("Subclass must implement the TabLayoutViewControllerDataSource viewControllers(for:) method")
        return []
    }
    
}


// MARK: - Публичные методы

extension TabLayoutButtonBarViewController {
    
    func configure(cell: ButtonBarCellType, for indicatorInfo: TabLayoutIndicatorInfo) {
        guard let tabLayoutButtonBarCollectionViewCell = cell as? TabLayoutButtonBarCollectionViewCell else {
            fatalError("You must override this method to set up ButtonBarCellType accordingly")
        }
        
        tabLayoutButtonBarCollectionViewCell.configure(with: indicatorInfo)
        tabLayoutButtonBarCollectionViewCell.contentBackgroundColor = self.settings.buttonBarItemBackgroundColor
        tabLayoutButtonBarCollectionViewCell.titleFont = self.settings.buttonBarItemFont
    }
    
}


// MARK: - Приватные методы

private extension TabLayoutButtonBarViewController {
    
    /// Вычислить ширину для широкой ячейки
    func calculateStretchedCellWidth(_ cellWidths: [CGFloat],
                                     suggestedStretchedCellWidth: CGFloat,
                                     collectionViewAvailableVisibleWidth: CGFloat,
                                     cellsSpacingTotal: CGFloat,
                                     previousNumberOfLargeCells: Int = 0) -> CGFloat {
        
        var numberOfLargeCells = 0
        var totalWidthOfLargeCells: CGFloat = 0
        
        for cellWidth in cellWidths where cellWidth > suggestedStretchedCellWidth {
            numberOfLargeCells += 1
            totalWidthOfLargeCells += cellWidth
        }
        
        guard numberOfLargeCells > previousNumberOfLargeCells else { return suggestedStretchedCellWidth }
        
        let numberOfSmallCells = cellWidths.count - numberOfLargeCells
        let newSuggestedStretchedCellWidth = (collectionViewAvailableVisibleWidth - totalWidthOfLargeCells - cellsSpacingTotal) / CGFloat(numberOfSmallCells) // swiftlint:disable:this line_length
        
        return calculateStretchedCellWidth(cellWidths,
                                           suggestedStretchedCellWidth: newSuggestedStretchedCellWidth,
                                           collectionViewAvailableVisibleWidth: collectionViewAvailableVisibleWidth,
                                           cellsSpacingTotal: cellsSpacingTotal,
                                           previousNumberOfLargeCells: numberOfLargeCells)
    }
    
    /// Рассчитать ширину элементов на панели вкладок
    func calculateButtonBarItemsWidths() -> [CGFloat] {
        var collectionViewContentWidth: CGFloat = 0
        var cellsWidths: [CGFloat] = []
        
        for viewController in self.viewControllers {
            guard let indicatorInfoProvider = viewController as? TabLayoutIndicatorInfoProvider else { preconditionFailure() }
            
            let indicatorInfo = indicatorInfoProvider.indicatorInfo(for: self)
            let width = self.widthForButtonBarItem(indicatorInfo)
            
            collectionViewContentWidth += width
            cellsWidths.append(width)
        }
        
        guard let flowLayout = self.buttonBarView.collectionViewLayout as? UICollectionViewFlowLayout else { return [] }
        
        let cellsSpacingTotal = CGFloat(self.tabsCount - 1) * flowLayout.minimumLineSpacing
        collectionViewContentWidth += cellsSpacingTotal
        
        let collectionViewAvailableVisibleWidth = self.buttonBarView.frame.size.width - flowLayout.sectionInset.left - flowLayout.sectionInset.right
        if self.settings.buttonBarItemsShouldFillAvailableWidth && collectionViewAvailableVisibleWidth >= collectionViewContentWidth {
            let meanStretchedCellWidth = (collectionViewAvailableVisibleWidth - cellsSpacingTotal) / CGFloat(self.tabsCount)
            let suggestedStretchedCellWidth = calculateStretchedCellWidth(cellsWidths,
                                                                          suggestedStretchedCellWidth: meanStretchedCellWidth,
                                                                          collectionViewAvailableVisibleWidth: collectionViewAvailableVisibleWidth,
                                                                          cellsSpacingTotal: cellsSpacingTotal)
            
            let stretchedCellsWidths: [CGFloat] = cellsWidths.map {
                let cellWidth = $0 > suggestedStretchedCellWidth ? $0 : suggestedStretchedCellWidth
                return cellWidth
            }
            
            return stretchedCellsWidths
        } else {
            return cellsWidths
        }
    }
    
    /// Получить ячейку для элемента на панели вкладок
    func cellForItem(at indexPath: IndexPath) -> ButtonBarCellType? {
        return self.buttonBarView.cellForItem(at: indexPath) as? ButtonBarCellType
    }
    
    /// Подготовка индекса элемента
    func prepareCellIndex(fromValue value: Int) -> Int {
        if value < 0 {
            return 0
        } else if value > self.tabsCount - 1 {
            return self.tabsCount - 1
        } else {
            return value
        }
    }
    
}


// MARK: - TabLayoutViewControllerDataSource

extension TabLayoutButtonBarViewController: TabLayoutViewControllerDataSource { }


// MARK: - TabLayoutViewControllerProgressiveDelegate

extension TabLayoutButtonBarViewController: TabLayoutViewControllerProgressiveDelegate {
    
    func tabLayoutViewController(_ tabLayoutViewController: TabLayoutViewController,
                                 didUpdateIndicatorFromIndex fromIndex: Int,
                                 toIndex: Int) {
        
        guard self.shouldUpdateButtonBarView else { return }
        
        let isIndexChanged = fromIndex != toIndex
        if isIndexChanged {
            self.buttonBarView.move(toIndex: toIndex, barScroll: .yes, animated: true)
        }
        
        let inactiveCellIndexPath = IndexPath(item: fromIndex, section: 0)
        let activeCellIndexPath = IndexPath(item: toIndex, section: 0)
        
        self.buttonBarView.reloadItems(at: [inactiveCellIndexPath, activeCellIndexPath].unique)
        
        let inactiveCell = cellForItem(at: inactiveCellIndexPath)
        let activeCell = cellForItem(at: activeCellIndexPath)
        
        self.didChangeCurrentIndex?(inactiveCell, activeCell, true)
    }
    
    func tabLayoutViewController(_ tabLayoutViewController: TabLayoutViewController,
                                 didUpdateIndicatorFromIndex fromIndex: Int,
                                 toIndex: Int,
                                 withProgressPercentage progressPercentage: CGFloat,
                                 indexChanged: Bool) {
        
        guard self.shouldUpdateButtonBarView else { return }
        
        let fromCellIndex = prepareCellIndex(fromValue: fromIndex)
        let toCellIndex = prepareCellIndex(fromValue: toIndex)
        
        self.buttonBarView.move(fromIndex: fromCellIndex, toIndex: toCellIndex, progressPercentage: progressPercentage, barScroll: .yes)
        
        let fromCellIndexPath = IndexPath(item: fromCellIndex, section: 0)
        let toCellIndexPath = IndexPath(item: toCellIndex, section: 0)
        
        self.buttonBarView.reloadItems(at: [fromCellIndexPath, toCellIndexPath].unique)
        
        let fromCell = cellForItem(at: fromCellIndexPath)
        let toCell = cellForItem(at: toCellIndexPath)
        
        self.didChangeCurrentIndexProgressive?(fromCell, toCell, progressPercentage, indexChanged, true)
    }
    
}
