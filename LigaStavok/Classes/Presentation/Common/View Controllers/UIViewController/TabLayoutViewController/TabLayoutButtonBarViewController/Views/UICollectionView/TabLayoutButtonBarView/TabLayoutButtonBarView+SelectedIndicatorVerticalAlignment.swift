//
//  TabLayoutButtonBarView+SelectedIndicatorVerticalAlignment.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension TabLayoutButtonBarView {
    
    /// Вертикальное выравнивание индикатора выбранного элемента
    enum SelectedIndicatorVerticalAlignment {
        
        /// По верхнему краю
        case top
        /// Посередине
        case middle
        /// По нижнему краю
        case bottom
        
    }
    
}
