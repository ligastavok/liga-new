//
//  TabLayoutButtonBarView+BarScroll.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension TabLayoutButtonBarView {
    
    /// Тип прокрутки панели
    enum BarScroll {
        
        /// Не прокручивать
        case no
        /// Прокручивать
        case yes
        /// Прокручивать, если элемент за границей видимой части панели
        case onlyIfOutOfScreen
        
    }
    
}
