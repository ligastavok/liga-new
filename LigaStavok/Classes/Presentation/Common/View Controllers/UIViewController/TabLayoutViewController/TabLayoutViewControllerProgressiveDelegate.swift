//
//  TabLayoutViewControllerProgressiveDelegate.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 11.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import CoreGraphics

protocol TabLayoutViewControllerProgressiveDelegate: TabLayoutViewControllerDelegate {
    
    /// Изменен индикатор
    func tabLayoutViewController(_ tabLayoutViewController: TabLayoutViewController,
                                 didUpdateIndicatorFromIndex fromIndex: Int,
                                 toIndex: Int,
                                 withProgressPercentage progressPercentage: CGFloat,
                                 indexChanged: Bool)
    
}
