//
//  TabLayoutViewController+Behaviour.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension TabLayoutViewController {
    
    /// Поведение
    enum Behaviour {
        
        /// Изменение состояния при преодолении середины перехода между экранами
        case common(skipIntermediateViewControllers: Bool)
        /// Поведение, в зависимости от состояния перехода
        case progressive(skipIntermediateViewControllers: Bool)
        
    }
    
}


// MARK: - Публичные свойства

extension TabLayoutViewController.Behaviour {
    
    /// Индикатор, изменяющийся в зависимости от состояния перехода
    var isProgressiveIndicator: Bool {
        switch self {
        case .common:
            return false
        case .progressive:
            return true
        }
    }
    
    /// Признак, указывающий на необходимость пропуска промежуточных элемнетов при переходе
    var skipIntermediateViewControllers: Bool {
        switch self {
        case .common(let skipIntermediateViewControllers):
            return skipIntermediateViewControllers
        case .progressive(let skipIntermediateViewControllers):
            return skipIntermediateViewControllers
        }
    }
    
}
