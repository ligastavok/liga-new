//
//  TabLayoutViewControllerDelegate.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol TabLayoutViewControllerDelegate: class {
    
    /// Изменен индикатор
    func tabLayoutViewController(_ tabLayoutViewController: TabLayoutViewController,
                                 didUpdateIndicatorFromIndex fromIndex: Int,
                                 toIndex: Int)
    
}
