//
//  PullUpDismissAnimationController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class PullUpDismissAnimationController: NSObject { }


// MARK: - UIViewControllerAnimatedTransitioning

extension PullUpDismissAnimationController: UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.25
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        
        guard let fromViewController = transitionContext.viewController(forKey: .from) else { return }
        
        // Анимация перехода
        let animationDuration = transitionDuration(using: transitionContext)
        let animations = {
            fromViewController.view.frame.origin.y = containerView.frame.size.height
        }
        let completion: ((Bool) -> Void) = { finished in
            transitionContext.completeTransition(finished)
        }
        
        if #available(iOS 10.0, *) {
            let propertyAnimator = UIViewPropertyAnimator(duration: animationDuration,
                                                          timingParameters: UISpringTimingParameters(dampingRatio: 1.3))
            propertyAnimator.addAnimations(animations)
            propertyAnimator.addCompletion { completion($0 == .end) }
            
            propertyAnimator.startAnimation()
        } else {
            UIView.animate(withDuration: animationDuration,
                           delay: 0,
                           options: [.curveEaseIn],
                           animations: animations,
                           completion: completion)
        }
    }

}
