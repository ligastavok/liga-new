//
//  PullUpMinimalPosition.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 02.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import CoreGraphics

/// Минимальное расстояние
enum PullUpMinimalPosition {
    
    /// Расстояние в точках
    case points(value: CGFloat)
    /// Расстояние в процентах
    case percentages(value: CGFloat)
    
}
