//
//  PullUpPresentable.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

/// Информация об отображаемом объекте
protocol PullUpPresentable: class {
    
    /// Высота содержимого
    var contentHeight: CGFloat? { get }
    
    /// Нижний отступ
    var bottomOffset: CGFloat? { get }
    
    /// Scroll view, содержащийся на view controller'е
    var contentScrollView: UIScrollView? { get }
    
    /// Коэффициент перемещения
    var translationMultiplier: CGFloat? { get }
    
    /// Смещения для закрытия
    var dismissOffset: PullUpDismissOffset { get }
    
    /// Методы закрытия
    var dismissMethods: PullUpDismissMethod { get }
    
}
