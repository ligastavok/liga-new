//
//  PullUpPresentationManager.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class PullUpPresentationManager: NSObject { }


// MARK: - UIViewControllerTransitioningDelegate

extension PullUpPresentationManager: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        return PullUpPresentAnimationController()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return PullUpDismissAnimationController()
    }
    
    func presentationController(forPresented presented: UIViewController,
                                presenting: UIViewController?,
                                source: UIViewController) -> UIPresentationController? {
        
        let presentationController = PullUpPresentationController(presentedViewController: presented,
                                                                  presenting: source)
        return presentationController
    }
    
}
