//
//  PullUpAppearanceProvider.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

/// Объект, обеспечивающий доступ к настройкам отображения
protocol PullUpAppearanceProvider: class {
    
    /// Цвет затемнения
    var dimmingColor: UIColor { get }

    /// Прозрачность затемнения
    var dimmingAlpha: CGFloat { get }
    
    /// Минимальное расстояние до верха
    var minimalPosition: PullUpMinimalPosition { get }
    
    /// Радиус скругления для отображаемого контента
    var contentCornerRadius: CGFloat? { get }
    
}
