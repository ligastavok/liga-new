//
//  PullUpPresenting.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol PullUpPresenting: class {
    
    /// View controller, с которого осуществляется навигация
    var viewController: UIViewController? { get }
    /// Менеджер перехода
    var presentationManager: PullUpPresentationManager? { get }
    
    /// Отобразить view controller
    func pullUp(_ viewController: UIViewController, animated: Bool, completion: (() -> Void)?)
    
}

extension PullUpPresenting {
    
    func pullUp(_ viewController: UIViewController, animated: Bool, completion: (() -> Void)? = nil) {
        viewController.transitioningDelegate = self.presentationManager
        viewController.modalPresentationStyle = .custom
        
        self.viewController?.present(viewController, animated: true, completion: completion)
    }
    
}
