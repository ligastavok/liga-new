//
//  PullUpHandleProvider.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

/// Объект, обеспечивающий доступ к индикатору касания
protocol PullUpHandleProvider: class {
    
    /// Индикатор касания
    var handleView: UIView! { get }
    
}
