//
//  PullUpPresentationController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class PullUpPresentationController: UIPresentationController {
    
    // MARK: - Приватные свойства
    
    /// Верхний отображаемый view controller
    private lazy var topPresentedViewController: UIViewController = {
        if let navigationController = self.presentedViewController as? UINavigationController,
            let firstViewController = navigationController.viewControllers.first {
            
            return firstViewController
        }
        
        return self.presentedViewController
    }()
    
    /// Информация об отображаемом объекте
    private lazy var presentable: PullUpPresentable? = {
        return self.topPresentedViewController as? PullUpPresentable
    }()
    
    /// Объект, обеспечивающий доступ к индикатору касания
    private lazy var handleProvider: PullUpHandleProvider? = {
        return self.topPresentedViewController as? PullUpHandleProvider
    }()
    
    /// Объект, обеспечивающий доступ к настройкам отображения
    private lazy var appearanceProvider: PullUpAppearanceProvider? = {
        return self.topPresentedViewController as? PullUpAppearanceProvider
    }()
    
    /// Подписка на изменения позиции содержимого в scroll view
    private var scrollViewObserver: NSKeyValueObservation?
    
    /// Жест смахивания по индикатору касания для закрытия
    private lazy var handleDismissingPanGestureRecognizer: UIPanGestureRecognizer = {
        return .init(target: self, action: #selector(handleDismissingPan(_:)))
    }()
    
    /// Жест смахивания по фону для закрытия
    private lazy var backgroundDismissingPanGestureRecognizer: UIPanGestureRecognizer = {
        return .init(target: self, action: #selector(handleDismissingPan(_:)))
    }()
    /// Жест касания по фону для закрытия
    private lazy var backgroundDismissingTapGestureRecognizer: UITapGestureRecognizer = {
        return .init(target: self, action: #selector(handleDismissingTap))
    }()
    
    /// Задний фон для отображаемого view controller'а
    private lazy var dimmingView: UIView = {
        let dimmingView = UIView()
        dimmingView.backgroundColor = self.dimmingColor
        dimmingView.alpha = 0
        
        return dimmingView
    }()
    
    /// Размер содержимого
    private var contentFrame: CGRect = .zero
    
}


// MARK: - Приватные свойства

private extension PullUpPresentationController {
    
    /// Высота содержимого
    var contentHeight: CGFloat? {
        return self.presentable?.contentHeight
    }
    
    /// Нижний отступ
    var bottomOffset: CGFloat {
        return self.presentable?.bottomOffset ?? UIWindow.keyWindowSafeAreaInsets.bottom
    }
    
    /// Scroll view, содержащийся на view controller’е
    var contentScrollView: UIScrollView? {
        return self.presentable?.contentScrollView
    }
    
    /// Коэффициент перемещения
    var translationMultiplier: CGFloat {
        return self.presentable?.translationMultiplier ?? 0.5
    }
    
    /// Смещения для закрытия
    var dismissOffset: CGFloat {
        let dismissOffset = self.presentable?.dismissOffset ?? .points(value: 100)
        
        switch dismissOffset {
        case .points(let value):
            return value
        case .percentages(let value):
            return self.contentFrame.height * value
        }
    }
    
    /// Методы закрытия
    var dismissMethods: PullUpDismissMethod {
        return self.presentable?.dismissMethods ?? [.swipe, .tap]
    }
    
    /// Индикатор касания
    var handleView: UIView? {
        return self.handleProvider?.handleView
    }
    
    /// Цвет затемнения
    var dimmingColor: UIColor {
        return self.appearanceProvider?.dimmingColor ?? .black
    }
    
    /// Прозрачность затемнения
    var dimmingAlpha: CGFloat {
        return self.appearanceProvider?.dimmingAlpha ?? 0.45
    }
    
    /// Минимальное расстояние до верха
    var minimalPosition: PullUpMinimalPosition {
        return self.appearanceProvider?.minimalPosition ?? .percentages(value: 0.25)
    }
    
    /// Радиус скругления для отображаемого контента
    var contentCornerRadius: CGFloat {
        return self.appearanceProvider?.contentCornerRadius ?? 12
    }
    
    /// Минимальный верхний отступ
    var minimalTopOffset: CGFloat {
        return max(UIWindow.keyWindowSafeAreaInsets.top, UIApplication.shared.statusBarFrame.height)
    }
    
}


// MARK: - UIContentContainer

extension PullUpPresentationController {
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        var minimalHeight: CGFloat = {
            let parentHeight = parentSize.height - (self.minimalTopOffset + self.bottomOffset)
            
            switch self.minimalPosition {
            case .points(let value):
                return parentHeight - value
            case .percentages(let value):
                return parentHeight * (1 - value)
            }
        }()
        
        if let contentHeight = self.contentHeight, contentHeight < minimalHeight {
            minimalHeight = contentHeight
        }
        
        return .init(width: parentSize.width, height: minimalHeight + self.bottomOffset)
    }
    
}


// MARK: - UIPresentationController

extension PullUpPresentationController {
    
    override var presentedView: UIView? {
        return super.presentedView
    }
    
    override func containerViewWillLayoutSubviews() {
        self.contentFrame = self.frameOfPresentedViewInContainerView
        self.presentedView?.frame = self.contentFrame
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        guard let containerView = self.containerView else { return .zero }
        
        let size = self.size(forChildContentContainer: self.presentedViewController,
                             withParentContainerSize: containerView.bounds.size)
        
        let originY = containerView.bounds.height - size.height
        let origin: CGPoint = .init(x: 0, y: originY)
        
        return .init(origin: origin, size: size)
    }
    
    override var shouldPresentInFullscreen: Bool {
        return false
    }
    
    override func presentationTransitionWillBegin() {
        guard let containerView = self.containerView,
            let presentedView = self.presentedView else { return }
        
        // Настройка заднего фона
        containerView.addSubview(self.dimmingView)
        self.dimmingView.pinEdgesToSuperview()
        
        // Настройка отображаемого view controller'а
        if self.contentCornerRadius > 0 {
            presentedView.round(corners: [.topLeft, .topRight], radius: self.contentCornerRadius)
        }
        
        // Анимация отображения
        guard let coordinator = self.presentedViewController.transitionCoordinator else {
            self.dimmingView.alpha = self.dimmingAlpha
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = self.dimmingAlpha
        })
        
        self.presentedView?.layoutIfNeeded()
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool) {
        guard completed else { return }
        
        // Добавление жестов
        if self.dismissMethods.isSwipeAllowed {
            self.dimmingView.addGestureRecognizer(self.backgroundDismissingPanGestureRecognizer)
            self.handleView?.addGestureRecognizer(self.handleDismissingPanGestureRecognizer)
        }
        if self.dismissMethods.isTapAllowed {
            self.dimmingView.addGestureRecognizer(self.backgroundDismissingTapGestureRecognizer)
        }
        
        // Подписка на изменения scroll view
        self.scrollViewObserver = self.contentScrollView?.observe(\.contentOffset, options: [.new]) { [weak self] scrollView, observedChange in
            guard let offset = observedChange.newValue else { return }
            self?.updateContent(in: scrollView, forOffset: offset.y)
        }
    }
    
    override func dismissalTransitionWillBegin() {
        guard let presentedView = self.presentedView else { return }
        
        // Отмена редактирования
        presentedView.endEditing(true)
        
        // Анимация отображения
        guard let coordinator = self.presentedViewController.transitionCoordinator else {
            self.dimmingView.alpha = 0
            return
        }
        
        coordinator.animate(alongsideTransition: { _ in
            self.dimmingView.alpha = 0
        })
    }
    
    override func dismissalTransitionDidEnd(_ completed: Bool) {
        guard completed else { return }
        
        self.scrollViewObserver?.invalidate()
        self.scrollViewObserver = nil
    }
    
}


// MARK: - Приватные методы

private extension PullUpPresentationController {
    
    /// Обновление с учетом позиции
    func update(for offset: CGFloat, in presentedView: UIView) {
        guard offset < self.contentFrame.size.height else { return }
        
        let originY = max(self.contentFrame.origin.y + offset, self.contentFrame.origin.y)
        presentedView.frame.origin.y = originY
        
        self.dimmingView.alpha = {
            let percentage = (self.contentFrame.size.height - offset) / self.contentFrame.size.height
            return percentage * self.dimmingAlpha
        }()
        
        if offset >= self.dismissOffset {
            if #available(iOS 10.0, *) {
                let feedbackGenerator = UIImpactFeedbackGenerator(style: .medium)
                feedbackGenerator.impactOccurred()
            }
            
            dismiss()
        }
    }
    
    /// Обновление, с учетом позиции содержимого в scroll view
    func updateContent(in scrollView: UIScrollView, forOffset offsetY: CGFloat) {
        guard let presentedView = self.presentedView, scrollView.isUserInteractionEnabled else { return }
        
        guard offsetY <= 0 else {
            if scrollView.contentSize.height <= scrollView.bounds.height {
                scrollView.setContentOffset(.zero, animated: false)
            } else if scrollView.contentSize.height - scrollView.bounds.height < offsetY {
                scrollView.setContentOffset(.init(x: 0, y: scrollView.contentSize.height - scrollView.bounds.height),
                                            animated: false) 
            }
            
            return
        }
        
        let scrollViewTransform = CGAffineTransform(translationX: 0, y: offsetY)
        scrollView.transform = scrollViewTransform
        
        update(for: abs(offsetY), in: presentedView)
    }
    
    /// Обработка жеста смахивания, осуществляемого для закрытия
    @objc
    func handleDismissingPan(_ gestureRecognizer: UIPanGestureRecognizer) {
        guard let presentedView = self.presentedView else { return }

        let translationPoint = gestureRecognizer.translation(in: self.presentedView)
        let offsetY = max(translationPoint.y * self.translationMultiplier, 0)
        
        switch gestureRecognizer.state {
        case .changed, .began:
            update(for: offsetY, in: presentedView)
        case .ended:
            if offsetY >= self.dismissOffset {
                dismiss()
            } else {
                UIView.animate(withDuration: 0.2) {
                    presentedView.frame.origin.y = self.contentFrame.origin.y
                    self.dimmingView.alpha = self.dimmingAlpha
                }
            }
            
        default:
            break
        }
    }
    
    /// Обработка жеста касания, осуществляемого для закрытия
    @objc
    func handleDismissingTap() {
        dismiss()
    }
    
    /// Закрытие экрана
    func dismiss() {
        self.dimmingView.removeGestureRecognizer(self.backgroundDismissingTapGestureRecognizer)
        self.dimmingView.removeGestureRecognizer(self.backgroundDismissingPanGestureRecognizer)
        self.handleView?.removeGestureRecognizer(self.handleDismissingPanGestureRecognizer)
        
        self.contentScrollView?.isUserInteractionEnabled = false
        
        self.presentingViewController.dismiss(animated: true)
    }
    
}
