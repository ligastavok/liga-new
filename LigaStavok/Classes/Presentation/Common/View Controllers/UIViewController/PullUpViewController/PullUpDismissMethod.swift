//
//  PullUpDismissMethod.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Метод закрытия
struct PullUpDismissMethod: OptionSet {
    
    // MARK: - OptionSet
    
    let rawValue: Int
    
    
    // MARK: - Публичные свойства
    
    /// Закрытие по касанию
    static let tap: PullUpDismissMethod = .init(rawValue: 1 << 0)
    /// Закрытие по смахиванию
    static let swipe: PullUpDismissMethod = .init(rawValue: 1 << 1)
    
}


// MARK: - Публичные свойства

extension PullUpDismissMethod {
    
    /// Возможно закрытие по касанию
    var isTapAllowed: Bool {
        return self.contains(.tap)
    }
    
    /// Возможно закрытие по смахиванию
    var isSwipeAllowed: Bool {
        return self.contains(.swipe)
    }
    
}
