//
//  PullUpPresentAnimationController.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class PullUpPresentAnimationController: NSObject { }


// MARK: - UIViewControllerAnimatedTransitioning

extension PullUpPresentAnimationController: UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        
        guard let toViewController = transitionContext.viewController(forKey: .to) else { return }
        
        // Добавление конечного контроллера в иерархию контейнера, в случае отображения
        containerView.addSubview(toViewController.view)
        
        // Вычисление фреймов для видимого и невидимого состояния
        let presentedFrame = transitionContext.finalFrame(for: toViewController)
        let dismissedFrame: CGRect = {
            var dismissedFrame = presentedFrame
            dismissedFrame.origin.y = containerView.frame.size.height
            
            return dismissedFrame
        }()
        
        toViewController.view.frame = dismissedFrame
        
        // Анимация перехода
        let animationDuration = transitionDuration(using: transitionContext)
        let animations = {
            toViewController.view.frame = presentedFrame
        }
        let completion: ((Bool) -> Void) = { finished in
            transitionContext.completeTransition(finished)
        }
        
        if #available(iOS 10.0, *) {
            let propertyAnimator = UIViewPropertyAnimator(duration: animationDuration,
                                                          timingParameters: UISpringTimingParameters(dampingRatio: 1))
            propertyAnimator.addAnimations(animations)
            propertyAnimator.addCompletion { completion($0 == .end) }
            
            propertyAnimator.startAnimation()
        } else {
            UIView.animate(withDuration: animationDuration,
                           delay: 0,
                           options: [.curveEaseOut],
                           animations: animations,
                           completion: completion)
        }
    }
    
}
