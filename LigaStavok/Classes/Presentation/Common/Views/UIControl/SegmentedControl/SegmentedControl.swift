//
//  SegmentedControl.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 25.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class SegmentedControl: UIControl {
    
    // MARK: - UIView
    
    override var backgroundColor: UIColor? {
        willSet {
            self.shapeLayer?.fillColor = newValue?.cgColor
        }
    }
    
    
    // MARK: - Публичные свойства
    
    /// Индекс выбранного элемента
    private(set) var selectedSegmentIndex = 0
    
    /// Отступ выбранного элемента
    @IBInspectable
    var selectedSegmentInset: CGFloat = 0 {
        didSet {
            setNeedsLayout()
        }
    }
    
    /// Шрифт
    var titleFont: UIFont = .systemFont(ofSize: 13) {
        willSet {
            self.titlesLabels.forEach { $0.font = newValue }
            self.selectedTitlesLabels.forEach { $0.font = newValue }
        }
    }
    
    /// Цвет надписей
    @IBInspectable
    var titleColor: UIColor! {
        willSet {
            self.titlesLabels.forEach { $0.textColor = newValue }
        }
    }
    
    /// Цвет надписи выбранного элемента
    @IBInspectable
    var selectedTitleColor: UIColor! {
        willSet {
            self.selectedTitlesLabels.forEach { $0.textColor = newValue }
        }
    }
    
    /// Длительность анимации
    var animationDuration: TimeInterval = 0.2
    
    /// Цвет рамки
    @IBInspectable
    var borderColor: UIColor? {
        willSet {
            self.borderShapeLayer.fillColor = newValue?.cgColor
        }
    }
    
    /// Толщина рамки
    @IBInspectable
    var borderWidth: CGFloat = 1 {
        willSet {
            guard !self.titlesLabels.isEmpty else { return }
            configure()
        }
    }
    
    /// Радиус скругления
    @IBInspectable
    var cornerRadius: CGFloat = 6 {
        didSet {
            guard !self.titlesLabels.isEmpty else { return }
            configure()
        }
    }
    
    /// Разделитель для инициализации в одну строку
    @IBInspectable
    var singleLineTitlesSeparator: String = " | "
    
    
    // MARK: - Приватные свойства
    
    /// Маска
    private weak var maskShapeLayer: CAShapeLayer!
    /// Рамка
    private weak var borderShapeLayer: CAShapeLayer!
    
    /// Контейнер
    private weak var containerView: UIView!
    /// Надписи
    private var titlesLabels: [UILabel] = []
    
    /// Задний фон выбранного элемента
    private weak var selectedSegmentBackgroundView: UIView!
    /// Положение заднего фона при начале движения
    private var initialSelectedSegmentBackgroundViewFrame: CGRect?
    
    /// Контейнер для выбранных элементов
    private weak var selectedContainerView: UIView!
    /// Выбранные надписи
    private var selectedTitlesLabels: [UILabel] = []
    
    /// Маска для надписей
    private var selectedContainerMaskView: UIView!
    
    /// Распознание касания
    private lazy var tapGestureRecognizer: UITapGestureRecognizer = {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        return gestureRecognizer
    }()
    
    /// Распознание движения
    private lazy var panGestureRecognizer: UIPanGestureRecognizer = {
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(gesture:)))
        gestureRecognizer.delegate = self
        
        return gestureRecognizer
    }()
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Маска
        let maskShapeLayer = CAShapeLayer()
        self.layer.mask = maskShapeLayer
        self.maskShapeLayer = maskShapeLayer
        
        // Контейнер
        let containerView = UIView()
        addSubview(containerView)
        self.containerView = containerView
        
        // Рамка
        let borderShapeLayer = CAShapeLayer()
        borderShapeLayer.fillRule = kCAFillRuleEvenOdd
        self.containerView.layer.addSublayer(borderShapeLayer)
        self.borderShapeLayer = borderShapeLayer
        
        // Задний фон выбранного элемента
        let selectedSegmentBackgroundView = UIView()
        object_setClass(selectedSegmentBackgroundView.layer, CAShapeLayer.self)
        addSubview(selectedSegmentBackgroundView)
        self.selectedSegmentBackgroundView = selectedSegmentBackgroundView
        
        // Контейнер для выбранных элементов
        let selectedContainerView = UIView()
        addSubview(selectedContainerView)
        self.selectedContainerView = selectedContainerView
        
        // Маска для надписей
        let selectedContainerMaskView = UIView()
        selectedContainerMaskView.backgroundColor = .black
        object_setClass(selectedContainerMaskView.layer, CAShapeLayer.self)
        self.selectedContainerView.layer.mask = selectedContainerMaskView.layer
        self.selectedContainerMaskView = selectedContainerMaskView
        
        // Настройка цветов
        self.borderColor = self.tintColor
        self.selectedSegmentColor = self.tintColor
        self.titleColor = self.tintColor
        self.selectedTitleColor = .white
        
        // Настройка рамки
        self.borderWidth = 1
        
        // Настройка жестов
        addGestureRecognizer(self.tapGestureRecognizer)
        addGestureRecognizer(self.panGestureRecognizer)
    }
    
}


// MARK: - Публичные свойства

extension SegmentedControl {
    
    /// Элементы в строку
    @IBInspectable
    var singleLineTitles: String {
        get {
            return self.titles.joined(separator: self.singleLineTitlesSeparator)
        }
        set {
            self.titles = newValue.components(separatedBy: self.singleLineTitlesSeparator)
        }
    }
    
    /// Элементы
    var titles: [String] {
        get {
            return self.titlesLabels.compactMap { $0.text }
        }
        set {
            self.titlesLabels.forEach { $0.removeFromSuperview() }
            self.selectedTitlesLabels.forEach { $0.removeFromSuperview() }
            
            self.titlesLabels = newValue.map { title in
                let label = UILabel()
                label.text = title
                label.textColor = self.titleColor
                label.font = self.titleFont
                label.textAlignment = .center
                label.lineBreakMode = .byTruncatingTail
                
                self.containerView.addSubview(label)
                return label
            }
            
            self.selectedTitlesLabels = newValue.map { title in
                let label = UILabel()
                label.text = title
                label.textColor = self.selectedTitleColor
                label.font = self.titleFont
                label.textAlignment = .center
                label.lineBreakMode = .byTruncatingTail
                
                self.selectedContainerView.addSubview(label)
                return label
            }
        }
    }
    
    /// Цвет выбранного элемента
    @IBInspectable
    var selectedSegmentColor: UIColor? {
        get {
            return self.selectedSegmentBackgroundViewShapeLayer?.fillColor.map { UIColor(cgColor: $0) }
        }
        set {
            self.selectedSegmentBackgroundViewShapeLayer?.fillColor = newValue?.cgColor
        }
    }
    
}


// MARK: - Приватные свойства

private extension SegmentedControl {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
    /// Слой заднего фона выбранного элемента
    var selectedSegmentBackgroundViewShapeLayer: CAShapeLayer? {
        return self.selectedSegmentBackgroundView.layer as? CAShapeLayer
    }
    
    /// Слой маски для надписей
    var selectedContainerMaskViewShapeLayer: CAShapeLayer? {
        return self.selectedContainerMaskView.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension SegmentedControl {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        configure()
    }
    
}


// MARK: - Публичные методы

extension SegmentedControl {
    
    /// Выбрать элемент
    func selectSegment(atIndex index: Int, animated: Bool) {
        guard index < self.titlesLabels.count else { return }
        
        var isCanceled = false
        if self.selectedSegmentIndex == index {
            isCanceled = true
        }
        
        self.selectedSegmentIndex = index
        
        UIView.animate(withDuration: animated ? self.animationDuration : 0,
                       delay: 0,
                       options: [.beginFromCurrentState, .curveEaseOut],
                       animations: {
            self.setNeedsLayout()
            self.layoutIfNeeded()
        })
        
        guard !isCanceled else { return }
        sendActions(for: .valueChanged)
    }
    
}


// MARK: - Приватные методы

private extension SegmentedControl {
    
    /// Обработка касания
    @objc
    func handleTap(gesture: UIPanGestureRecognizer) {
        let location = gesture.location(in: self)
        let segmentWidth = self.bounds.width / CGFloat(self.titlesLabels.count)
        let index = Int(location.x / segmentWidth)
        
        selectSegment(atIndex: index, animated: true)
    }
    
    /// Обработка движения
    @objc
    func handlePan(gesture: UIPanGestureRecognizer) {
        switch gesture.state {
        case .began:
            self.initialSelectedSegmentBackgroundViewFrame = self.selectedSegmentBackgroundView.frame
        case .changed:
            var frame = self.initialSelectedSegmentBackgroundViewFrame ?? .zero
            
            let calculatedX = frame.origin.x + gesture.translation(in: self).x
            
            let leftBounds = self.selectedSegmentInset
            let rigthBounds = self.bounds.width - self.selectedSegmentInset - frame.width
            
            frame.origin.x = max(leftBounds, min(rigthBounds, calculatedX))
            
            self.selectedSegmentBackgroundView.frame = frame
            self.selectedContainerMaskView.frame = frame
        case .ended, .failed, .cancelled:
            let segmentWidth = self.bounds.width / CGFloat(self.titlesLabels.count)
            let calculatedIndex = Int(self.selectedSegmentBackgroundView.center.x / segmentWidth)
            
            let minIndex = 0
            let maxIndex = self.titlesLabels.count - 1
            
            let index = max(minIndex, min(maxIndex, calculatedIndex))
            selectSegment(atIndex: index, animated: true)
            
        default:
            break
        }
    }
    
    /// Настройка
    func configure() {
        
        // Настройка границ
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.cornerRadius)
        self.shapeLayer?.path = path.cgPath
        
        // Настройка маски
        self.maskShapeLayer.path = path.cgPath
        
        // Настройка рамки
        let borderInnerRectWidth = self.bounds.width - self.borderWidth * 2
        let borderInnerRectHeight = self.bounds.height - self.borderWidth * 2
        let borderInnerRect = CGRect(x: self.borderWidth,
                                     y: self.borderWidth,
                                     width: borderInnerRectWidth,
                                     height: borderInnerRectHeight)
        
        let borderInnerPath = UIBezierPath(roundedRect: borderInnerRect, cornerRadius: self.cornerRadius)
        
        let borderPath = UIBezierPath()
        borderPath.append(path)
        borderPath.append(borderInnerPath)
        
        self.borderShapeLayer.path = borderPath.cgPath
        
        // Настройка контейнеров
        self.containerView.frame = self.bounds
        self.selectedContainerView.frame = self.bounds
        
        // Настройка заднего фона выбранного элемента
        let segmentWidth = self.bounds.width / CGFloat(self.titlesLabels.count)
        let segmentHeight = self.bounds.height
        
        let selectedSegmentOriginX = self.selectedSegmentInset + CGFloat(self.selectedSegmentIndex) * segmentWidth
        let selectedSegmentOriginY = self.selectedSegmentInset
        let selectedSegmentWidth = segmentWidth - self.selectedSegmentInset * 2
        let selectedSegmentHeight = segmentHeight - self.selectedSegmentInset * 2
        
        let selectedSegmentFrame = CGRect(x: selectedSegmentOriginX,
                                          y: selectedSegmentOriginY,
                                          width: selectedSegmentWidth,
                                          height: selectedSegmentHeight)
        self.selectedSegmentBackgroundView.frame = selectedSegmentFrame
        
        let selectedSegmentPath = UIBezierPath(roundedRect: self.selectedSegmentBackgroundView.bounds,
                                               cornerRadius: self.cornerRadius)
        self.selectedSegmentBackgroundViewShapeLayer?.path = selectedSegmentPath.cgPath
        
        // Настройка маски для надписей
        self.selectedContainerMaskView.frame = selectedSegmentFrame
        self.selectedContainerMaskViewShapeLayer?.path = selectedSegmentPath.cgPath
        
        // Настройка надписей
        zip(self.titlesLabels, self.selectedTitlesLabels).enumerated().forEach { item in
            let title = item.element.0
            let selectedTitle = item.element.1
            
            let originX = floor(self.selectedSegmentInset + segmentWidth * CGFloat(item.offset))
            let originY = self.selectedSegmentInset
            
            let origin = CGPoint(x: originX, y: originY)
            let size = self.selectedSegmentBackgroundView.frame.size
            let frame = CGRect(origin: origin, size: size)
            
            title.frame = frame
            selectedTitle.frame = frame
        }
    }
    
}


// MARK: - UIGestureRecognizerDelegate

extension SegmentedControl: UIGestureRecognizerDelegate {
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.panGestureRecognizer {
            return self.selectedSegmentBackgroundView.frame.contains(gestureRecognizer.location(in: self))
        }
        
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
    
}
