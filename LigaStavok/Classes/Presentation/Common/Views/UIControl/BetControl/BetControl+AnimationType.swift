//
//  BetControl+AnimationType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 18.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension BetControl {
    
    /// Тип анимации
    enum AnimationType {
        
        /// Мигание границы
        case borderBlinking
        /// Подсвечивание надписи
        case highlightingValue
        
    }
    
}


// MARK: - Публичные свойства

extension BetControl.AnimationType {
    
    /// Продолжительность анимации
    var animationDuration: TimeInterval {
        switch self {
        case .borderBlinking:
            return 3
        case .highlightingValue:
            return 3
        }
    }
    
}


// MARK: - Публичные методы

extension BetControl.AnimationType {
    
    /// Получить цвет для типа изменения коэффициента
    func color(for valueChangeType: BetValueChangeType) -> UIColor? {
        switch self {
        case .borderBlinking:
            switch valueChangeType {
            case .ascending:
                return #colorLiteral(red: 0, green: 0.831372549, blue: 0.6431372549, alpha: 1)
            case .descending:
                return #colorLiteral(red: 0.8745098039, green: 0.1960784314, blue: 0.1411764706, alpha: 1)
            case .none:
                return nil
            }
        case .highlightingValue:
            switch valueChangeType {
            case .ascending:
                return #colorLiteral(red: 0, green: 0.831372549, blue: 0.6431372549, alpha: 1)
            case .descending:
                return #colorLiteral(red: 0.8745098039, green: 0.1960784314, blue: 0.1411764706, alpha: 1)
            case .none:
                return .black
            }
        }
    }
    
}
