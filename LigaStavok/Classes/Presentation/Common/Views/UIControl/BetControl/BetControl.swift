//
//  BetControl.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class BetControl: UIControl, NibLoadableSelfOwnerView {
    
    // MARK: - UIControl
    
    override var isEnabled: Bool {
        didSet {
            setNeedsLayout()
        }
    }
    
    
    // MARK: - Outlet
    
    /// Название типа
    @IBOutlet
    private weak var betTypeLabel: UILabel!
    /// Значение
    @IBOutlet
    private weak var valueLabel: UILabel!
    
    
    // MARK: - Публичные свойства
    
    /// Тип анимации
    var animationType: AnimationType = .highlightingValue
    
    /// Состояние
    @IBInspectable
    var isActive: Bool = false {
        didSet {
            setNeedsLayout()
        }
    }
    
    /// Значение
    @IBInspectable
    var value: Double = 0 {
        willSet {
            if newValue == 0 {
                self.valueLabel.text = nil
            } else {
                let value = NSNumber(value: newValue)
                self.valueLabel.text = NumberFormatter.coefficientNumberFormater.string(from: value)
            }
            
            setNeedsLayout()
        }
    }
    
    /// Тип изменения коэффициента
    var valueChangeType: BetValueChangeType = .none {
        willSet {
            guard newValue != .none else { return }
            guard self.isEnabled && !self.isActive else { return }
            
            var stopDelay = self.animationType.animationDuration
            switch newValue {
            case .ascending(let lastUpdate):
                calculateAnimatingStopDelay(&stopDelay, withLastUpdate: lastUpdate)
            case .descending(let lastUpdate):
                calculateAnimatingStopDelay(&stopDelay, withLastUpdate: lastUpdate)
                
            case .none:
                break
            }
            
            guard stopDelay > 0 else { return }
            animate(stopDelay: stopDelay, valueChangeType: newValue)
        }
    }
    
    
    // MARK: - Приватные свойства
    
    /// Маска
    private weak var maskShapeLayer: CAShapeLayer!
    
    /// Граница типа
    private weak var betTypeBorderShapeLayer: CAShapeLayer!
    /// Граница значения
    private weak var valueBorderShapeLayer: CAShapeLayer!
    
    /// Мигающая граница
    private weak var blinkingBorderView: UIView?
    /// Цвет подсвеченного значения
    private var valueHighlightedColor: UIColor?
    
    /// Таймер анимации
    private var animatingTimer: Timer?
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
        // Маска
        let maskShapeLayer = CAShapeLayer()
        self.layer.mask = maskShapeLayer
        self.maskShapeLayer = maskShapeLayer
        
        // Граница типа
        let betTypeBorderShapeLayer = CAShapeLayer()
        betTypeBorderShapeLayer.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.betTypeLabel.layer.addSublayer(betTypeBorderShapeLayer)
        self.betTypeBorderShapeLayer = betTypeBorderShapeLayer
        
        // Граница значения
        let valueBorderShapeLayer = CAShapeLayer()
        valueBorderShapeLayer.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.valueLabel.layer.addSublayer(valueBorderShapeLayer)
        self.valueBorderShapeLayer = valueBorderShapeLayer
        
        // Мигающая граница
        let blinkingBorderView = UIView()
        object_setClass(blinkingBorderView.layer, CAShapeLayer.self)
        blinkingBorderView.alpha = 0
        blinkingBorderView.backgroundColor = .clear
        blinkingBorderView.isUserInteractionEnabled = false
        addSubview(blinkingBorderView)
        self.blinkingBorderView = blinkingBorderView
        self.blinkingBorderViewShapeLayer?.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.blinkingBorderViewShapeLayer?.lineWidth = 1
    }
    
}


// MARK: - Публичные свойства

extension BetControl {
    
    /// Название типа
    @IBInspectable
    var betType: String? {
        get {
            return self.betTypeLabel.text
        }
        set {
            self.betTypeLabel.text = newValue
        }
    }
    
}


// MARK: - Приватные свойства

private extension BetControl {
    
    /// Слой мигающей границы
    var blinkingBorderViewShapeLayer: CAShapeLayer? {
        return self.blinkingBorderView?.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension BetControl {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let padding: CGFloat = 10
        let extendedBounds = self.bounds.insetBy(dx: -padding, dy: -padding)
        
        return extendedBounds.contains(point)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.betTypeLabel.layoutIfNeeded()
        self.valueLabel.layoutIfNeeded()
        
        // Маска
        let maskPath = UIBezierPath(roundedRect: self.bounds,
                                    topLeftRadius: 6,
                                    topRightRadius: 4,
                                    bottomLeftRadius: 6,
                                    bottomRightRadius: 4)
        self.maskShapeLayer.path = maskPath?.cgPath
        
        // Граница типа
        let betTypeBorderPath = createBorderPathForBetTypeLabel()
        self.betTypeBorderShapeLayer.path = betTypeBorderPath
        
        // Граница значения
        let valueBorderPath = createBorderPathForValueLabel()
        self.valueBorderShapeLayer.path = valueBorderPath
        
        // Мигающая граница
        self.blinkingBorderView?.frame = self.bounds
        
        let blinkingBorderPath = createPathForBlinkingBorder()
        self.blinkingBorderViewShapeLayer?.path = blinkingBorderPath
        
        // Настройка внешнего вида
        configure()
    }
    
}


// MARK: - Публичные методы

extension BetControl {
    
    /// Остановить анимацию
    func stopAnimation() {
        self.animatingTimer?.invalidate()
        
        switch self.animationType {
        case .borderBlinking:
            self.blinkingBorderView?.alpha = 0
            self.blinkingBorderView?.layer.removeAllAnimations()
        case .highlightingValue:
            self.valueHighlightedColor = nil
            self.valueLabel.textColor = .black
        }
    }
    
}


// MARK: - Приватные методы

private extension BetControl {
    
    /// Получить границу для типа
    func createBorderPathForBetTypeLabel() -> CGPath? {
        let path = UIBezierPath(roundedRect: self.betTypeLabel.bounds,
                                topLeftRadius: 6,
                                topRightRadius: 0,
                                bottomLeftRadius: 6,
                                bottomRightRadius: 0)
        return path?.cgPath
    }
    
    /// Получить границу для значения
    func createBorderPathForValueLabel() -> CGPath {
        let path = UIBezierPath()
        
        let rect = self.valueLabel.bounds
        let topRightRadius: CGFloat = 4
        let bottomRightRadius: CGFloat = 4

        let topLeft = CGPoint(x: rect.minX, y: rect.minY)
        let topRight = CGPoint(x: rect.maxX - topRightRadius, y: rect.minY + topRightRadius)
        let bottomLeft = CGPoint(x: rect.minX, y: rect.maxY)
        let bottomRight = CGPoint(x: rect.maxX - bottomRightRadius, y: rect.maxY - bottomRightRadius)

        path.move(to: topLeft)

        path.addLine(to: CGPoint(x: rect.maxX - topRightRadius, y: rect.minY))
        path.addArc(withCenter: topRight,
                    radius: topRightRadius,
                    startAngle: -.pi / 2,
                    endAngle: 0,
                    clockwise: true)

        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - bottomRightRadius))
        path.addArc(withCenter: bottomRight,
                    radius: bottomRightRadius,
                    startAngle: 0,
                    endAngle: .pi / 2,
                    clockwise: true)

        path.addLine(to: bottomLeft)
        
        return path.cgPath
    }
    
    /// Получить путь для мигающей границы
    func createPathForBlinkingBorder() -> CGPath? {
        guard let rect = self.blinkingBorderView?.frame.insetBy(dx: 0.5, dy: 0.5),
            let minY = self.blinkingBorderView?.frame.minY else { return nil }
        
        guard let path = UIBezierPath(roundedRect: rect,
                                      topLeftRadius: 6,
                                      topRightRadius: 4,
                                      bottomLeftRadius: 6,
                                      bottomRightRadius: 4) else { return nil }
        
        path.move(to: CGPoint(x: self.betTypeLabel.frame.width, y: minY))
        path.addLine(to: CGPoint(x: self.betTypeLabel.frame.width, y: self.bounds.maxY))
        
        return path.cgPath
    }
    
    /// Настройка
    func configure() {
        
        // Начало анимации
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        
        // Настройки границ
        let borderWidth: CGFloat = self.isEnabled ? (self.isActive ? 0 : 2) : 2
        let borderColor: CGColor? = self.isEnabled ? (self.isActive ? nil : #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)) : #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
        
        // Настройка типа
        self.betTypeBorderShapeLayer.lineWidth = borderWidth
        self.betTypeBorderShapeLayer.strokeColor = borderColor
        
        self.betTypeLabel.backgroundColor = self.isEnabled ? (self.isActive ? #colorLiteral(red: 0, green: 0.4156862745, blue: 0.2901960784, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)) : #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        
        // Настройка значения
        self.valueBorderShapeLayer.lineWidth = borderWidth
        self.valueBorderShapeLayer.strokeColor = borderColor
        
        self.valueLabel.layer.backgroundColor = self.isEnabled ? (self.isActive ? #colorLiteral(red: 0, green: 0.6039215686, blue: 0.431372549, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)) : #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        
        // Конец анимации
        CATransaction.commit()
        
        // Настройка цвета текста типа
        self.betTypeLabel.textColor = self.isActive ? .white : #colorLiteral(red: 0.5921568627, green: 0.5921568627, blue: 0.5921568627, alpha: 1)
        
        // Настройка цвета текста значения
        switch self.animationType {
        case .borderBlinking:
            self.valueLabel.textColor = self.isActive ? .white : .black
        case .highlightingValue:
            self.valueLabel.textColor = self.isActive ? .white : self.valueHighlightedColor ?? .black
        }
    }
    
    /// Вычисление времени анимации
    func calculateAnimatingStopDelay(_ stopDelay: inout TimeInterval, withLastUpdate lastUpdate: TimeInterval) {
        let now = Date.timeIntervalSinceReferenceDate
        stopDelay += lastUpdate - now
        
        guard stopDelay > 0 else { return }
        stopDelay = TimeInterval(ceil(stopDelay))
    }
    
    /// Анимировать
    func animate(stopDelay: TimeInterval, valueChangeType: BetValueChangeType) {
        switch self.animationType {
        case .borderBlinking:
            self.blinkingBorderViewShapeLayer?.strokeColor = self.animationType.color(for: valueChangeType)?.cgColor
            blink(stopDelay: stopDelay)
        case .highlightingValue:
            self.valueHighlightedColor = self.animationType.color(for: valueChangeType)
            self.valueLabel.setNeedsLayout()
            
            highlightValue(stopDelay: stopDelay)
        }
    }
    
    /// Начать мигание
    func blink(stopDelay: TimeInterval) {
        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: [.curveEaseInOut, .autoreverse, .repeat],
                       animations: { [weak self] in
            self?.blinkingBorderView?.alpha = 1
        }, completion: { [weak self] _ in
            self?.blinkingBorderView?.alpha = 0
        })
        
        let timer = Timer(timeInterval: stopDelay,
                          target: self,
                          selector: #selector(stopBlinking(_:)),
                          userInfo: nil,
                          repeats: false)
        self.animatingTimer = timer
        
        RunLoop.main.add(timer, forMode: .defaultRunLoopMode)
    }
    
    /// Начать подсветку значения
    func highlightValue(stopDelay: TimeInterval) {
        let timer = Timer(timeInterval: stopDelay,
                          target: self,
                          selector: #selector(stopHighlightingValue(_:)),
                          userInfo: nil,
                          repeats: false)
        self.animatingTimer = timer
        
        RunLoop.main.add(timer, forMode: .defaultRunLoopMode)
    }
    
    /// Вышло время таймера мигания
    @objc
    func stopBlinking(_ timer: Timer) {
        guard timer.isValid else { return }
        
        self.blinkingBorderView?.alpha = 0
        self.blinkingBorderView?.layer.removeAllAnimations()
    }
    
    /// Вышло время таймера подсветки
    @objc
    func stopHighlightingValue(_ timer: Timer) {
        guard timer.isValid else { return }
        
        self.valueHighlightedColor = nil
        self.valueLabel.textColor = .black
    }
    
}
