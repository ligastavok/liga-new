//
//  FavoritesControl.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 22.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class FavoritesControl: UIControl, NibLoadableSelfOwnerView {
    
    // MARK: - UIControl
    
    override var isEnabled: Bool {
        willSet {
            self.alpha = newValue ? 1 : 0.75
        }
    }
    
    override var isHighlighted: Bool {
        willSet {
            guard newValue != self.isHighlighted else { return }
            
            let backgroundColor = newValue ? self.color?.contrastColor : self.color
            UIView.animate(withDuration: 0.2) { [weak self] in
                self?.backgroundColor = backgroundColor
            }
        }
    }
    
    
    // MARK: - Публичные свойства
    
    @IBInspectable
    var isActive: Bool = false {
        didSet {
            configure()
        }
    }
    
    
    // MARK: - Приватные свойства
    
    /// Цвет заднего фона
    private var color: UIColor!
    
    /// Маска
    private weak var maskLayer: CAShapeLayer!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Настройка слоя
        self.shapeLayer?.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        // Маска
        let maskLayer = CAShapeLayer()
        self.layer.mask = maskLayer
        self.maskLayer = maskLayer
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
    }
    
}


// MARK: - Приватные свойства

private extension FavoritesControl {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension FavoritesControl {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(ovalIn: self.bounds)
        self.shapeLayer?.path = path.cgPath
        
        self.maskLayer.path = path.cgPath
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(square: 24)
    }
    
}


// MARK: - Приватные методы

private extension FavoritesControl {
    
    /// Настройка
    func configure() {
        self.color = self.isActive ? #colorLiteral(red: 1, green: 0.6745098039, blue: 0, alpha: 1) : #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        self.backgroundColor = self.color
    }
    
}
