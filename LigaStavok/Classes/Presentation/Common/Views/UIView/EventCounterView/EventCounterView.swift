//
//  EventCounterView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class EventCounterView: UIView, NibLoadableSelfOwnerView {
    
    // MARK: - Outlet
    
    /// Количество
    @IBOutlet
    private weak var countLabel: UILabel!
    
    
    // MARK: - Публичные свойства
    
    /// Тип событий
    @IBInspectable
    var isLive: Bool = false {
        willSet {
            let backgroundColor = newValue ? #colorLiteral(red: 0, green: 0.8078431373, blue: 0.5764705882, alpha: 1) : #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
            self.shapeLayer?.fillColor = backgroundColor.cgColor
            self.countLabel.backgroundColor = backgroundColor
            
            self.countLabel.textColor = newValue ? .white : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
        }
    }
    
    /// Количество
    @IBInspectable
    var count: Int = 0 {
        willSet {
            self.countLabel.text = "\(newValue)"
        }
    }
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
        // Настройка овала
        self.layer.masksToBounds = true
        
        // Другие настройки
        self.isLive = false
        self.count = 0
    }
    
}


// MARK: - Приватные свойства

private extension EventCounterView {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension EventCounterView {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Настройка овала
        let ovalPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.bounds.height / 2)
        self.shapeLayer?.path = ovalPath.cgPath
    }
    
}
