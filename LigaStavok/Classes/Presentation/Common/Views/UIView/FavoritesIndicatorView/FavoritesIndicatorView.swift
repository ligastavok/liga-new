//
//  FavoritesIndicatorView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class FavoritesIndicatorView: UIView, NibLoadableSelfOwnerView {
    
    // MARK: - Публичные свойства
    
    @IBInspectable
    var isActive: Bool = false {
        didSet {
            configure()
        }
    }
    
    
    // MARK: - Приватные свойства
    
    /// Маска
    private weak var maskLayer: CAShapeLayer!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
        // Настройка слоя
        self.shapeLayer?.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        // Маска
        let maskLayer = CAShapeLayer()
        self.layer.mask = maskLayer
        self.maskLayer = maskLayer
    }
    
}


// MARK: - Приватные свойства

private extension FavoritesIndicatorView {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension FavoritesIndicatorView {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(ovalIn: self.bounds)
        self.shapeLayer?.path = path.cgPath
        
        self.maskLayer.path = path.cgPath
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(square: 24)
    }
    
}


// MARK: - Приватные методы

private extension FavoritesIndicatorView {
    
    /// Настройка
    func configure() {
        self.backgroundColor = self.isActive ? #colorLiteral(red: 1, green: 0.6745098039, blue: 0, alpha: 1) : #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
    }
    
}
