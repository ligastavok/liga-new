//
//  LoadingView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 11.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class LoadingView: UIView, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Индикатор активности
    @IBOutlet
    private weak var activityIndicatorView: UIActivityIndicatorView! {
        willSet {
            newValue.startAnimating()
        }
    }
    
    /// Надпись
    @IBOutlet
    private weak var titleLabel: UILabel! {
        willSet {
            newValue.text = Localization.LoadingView.title
        }
    }
    
}


// MARK: - Публичные свойства

extension LoadingView {
    
    /// Цвет индикатора активности
    var activityIndicatorColor: UIColor? {
        get {
            return self.activityIndicatorView.color
        }
        set {
            self.activityIndicatorView.color = newValue
        }
    }
    
    /// Шрифт надписи
    var font: UIFont {
        get {
            return self.titleLabel.font
        }
        set {
            self.titleLabel.font = newValue
        }
    }
    
    /// Текст надписи
    var textColor: UIColor {
        get {
            return self.titleLabel.textColor
        }
        set {
            self.titleLabel.textColor = newValue
        }
    }
    
}
