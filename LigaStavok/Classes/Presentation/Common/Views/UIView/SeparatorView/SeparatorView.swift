//
//  SeparatorView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class SeparatorView: UIView { }


// MARK: - NSObject

extension SeparatorView {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        runtimeSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        runtimeSetup()
    }
    
}


// MARK: - UIView

extension SeparatorView {
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 0, height: 1)
    }
    
}


// MARK: - Приватные методы

private extension SeparatorView {
    
    /// Настройка при выполнении
    func runtimeSetup() {
        self.backgroundColor = #colorLiteral(red: 0.8745098039, green: 0.8745098039, blue: 0.8745098039, alpha: 1)
    }
    
}
