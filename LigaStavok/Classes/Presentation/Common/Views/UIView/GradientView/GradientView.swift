//
//  GradientView.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/4/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class GradientView: UIView {
    
    // MARK: - Публичные свойства
    
    /// Начальный цвет
    @IBInspectable
    var startColor: UIColor = .clear {
        didSet {
            configure()
        }
    }
    
    /// Конечный цвет
    @IBInspectable
    var endColor: UIColor = .clear {
        didSet {
            configure()
        }
    }
    
}


// MARK: - Публичные свойства

extension GradientView {
    
    /// Координата начального цвета
    @IBInspectable
    var startPoint: CGPoint {
        get {
            return self.gradientLayer?.startPoint ?? .zero
        }
        set {
            self.gradientLayer?.startPoint = newValue
        }
    }
    
    /// Координата конечного цвета
    @IBInspectable
    var endPoint: CGPoint {
        get {
            return self.gradientLayer?.endPoint ?? .zero
        }
        set {
            self.gradientLayer?.endPoint = newValue
        }
    }
    
}


// MARK: - Приватные свойства

extension GradientView {
    
    var gradientLayer: CAGradientLayer? {
        return self.layer as? CAGradientLayer
    }
    
}


// MARK: - UIView

extension GradientView {
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }

}


// MARK: - Приватные методы

private extension GradientView {
    
    /// Настройка
    func configure() {
        self.gradientLayer?.colors = [self.startColor.cgColor, self.endColor.cgColor]
    }
    
}
