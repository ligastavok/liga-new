//
//  StatusBarMessageView.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/10/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class StatusBarMessageView: UIView, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Сообщение
    @IBOutlet
    private weak var messageLabel: UILabel!
    
    
    // MARK: - Приватные свойства
    
    /// Продолжительность анимации
    private let animationDuration: TimeInterval = 0.20
    
    /// Состояние анимации
    private var isAnimated = false
    /// Состояние видимости
    private(set) var isShown: Bool = false {
        willSet {
            let animated = self.isShown != newValue
            setShownState(newValue, animated: animated)
        }
    }
    
    /// Сообщение
    private var message = Localization.StatusBarMessageView.internetNotConnected

}


// MARK: - Публичные методы

extension StatusBarMessageView {
    
    /// Показать сообщение
    func show(message: String? = nil, timeout: TimeInterval = 0) {
        self.message = message ?? self.message
        self.isShown = true
        
        guard timeout > 0 else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + timeout) { [weak self] in
            self?.hide()
        }
    }
    
    /// Скрыть сообщение
    func hide() {
        self.isShown = false
    }
    
}


// MARK: Приватные методы

private extension StatusBarMessageView {
    
    /// Настройка отображения
    func setShownState(_ shown: Bool, animated: Bool) {
        if !animated && self.isAnimated {
            return
        }
        
        self.isAnimated = animated
        
        let duration = animated ? self.animationDuration : 0
        UIApplication.shared.setStatusBarHidden(shown, withAnimationDuration: duration)
        
        if shown {
            addToTopViewController()
        }
        
        UIView.animate(withDuration: duration, animations: { [weak self] in
            guard let `self` = self else { return }
            
            self.messageLabel.text = self.message
            self.alpha = shown ? 1 : 0
            self.frame = self.getStatusFrameForShownState(shown)
        }, completion: { [weak self] _ in
            self?.isAnimated = false
            
            if !shown {
                self?.removeFromSuperview()
            }
        })
    }
    
    /// Поместить вью на активный вью контроллер
    func addToTopViewController() {
        guard let topViewController = UIApplication.topViewController?.navigationController else { return }
        guard topViewController.view.subviews.first(where: { $0 is StatusBarMessageView }) == nil else { return }
        
        removeFromSuperview()
        topViewController.view.addSubview(self)
    }
    
    /// Получить frame
    func getStatusFrameForShownState(_ shown: Bool) -> CGRect {
        let statusBarFrame = UIApplication.shared.statusBarFrame
        let y = shown ? 0 : -statusBarFrame.height
        
        return CGRect(x: 0, y: y, width: statusBarFrame.width, height: statusBarFrame.height)
    }
    
}
