//
//  BetsPlaceholderView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 08.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class BetsPlaceholderView: UIView, NibLoadableSelfOwnerView {
    
    // MARK: - Outlet
    
    /// Текст
    @IBOutlet
    private weak var textLabel: UILabel!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
        // Настройка слоя
        self.shapeLayer?.fillColor = #colorLiteral(red: 0.9607843137, green: 0.9607843137, blue: 0.9607843137, alpha: 1)
        self.shapeLayer?.lineWidth = 1
        self.shapeLayer?.strokeColor = #colorLiteral(red: 0.8980392157, green: 0.8980392157, blue: 0.8980392157, alpha: 1)
    }
    
}


// MARK: - Публичные свойства

extension BetsPlaceholderView {
    
    /// Тип событий
    @IBInspectable
    var text: String? {
        get {
            return self.textLabel.text
        }
        set {
            self.textLabel.text = newValue
        }
    }
    
}


// MARK: - Приватные свойства

private extension BetsPlaceholderView {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension BetsPlaceholderView {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 6)
        self.shapeLayer?.path = path.cgPath
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 0, height: 26)
    }
    
}
