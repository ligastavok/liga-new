//
//  LiveIndicatorView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class LiveIndicatorView: UIView {
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        self.shapeLayer?.fillColor = #colorLiteral(red: 0, green: 0.6039215686, blue: 0.431372549, alpha: 1)
    }
    
}


// MARK: - Приватные свойства

private extension LiveIndicatorView {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension LiveIndicatorView {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(ovalIn: self.bounds)
        self.shapeLayer?.path = path.cgPath
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(square: 5)
    }
    
}
