//
//  VisualEffectView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 29.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class VisualEffectView: UIVisualEffectView {
    
    // MARK: - Приватные свойства
    
    /// Эффект размытия
    private var blurEffect: UIBlurEffect?
    
    
    // MARK: - Инициализация
    
    override init(effect: UIVisualEffect?) {
        super.init(effect: effect)
        
        initPhase2()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        if let blurEffectType = NSClassFromString("_UICustomBlurEffect") as? UIBlurEffect.Type {
            self.blurEffect = blurEffectType.init()
        }
        
        self.scale = 1
    }
    
}


// MARK: - Публичные свойства

extension VisualEffectView {
    
    /// Оттенок
    @IBInspectable
    var colorTint: UIColor? {
        get {
            return blurEffectValue(forKey: "colorTint") as? UIColor
        }
        set {
            blurEffectSetValue(newValue, forKey: "colorTint")
        }
    }
    
    /// Прозрачность оттенка
    @IBInspectable
    var colorTintAlpha: CGFloat {
        get {
            let value = blurEffectValue(forKey: "colorTintAlpha") as? CGFloat
            return value ?? 0
        }
        set {
            blurEffectSetValue(newValue, forKey: "colorTintAlpha")
        }
    }

    /// Радиус размытия
    @IBInspectable
    var blurRadius: CGFloat {
        get {
            let value = blurEffectValue(forKey: "blurRadius") as? CGFloat
            return value ?? 0
        }
        set {
            blurEffectSetValue(newValue, forKey: "blurRadius")
        }
    }

    /// Фактор масштабирования
    @IBInspectable
    var scale: CGFloat {
        get {
            let value = blurEffectValue(forKey: "scale") as? CGFloat
            return value ?? 1
        }
        set {
            blurEffectSetValue(newValue, forKey: "scale")
        }
    }
    
}


// MARK: - Приватные методы

private extension VisualEffectView {
    
    /// Получить значение по ключу у эффекта размытия
    func blurEffectValue(forKey key: String) -> Any? {
        return self.blurEffect?.value(forKeyPath: key)
    }
    
    /// Установить значение по ключу для эффекта размытия
    func blurEffectSetValue(_ value: Any?, forKey key: String) {
        self.blurEffect?.setValue(value, forKeyPath: key)
        self.effect = self.blurEffect
    }
    
}
