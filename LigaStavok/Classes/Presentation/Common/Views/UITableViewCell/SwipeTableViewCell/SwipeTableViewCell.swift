//
//  SwipeTableViewCell.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

class SwipeTableViewCell: UITableViewCell {
    
    // MARK: - Outlet
    
    @IBOutlet
    weak var scrollView: UIScrollView! {
        willSet {
            newValue.delegate = self
            newValue.scrollsToTop = false
        }
    }
    
    // MARK: - Публичные свойства
    
    /// Выполняется анимация
    var isAnimating = false
    
    
    // MARK: - Приватные свойства
    
    private weak var tableView: UITableView?
    
    private lazy var tapGestureRecognizer: UITapGestureRecognizer = {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap(gesture:)))
        gestureRecognizer.delegate = self
        
        return gestureRecognizer
    }()
    
    
    // MARK: - Инициализация
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initPhase2()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        addGestureRecognizer(self.tapGestureRecognizer)
    }
    
    
    // MARK: - Деинициализация
    
    deinit {
        self.tableView?.panGestureRecognizer.removeTarget(self, action: nil)
    }
    
}


// MARK: - Публичные свойства

extension SwipeTableViewCell {
    
    /// Возможность взаимодействия
    @objc
    var isAvailable: Bool {
        return true
    }
    
}


// MARK: - Приватные свойства

private extension SwipeTableViewCell {
    
    /// Состояние
    var isClosed: Bool {
        return self.scrollView.contentOffset.x > -self.scrollView.contentInset.left
    }
    
    /// Ячейки
    var cells: [SwipeTableViewCell]? {
        return self.tableView?.visibleCells.compactMap { $0 as? SwipeTableViewCell }
    }
    
}


// MARK: - UIView

extension SwipeTableViewCell {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        guard let superview = self.superview else { return false }
        let point = convert(point, to: superview)
        
        let cells = self.cells?.filter { !$0.isClosed && !$0.isAnimating && $0 != self }
        for cell in cells ?? [] where !cell.contains(point: point) {
            cells?.forEach { $0.hideButtons() }
            return false
        }
        
        return contains(point: point)
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        var view: UIView = self
        while let superview = view.superview {
            view = superview
            
            guard let tableView = superview as? UITableView else { continue }
            self.tableView = tableView
            
            tableView.panGestureRecognizer.removeTarget(self, action: nil)
            tableView.panGestureRecognizer.addTarget(self, action: #selector(handleTablePan(gesture:)))
            
            return
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.scrollView.layoutIfNeeded()
        self.scrollView.setContentOffset(.zero, animated: false)
    }
    
}


// MARK: - UITableViewCell

extension SwipeTableViewCell {
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.scrollView.isScrollEnabled = true
    }
    
}


// MARK: - UIGestureRecognizerDelegate

extension SwipeTableViewCell {
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer == self.tapGestureRecognizer, self.isAvailable {
            let cell = self.cells?.first(where: { !$0.isClosed && !$0.isAnimating })
            return cell != nil
        }
        
        return super.gestureRecognizerShouldBegin(gestureRecognizer)
    }
    
}


// MARK: - Публичные методы

extension SwipeTableViewCell {
    
    /// Скрыть кнопки
    func hideButtons() {
        guard !self.isClosed else { return }
        
        self.isAnimating = true
        self.scrollView.setContentOffset(.zero, animated: true)
        
        setScrollEnabled(true)
    }
    
    /// Рассчет координаты при открытии
    @objc
    func willShowButton(_ scrollView: UIScrollView,
                        withVelocity velocity: CGPoint,
                        targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee.x = -scrollView.contentInset.left
    }
    
}


// MARK: - Приватные методы

private extension SwipeTableViewCell {
    
    /// Проверка на вхождение точки
    func contains(point: CGPoint) -> Bool {
        return point.y > self.frame.minY && point.y < self.frame.maxY
    }
    
    /// Обработка скролла таблицы
    @objc
    func handleTablePan(gesture: UIPanGestureRecognizer) {
        guard case .began = gesture.state else { return }
        hideButtons()
    }
    
    /// Обработка касания в ячейке
    @objc
    func handleTap(gesture: UITapGestureRecognizer) {
        hideButtons()
    }
    
    /// Управление доступностью скролла
    func setScrollEnabled(_ enabled: Bool) {
        guard var cells = self.cells else { return }
        if !enabled {
            cells = cells.filter { $0 != self }
        }
        
        cells.forEach { $0.scrollView.isScrollEnabled = enabled }
    }
    
}


// MARK: - UIScrollViewDelegate

extension SwipeTableViewCell: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x > 0 || !self.isAvailable {
            scrollView.contentOffset = .zero
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        setScrollEnabled(false)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView,
                                   withVelocity velocity: CGPoint,
                                   targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView.contentOffset.x < -scrollView.contentInset.left * 0.6 {
            willShowButton(scrollView, withVelocity: velocity, targetContentOffset: targetContentOffset)
        } else {
            let milliseconds = scrollView.contentOffset.x / -velocity.x
            if velocity.x == 0 || milliseconds < 0 || milliseconds > 300 {
                self.isAnimating = true
                scrollView.setContentOffset(.zero, animated: true)
            } else {
                targetContentOffset.pointee = .zero
            }
        }
        
        setScrollEnabled(true)
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        self.isAnimating = false
    }
    
}
