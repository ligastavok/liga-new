//
//  EventTableViewCellBetsContainerView.swift
//  LigaStavok
//
//  Created by Artem on 24/07/2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EventTableViewCellBetsContainerView: UIView { }


// MARK: - UIView

extension EventTableViewCellBetsContainerView {
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let padding: CGFloat = 10
        let extendedBounds = self.bounds.insetBy(dx: -padding, dy: -padding)
        
        return extendedBounds.contains(point)
    }
    
}
