//
//  EventTableViewCellContentView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 22.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class EventTableViewCellContentView: UIView {
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Настройка отрисовки
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        // Задний фон
        self.shapeLayer?.fillColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        // Тень
        self.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.2)
        self.layer.shadowRadius = 0
        self.layer.shadowOpacity = 2
        self.layer.shadowOffset = CGSize(width: 0, height: 1)
    }
    
}


// MARK: - Приватные свойства

private extension EventTableViewCellContentView {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - UIView

extension EventTableViewCellContentView {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 6)
        self.shapeLayer?.path = path.cgPath
        
        self.layer.shadowPath = path.cgPath
    }
    
}
