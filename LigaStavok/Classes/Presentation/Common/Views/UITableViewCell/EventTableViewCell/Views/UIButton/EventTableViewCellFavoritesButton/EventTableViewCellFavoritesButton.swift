//
//  EventTableViewCellFavoritesButton.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class EventTableViewCellFavoritesButton: Button { }


// MARK: - UIView

extension EventTableViewCellFavoritesButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let mask = CAShapeLayer()
        
        let path = UIBezierPath(roundedRect: self.bounds,
                                byRoundingCorners: [.topRight, .bottomRight],
                                cornerRadii: CGSize(square: 6))
        mask.path = path.cgPath
        
        self.layer.mask = mask
    }
    
}
