//
//  EventTableViewCellContentBlockView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class EventTableViewCellContentBlockView: UIView, NibLoadableSelfOwnerView {
    
    // MARK: - Outlet
    
    /// Иконка
    @IBOutlet
    private weak var iconImageView: UIImageView!
    /// Название
    @IBOutlet
    private weak var titleLabel: UILabel!
    
    
    // MARK: - Публичные свойства
    
    /// Режим отображения
    var mode: EventViewModel.BlockMode = .unblocked {
        willSet {
            switch newValue {
            case .unblocked:
                self.iconImageView.image = nil
                self.titleLabel.text = nil
                self.isHidden = true
            case .completed:
                self.iconImageView.image = .init(asset: .iconEventCompleted)
                self.titleLabel.text = "Событие завершено"
                self.isHidden = false
            case .removed:
                self.iconImageView.image = .init(asset: .iconEventRemoved)
                self.titleLabel.text = "Событие удалено из линии"
                self.isHidden = false
            }
        }
    }
    
    
    // MARK: - Приватные свойства
    
    /// Маска
    private weak var maskLayer: CAShapeLayer!
    
    
    // MARK: - Инициализация
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        initPhase2()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initPhase2()
    }
    
    /// Инициализация объекта
    private func initPhase2() {
        
        // Загрузка из Xib
        loadFromNibAndAttachToSelf()
        
        // Настройка слоя
        self.shapeLayer?.fillColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        
        // Маска
        let maskLayer = CAShapeLayer()
        self.layer.mask = maskLayer
        self.maskLayer = maskLayer
    }
    
}


// MARK: - Приватные свойства

private extension EventTableViewCellContentBlockView {
    
    var shapeLayer: CAShapeLayer? {
        return self.layer as? CAShapeLayer
    }
    
}


// MARK: - NSObject

extension EventTableViewCellContentBlockView {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        runtimeSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        runtimeSetup()
    }
    
}


// MARK: - UIView

extension EventTableViewCellContentBlockView {
    
    override class var layerClass: AnyClass {
        return CAShapeLayer.self
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Настройка границы
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: 6)
        self.shapeLayer?.path = path.cgPath
        
        self.maskLayer.path = path.cgPath
    }
    
}


// MARK: - Приватные методы

private extension EventTableViewCellContentBlockView {
    
    /// Настройка при выполнении
    func runtimeSetup() {
        self.isHidden = true
    }
    
}
