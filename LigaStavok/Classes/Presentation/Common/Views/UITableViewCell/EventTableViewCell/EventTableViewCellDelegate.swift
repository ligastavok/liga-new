//
//  EventTableViewCellDelegate.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol EventTableViewCellDelegate: class {
    
    /// Выбрана ячейка
    func eventTableViewCellDidSelectRow(_ eventTableViewCell: EventTableViewCell)
    /// Нажата кнопка "Избранное"
    func eventTableViewCellDidTapFavoritesButton(_ eventTableViewCell: EventTableViewCell)
    
}
