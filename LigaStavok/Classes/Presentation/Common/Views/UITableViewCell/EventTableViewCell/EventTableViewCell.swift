//
//  EventTableViewCell.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EventTableViewCell: SwipeTableViewCell, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Кнопка "Избранное"
    @IBOutlet
    private weak var favoritesButton: UIButton! {
        willSet {
            newValue.adjustsImageWhenHighlighted = false
        }
    }
    
    /// Ширина контейнера иконок
    @IBOutlet
    private weak var iconsContainerViewWidthLayoutConstraint: NSLayoutConstraint!
    
    /// Индикатор "Избранное"
    @IBOutlet
    private weak var favoritesIndicatorView: UIView!
    /// Иконка
    @IBOutlet
    private weak var iconImageView: UIImageView!
    
    /// Название
    @IBOutlet
    private weak var titleLabel: UILabel!
    
    /// Индикатор live-состояния
    @IBOutlet
    private weak var liveIndicatorView: UIView!
    /// Время
    @IBOutlet
    private weak var timeLabel: UILabel!
    
    /// Видео
    @IBOutlet
    private weak var videoIconImageView: UIImageView!
    
    /// Название первой команды
    @IBOutlet
    private weak var firstNameLabel: UILabel!
    /// Счет первой команды
    @IBOutlet
    private weak var firstScoreLabel: UILabel!
    /// Название второй команды
    @IBOutlet
    private weak var secondNameLabel: UILabel!
    /// Счет второй команды
    @IBOutlet
    private weak var secondScoreLabel: UILabel!
    
    /// Информация о пари
    @IBOutlet
    private weak var betsPlaceholderView: BetsPlaceholderView!
    
    /// Победа первой команды
    @IBOutlet
    private weak var firstWinnerBetControl: BetControl!
    /// Ничья
    @IBOutlet
    private weak var drawBetControl: BetControl!
    /// Победа второй команды
    @IBOutlet
    private weak var secondWinnerBetControl: BetControl!
    
    /// Блокировка события
    @IBOutlet
    private weak var blockView: EventTableViewCellContentBlockView!
    
    
    // MARK: - Публичные свойства
    
    weak var delegate: EventTableViewCellDelegate?
    
    
    // MARK: - Приватные свойства
    
    /// Модель данных
    private var viewModel: EventViewModel!
    
    /// Жест касания по ячейке
    private lazy var cellTapGestureRecognizer: UIGestureRecognizer = {
        let cellTapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleCellTap(_:)))
        cellTapGestureRecognizer.delegate = self
        
        return cellTapGestureRecognizer
    }()
    
    /// Иконка, отображаемая при долгом касании
    private weak var longTapIconImageView: UIImageView?
    
}


// MARK: - NSObject

extension EventTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.scrollView.contentInset = UIEdgeInsets(top: 0, left: self.favoritesButton.frame.width, bottom: 0, right: 0)
        
        addGestureRecognizer(self.cellTapGestureRecognizer)
        
        let longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongTap(_:)))
        self.blockView.addGestureRecognizer(longTapGesture)
    }
    
}


// MARK: - UITableViewCell

extension EventTableViewCell {
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.firstWinnerBetControl.stopAnimation()
        self.drawBetControl.stopAnimation()
        self.secondWinnerBetControl.stopAnimation()
    }
    
}


// MARK: - SwipeTableViewCell

extension EventTableViewCell {
    
    override var isAvailable: Bool {
        guard let viewModel = self.viewModel else {
            return false
        }
        
        return viewModel.blockMode == .unblocked
    }
    
}


// MARK: - UIGestureRecognizerDelegate

extension EventTableViewCell {
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard gestureRecognizer == self.cellTapGestureRecognizer else { return true }
        
        if touch.view == self.firstWinnerBetControl || touch.view == self.drawBetControl || touch.view == self.secondWinnerBetControl {
            return false
        }
        
        return true
    }
    
}


// MARK: - SwipeTableViewCell

extension EventTableViewCell {
    
    override func willShowButton(_ scrollView: UIScrollView,
                                 withVelocity velocity: CGPoint,
                                 targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let milliseconds = scrollView.contentOffset.x / -velocity.x
        if velocity.x == 0 || milliseconds < 0 || milliseconds > 300 {
            self.isAnimating = true
            scrollView.setContentOffset(.zero, animated: true)
        } else {
            targetContentOffset.pointee = .zero
        }
        
        toggleFavorites()
    }
    
}


// MARK: - Action

private extension EventTableViewCell {
    
    /// Нажата кнопка "Победа первой команды"
    @IBAction
    func firstWinnerBetControlTapped() {
        self.viewModel.isFirstWinnerBetSelected = !self.viewModel.isFirstWinnerBetSelected
        self.firstWinnerBetControl.isActive = self.viewModel.isFirstWinnerBetSelected
        
        if self.viewModel.isFirstWinnerBetSelected {
            self.firstWinnerBetControl.stopAnimation()
        } else {
            self.firstWinnerBetControl.valueChangeType = self.viewModel.firstWinnerBetValueChangeType
        }
    }
    
    /// Нажата кнопка "Ничья"
    @IBAction
    func drawBetControlTapped() {
        self.viewModel.isDrawBetSelected = !self.viewModel.isDrawBetSelected
        self.drawBetControl.isActive = self.viewModel.isDrawBetSelected
        
        if self.viewModel.isDrawBetSelected {
            self.drawBetControl.stopAnimation()
        } else {
            self.drawBetControl.valueChangeType = self.viewModel.drawBetValueChangeType
        }
    }
    
    /// Нажата кнопка "Победа второй команды"
    @IBAction
    func secondWinnerBetControlTapped() {
        self.viewModel.isSecondWinnerBetSelected = !self.viewModel.isSecondWinnerBetSelected
        self.secondWinnerBetControl.isActive = self.viewModel.isSecondWinnerBetSelected
        
        if self.viewModel.isSecondWinnerBetSelected {
            self.secondWinnerBetControl.stopAnimation()
        } else {
            self.secondWinnerBetControl.valueChangeType = self.viewModel.secondWinnerBetValueChangeType
        }
    }
    
}


// MARK: - Публичные методы

extension EventTableViewCell {
    
    /// Настройка
    func configure(with viewModel: EventViewModel) {
        self.viewModel = viewModel
        
        self.iconsContainerViewWidthLayoutConstraint.constant = viewModel.isFavoritesIndicatorVisible ? 56 : 24
        self.favoritesIndicatorView.isHidden = !viewModel.isFavoritesIndicatorVisible
        
        self.iconImageView.image = viewModel.iconName.image
        self.iconImageView.backgroundColor = viewModel.iconBackgroundColor
        
        self.titleLabel.text = viewModel.title
        
        self.liveIndicatorView.isHidden = !viewModel.isLiveIndicatorVisible
        self.timeLabel.text = viewModel.timeValue
        
        self.videoIconImageView.isHidden = !viewModel.isVideoIconVisible
        
        self.firstNameLabel.text = viewModel.firstName
        self.secondNameLabel.text = viewModel.secondName
        
        self.firstScoreLabel.text = viewModel.firstScore
        self.firstScoreLabel.isHidden = viewModel.isScoresHidden
        self.secondScoreLabel.text = viewModel.secondScore
        self.secondScoreLabel.isHidden = viewModel.isScoresHidden
        
        self.betsPlaceholderView.isHidden = !viewModel.isBetsHidden
        
        self.firstWinnerBetControl.isHidden = viewModel.isBetsHidden
        self.drawBetControl.isHidden = viewModel.isBetsHidden
        self.secondWinnerBetControl.isHidden = viewModel.isBetsHidden
        
        if viewModel.isBetsHidden {
            self.betsPlaceholderView.text = viewModel.betsPlaceholder
        } else {
            self.firstWinnerBetControl.isEnabled = viewModel.isFirstWinnerBetEnabled
            self.firstWinnerBetControl.isActive = viewModel.isFirstWinnerBetSelected
            self.firstWinnerBetControl.value = viewModel.firstWinnerBetValue
            self.firstWinnerBetControl.valueChangeType = viewModel.firstWinnerBetValueChangeType
            self.firstWinnerBetControl.layoutIfNeeded()
            
            self.drawBetControl.isEnabled = viewModel.isDrawBetEnabled
            self.drawBetControl.isActive = viewModel.isDrawBetSelected
            self.drawBetControl.value = viewModel.drawBetValue
            self.drawBetControl.valueChangeType = viewModel.drawBetValueChangeType
            self.drawBetControl.layoutIfNeeded()
            
            self.secondWinnerBetControl.isEnabled = viewModel.isSecondWinnerBetEnabled
            self.secondWinnerBetControl.isActive = viewModel.isSecondWinnerBetSelected
            self.secondWinnerBetControl.value = viewModel.secondWinnerBetValue
            self.secondWinnerBetControl.valueChangeType = viewModel.secondWinnerBetValueChangeType
            self.secondWinnerBetControl.layoutIfNeeded()
        }
        
        self.blockView.mode = viewModel.blockMode
    }
    
    /// Переключить "Избранное"
    func toggleFavorites() {
        guard self.isAvailable else { return }
        
        self.viewModel.isFavoritesIndicatorVisible = !self.viewModel.isFavoritesIndicatorVisible
        let isFavoritesIndicatorVisible = self.viewModel.isFavoritesIndicatorVisible
        
        // Тактильный отклик
        if #available(iOS 10.0, *) {
            let feedbackGenerator = UIImpactFeedbackGenerator(style: .light)
            feedbackGenerator.impactOccurred()
        }
        
        // Настройка перед отображением
        if isFavoritesIndicatorVisible {
            self.favoritesIndicatorView.isHidden = false
            
            let scaleFactor: CGFloat = 0.5
            self.favoritesIndicatorView.transform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
        }
        
        // Ширина контейнера иконок
        self.iconsContainerViewWidthLayoutConstraint.constant = isFavoritesIndicatorVisible ? 56 : 24
        
        // Анимация отображения
        UIView.animate(withDuration: 0.3, animations: { [weak self] in
            self?.layoutIfNeeded()
            
            // Отображение иконки "Избранное"
            self?.favoritesIndicatorView.alpha = isFavoritesIndicatorVisible ? 1 : 0
            
            let scaleFactor: CGFloat = isFavoritesIndicatorVisible ? 1 : 0.5
            self?.favoritesIndicatorView.transform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
        }, completion: { [weak self] finished in
            guard finished else { return }
            
            // Настройка после скрытия
            if !isFavoritesIndicatorVisible {
                self?.favoritesIndicatorView.isHidden = true
            }
        })
        
        self.delegate?.eventTableViewCellDidTapFavoritesButton(self)
    }
    
}


// MARK: - Приватные методы

private extension EventTableViewCell {
    
    /// Обработка касания в ячейке
    @objc
    func handleCellTap(_ gestureRecognizer: UIPanGestureRecognizer) {
        self.delegate?.eventTableViewCellDidSelectRow(self)
    }
    
    /// Обработка долгого касания в ячейке
    @objc
    func handleLongTap(_ gestureRecognizer: UIGestureRecognizer) {
        switch gestureRecognizer.state {
        case .began:
            hideBlockView()
        case .cancelled, .failed, .ended:
            showBlockView()
            
        default:
            break
        }
    }
    
    /// Отображение сообщения о блокировке события
    func showBlockView() {
        guard let longTapIconImageView = self.longTapIconImageView else { return }
        
        UIView.transition(from: longTapIconImageView,
                          to: self.iconImageView,
                          duration: 0.5,
                          options: [.transitionCrossDissolve, .showHideTransitionViews]) { [weak self] _ in
                            self?.longTapIconImageView?.removeFromSuperview()
        }
        
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.blockView.alpha = 1
        }
    }
    
    /// Скрытие сообщения о блокировке события
    func hideBlockView() {
        guard let iconContainerView = self.iconImageView.superview else { return }
        
        // Создание изображения
        let image: UIImage = .init(asset: self.blockView.mode == .removed ? .iconEventRemoved : .iconEventCompleted)
        
        let longTapIconImageView = UIImageView(image: image)
        self.longTapIconImageView = longTapIconImageView
        
        longTapIconImageView.isHidden = true
        longTapIconImageView.frame = self.iconImageView.frame
        
        iconContainerView.addSubview(longTapIconImageView)
        
        // Анимация
        UIView.transition(from: self.iconImageView,
                          to: longTapIconImageView,
                          duration: 0.5,
                          options: [.transitionCrossDissolve, .showHideTransitionViews])
        
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.blockView.alpha = 0
        }
        
        // TODO: Перенести в нужное место
        self.betsPlaceholderView.text = "Коэффициентов нет"
    }
    
}
