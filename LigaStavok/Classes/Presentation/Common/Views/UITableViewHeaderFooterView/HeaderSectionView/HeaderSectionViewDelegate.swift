//
//  HeaderSectionViewDelegate.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol HeaderSectionViewDelegate: class {
    
    /// Нажата кнопка
    func buttonTapped(in lineHeaderSectionView: HeaderSectionView)
    
}
