//
//  HeaderSectionView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class HeaderSectionView: UITableViewHeaderFooterView, NibLoadableView {
    
    // MARK: - Outlet
    
    /// Название
    @IBOutlet
    private weak var titleLabel: UILabel!
    
    /// Кнопка
    @IBOutlet
    private weak var accessoryButton: UIButton!
    
    
    // MARK: - Публичные свойства
    
    weak var delegate: HeaderSectionViewDelegate?
    
    
    // MARK: - Приватные свойства
    
    /// Ширина кнопки
    private weak var accessoryButtonWidthLayoutConstraint: NSLayoutConstraint?
    
}


// MARK: - NSObject

extension HeaderSectionView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9568627451, alpha: 1)
        self.accessoryButton.titleLabel?.backgroundColor = self.contentView.backgroundColor
    }
    
}


// MARK: - UIView

extension HeaderSectionView {
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 0, height: 38)
    }
    
}


// MARK: - Action

extension HeaderSectionView {
    
    /// Нажата кнопка
    @IBAction
    func accessoryButtonTapped() {
        self.delegate?.buttonTapped(in: self)
    }
    
}


// MARK: - Публичные методы

extension HeaderSectionView {
    
    /// Настройка
    func configure(with viewModel: HeaderSectionViewModel) {
        self.titleLabel.text = viewModel.title
        
        self.accessoryButton.titleLabel?.text = viewModel.buttonTitle
        self.accessoryButton.setTitle(viewModel.buttonTitle, for: .normal)
        
        self.accessoryButton.isHidden = viewModel.buttonTitle == nil
        if self.accessoryButton.isHidden && self.accessoryButtonWidthLayoutConstraint == nil {
            let accessoryButtonWidthLayoutConstraint = self.accessoryButton.widthAnchor.constraint(equalToConstant: 0)
            self.accessoryButtonWidthLayoutConstraint = accessoryButtonWidthLayoutConstraint
            
            accessoryButtonWidthLayoutConstraint.isActive = true
        } else if !self.accessoryButton.isHidden && self.accessoryButtonWidthLayoutConstraint != nil {
            self.accessoryButtonWidthLayoutConstraint?.isActive = false
        }
    }
    
}
