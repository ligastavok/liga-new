//
//  Button.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
class Button: UIButton {
    
    // MARK: - UIControl
    
    override var isEnabled: Bool {
        willSet {
            self.alpha = newValue ? 1 : 0.75
        }
    }
    
    override var isHighlighted: Bool {
        willSet {
            guard newValue != self.isHighlighted else { return }
            
            willUpdateHighlighted(newValue)
        }
    }
    
    
    // MARK: - Публичные свойства
    
    /// Цвет фона
    @IBInspectable
    var color: UIColor? {
        willSet {
            self.backgroundColor = newValue
        }
    }
    
}


// MARK: - Публичные свойства

extension Button {
    
    /// Цвет границы
    @IBInspectable
    var borderColor: UIColor? {
        get {
            return self.layer.borderColor.map { UIColor(cgColor: $0) }
        }
        set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    /// Ширина границы
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return self.layer.borderWidth
        }
        set {
            self.layer.borderWidth = newValue
        }
    }
    
    /// Радиус скругления
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
        }
    }
    
    /// Смещение тени
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        }
        set {
            self.layer.shadowOffset = newValue
        }
    }
    
    /// Радиус тени
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return self.layer.shadowRadius
        }
        set {
            self.layer.shadowRadius = newValue
        }
    }
    
    /// Прозрачность тени
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        }
        set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    /// Цвет тени
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            return self.layer.shadowColor.map { UIColor(cgColor: $0) }
        }
        set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
}


// MARK: - Публичные методы

extension Button {
    
    /// Собирается быть выполнено обновление состояния выделения
    @objc
    func willUpdateHighlighted(_ highlighted: Bool) {
        let backgroundColor = highlighted ? self.color?.contrastColor : self.color
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.backgroundColor = backgroundColor
        }
    }
    
}
