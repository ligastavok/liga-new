//
//  ColoredImageView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 06.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class ColoredImageView: UIImageView {
    
    // MARK: - Публичные свойства
    
    /// Цвет изображения
    @IBInspectable
    var maskColor: UIColor? {
        willSet {
            guard let color = newValue else { return }
            self.image = self.image?.mask(with: color)
        }
    }
    
}
