//
//  RoundedImageView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

@IBDesignable
final class RoundedImageView: UIImageView { }


// MARK: - NSObject

extension RoundedImageView {
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
        runtimeSetup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        runtimeSetup()
    }
    
}


// MARK: - UIView

extension RoundedImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let radius = max(self.bounds.width, self.bounds.height) / 2
        let path = UIBezierPath(roundedRect: self.bounds, cornerRadius: radius)
        
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        
        self.layer.mask = mask
    }
    
}


// MARK: - Приватные методы

private extension RoundedImageView {
    
    /// Настройка при выполнении
    func runtimeSetup() {
        self.layer.masksToBounds = true
    }
    
}
