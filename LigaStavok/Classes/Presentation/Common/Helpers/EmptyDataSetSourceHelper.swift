//
//  EmptyDataSetSourceHelper.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

final class EmptyDataSetSourceHelper { }


// MARK: - Публичные методы

extension EmptyDataSetSourceHelper {
    
    /// Создать заголовок
    func generateTitle(withText text: String) -> NSAttributedString {
        let font: UIFont = .systemFont(ofSize: 15)
        let textColor = #colorLiteral(red: 0, green: 0, blue: 0.09803921569, alpha: 0.22)
        
        let attributes: [NSAttributedStringKey: Any] = [
            .font: font,
            .foregroundColor: textColor
        ]
        let attributedString = NSAttributedString(string: text, attributes: attributes)
        return attributedString
    }
    
    /// Создать
    func generateButtonTitle(withText text: String) -> NSAttributedString {
        let font: UIFont = .systemFont(ofSize: 15)
        let textColor = #colorLiteral(red: 0, green: 0.4156862745, blue: 0.2901960784, alpha: 1)
        
        let attributes: [NSAttributedStringKey: Any] = [
            .font: font,
            .foregroundColor: textColor
        ]
        let attributedString = NSAttributedString(string: text, attributes: attributes)
        return attributedString
    }
    
    func generateLoadingView(for scrollView: UIScrollView) -> UIView {
        let loadingView = LoadingView.loadFromNib()
        loadingView.backgroundColor = scrollView.backgroundColor
        
        return loadingView
    }
    
}
