//
//  CoefficientNumberFormater.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 02.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

enum NumberFormatter {

    static let coefficientNumberFormater: Foundation.NumberFormatter = {
        let numberFormatter = Foundation.NumberFormatter()
        numberFormatter.maximumFractionDigits = 2
        numberFormatter.minimumFractionDigits = 2

        return numberFormatter
    }()

}
