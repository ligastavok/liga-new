//
//  ImageAsset.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

enum ImageAsset: String {
    
    /// Placeholder
    case placeholder
    /// Placeholder для навигационной панели на экране "Роспись дисциплины"
    case placeholderSportTournaments = "placeholder_sport_tournaments"
    /// Placeholder "Барселона"
    case placeholderBarcelona = "placeholder_barcelona"
    /// Placeholder "Манчестер Юнайтед"
    case placeholderManchesterUnited = "placeholder_manchester_united"
    
    
    // MARK: - Common
    
    /// Избранное
    case iconFavorites = "icon_favorites"
    /// Видео
    case iconVideo = "icon_video"
    /// Активная галочка
    case iconCheck = "icon_check"
    /// Снатая галочка
    case iconUncheck = "icon_uncheck"
    /// Активная звездочка
    case iconStar = "icon_star"
    /// Снатая звездочка
    case iconUnstar = "icon_unstar"
    /// Поиск
    case iconSearch = "icon_search"
    /// Индикатор отсутствия незаблокированных исходов
    case iconLocked = "icon_locked"
    /// Иконка завершенного события
    case iconEventCompleted = "icon_event_completed"
    /// Иконка удаленного события
    case iconEventRemoved = "icon_event_removed"
    /// Индикатор перехода
    case iconDisclosureIndicator = "icon_disclosure_indicator"
    /// Указатель открытия блока таблицы
    case iconOpenCell = "icon_open_cell"
    /// Указатель закрытия блока таблицы
    case iconCloseCell = "icon_close_cell"
    
    
    // MARK: - TabBar
    
    /// Календарь
    case tabBarCalendar = "tabbar_calendar"
    /// Звездочка
    case tabBarFavorites = "tabbar_favorites"
    /// Профиль
    case tabBarUser = "tabbar_user"
    /// Купоны
    case tabBarShoppingCart = "tabbar_shopping_cart"
    
    
    // MARK: - NavigationBar
    
    /// Поиск
    case navigationBarSearch = "navigationbar_search"
    /// Избранное
    case navigationBarFavorites = "navigationbar_favorites"
    /// Неизбранное
    case navigationBarUnfavorites = "navigationbar_unfavorites"
    /// Корзина
    case navigationBarTrash = "navigationbar_trash"
    /// Настройки
    case navigationBarSetting = "navigationbar_setting"
    
    
    // MARK: - Sport
    
    /// Горные лыжи
    case sportAlpineSkiing = "sport_alpine_skiing"
    /// Американский футбол
    case sportAmFootball = "sport_am_football"
    /// Стрельба из лука
    case sportArchery = "sport_archery"
    /// Спортивная гимнастика
    case sportArtisticGimnastics = "sport_artistic_gimnastics"
    /// Легкая атлетика
    case sportAthletics = "sport_athletics"
    /// Австралийский футбол
    case sportAussieRules = "sport_aussie_rules"
    /// Авто-мото спорт
    case sportAutoMoto = "sport_auto-moto"
    /// Бадминтон
    case sportBadminton = "sport_badminton"
    /// Хоккей с мячом
    case sportBandy = "sport_bandy"
    /// Бейсбол
    case sportBaseball = "sport_baseball"
    /// Баскетбол
    case sportBasketball = "sport_basketball"
    /// Биатлон
    case sportBiathlon = "sport_biathlon"
    /// Бобслей
    case sportBobsleigh = "sport_bobsleigh"
    /// Боулинг
    case sportBouling = "sport_bouling"
    /// Шары
    case sportBowls = "sport_bowls"
    /// Бокс
    case sportBoxing = "sport_boxing"
    /// Байдарки и каноэ
    case sportCanoeing = "sport_canoeing"
    /// Шахматы
    case sportChess = "sport_chess"
    /// Крикет
    case sportCricket = "sport_cricket"
    /// Кёрлинг
    case sportCurling = "sport_curling"
    /// Велоспорт
    case sportCycling = "sport_cycling"
    /// Дартс
    case sportDarts = "sport_darts"
    /// Иконка вида спорта "по умолчанию"
    case sportDefault = "sport_default"
    /// Прыжки в воду
    case sportDiving = "sport_diving"
    /// Киберспорт
    case sportESports = "sport_e-sports"
    /// Выборы
    case sportElections = "sport_elections"
    /// Конный спорт
    case sportEquestrianSport = "sport_equestrian_sport"
    /// Фехтование
    case sportFencing = "sport_fencing"
    /// Хоккей на траве
    case sportFieldHockey = "sport_field_hockey"
    /// Фигурное катание
    case sportFigureSkating = "sport_figure_skating"
    /// Флорбол
    case sportFloorball = "sport_floorball"
    /// Формула 1
    case sportFormulaOne = "sport_formula1"
    /// Фристайл
    case sportFreestyleSkiing = "sport_freestyle_skiing"
    /// Гольф
    case sportGolf = "sport_golf"
    /// Греко-римская борьба
    case sportGrWrestling = "sport_gr_wrestling"
    /// Гандбол
    case sportHandball = "sport_handball"
    /// Скачки
    case sportHorcerase = "sport_horcerace"
    /// Хоккей
    case sportIceHockey = "sport_ice_hockey"
    /// Дзю-До
    case sportJudo = "sport_judo"
    /// Сани
    case sportLuge = "sport_luge"
    /// Бои без правил
    case sportMartialArts = "sport_martial_arts"
    /// Смешанные единоборства
    case sportMma = "sport_mma"
    /// Нетбол
    case sportNetball = "sport_netball"
    /// Лыжное двоеборье
    case sportNordicCombined = "sport_nordic_combined"
    /// Олимпиада
    case sportOlimpicGames = "sport_olimpic_games"
    /// Игроки
    case sportPlayers = "sport_players"
    /// Покер
    case sportPoker = "sport_poker"
    /// Политика
    case sportPolitics = "sport_politics"
    /// Художественная гимнастика
    case sportRhythmic = "sport_rhythmic"
    /// Гребля
    case sportRowing = "sport_rowing"
    /// Регби
    case sportRugby = "sport_rugby"
    /// Парусный спорт
    case sportSailing = "sport_sailing"
    /// Стрельба
    case sportShooting = "sport_shooting"
    /// Шорттрек
    case sportShortTrack = "sport_short_track"
    /// Шоу-бизнес
    case sportShowBusiness = "sport_show_business"
    /// Синхронное плавание
    case sportSinchronizedSwimming = "sport_sinchronized_swimming"
    /// Коньки
    case sportSkate = "sport_skate"
    /// Скелетон
    case sportSkeleton = "sport_skeleton"
    /// Прыжки с трамплина
    case sportSkiJumping = "sport_ski_jumping"
    /// Лыжи
    case sportSki = "sport_ski"
    /// Снукер
    case sportSnooker = "sport_snooker"
    /// Сноуборд
    case sportSnowboard = "sport_snowboard"
    /// Футбол
    case sportSoccer = "sport_soccer"
    /// Софтбол
    case sportSoftball = "sport_softball"
    /// Спецпредложение
    case sportSpecialOffer = "sport_special_offer"
    /// Сквош
    case sportSqwosh = "sport_sqwosh"
    /// Плавание
    case sportSwimming = "sport_swimming"
    /// Настольный тенис
    case sportTableTennis = "sport_table_tennis"
    /// Тхэквондо
    case sportTaekwondo = "sport_taekwondo"
    // FIXME: - Неизвестный вид спорта
    case sportTanks = "sport_tanks"
    /// Телеигра
    case sportTeleigra = "sport_teleigra"
    /// Теннис
    case sportTennis = "sport_tennis"
    /// Волейбол
    case sportVolleyball = "sport_volleyball"
    /// Водное поло
    case sportWaterPolo = "sport_water_polo"
    /// Тяжелая атлетика
    case sportWeightlifting = "sport_weightlifting"
    /// Вольная борьба
    case sportWrestling = "sport_wrestling"
    
    
    // MARK: - SportsFilter
    
    /// Фильтр "Видео"
    case sportsFilterVideo = "sportsfilter_video"
    
    
    // MARK: - Cart
    
    case cartExpress = "cart_express"
    /// Система
    case castSystem = "cart_system"
    
    
    // MARK: - Публичные свойства
    
    /// Изображение
    var image: UIImage {
        return UIImage(asset: self)
    }
    
}


// MARK: - UIImage

extension UIImage {
    
    // MARK: - Инициализация
    
    convenience init!(asset: ImageAsset) {
        self.init(named: asset.rawValue)
    }
    
    convenience init!(asset: ImageAsset, in bundle: Bundle?, compatibleWith traitCollection: UITraitCollection?) {
        self.init(named: asset.rawValue, in: bundle, compatibleWith: traitCollection)
    }
    
}
