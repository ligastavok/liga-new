//
//  UIConstants.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

enum UIConstants {
    
    /// Цвет заднего фона панели вкладок
    static let tabBarBackgroundColor = #colorLiteral(red: 0, green: 0.6039215686, blue: 0.431372549, alpha: 1)
    /// Цвет тени панели вкладок
    static let tabBarShadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.15)
    /// Цвет иконки и текста вкладки
    static let tabBarItemTintColor = #colorLiteral(red: 0, green: 0.4156862745, blue: 0.2901960784, alpha: 1)
    /// Цвет иконки и текста выбранной вкладки
    static let tabBarItemSelectedTintColor: UIColor = .white
    
    /// Цвет заднего фона навигационной панели
    static let navigationBarBackgroundColor = #colorLiteral(red: 0, green: 0.5960784314, blue: 0.4235294118, alpha: 1)
    /// Цвет тени навигационной панели
    static let navigationBarShadowColor: UIColor = .clear
    /// Цвет иконок на навигационной панели
    static let navigationBarItemTintColor: UIColor = .white
    /// Цвет заголовка
    static let navigationBarTitleTintColor: UIColor = .white
    
}
