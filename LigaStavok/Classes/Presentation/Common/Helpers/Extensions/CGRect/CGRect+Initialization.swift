//
//  CGRect+Initialization.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 18.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import CoreGraphics

extension CGRect {
    
    /// Центрированный прямоугольник
    init(size: CGSize, centeredIn rect: CGRect) {
        let x = (rect.width - size.width) / 2
        let y = (rect.height - size.height) / 2
        let origin = CGPoint(x: x, y: y)
        
        self.init(origin: origin, size: size)
    }
    
}
