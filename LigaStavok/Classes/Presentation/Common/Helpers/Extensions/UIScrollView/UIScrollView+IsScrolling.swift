//
//  UIScrollView+IsScrolling.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 31.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIScrollView {
    
    /// Активная прокрутка
    var isScrolling: Bool {
        return self.isDragging || self.isDecelerating
    }
    
}
