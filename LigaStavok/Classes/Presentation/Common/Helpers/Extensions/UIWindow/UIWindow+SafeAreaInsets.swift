//
//  UIWindow+SafeAreaInsets.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIWindow {
    
    /// Отступ для главного окна
    class var keyWindowSafeAreaInsets: UIEdgeInsets {
        guard #available(iOS 11.0, *), let keyWindow = UIApplication.shared.keyWindow else { return .zero }
        return keyWindow.safeAreaInsets
    }
    
}
