//
//  UIWindow+Transit.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

import UIKit

extension UIWindow {
    
    /// Выполнить переход к view controller на главном окне и сделать его основным
    class func keyWindowTransit(to viewController: UIViewController) {
        UIApplication.shared.keyWindow?.transit(to: viewController)
    }
    
    /// Выполнить переход к view controller и сделать его основным
    private func transit(to viewController: UIViewController) {
        let currentSubviews = self.subviews
        
        guard let rootViewController = self.rootViewController else { return }
        insertSubview(viewController.view, belowSubview: rootViewController.view)
        
        UIView.transition(with: self,
                          duration: 0.4,
                          options: .transitionCrossDissolve,
                          animations: {
            self.rootViewController = viewController
        }, completion: { _ in
            currentSubviews.forEach { $0.removeFromSuperview() }
        })
    }
    
}
