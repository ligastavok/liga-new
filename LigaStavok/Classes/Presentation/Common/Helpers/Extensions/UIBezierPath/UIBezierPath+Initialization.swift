//
//  UIBezierPath+Initialization.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIBezierPath {
    
    convenience init?(roundedRect rect: CGRect,
                      topLeftRadius: CGFloat = 0,
                      topRightRadius: CGFloat = 0,
                      bottomLeftRadius: CGFloat = 0,
                      bottomRightRadius: CGFloat = 0) {
        
        self.init()

        guard bottomLeftRadius + bottomRightRadius <= rect.size.width else { return nil }
        guard topLeftRadius + topRightRadius <= rect.size.width else { return nil }
        guard topLeftRadius + bottomLeftRadius <= rect.size.height else { return nil }
        guard topRightRadius + bottomRightRadius <= rect.size.height else { return nil }

        let topLeft = CGPoint(x: rect.minX + topLeftRadius, y: rect.minY + topLeftRadius)
        let topRight = CGPoint(x: rect.maxX - topRightRadius, y: rect.minY + topRightRadius)
        let bottomLeft = CGPoint(x: rect.minX + bottomLeftRadius, y: rect.maxY - bottomLeftRadius)
        let bottomRight = CGPoint(x: rect.maxX - bottomRightRadius, y: rect.maxY - bottomRightRadius)

        let topMidpoint = CGPoint(x: rect.midX, y: rect.minY)
        self.move(to: topMidpoint)

        if topRightRadius == 0 {
            self.addLine(to: topRight)
        } else {
            self.addLine(to: CGPoint(x: rect.maxX - topRightRadius, y: rect.minY))
            self.addArc(withCenter: topRight,
                        radius: topRightRadius,
                        startAngle: -.pi / 2,
                        endAngle: 0,
                        clockwise: true)
        }

        if bottomRightRadius == 0 {
            self.addLine(to: bottomRight)
        } else {
            self.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - bottomRightRadius))
            self.addArc(withCenter: bottomRight,
                        radius: bottomRightRadius,
                        startAngle: 0,
                        endAngle: .pi / 2,
                        clockwise: true)
        }

        if bottomLeftRadius == 0 {
            self.addLine(to: bottomLeft)
        } else {
            self.addLine(to: CGPoint(x: rect.minX + bottomLeftRadius, y: rect.maxY))
            self.addArc(withCenter: bottomLeft,
                        radius: bottomLeftRadius,
                        startAngle: .pi / 2,
                        endAngle: .pi,
                        clockwise: true)
        }

        if topLeftRadius == 0 {
            self.addLine(to: topLeft)
        } else {
            self.addLine(to: CGPoint(x: rect.minX, y: rect.minY + topLeftRadius))
            self.addArc(withCenter: topLeft,
                        radius: topLeftRadius,
                        startAngle: .pi,
                        endAngle: -.pi / 2,
                        clockwise: true)
        }

        self.close()
    }
    
}
