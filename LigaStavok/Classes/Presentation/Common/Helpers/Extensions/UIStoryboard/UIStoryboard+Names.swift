//
//  UIStoryboard+Names.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    /// Файлы storyboard
    enum Storyboard: String {
        
        /// Линия
        case line
        /// Поиск
        case lineSearch
        /// Live-события
        case liveEvents
        /// Видеотрансляции
        case videoboard
        /// Роспись вида спорта
        case sportTournaments
        /// Роспись турнира
        case tournamentEvents
        /// Событие
        case event
        /// Статистика по событию
        case eventStatistics
        /// Live-трансляции
        case liveTranslations
        /// Фильтр по виду спорта
        case sportsFilter
        
        /// Избранное
        case favorites
        
        /// Профиль
        case profile
        
        /// Купонник
        case cart
        
    }
    
}


// MARK: - Публичные свойства

extension UIStoryboard.Storyboard {
    
    /// Название файла
    var name: String {
        return self.rawValue.capitalizingFirstLetter
    }
    
}
