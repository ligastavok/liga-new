//
//  UIColor+Brightness.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIColor {
    
    /// Светлее
    var lighter: UIColor {
        return lighter(by: 0.2)
    }
    
    /// Темнее
    var darker: UIColor {
        return darker(by: 0.2)
    }
    
    /// Контрастный цвет
    var contrastColor: UIColor {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        guard getRed(&red, green: &green, blue: &blue, alpha: &alpha) else { return self }
        
        red *= 255.0
        green *= 255.0
        blue *= 255.0
        
        // http://www.nbdtech.com/Blog/archive/2008/04/27/Calculating-the-Perceived-Brightness-of-a-Color.aspx
        let brightness = sqrt(red * red * 0.241 + green * green * 0.691 + blue * blue * 0.068)
        if brightness < 130 {
            return self.lighter
        } else {
            return self.darker
        }
    }
    
    /// Светлее
    func lighter(by percentage: CGFloat) -> UIColor {
        return self.adjust(by: abs(percentage))
    }
    
    /// Темнее
    func darker(by percentage: CGFloat) -> UIColor {
        return self.adjust(by: -1 * abs(percentage))
    }
    
    /// Корректировать
    func adjust(by percentage: CGFloat) -> UIColor {
        var red: CGFloat = 0, green: CGFloat = 0, blue: CGFloat = 0, alpha: CGFloat = 0
        guard self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) else { return self }
        
        red = min(red + percentage, 1.0)
        green = min(green + percentage, 1.0)
        blue = min(blue + percentage, 1.0)
        
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
}
