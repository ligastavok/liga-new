//
//  UINavigationController+Transparent.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/2/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UINavigationController {

    /// Сделать NavigationBar прозрачным для StretchyHeader
    func setNavigationBarTransparent(_ transparent: Bool, animated: Bool) {
        let duration = animated ? 0.3 : 0
        UIView.animate(withDuration: duration) {
            self.navigationBar.isTranslucent = transparent
            self.navigationBar.setBackgroundImage(transparent ? UIImage() : nil, for: .`default`)
        }
    }

}
