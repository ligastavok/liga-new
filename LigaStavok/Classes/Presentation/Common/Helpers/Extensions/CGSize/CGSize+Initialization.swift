//
//  CGSize+Initialization.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

import CoreGraphics

extension CGSize {
    
    /// Равные ширина и высота
    init(square length: CGFloat) {
        self.init(width: length, height: length)
    }
    
}
