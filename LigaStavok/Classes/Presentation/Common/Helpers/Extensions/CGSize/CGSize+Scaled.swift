//
//  CGSize+Scaled.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 18.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import CoreGraphics

extension CGSize {
    
    /// Размер, пропорционально измененный до указанной ширины
    func scaled(toWidth width: CGFloat) -> CGSize {
        let scale = width / self.width
        guard scale != 1 else { return self }
        
        let height = self.height * scale
        
        return CGSize(width: width, height: height)
    }
    
    /// Размер, пропорционально измененный до указанной высоты
    func scaled(toHeight height: CGFloat) -> CGSize {
        let scale = height / self.height
        guard scale != 1 else { return self }
        
        let width = self.width * scale
        
        return CGSize(width: width, height: height)
    }
    
    /// Размер, пропорционально измененный до указанных границ
    func scaled(toFit size: CGSize) -> CGSize {
        let scale = min(1, min(size.width / self.width, size.height / self.height))
        guard scale != 1 else { return self }
        
        let width = self.width * scale
        let height = self.height * scale
        
        return CGSize(width: width, height: height)
    }
    
    /// Размер, пропорционально измененный в указанных границах
    func scaled(toFill size: CGSize) -> CGSize {
        let scale = min(1, max(size.width / self.width, size.height / self.height))
        guard scale != 1 else { return self }
        
        let width = self.width * scale
        let height = self.height * scale
        
        return CGSize(width: width, height: height)
    }
    
}
