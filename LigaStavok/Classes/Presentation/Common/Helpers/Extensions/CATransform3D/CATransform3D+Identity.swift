//
//  CATransform3D+Identity.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import QuartzCore

extension CATransform3D {
    
    /// Исходная трансформация
    static var identity: CATransform3D {
        return CATransform3DIdentity
    }
    
}
