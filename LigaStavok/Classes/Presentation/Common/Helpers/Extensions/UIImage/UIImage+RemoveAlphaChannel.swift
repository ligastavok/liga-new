//
//  UIImage+RemoveAlphaChannel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIImage {
    
    /// Изображение без alpha-канала
    func removeAlphaChannel() -> UIImage {
        guard let cgImage = self.cgImage else { return self }
        let rect = CGRect(origin: .zero, size: self.size)
        
        let width = cgImage.width
        let height = cgImage.height
        let bitsPerComponent = cgImage.bitsPerComponent
        let bytesPerRow = cgImage.bytesPerRow
        let space = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGImageAlphaInfo.noneSkipLast.rawValue | CGBitmapInfo.byteOrder32Little.rawValue
        
        guard let bitmapContext = CGContext(data: nil,
                                            width: width,
                                            height: height,
                                            bitsPerComponent: bitsPerComponent,
                                            bytesPerRow: bytesPerRow,
                                            space: space,
                                            bitmapInfo: bitmapInfo) else { return self }
        bitmapContext.draw(cgImage, in: rect)
        
        guard let image = bitmapContext.makeImage() else { return self }
        return UIImage(cgImage: image)
    }
    
}
