//
//  UIImage+ImageWithAlpha.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIImage {
    
    /// Изображение с установленным значением прозрачности
    func image(withAlpha alpha: CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        defer { UIGraphicsEndImageContext() }
        
        draw(at: .zero, blendMode: .normal, alpha: alpha)
        
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        return newImage
    }
    
}
