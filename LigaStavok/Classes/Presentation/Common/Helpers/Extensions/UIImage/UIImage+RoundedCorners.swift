//
//  UIImage+RoundedCorners.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 18.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIImage {
    
    /// Изображение с закругленными углами
    func roundedCorners(with size: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, self.scale)
        defer { UIGraphicsEndImageContext() }
        
        guard let context = UIGraphicsGetCurrentContext() else { return self }
        
        let rect = CGRect(origin: .zero, size: size)
        let radius = max(size.width, size.height) / 2
        let path = UIBezierPath(roundedRect: rect, cornerRadius: radius)
        
        context.addPath(path.cgPath)
        context.clip()
        
        let imageSize = self.size.scaled(toFill: size)
        let imageRect = CGRect(size: imageSize, centeredIn: rect)
        draw(in: imageRect)
        
        guard let newImage = UIGraphicsGetImageFromCurrentImageContext() else { return self }
        return newImage
    }
    
}
