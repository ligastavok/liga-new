//
//  UIImage+ImageWithColor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

import UIKit

extension UIImage {
    
    /// Генерация изображения с указанным цветом указанного размера
    class func generate(with color: UIColor, size: CGSize = CGSize(square: 1)) -> UIImage? {
        let rect = CGRect(origin: .zero, size: size)
        
        UIGraphicsBeginImageContext(rect.size)
        defer { UIGraphicsEndImageContext() }
        
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        
        context.setFillColor(color.cgColor)
        context.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        return image
    }
    
}
