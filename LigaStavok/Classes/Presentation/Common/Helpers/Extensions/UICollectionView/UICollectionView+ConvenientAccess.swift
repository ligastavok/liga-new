//
//  UICollectionView+ConvenientAccess.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    /// Регистрация ячейки
    func register<T: UICollectionViewCell>(_: T.Type) where T: NibLoadableView {
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellWithReuseIdentifier: T.storyboardIdentifier)
    }
    
    /// Получить переиспользуемую ячейку
    func dequeueReusableCell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: T.storyboardIdentifier, for: indexPath) as? T else {
            fatalError("Невозможно найти ячейку с идентификатором \(T.storyboardIdentifier)")
        }
        
        return cell
    }
    
    /// Получить ячейку
    func cellForItem<T: UICollectionViewCell>(at indexPath: IndexPath) -> T? {
        guard let cell = cellForItem(at: indexPath) as? T else { return nil }
        return cell
    }
    
}
