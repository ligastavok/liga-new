//
//  UIApplication+StatusBar.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/10/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIApplication {
    
    /// Системный статусбар
    var statusBar: UIView? {
        return UIApplication.shared.value(forKey: "statusBar") as? UIView
    }
    
    /// Скрыть или показать системный статусбар
    func setStatusBarHidden(_ hidden: Bool, withAnimationDuration time: TimeInterval = 0) {
        guard let statusBar = self.statusBar else { return }
        
        UIView.animate(withDuration: time) {
            statusBar.alpha = hidden ? 0 : 1
        }
    }
    
}
