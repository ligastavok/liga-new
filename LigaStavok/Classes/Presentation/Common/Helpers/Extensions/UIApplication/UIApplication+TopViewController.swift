//
//  UIApplication.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/10/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIApplication {
    
    /// Верхний view controller
    class var topViewController: UIViewController? {
        return topViewController()
    }
    
    /// Верхний view controller, в дочернем view controller
    private class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = base as? UINavigationController {
            return topViewController(base: navigationController.visibleViewController)
        }
        if let tabBarController = base as? UITabBarController {
            if let selected = tabBarController.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
    
}
