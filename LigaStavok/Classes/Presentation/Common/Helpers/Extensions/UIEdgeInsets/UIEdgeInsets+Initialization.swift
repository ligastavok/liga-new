//
//  UIEdgeInsets+Initialization.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIEdgeInsets {
    
    init(top: CGFloat) {
        self.init(top: top, left: 0, bottom: 0, right: 0)
    }
    
    init(left: CGFloat) {
        self.init(top: 0, left: left, bottom: 0, right: 0)
    }
    
    init(bottom: CGFloat) {
        self.init(top: 0, left: 0, bottom: bottom, right: 0)
    }
    
    init(right: CGFloat) {
        self.init(top: 0, left: 0, bottom: 0, right: right)
    }
    
}
