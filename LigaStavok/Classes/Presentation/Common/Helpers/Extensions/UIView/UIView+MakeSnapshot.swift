//
//  UIView+MakeSnapshot.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 30.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Сделать снимок
    func makeSnapshot(opaque: Bool = false, afterScreenUpdates: Bool) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, opaque, 0)
        defer { UIGraphicsEndImageContext() }
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: afterScreenUpdates)
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
}
