//
//  UIView+Shadow.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 21.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Тень
    func dropShadow(shadowOffset: CGSize = CGSize(width: 0, height: 2), shadowRadius: CGFloat = 0.2, shadowOpacity: Float = 0.1) {
        self.layer.masksToBounds = false
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowRadius = shadowRadius
        self.layer.shadowOpacity = shadowOpacity
    }

}
