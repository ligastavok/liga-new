//
//  UIView+Pin.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIView {
    
    /// Привязать края к элементу
    @discardableResult
    func pinEdges(to view: UIView, with edgeInsets: UIEdgeInsets = .zero) -> [NSLayoutConstraint] {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints = [
            self.topAnchor.constraint(equalTo: view.topAnchor, constant: edgeInsets.top),
            self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: edgeInsets.right),
            self.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: edgeInsets.bottom),
            self.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: edgeInsets.left)
        ]
        NSLayoutConstraint.activate(constraints)
        
        return constraints
    }
    
    /// Привязать края к родительскому элементу
    @discardableResult
    func pinEdgesToSuperview(with edgeInsets: UIEdgeInsets = .zero) -> [NSLayoutConstraint] {
        guard let superview = self.superview else { return [] }
        return pinEdges(to: superview, with: edgeInsets)
    }
    
}
