//
//  CGAffineTransform+Transform3D.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 24.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import QuartzCore

extension CGAffineTransform {
    
    /// Преобразовать в 3D
    var transform3D: CATransform3D {
        return CATransform3DMakeAffineTransform(self)
    }
    
}
