//
//  UITableView+ConvenientAccess.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UITableView {
    
    /// Регистрация верхнего или нижнего колонтитула
    func register<T: UITableViewHeaderFooterView>(_: T.Type) where T: NibLoadableView {
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forHeaderFooterViewReuseIdentifier: T.nibName)
    }
    
    /// Регистрация ячейки
    func register<T: UITableViewCell>(_: T.Type) where T: NibLoadableView {
        let nib = UINib(nibName: T.nibName, bundle: nil)
        register(nib, forCellReuseIdentifier: T.storyboardIdentifier)
    }
    
    /// Получить переиспользуемый верхний или нижний колонтитул
    func dequeueReusableHeaderFooterView<T: UITableViewHeaderFooterView>() -> T where T: NibLoadableView {
        guard let headerFooterView = dequeueReusableHeaderFooterView(withIdentifier: T.nibName) as? T else {
            fatalError("Невозможно найти верхний или нижний колонтитул с идентификатором \(T.nibName)")
        }
        
        return headerFooterView
    }
    
    /// Получить переиспользуемую ячейку
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.storyboardIdentifier, for: indexPath) as? T else {
            fatalError("Невозможно найти ячейку с идентификатором \(T.storyboardIdentifier)")
        }
        
        return cell
    }
    
    /// Получить ячейку
    func cellForRow<T: UITableViewCell>(at indexPath: IndexPath) -> T? {
        guard let cell = cellForRow(at: indexPath) as? T else { return nil }
        return cell
    }
    
}
