//
//  UIViewController+AddChild.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Добавить дочерний view controller
    func add(child viewController: UIViewController, in view: UIView? = nil, pinToEdges: Bool = false, withConstraints: Bool = true) {
        let view: UIView = view ?? self.view
        
        viewController.willMove(toParentViewController: self)
        view.addSubview(viewController.view)
        addChildViewController(viewController)
        viewController.didMove(toParentViewController: self)
        
        guard pinToEdges else { return }
        
        viewController.view.translatesAutoresizingMaskIntoConstraints = !withConstraints
        
        if withConstraints {
            viewController.view.pinEdges(to: view)
        } else {
            viewController.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        }
    }
    
}
