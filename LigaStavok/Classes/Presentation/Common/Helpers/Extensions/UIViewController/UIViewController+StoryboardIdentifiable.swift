//
//  UIViewController+StoryboardIdentifiable.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIViewController: StoryboardIdentifiable { }
