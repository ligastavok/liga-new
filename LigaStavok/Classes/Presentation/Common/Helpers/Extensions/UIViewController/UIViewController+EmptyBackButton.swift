//
//  UIViewController+EmptyBackButton.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Удалить текст для кнопки "Назад"
    func makeBackButtonEmpty() {
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: .empty, style: .plain, target: nil, action: nil)
    }
    
}
