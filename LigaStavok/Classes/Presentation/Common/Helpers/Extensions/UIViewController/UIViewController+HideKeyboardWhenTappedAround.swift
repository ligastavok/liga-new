//
//  UIViewController+HideKeyboardWhenTappedAround.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 06.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Скрывать клавиатуру при тапе вне поля ввода
    @discardableResult
    func hideKeyboardWhenTappedAround() -> UIGestureRecognizer {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGestureRecognizer.cancelsTouchesInView = false
        
        self.view.addGestureRecognizer(tapGestureRecognizer)
        return tapGestureRecognizer
    }
    
    /// Скрыть клавиатуру
    @objc
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
}
