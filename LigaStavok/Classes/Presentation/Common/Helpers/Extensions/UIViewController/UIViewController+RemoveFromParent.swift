//
//  UIViewController+RemoveFromParent.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Удалить дочерний view controller
    func removeFromParent() {
        willMove(toParentViewController: nil)
        
        self.view.removeFromSuperview()
        removeFromParentViewController()
    }
    
}
