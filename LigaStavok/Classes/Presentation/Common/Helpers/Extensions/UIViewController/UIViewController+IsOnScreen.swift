//
//  UIViewController+IsOnScreen.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /// Признак отображения view controller'а в данный момент
    var isOnScreen: Bool {
        return self.isViewLoaded && self.view.window != nil
    }
    
}
