//
//  NSLayoutConstraint+ReportAmbiguity.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 22.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    
    /// Вывести в консоль информацию о неоднозначном позиционировании
    class func reportAmbiguity(_ view: UIView? = nil) {
        guard let view = view ?? UIApplication.shared.keyWindow, !view.subviews.isEmpty else { return }
        
        for subview in view.subviews {
            print("\(subview) hasAmbiguousLayout=\(subview.hasAmbiguousLayout)")
            reportAmbiguity(subview)
        }
    }
    
}
