//
//  NSLayoutConstraint+ListConstraints.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 22.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    
    /// Вывести в консоль список ограничений
    class func listConstraints(_ view: UIView? = nil) {
        guard let view = view ?? UIApplication.shared.keyWindow, !view.subviews.isEmpty else { return }
        
        for subview in view.subviews {
            let horizontalConstraints = subview.constraintsAffectingLayout(for: .horizontal)
            let verticalConstraints = subview.constraintsAffectingLayout(for: .vertical)
            
            print("\n\n\(subview)\nH: \(horizontalConstraints)\nV:\(verticalConstraints)")
            listConstraints(subview)
        }
    }
    
}
