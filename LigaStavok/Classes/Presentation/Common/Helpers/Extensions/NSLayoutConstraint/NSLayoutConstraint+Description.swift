//
//  NSLayoutConstraint+Description.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
    
    /// Вывод идентификатора ограничения, с которым возникла проблема
    open override var description: String {
        let id = self.identifier ?? .empty
        return "id: \(id), constant: \(self.constant)"
    }
    
}
