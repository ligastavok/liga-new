//
//  Router.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

import UIKit

/// Протокол роутера
protocol Router {
    
    /// Контроллер, с которого осуществляется навигация
    var viewController: UIViewController? { get }
    
}

extension Router {
    
    /// Выполнить переход к view controller и сделать его основным
    func makeRoot(_ viewController: UIViewController) {
        if let presenter = self.viewController, let navigationController = presenter.navigationController {
            navigationController.viewControllers.removeAll()
        }
        
        UIWindow.keyWindowTransit(to: viewController)
    }
    
    /// Push навигация
    func show(_ viewController: UIViewController, animated: Bool = true) {
        guard let presenter = self.viewController else {
            makeRoot(viewController)
            return
        }
        
        presenter.navigationController?.pushViewController(viewController, animated: animated)
    }
    
    /// Замена всех view controller в стеке на новый
    func showOnly(_ viewController: UIViewController) {
        guard let presenter = self.viewController else {
            makeRoot(viewController)
            return
        }
        
        presenter.navigationController?.viewControllers = [viewController]
    }
    
    /// Modal навигация
    func presentModally(_ viewController: UIViewController) {
        guard let presenter = self.viewController else {
            makeRoot(viewController)
            return
        }
        
        presenter.present(viewController, animated: true)
    }
    
    /// Закрытие модального view controller
    func dismiss() {
        self.viewController?.dismiss(animated: true, completion: nil)
    }
    
    /// Удалить экран из стека
    func pop(animated: Bool = true) {
        self.viewController?.navigationController?.popViewController(animated: animated)
    }
    
    /// Переход к корневому view controller
    func popToRoot() {
        _ = self.viewController?.navigationController?.popToRootViewController(animated: true)
    }
    
}
