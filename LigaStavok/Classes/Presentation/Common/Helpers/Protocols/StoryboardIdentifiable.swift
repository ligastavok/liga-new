//
//  StoryboardIdentifiable.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol StoryboardIdentifiable {
    
    /// Идентификатор на storyboard
    static var storyboardIdentifier: String { get }
    
}


// MARK: - UIViewController

extension StoryboardIdentifiable where Self: UIViewController {
    
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
    
}


// MARK: - UICollectionViewCell

extension StoryboardIdentifiable where Self: UICollectionViewCell {
    
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
    
}


// MARK: - UITableViewCell

extension StoryboardIdentifiable where Self: UITableViewCell {
    
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
    
}
