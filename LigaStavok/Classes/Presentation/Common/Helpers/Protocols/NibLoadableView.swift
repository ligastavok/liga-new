//
//  NibLoadableView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol NibLoadableView: class {
    
    /// Название Nib-файла
    static var nibName: String { get }
    
}


// MARK: - UIView

extension NibLoadableView where Self: UIView {
    
    static var nibName: String {
        return String(describing: self)
    }
    
    /// Загрузить view из xib
    static func loadFromNib() -> Self {
        let nibName = self.nibName
        let bundle = Bundle(for: self)
        let nib = UINib(nibName: nibName, bundle: bundle)
        guard let view = nib.instantiate(withOwner: nil, options: nil).first as? Self else { fatalError() }
        
        return view
    }
    
}
