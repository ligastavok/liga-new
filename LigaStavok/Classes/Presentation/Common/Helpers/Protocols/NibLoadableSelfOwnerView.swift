//
//  NibLoadableSelfOwnerView.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 26.02.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import UIKit

protocol NibLoadableSelfOwnerView: NibLoadableView {
    
    /// Название Nib-файла
    var nibName: String { get }
    
}


// MARK: - UIView

extension NibLoadableSelfOwnerView where Self: UIView {
    
    var nibName: String {
        return String(describing: type(of: self))
    }
    
    /// Загрузить view из xib
    func loadFromNib() -> UIView {
        let nibName = self.nibName
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: nibName, bundle: bundle)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else { fatalError() }
        
        return view
    }
    
    /// Загрузить view из xib и прикрепить к краям
    func loadFromNibAndAttachToSelf() {
        let view = loadFromNib()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(view)
        view.pinEdges(to: self)
    }
    
}
