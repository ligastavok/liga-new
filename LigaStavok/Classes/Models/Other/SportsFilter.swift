//
//  SportsFilter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 22.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Фильтр по виду спорта
enum SportsFilter {
    
    /// Нет фильтра
    case all
    /// Фильтр по наличию видеотрансляции
    case video
    /// Фильтр по виду спорта
    case game(gameId: Int)
    
}


// MARK: - Публичные свойства

extension SportsFilter {
    
    /// Идентификатор вида спорта
    var gameId: Int? {
        switch self {
        case .game(let gameId):
            return gameId
            
        default:
            return nil
        }
    }
    
}


// MARK: - Equatable

extension SportsFilter: Equatable {
    
    static func == (lhs: SportsFilter, rhs: SportsFilter) -> Bool {
        switch (lhs, rhs) {
        case (.all, .all):
            return true
        case (.video, .video):
            return true
        case (let .game(gameId1), let .game(gameId2)):
            return gameId1 == gameId2
            
        default:
            return false
        }
    }
    
}
