//
//  PeriodFilter.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 29.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Фильтр по периоду отображения
enum PeriodFilter: Int {
    
    /// Все
    case all = 0
    /// Сегодня
    case today
    /// Завтра
    case tomorrow
    /// Неделя
    case week
    
}


// MARK: - Публичные свойства

extension PeriodFilter {
    
    /// Значение
    var value: Int? {
        switch self {
        case .all:
            return nil
        case .today:
            let endOfDay = Date().dateFor(.endOfDay)
            return Int(endOfDay.serverDate.timeIntervalSince1970 * 1000)
        case .tomorrow:
            let tomorrow = Date().dateFor(.endOfDay).dateFor(.tomorrow)
            return Int(tomorrow.serverDate.timeIntervalSince1970 * 1000)
        case .week:
            let endOfWeek = Date().dateFor(.endOfWeek)
            return Int(endOfWeek.serverDate.timeIntervalSince1970 * 1000)
        }
    }
    
}
