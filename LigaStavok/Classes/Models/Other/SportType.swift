//
//  SportType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 25.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Вид спорта
enum SportType: Int {
    
    /// Call of Duty
    case eGamesCod = 15578
    /// Counter-Strike
    case eGamesCs = 12906
    /// Dota-2
    case eGamesDota2 = 12917
    /// League of Legends
    case eGamesLol = 12947
    /// OverWatch
    case eGamesOw = 15579
    /// Rocket League
    case eGamesRockl = 15580
    /// StarCraft II
    case eGamesSc2 = 12918
    /// World of Tanks
    case wot = 15776
    /// Австралийский футбол
    case aussieRules = 1445
    /// Авто-мото спорт
    case autoMoto = 115
    /// Американский футбол
    case amFootball = 23
    /// Бадминтон
    case badminton = 1224
    /// Байдарки и каноэ
    case canoeing = 1228
    /// Баскетбол
    case basketball = 25
    /// Бейсбол
    case baseball = 24
    /// Биатлон
    case biathlon = 26
    /// Бобслей
    case bobsleigh = 2245
    /// Бои без правил
    case martialArts = 2130
    /// Бокс
    case boxing = 27
    /// Боулинг
    case bouling = 1255
    /// Велоспорт
    case cycling = 1225
    /// Виртуальная футбольная лига
    case vfl = 4941
    /// Виртуальный футбольный евро кубок
    case vfec = 12416
    /// Виртуальный футбольный евро кубок (Итоги)
    case vfecOutright = 12417
    /// Водное поло
    case waterPolo = 355
    /// Волейбол
    case volleyball = 128
    /// Вольная борьба
    case wrestling = 1241
    /// Выборы
    case elections = 5190
    /// Выборы в ГД
    case electionsGd2011 = 4295
    /// Гандбол
    case handball = 30
    /// Гольф
    case golf = 29
    /// Горные лыжи
    case alpineSkiing = 2206
    /// Гребля
    case rowing = 1229
    /// Греко-римская борьба
    case grWrestling = 1240
    /// Дартс
    case darts = 6207
    /// Дзю-До
    case judo = 1233
    /// Е-Спорт
    case cybersport = 10014
    /// Игроки
    case players = 9157
    /// Индор Футбол
    case indoorfootball = 11316
    /// Индорхоккей
    case indoorhockey = 2240
    /// Кёрлинг
    case curling = 1507
    /// Конный спорт
    case equestriansport = 12891
    /// Коньки
    case skate = 1408
    /// Крикет
    case cricket = 1856
    /// Легкая атлетика
    case athletics = 1223
    /// Лыжи
    case ski = 1349
    /// Лыжное двоеборье
    case nordicCombined = 2248
    /// Настольный теннис
    case tableTennis = 1246
    /// Нетбол
    case netball = 3539
    /// Олимпиада
    case olimpicGames = 1234
    /// Парусный спорт
    case sailing = 1247
    /// Плавание
    case swimming = 1239
    /// Пляжный волейбол
    case beachVolley = 1896
    /// Пляжный гандбол
    case beachHandball = 696
    /// Пляжный футбол
    case beachSoccer = 569
    /// Покер
    case poker = 5814
    /// Политика
    case politics = 5180
    /// Прыжки в воду
    case diving = 1230
    /// Прыжки с трамплина
    case skiJumping = 2251
    /// Регби
    case rugby = 7486
    /// Регби 7
    case rugbySevens = 2216
    /// Регби лига
    case rugbyLeague = 2215
    /// Регби союз
    case rugbyUnion = 2214
    /// Сани
    case luge = 2247
    /// Синхронное плавание
    case sinchronizedSwimming = 1244
    /// Скачки
    case horcerase = 2358
    /// Сквош
    case sqwosh = 1356
    /// Скелетон
    case skeleton = 2250
    /// Смешанные единоборства
    case mma = 12727
    /// Сноуборд
    case snowboard = 2252
    /// Снукер
    case snooker = 32
    /// Собачьи бега
    case greyhound = 14693
    /// Софтбол
    case softball = 1238
    /// Спецпредложение
    case raznoe = 1296
    /// Спортивная гимнастика
    case artisticGimnastics = 1232
    /// Стрельба
    case shooting = 1227
    /// Стрельба из лука
    case archery = 1222
    /// Телеигра
    case teleigra = 136
    /// Теннис
    case tennis = 34
    /// Тхэквондо
    case taekwondo = 1242
    /// Тяжелая атлетика
    case weightlifting = 1245
    /// Фехтование
    case fencing = 1226
    /// Фигурное катание
    case figureSkating = 2244
    /// Флорбол
    case floorball = 1368
    /// Формула 1
    case formulaOne = 12726
    /// Фристайл
    case freestyleSkiing = 2246
    /// Футбол
    case soccer = 33
    /// Футбол. Итоги
    case soccerOutrights = 4293
    /// Футзал
    case futzal = 135
    /// Хоккей
    case iceHockey = 31
    /// Хоккей на траве
    case fieldHockey = 1231
    /// Хоккей с мячом
    case bandy = 1308
    /// Художественная гимнастика
    case rhythmic = 1243
    /// Шары
    case bowls = 2098
    /// Шахматы
    case chess = 28
    /// Шорттрек
    case shortTrack = 2249
    /// Шоу-бизнес
    case showBusiness = 5181
    
}


// MARK: - Публичные свойства

extension SportType {
    
    /// Иконка
    var imageAsset: ImageAsset {
        switch self {
        case .eGamesCod:
            return .sportESports
        case .eGamesCs:
            return .sportESports
        case .eGamesDota2:
            return .sportESports
        case .eGamesLol:
            return .sportESports
        case .eGamesOw:
            return .sportESports
        case .eGamesRockl:
            return .sportESports
        case .eGamesSc2:
            return .sportESports
        case .wot:
            return .sportTanks
        case .aussieRules:
            return .sportAussieRules
        case .autoMoto:
            return .sportAutoMoto
        case .amFootball:
            return .sportAmFootball
        case .badminton:
            return .sportBadminton
        case .canoeing:
            return .sportCanoeing
        case .basketball:
            return .sportBasketball
        case .baseball:
            return .sportBaseball
        case .biathlon:
            return .sportBiathlon
        case .bobsleigh:
            return .sportBobsleigh
        case .martialArts:
            return .sportMartialArts
        case .boxing:
            return .sportBoxing
        case .bouling:
            return .sportBouling
        case .cycling:
            return .sportCycling
        case .vfl:
            return .sportSoccer
        case .vfec:
            return .sportSoccer
        case .vfecOutright:
            return .sportSoccer
        case .waterPolo:
            return .sportWaterPolo
        case .volleyball:
            return .sportVolleyball
        case .wrestling:
            return .sportDefault
        case .elections:
            return .sportElections
        case .electionsGd2011:
            return .sportElections
        case .handball:
            return .sportHandball
        case .golf:
            return .sportGolf
        case .alpineSkiing:
            return .sportAlpineSkiing
        case .rowing:
            return .sportRowing
        case .grWrestling:
            return .sportGrWrestling
        case .darts:
            return .sportDarts
        case .judo:
            return .sportJudo
        case .cybersport:
            return .sportESports
        case .players:
            return .sportPlayers
        case .indoorfootball:
            return .sportSoccer
        case .indoorhockey:
            return .sportFieldHockey
        case .curling:
            return .sportCurling
        case .equestriansport:
            return .sportEquestrianSport
        case .skate:
            return .sportSkate
        case .cricket:
            return .sportCricket
        case .athletics:
            return .sportAthletics
        case .ski:
            return .sportSki
        case .nordicCombined:
            return .sportNordicCombined
        case .tableTennis:
            return .sportTableTennis
        case .netball:
            return .sportNetball
        case .olimpicGames:
            return .sportOlimpicGames
        case .sailing:
            return .sportSailing
        case .swimming:
            return .sportSwimming
        case .beachVolley:
            return .sportVolleyball
        case .beachHandball:
            return .sportHandball
        case .beachSoccer:
            return .sportSoccer
        case .poker:
            return .sportPoker
        case .politics:
            return .sportPolitics
        case .diving:
            return .sportDiving
        case .skiJumping:
            return .sportSkiJumping
        case .rugby:
            return .sportRugby
        case .rugbySevens:
            return .sportRugby
        case .rugbyLeague:
            return .sportRugby
        case .rugbyUnion:
            return .sportRugby
        case .luge:
            return .sportLuge
        case .sinchronizedSwimming:
            return .sportSinchronizedSwimming
        case .horcerase:
            return .sportHorcerase
        case .sqwosh:
            return .sportSqwosh
        case .skeleton:
            return .sportSkeleton
        case .mma:
            return .sportMma
        case .snowboard:
            return .sportSnowboard
        case .snooker:
            return .sportSnooker
        case .greyhound:
            return .sportDefault
        case .softball:
            return .sportSoftball
        case .raznoe:
            return .sportSpecialOffer
        case .artisticGimnastics:
            return .sportArtisticGimnastics
        case .shooting:
            return .sportShooting
        case .archery:
            return .sportArchery
        case .teleigra:
            return .sportTeleigra
        case .tennis:
            return .sportTennis
        case .taekwondo:
            return .sportTaekwondo
        case .weightlifting:
            return .sportWeightlifting
        case .fencing:
            return .sportFencing
        case .figureSkating:
            return .sportFigureSkating
        case .floorball:
            return .sportFloorball
        case .formulaOne:
            return .sportFormulaOne
        case .freestyleSkiing:
            return .sportFreestyleSkiing
        case .soccer:
            return .sportSoccer
        case .soccerOutrights:
            return .sportSoccer
        case .futzal:
            return .sportSoccer
        case .iceHockey:
            return .sportIceHockey
        case .fieldHockey:
            return .sportFieldHockey
        case .bandy:
            return .sportBandy
        case .rhythmic:
            return .sportRhythmic
        case .bowls:
            return .sportBowls
        case .chess:
            return .sportChess
        case .shortTrack:
            return .sportShortTrack
        case .showBusiness:
            return .sportShowBusiness
        }
    }
    
}
