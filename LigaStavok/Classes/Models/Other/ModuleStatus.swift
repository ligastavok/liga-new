//
//  ModuleStatus.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 11.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

enum ModuleStatus {
    
    /// Неизвестный
    case unknown
    /// Загрузка
    case loading(type: LoadingType)
    /// Данные загружены
    case loaded
    /// Ошибка
    case error(value: Error)
    
}


// MARK: - Публичные методы

extension ModuleStatus {
    
    /// Проверка равенства, не учитывая ассоциированное значение
    func checkEqualityWithoutAssociatedValue(with status: ModuleStatus) -> Bool {
        switch (self, status) {
        case (.unknown, .unknown):
            return true
        case (.loading, .loading):
            return true
        case (.loaded, .loaded):
            return true
        case (.error, .error):
            return true
            
        default:
            return false
        }
    }
    
}


// MARK: - Equatable

extension ModuleStatus: Equatable {
    
    static func == (lhs: ModuleStatus, rhs: ModuleStatus) -> Bool {
        switch (lhs, rhs) {
        case (.unknown, .unknown):
            return true
        case (let .loading(type1), let .loading(type2)):
            return type1 == type2
        case (.loaded, .loaded):
            return true
        case (let .error(error1), let .error(error2)):
            return error1.localizedDescription == error2.localizedDescription
            
        default:
            return false
        }
    }
    
}
