//
//  BetValueChangeType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 07.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Тип изменения коэффициента
enum BetValueChangeType {
    
    /// Значение не изменилось
    case none
    /// Значение увеличилось
    case ascending(lastUpdate: TimeInterval)
    /// Значение уменьшилось
    case descending(lastUpdate: TimeInterval)
    
}


// MARK: - Equatable

extension BetValueChangeType: Equatable {
    
    static func == (lhs: BetValueChangeType, rhs: BetValueChangeType) -> Bool {
        switch (lhs, rhs) {
        case (.none, .none):
            return true
        case (let .ascending(lastUpdate1), let .ascending(lastUpdate2)):
            return lastUpdate1 == lastUpdate2
        case (let .descending(lastUpdate1), let .descending(lastUpdate2)):
            return lastUpdate1 == lastUpdate2
            
        default:
            return false
        }
    }
    
}


// MARK: - Hashable

extension BetValueChangeType: Hashable {
    
    var hashValue: Int {
        return String(describing: self).hashValue
    }
    
}
