//
//  LoadingType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 11.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//
    
enum LoadingType {
    
    /// Загрузка из кэша
    case cache
    /// Загрузка по сети интернет
    case network
    
}


// MARK: - Equatable

extension LoadingType: Equatable {
    
    static func == (lhs: LoadingType, rhs: LoadingType) -> Bool {
        switch (lhs, rhs) {
        case (.cache, .cache):
            return true
        case (.network, .network):
            return true
            
        default:
            return false
        }
    }
    
}
