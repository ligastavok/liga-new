//
//  LiveEventsMode.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 07.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Режим работы модуля "Live-события"
enum LiveEventsMode {
    
    /// Все
    case all
    /// Видеотрансляции
    case videoTranslations
    
}


// MARK: - Публичные свойства

extension LiveEventsMode {
    
    /// Заголовок в навигационной панели
    var navigationBarTitle: String {
        switch self {
        case .all:
            return Localization.LiveEvents.liveNavigationBarTitle
        case .videoTranslations:
            return Localization.LiveEvents.videoTranslationsNavigationBarTitle
        }
    }
    
    /// Фильтр по виду спорта по-умолчанию
    var defaultSportsFilter: SportsFilter {
        switch self {
        case .all:
            return .all
        case .videoTranslations:
            return .video
        }
    }
    
}
