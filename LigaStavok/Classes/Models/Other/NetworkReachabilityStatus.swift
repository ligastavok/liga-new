//
//  NetworkReachabilityStatus.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 20.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Статус доступности интернет-соединения
enum NetworkReachabilityStatus {
    
    /// Неизвестно
    case unknown
    /// Недоступно
    case notReachable
    /// Доступно
    case reachable(connectionType: ConnectionType)
    
}
