//
//  ConnectionType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 20.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Тип интернет-соединения
enum ConnectionType {
    
    /// Ethernet или Wifi
    case wifi
    /// WWAN
    case cellular
    
}
