//
//  Favorite.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Избранное событие
final class Favorite {
    
    // MARK: - Публичные свойства
    
    /// ID Вида спорта
    let id: Int
    /// Общее количество событий по выборке
    let total: Int
    /// События
    let events: [EventObject]
    
    
    // MARK: - Инициализация
    
    init(id: Int, total: Int, events: [EventObject]) {
        self.id = id
        self.total = total
        self.events = events
    }
    
}
