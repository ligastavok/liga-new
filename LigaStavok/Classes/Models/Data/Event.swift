//
//  Event.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация о событии
final class Event {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор топика (лига, соревнование etc.)
    let topicId: Int
    /// Название топика
    let topicTitle: String
    /// Extra заголовок топика
    let subtopic: String?
    /// Человекопонятная дата начала события
    let gameDate: String // FIXME: - Выяснить формат "2018-12-31 23:59:00.0"
    // swiftlint:disable:next line_length
    /// Номер события в линии, внутренний параметр сортировки линии по приоритету (уникальный в рамках конкретной версии документа) - устаревает в силу ввода TopicLinePriority в сервисе Dispenser
    let position: Int
    /// Приоритет события (сейчас может быть 0 или 1) - устаревает в силу ввода EventLinePriority в сервисе Dispenser
    let priority: Int
    /// Приоритет топика
    let topicPriority: Int
    /// Название команды #1
    let firstTeamName: String
    /// Название команды #2
    let secondTeamName: String?
    /// Строка вида `КОМ {имя_команды} играет за CT` (существует для обратной совместимости с сервисами терминалов)
    let currentCtTeam: String? // FIXME: - Придумать нормальное название
    // swiftlint:disable:next line_length
    /// Идентификатор внешнего события в линии (Betradar, Betgenius, etc.), только для импортированных событий, а также только для событий с extId допускается использование виджета LMT, при условии покрытия вида спорта
    let externalId: String?
    /// Код статуса игры
    let status: String?
    /// Человекопонятное название состояния игры
    let statusTranslated: String?
    /// Прошедшее время матча
    let matchTime: String?
    /// Ожидаемое время окончания матча
    let remainingTime: String?
    /// Подающий игрок (теннис, настольный теннис, волейбол и т.п.)
    let server: String?
    /// Комментарий букмекера к матчу
    let comment: String
    /// Количество минут до начала матча
    let minutesBeforeStart: Int
    /// Тай-брейк
    let tiebreak: String?
    /// Счет в серии, обычно используется для вывода в комментарии к событию в доматчевых событиях
    let seriesScore: String?
    /// Название события
    let eventTitle: String
    /// Признак завершенного события (атрибут появляется когда событие завершилось)
    let expired: Bool
    /// Список участников события
    let competitors: [Competitor]
    /// Номер сезона
    let season: Int?
    /// Номер игрового дня
    let matchDay: Int?
    /// Номер канала в видеопотоке для видеотрансляции
    let videoChannel: String?
    /// URL видеопотока для видеотрансляции
    let videoStream: String?
    /// URL HLS видеопотока
    let videoStreamHls: String?
    /// Ссылка на обзор события
    let reviewUrl: String?
    /// Внутренний идентификатор поставщика
    let serviceId: Int
    
    
    // MARK: - Инициализация
    
    init(topicId: Int,
         topicTitle: String,
         subtopic: String?,
         gameDate: String,
         position: Int,
         priority: Int,
         topicPriority: Int,
         firstTeamName: String,
         secondTeamName: String?,
         currentCtTeam: String?,
         externalId: String?,
         status: String?,
         statusTranslated: String?,
         matchTime: String?,
         remainingTime: String?,
         server: String?,
         comment: String,
         minutesBeforeStart: Int,
         tiebreak: String?,
         seriesScore: String?,
         eventTitle: String,
         expired: Bool,
         competitors: [Competitor],
         season: Int?,
         matchDay: Int?,
         videoChannel: String?,
         videoStream: String?,
         videoStreamHls: String?,
         reviewUrl: String?,
         serviceId: Int) {
        
        self.topicId = topicId
        self.topicTitle = topicTitle
        self.subtopic = subtopic
        self.gameDate = gameDate
        self.position = position
        self.priority = priority
        self.topicPriority = topicPriority
        self.firstTeamName = firstTeamName
        self.secondTeamName = secondTeamName
        self.currentCtTeam = currentCtTeam
        self.externalId = externalId
        self.status = status
        self.statusTranslated = statusTranslated
        self.matchTime = matchTime
        self.remainingTime = remainingTime
        self.server = server
        self.comment = comment
        self.minutesBeforeStart = minutesBeforeStart
        self.tiebreak = tiebreak
        self.seriesScore = seriesScore
        self.eventTitle = eventTitle
        self.expired = expired
        self.competitors = competitors
        self.season = season
        self.matchDay = matchDay
        self.videoChannel = videoChannel
        self.videoStream = videoStream
        self.videoStreamHls = videoStreamHls
        self.reviewUrl = reviewUrl
        self.serviceId = serviceId
    }
    
}
