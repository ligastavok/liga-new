//
//  SportTreeCategory.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Элемент дерева видов спорта (категория)
final class SportTreeCategory {
    
    // MARK: - Публичные свойства
    
    /// Список категорий
    let count: Int
    /// Количество событий по данному виду спорта
    let gameId: Int
    /// ID вида спорта
    let id: Int
    /// Приоритет вида спорта
    let priority: Int
    /// Транслитеризированное название вида спорта, допускается использовать это название в качестве ЧПУ
    let sname: String
    /// Название вида спорта
    let title: String
    /// Список чемпионатов
    let tournaments: [SportTreeTournament]
    
    
    // MARK: - Инициализация
    
    init(count: Int,
         gameId: Int,
         id: Int,
         priority: Int,
         sname: String,
         title: String,
         tournaments: [SportTreeTournament]) {
        
        self.count = count
        self.gameId = gameId
        self.id = id
        self.priority = priority
        self.sname = sname
        self.title = title
        self.tournaments = tournaments
    }
    
}
