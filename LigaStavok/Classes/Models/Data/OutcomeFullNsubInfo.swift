//
//  OutcomeFullNsubInfo.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация о маркете
final class OutcomeFullNsubInfo {
    
    // MARK: - Публичные свойства
    
    /// ID маркета
    let id: Int
    /// Название маркета
    let title: String
    /// Тип маркета ('WIN', 'TTL', 'HAN' etc.)
    let type: String
    /// Порядок сортировки маркета
    let position: Int
    /// Флаг блокировки маркета (все исходы в нем заблокированы)
    let locked: Bool
    /// Флаг поломанности маркета (все исходы в нем поломаны)
    let corrupted: Bool
    /// Коллекция исходов
    let nsub: OutcomesList
    
    
    // MARK: - Инициализация
    
    init(id: Int,
         title: String,
         type: String,
         position: Int,
         locked: Bool,
         corrupted: Bool,
         nsub: OutcomesList) {
        
        self.id = id
        self.title = title
        self.type = type
        self.position = position
        self.locked = locked
        self.corrupted = corrupted
        self.nsub = nsub
    }
    
}
