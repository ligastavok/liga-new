//
//  MarketInfo.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация о ставке
final class MarketInfo {
    
    // MARK: - Публичные свойства
    
    /// Ставка
    let market: Market
    /// Исходы
    let outcomes: OutcomesList? // FIXME: - В документации { "<outcome_key>": <outcome> }, а от сервера пришло null
    
    
    // MARK: - Инициализация
    
    init(market: Market, outcomes: OutcomesList?) {
        self.market = market
        self.outcomes = outcomes
    }
    
}
