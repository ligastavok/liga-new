//
//  Score.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Счет
final class Score {
    
    // MARK: - Публичные свойства
    
    /// Название типа объекта счета
    let title: String
    /// Внутренний идентификатор генератора линии
    let nodeId: Int
    /// Тип объекта счета
    let type: String
    /// Счет команды №1
    let firstTeamScore: String
    /// Счет команды №2
    let secondTeamScore: String
    /// Идентификатор блока
    let id: String
    /// Номер части матча (в нотации счетов)
    let partNumber: String
    /// Код блока (в нотации счетов)
    let part: String
    
    
    // MARK: - Инициализация
    
    init(title: String,
         nodeId: Int,
         type: String,
         firstTeamScore: String,
         secondTeamScore: String,
         id: String,
         partNumber: String,
         part: String) {
        
        self.title = title
        self.nodeId = nodeId
        self.type = type
        self.firstTeamScore = firstTeamScore
        self.secondTeamScore = secondTeamScore
        self.id = id
        self.partNumber = partNumber
        self.part = part
    }
    
}
