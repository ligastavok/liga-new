//
//  ProposedType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 14.06.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Типы предложения
enum ProposedType: String {
    
    /// Основная линия
    case mainOffer = "MAINOFFER"
    /// Статистика
    case statistics = "STATISTICS"
    /// Игроки
    case playerProps = "PLAYERPROPS"
    // FIXME: - Выяснить название значения
    case outrights = "OUTRIGHTS"
    // FIXME: - Выяснить название значения
    case outrightsCupGames = "OUTRIGHTSCUPGAMES"
    /// Специальные ставки
    case specials = "SPECIALS"
    
}
