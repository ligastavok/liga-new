//
//  OutcomeFullInfo.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Исход с маркетами
final class OutcomeFullInfo {
    
    // MARK: - Публичные свойства
    
    /// ID блока
    let id: Int
    /// Название блока
    let title: String
    /// Код блока
    let code: String?
    /// Порядок сортировки блока
    let position: Int
    /// Флаг блокировки блока (все исходы в нем заблокированы)
    let locked: Bool
    /// Флаг поломанности блока (все исходы в нем поломаны)
    let corrupted: Bool
    /// Коллекция маркетов
    let nsub: OutcomeFullNsub
    
    
    // MARK: - Инициализация
    
    init(id: Int,
         title: String,
         code: String?,
         position: Int,
         locked: Bool,
         corrupted: Bool,
         nsub: OutcomeFullNsub) {
        
        self.id = id
        self.title = title
        self.code = code
        self.position = position
        self.locked = locked
        self.corrupted = corrupted
        self.nsub = nsub
    }
    
}
