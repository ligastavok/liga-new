//
//  Outcome.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Объект исхода
final class Outcome {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор исхода
    let id: Int
    /// Идентификатор значения исхода
    let facId: Int64
    /// Позиция исхода, предназначено для сортировки
    let position: Int
    /// Заголовок исхода
    let title: String
    /// Значение исхода
    let value: Double
    /// Опциональное поле, обычно содержит базу маркетов (например, для тоталов и фор)
    let adValue: String
    /// Опциональное поле, содержит дополнительное название исхода
    let adTitle: String?
    /// Флаг блокировки исхода
    let locked: Bool
    /// Флаг поломанности исхода
    let corrupted: Bool
    /// Ключ исхода
    let outcomeKey: String
    /// Идентификатор события
    let eventId: Int
    /// Тип события (live или prematch)
    let eventType: String
    /// Идентификатор вида спорта
    let eventGameId: Int
    /// Опциональное поле, комментарий к событию
    let eventComment: String
    /// Словарь учавствующих команд в событии
    let eventTeam: EventTeam
    /// Идентификатор блока
    let partId: Int
    /// Имя блока
    let partName: String
    /// Тип блока
    let partType: String? // FIXME: - Выяснить почему в документации str, а пришло null
    /// Заголовок блока
    let partTitle: String
    /// Идентификатор маркета
    let marketId: Int
    /// Тип маркета
    let marketType: MarketType?
    /// Заголовок маркета
    let marketTitle: String
    /// Идентификатор топика
    let topicId: Int
    
    
    // MARK: - Инициализация
    
    init(id: Int,
         facId: Int64,
         position: Int,
         title: String,
         value: Double,
         adValue: String,
         adTitle: String?,
         locked: Bool,
         corrupted: Bool,
         outcomeKey: String,
         eventId: Int,
         eventType: String,
         eventGameId: Int,
         eventComment: String,
         eventTeam: EventTeam,
         partId: Int,
         partName: String,
         partType: String?,
         partTitle: String,
         marketId: Int,
         marketType: MarketType?,
         marketTitle: String,
         topicId: Int) {
        
        self.id = id
        self.facId = facId
        self.position = position
        self.title = title
        self.value = value
        self.adValue = adValue
        self.adTitle = adTitle
        self.locked = locked
        self.corrupted = corrupted
        self.outcomeKey = outcomeKey
        self.eventId = eventId
        self.eventType = eventType
        self.eventGameId = eventGameId
        self.eventComment = eventComment
        self.eventTeam = eventTeam
        self.partId = partId
        self.partName = partName
        self.partType = partType
        self.partTitle = partTitle
        self.marketId = marketId
        self.marketType = marketType
        self.marketTitle = marketTitle
        self.topicId = topicId
    }
    
}
