//
//  EventObjectWithJSON.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 02.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

/// Общая структура объекта события
final class EventObjectWithJSON: EventObject {
    
    // MARK: - Публичные свойства
    
    /// JSON
    var json: JSON
    
    
    // MARK: - Инициализация
        
    init(eventObject: EventObject, json: JSON) {
        
        self.json = json
        
        // swiftlint:disable:next empty_line_after_super
        super.init(id: eventObject.id,
                   priority: eventObject.priority,
                   popularity: eventObject.popularity,
                   ids: eventObject.ids,
                   gameTitle: eventObject.gameTitle,
                   topicTitle: eventObject.topicTitle,
                   tournamentTitle: eventObject.tournamentTitle,
                   categoryTitle: eventObject.categoryTitle,
                   hash: eventObject.hash,
                   namespace: eventObject.namespace,
                   gameTimestamp: eventObject.gameTimestamp,
                   gameId: eventObject.gameId,
                   name: eventObject.name,
                   title: eventObject.title,
                   status: eventObject.status,
                   event: eventObject.event,
                   scores: eventObject.scores,
                   parts: eventObject.parts,
                   hasUnlocked: eventObject.hasUnlocked,
                   corrupted: eventObject.corrupted,
                   outcomesTypes: eventObject.outcomesTypes,
                   outcomes: eventObject.outcomes,
                   outcomesWinner: eventObject.outcomesWinner,
                   outcomesDouble: eventObject.outcomesDouble,
                   outcomesHandicap1: eventObject.outcomesHandicap1,
                   outcomesHandicap2: eventObject.outcomesHandicap2,
                   outcomesTotal1: eventObject.outcomesTotal1,
                   outcomesTotal2: eventObject.outcomesTotal2,
                   outcomesTotal3: eventObject.outcomesTotal3,
                   outcomesOdd: eventObject.outcomesOdd,
                   outcomesNextGoal: eventObject.outcomesNextGoal,
                   outcomesYepNope: eventObject.outcomesYepNope,
                   outcomesSummary: eventObject.outcomesSummary,
                   outcomesStrz: eventObject.outcomesStrz,
                   outcomesWin1: eventObject.outcomesWin1,
                   outcomesCompare: eventObject.outcomesCompare,
                   outcomesPlace3Way: eventObject.outcomesPlace3Way,
                   outcomesPolePosition: eventObject.outcomesPolePosition,
                   outcomesQualify3: eventObject.outcomesQualify3,
                   outcomesQualify4: eventObject.outcomesQualify4,
                   outcomesQualifyGroup: eventObject.outcomesQualifyGroup,
                   outcomesGroupPlace2: eventObject.outcomesGroupPlace2,
                   outcomesGroupPlace4: eventObject.outcomesGroupPlace4,
                   outcomes1Goal2Way: eventObject.outcomes1Goal2Way,
                   outcomes2Goals1Way: eventObject.outcomes2Goals1Way,
                   outcomes3Goals1Way: eventObject.outcomes3Goals1Way,
                   counters: eventObject.counters,
                   widget: eventObject.widget,
                   proposedTypes: eventObject.proposedTypes)
    }
    
}
