//
//  EventTeam.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Словарь учавствующих команд в событии
final class EventTeam {
    
    // MARK: - Публичные свойства
    
    /// Первая команда
    let first: String
    /// Вторая команда
    let second: String?
    
    
    // MARK: - Инициализация
    
    init(first: String, second: String?) {
        self.first = first
        self.second = second
    }
    
}
