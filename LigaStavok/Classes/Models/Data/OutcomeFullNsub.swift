//
//  OutcomeFullNsub.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Коллекция маркетов
typealias OutcomeFullNsub = [MarketKey: OutcomeFullNsubInfo]
