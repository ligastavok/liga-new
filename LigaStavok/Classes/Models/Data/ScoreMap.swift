//
//  ScoreMap.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация о счете матча
final class ScoreMap {
    
    // MARK: - Публичные свойства
    
    /// Общий счет
    let total: Score? // FIXME: - Выяснить почему в документации <score>, а от сервера пришло ""
    /// Текущий счет
    let current: Score? // FIXME: - Выяснить почему в документации <score>, а от сервера пришло ""
    /// Красные карточки
    let red: Score? // FIXME: - Выяснить почему в документации <score>, а от сервера пришло ""
    /// Счет игры
    let gamescore: Score? // FIXME: - Выяснить почему в документации <score>, а от сервера пришло ""
    /// Общий счет с учетом овертайма
    let overtime: Score? // FIXME: - Выяснить почему в документации <score>, а от сервера пришло ""
    /// Пенальти
    let penalty: Score? // FIXME: - Выяснить почему в документации <score>, а от сервера пришло ""
    /// Коллекция данных по счетам разных частей матча
    let all: [Score]
    
    
    // MARK: - Инициализация
    
    init(total: Score?,
         current: Score?,
         red: Score?,
         gamescore: Score?,
         overtime: Score?,
         penalty: Score?,
         all: [Score]) {
        
        self.total = total
        self.current = current
        self.red = red
        self.gamescore = gamescore
        self.overtime = overtime
        self.penalty = penalty
        self.all = all
    }
    
}
