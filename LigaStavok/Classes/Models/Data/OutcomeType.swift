//
//  OutcomeType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Поддерживаемые типы основных маркетов
enum OutcomeType: String {
    
    /// Основные исходы на победителя для всех блоков
    case outcomesWinner
    /// Основные исходы на двойной шанс для всех блоков
    case outcomesDouble
    /// Основные исходы на фору (первая выборка) для всех блоков
    case outcomesHandicap1
    /// Основные исходы на фору (вторая выборка) для всех блоков
    case outcomesHandicap2
    /// Основные исходы на тотал (первая выборка) для всех блоков
    case outcomesTotal1
    /// Основные исходы на тотал (вторая выборка) для всех блоков
    case outcomesTotal2
    /// Основные исходы на фору (третья выборка) для всех блоков
    case outcomesTotal3
    /// Основные исходы на чет / нечет для всех блоков
    case outcomesOdd
    /// Основные исходы на следующий гол для всех блоков
    case outcomesNextGoal
    /// Основные исходы на да / нет для всех блоков,
    case outcomesYepNope
    // FIXME: - Выяснить название значения
    case outcomesSummary
    // FIXME: - Выяснить название значения
    case outcomesStrz
    
}
