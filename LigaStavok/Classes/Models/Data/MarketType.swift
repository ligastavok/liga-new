//
//  MarketType.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Тип маркета
enum MarketType: String {
    
    /// Исход на победителя
    case win = "WIN"
    /// Исход на чет / нечет
    case odd = "ODD"
    /// Исход на следующий гол
    case nxg = "NXG"
    /// Исход на двойной шанс
    case dbl = "DBL"
    /// Исход на тотал
    case ttl = "TTL"
    /// Исход на фору
    case han = "HAN"
    // FIXME: - Выяснить название значения
    case err = "ERR"
    // FIXME: - Выяснить название значения
    case win2 = "WIN2"
    // FIXME: - Выяснить название значения
    case strz = "STRZ"
    // Маркет точный счет
    case exa = "EXA"

    // Маркет всего голов
    // Маркет количество голов хозяев
}
