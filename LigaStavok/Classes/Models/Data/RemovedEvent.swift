//
//  RemovedEvent.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Удаленное событие
final class RemovedEvent {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор события
    let id: Int
    /// Пространство имен
    let namespace: Namespace?
    /// ID вида спорта (см. справочник видов спорта)
    let gameId: Int
    /// Удаленное событие
    let event: Event
    
    
    // MARK: - Инициализация
    
    init(id: Int,
         namespace: Namespace?,
         gameId: Int,
         event: Event) {
        
        self.id = id
        self.namespace = namespace
        self.gameId = gameId
        self.event = event
    }
    
}
