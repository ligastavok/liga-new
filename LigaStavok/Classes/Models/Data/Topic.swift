//
//  Topic.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/2/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Топик
final class Topic {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор вида спорта
    let gameId: Int?
    /// Идентификатор текущего топика
    let id: Int
    /// Название спорта
    let title: String
    /// Пространство имен
    let namespace: [Namespace]
    /// Количество текущих событий по виду спорта
    let quantity: Int
    
    
    // MARK: - Инициалиазация
    
    init(gameId: Int?, topicId: Int, title: String, namespace: [Namespace], quantity: Int) {
        self.gameId = gameId
        self.id = topicId
        self.title = title
        self.namespace = namespace
        self.quantity = quantity
    }
    
}
