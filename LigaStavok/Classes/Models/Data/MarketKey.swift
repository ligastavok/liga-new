//
//  MarketKey.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Ключ маркета
enum MarketKey {
    
    /// FULLTIME
    case main
    /// OVERALLTIME
    case overAllTime
    /// Для всех остальных
    case key(number: Int)
    
    
    // MARK: - Инициализация
    
    init?(key: String) {
        switch key {
        case "main":
            self = .main
        case "ot":
            self = .overAllTime
        case _ where key.hasPrefix("_"):
            let startIndex = key.index(key.startIndex, offsetBy: 1)
            guard let number = Int(key[startIndex...]) else { return nil }
            
            self = .key(number: number)
            
        default:
            return nil
        }
    }
    
}


// MARK: - Equatable

extension MarketKey: Equatable {
    
    static func == (lhs: MarketKey, rhs: MarketKey) -> Bool {
        switch (lhs, rhs) {
        case (.main, .main):
            return true
        case (.overAllTime, .overAllTime):
            return true
        case (let .key(number1), let .key(number2)):
            return number1 == number2
            
        default:
            return false
        }
    }
    
}


// MARK: - Hashable

extension MarketKey: Hashable {
    
    var hashValue: Int {
        return String(describing: self).hashValue
    }
    
}
