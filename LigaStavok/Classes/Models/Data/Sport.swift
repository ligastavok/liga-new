//
//  Sport.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Вид спорта
final class Sport {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор вида спорта
    let gameId: Int
    /// Название спорта
    let title: String
    /// Пространство имен
    let namespace: [Namespace]
    /// Количество текущих событий по виду спорта
    let quantity: Int
    
    
    // MARK: - Инициалиазация
    
    init(gameId: Int, title: String, namespace: [Namespace], quantity: Int) {
        self.gameId = gameId
        self.title = title
        self.namespace = namespace
        self.quantity = quantity
    }
    
}


// MARK: - Entity

extension Sport: Entity { }
