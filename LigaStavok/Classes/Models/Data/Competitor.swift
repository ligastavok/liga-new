//
//  Competitor.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Участник события
final class Competitor {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор участника
    let id: Int
    /// Имя участника
    let name: String
    /// Порядковый номер участника
    let number: Int
    /// Статус участника
    let state: CompetitorState?
    /// Результат участника (атрибут появляется, когда известен результат по участнику)
    let resultPlace: Int
    
    
    // MARK: - Инициализация
    
    init(id: Int,
         name: String,
         number: Int,
         state: CompetitorState?,
         resultPlace: Int) {
        
        self.id = id
        self.name = name
        self.number = number
        self.state = state
        self.resultPlace = resultPlace
    }
    
}
