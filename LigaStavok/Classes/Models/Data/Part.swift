//
//  Part.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Часть матча
final class Part {
    
    // MARK: - Публичные свойства
    
    /// ID блока
    let id: Int
    /// Название блока (части матча)
    let title: String
    /// Код блока ('FULLTIME', 'OVERTIME', 'GAMESCORE')
    let code: String?
    /// Флаг основной части матча (признак для отображения исходов этого блока в списке событий)
    let main: Bool
    
    
    // MARK: - Инициализация
    
    init(id: Int, title: String, code: String?, main: Bool) {
        self.id = id
        self.title = title
        self.code = code
        self.main = main
    }
    
}
