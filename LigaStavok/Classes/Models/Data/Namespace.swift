//
//  Namespace.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

enum Namespace: String {
    
    /// Событие еще не началось
    case prematch
    /// Начавшееся событие
    case live
    
}
