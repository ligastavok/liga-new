//
//  SportTree.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Дерево видов спорта
final class SportTree {
    
    // MARK: - Публичные свойства
    
    /// Список популярных чемпионатов
    let popular: [SportTreeTournament]
    /// Список видов спорта
    let tree: [SportTreeSport]
    
    
    // MARK: - Инициализация
    
    init(popular: [SportTreeTournament], tree: [SportTreeSport]) {
        self.popular = popular
        self.tree = tree
    }
    
}
