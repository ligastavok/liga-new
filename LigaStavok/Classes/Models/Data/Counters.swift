//
//  Counters.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 05.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Счетчики всех событий
final class Counters {
    
    // MARK: - Публичные свойства
    
    /// Список идентификаторов видов спорта
    let sportsLive24: [String: Int]
    // FIXME: - Выяснить название поля
    let soon: Int
    // FIXME: - Выяснить название поля
    let live24: Int
    // FIXME: - Выяснить название поля
    let live: Int
    // FIXME: - Выяснить название поля
    let scoreboardPages: Int
    // FIXME: - Выяснить название поля
    let prematch: Int
    // FIXME: - Выяснить название поля
    let video: Int
    // FIXME: - Выяснить название поля
    let top: Int
    // FIXME: - Выяснить название поля
    let externalEmbed: Int
    // FIXME: - Выяснить название поля
    let all: Int
    /// Список идентификаторов видов спорта
    let sportsScoreboard: [String: Int]
    
    
    // MARK: - Инициализация
    
    init(sportsLive24: [String: Int],
         soon: Int,
         live24: Int,
         live: Int,
         scoreboardPages: Int,
         prematch: Int,
         video: Int,
         top: Int,
         externalEmbed: Int,
         all: Int,
         sportsScoreboard: [String: Int]) {
        
        self.sportsLive24 = sportsLive24
        self.soon = soon
        self.live24 = live24
        self.live = live
        self.scoreboardPages = scoreboardPages
        self.prematch = prematch
        self.video = video
        self.top = top
        self.externalEmbed = externalEmbed
        self.all = all
        self.sportsScoreboard = sportsScoreboard
    }
    
}
