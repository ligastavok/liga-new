//
//  CompetitorState.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Статус участника
enum CompetitorState: String {
    
    /// Участвует
    case active
    /// Не участвует
    case inActive

}
