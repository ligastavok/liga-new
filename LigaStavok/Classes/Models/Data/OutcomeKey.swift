//
//  OutcomeKey.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Ключ исхода
enum OutcomeKey {
    
    /// _1 (WIN)
    case underscoreFirst
    /// x (WIN, NXG)
    case x
    /// _2 (WIN)
    case underscoreSecond
    /// odd (ODD)
    case odd
    /// even (ODD)
    case even
    /// 1 (NXG, HAN)
    case first
    /// 2 (NXG, HAN)
    case second
    /// 1x (DBL)
    case firstX
    /// _12 (DBL)
    case underscoreDouble
    /// x2 (DBL)
    case xSecond
    /// gross (TTL)
    case gross
    /// less (TTL)
    case less
    /// yes (ERR)
    case yes
    /// no (ERR)
    case no
    /// Остальные (WIN2, STRZ)
    case key(number: Int)
    
    
    // MARK: - Инициализация
    
    init?(key: String) {
        switch key {
        case "_1":
            self = .underscoreFirst
        case "x":
            self = .x
        case "_2":
            self = .underscoreSecond
        case "odd":
            self = .odd
        case "even":
            self = .even
        case "1":
            self = .first
        case "2":
            self = .second
        case "1x":
            self = .firstX
        case "_12":
            self = .underscoreDouble
        case "x2":
            self = .xSecond
        case "gross":
            self = .gross
        case "less":
            self = .less
        case "yes":
            self = .yes
        case "no":
            self = .no
        case _ where key.hasPrefix("_"):
            let startIndex = key.index(key.startIndex, offsetBy: 1)
            guard let number = Int(key[startIndex...]) else { return nil }
            
            self = .key(number: number)
            
        default:
            return nil
        }
    }
    
}


// MARK: - Equatable

extension OutcomeKey: Equatable {

    static func == (lhs: OutcomeKey, rhs: OutcomeKey) -> Bool {
        switch (lhs, rhs) {
        case (.underscoreFirst, .underscoreFirst):
            return true
        case (.x, .x):
            return true
        case (.underscoreSecond, .underscoreSecond):
            return true
        case (.odd, .odd):
            return true
        case (.even, .even):
            return true
        case (.first, .first):
            return true
        case (.second, .second):
            return true
        case (.firstX, .firstX):
            return true
        case (.underscoreDouble, .underscoreDouble):
            return true
        case (.xSecond, .xSecond):
            return true
        case (.gross, .gross):
            return true
        case (.less, .less):
            return true
        case (.yes, .yes):
            return true
        case (.no, .no):
            return true
        case (let .key(number1), let .key(number2)):
            return number1 == number2

        default:
            return false
        }
    }

}


// MARK: - Hashable

extension OutcomeKey: Hashable {

    var hashValue: Int {
        return String(describing: self).hashValue
    }

}
