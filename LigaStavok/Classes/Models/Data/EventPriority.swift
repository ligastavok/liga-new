//
//  EventPriority.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Приоритет события по признакам
final class EventPriority {
    
    // MARK: - Публичные свойства
    
    /// Вид спорта
    let sport: Int
    /// Топик
    let topic: Int
    /// Событие
    let event: Int
    /// Категория
    let category: Int
    /// Турнир
    let tournament: Int
    
    
    // MARK: - Инициализация
    
    init(sport: Int,
         topic: Int,
         event: Int,
         category: Int,
         tournament: Int) {
        
        self.sport = sport
        self.topic = topic
        self.event = event
        self.category = category
        self.tournament = tournament
    }
    
}
