//
//  SportTreeTournament.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Элемент дерева видов спорта (чемпионат)
final class SportTreeTournament {
    
    // MARK: - Публичные свойства
    
    /// ID категории
    let categoryId: Int?
    /// Количество событий в чемпионате
    let count: Int
    /// ID событий
    let eventIds: [Int]
    /// ID вида спорта
    let gameId: Int
    /// ID чемпионата
    let id: Int
    /// Популярность чемпионата
    let popularity: Int
    /// Приоритет чемпионата
    let priority: Int
    /// Транслитеризированное название чемпионата, допускается использовать это название в качестве ЧПУ
    let sname: String
    /// Название чемпионата
    let title: String
    
    
    // MARK: - Инициализация
    
    init(categoryId: Int?,
         count: Int,
         eventIds: [Int],
         gameId: Int,
         id: Int,
         popularity: Int,
         priority: Int,
         sname: String,
         title: String) {
        
        self.categoryId = categoryId
        self.count = count
        self.eventIds = eventIds
        self.gameId = gameId
        self.id = id
        self.popularity = popularity
        self.priority = priority
        self.sname = sname
        self.title = title
    }
    
}
