//
//  EventIdentifiers.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Идентификаторы связанных сущностей с событием
final class EventIdentifiers {
    
    // MARK: - Публичные свойства
    
    /// ID топика
    let topicId: Int
    /// ID чемпионата
    let tournamentId: Int
    /// ID категории
    let categoryId: Int
    /// ID вида спорта
    let gameId: Int
    // swiftlint:disable:next line_length
    /// Оригинальный ID события в системе поставщика (Betradar, Betgenius, etc.), только для импортированных событий, а также только для событий с extId допускается использование виджета LMT, при условии покрытия вида спорта
    let externalId: Int?
    /// Внутренний идентификатор поставщика
    let serviceId: Int?
    
    
    // MARK: - Инициализация
    
    init(topicId: Int, tournamentId: Int, categoryId: Int, gameId: Int, externalId: Int?, serviceId: Int?) {
        self.topicId = topicId
        self.tournamentId = tournamentId
        self.categoryId = categoryId
        self.gameId = gameId
        self.externalId = externalId
        self.serviceId = serviceId
    }
    
}
