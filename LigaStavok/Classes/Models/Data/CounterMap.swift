//
//  CounterMap.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Счетчик
final class CounterMap {
    
    // MARK: - Публичные свойства
    
    /// Общее количество исходов
    let outcomesQuantity: Int
    /// Количество неактивных исходов
    let outcomesDisabledQuantity: Int
    /// Количество активных исходов
    let outcomesEnabledQuantity: Int
    /// Количество исходов в основном блоке
    let outcomesMainQuantity: Int
    /// Количество основных блоков
    let outcomesRowsQuantity: Int
    
    
    // MARK: - Инициализация
    
    init(outcomesQuantity: Int,
         outcomesDisabledQuantity: Int,
         outcomesEnabledQuantity: Int,
         outcomesMainQuantity: Int,
         outcomesRowsQuantity: Int) {
        
        self.outcomesQuantity = outcomesQuantity
        self.outcomesDisabledQuantity = outcomesDisabledQuantity
        self.outcomesEnabledQuantity = outcomesEnabledQuantity
        self.outcomesMainQuantity = outcomesMainQuantity
        self.outcomesRowsQuantity = outcomesRowsQuantity
    }
    
}
