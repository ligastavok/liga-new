//
//  SportTreeSport.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Элемент дерева видов спорта (вид спорта)
final class SportTreeSport {
    
    // MARK: - Публичные свойства
    
    /// Список категорий
    let categories: [SportTreeCategory]
    /// Количество событий по данному виду спорта
    let count: Int
    /// ID вида спорта
    let id: Int
    /// Приоритет вида спорта
    let priority: Int
    /// Транслитеризированное название вида спорта, допускается использовать это название в качестве ЧПУ
    let sname: String
    /// Название вида спорта
    let title: String
    
    
    // MARK: - Инициализация
    
    init(categories: [SportTreeCategory],
         count: Int,
         id: Int,
         priority: Int,
         sname: String,
         title: String) {
        
        self.categories = categories
        self.count = count
        self.id = id
        self.priority = priority
        self.sname = sname
        self.title = title
    }
    
}
