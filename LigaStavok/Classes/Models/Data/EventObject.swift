//
//  EventObject.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Общая структура объекта события
class EventObject {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор события
    let id: Int
    /// Список параметров, указывающих на приоритет события по признакам
    let priority: EventPriority
    /// Список параметров, указывающих на популярность события по признакам
    let popularity: EventPopularity
    /// Идентификаторы связанных сущностей с событием
    let ids: EventIdentifiers
    /// Заголовок вида спорта
    let gameTitle: String
    /// Заголовок топика
    let topicTitle: String
    /// Заголовок турнира (турнир, матч, чемпионат)
    let tournamentTitle: String
    /// Заголовок категории (как правило это страна)
    let categoryTitle: String
    /// Последовательность, идентифицирующая версию документа линии, из которого взят образ события
    let hash: Double
    /// Пространство имен
    let namespace: Namespace?
    /// Время начала события (unixtimestamp)
    let gameTimestamp: Date
    /// Идентификатор вида спорта (см. справочник видов спорта)
    let gameId: Int
    /// Внутреннее имя названия спорта
    let name: String
    /// Название спорта
    let title: String
    /// Зарезервированное поле
    let status: Int
    /// Информация о событии
    let event: Event
    /// Информация о счете матча
    let scores: ScoreMap
    /// Коллекция блоков события (блоки, части игры: сеты, таймы, периоды etc.)
    let parts: PartMap
    /// Флаг подтверждающий, что событие содержит незаблокированные исходы
    let hasUnlocked: Bool
    /// Флаг подтверждающий, что событие содержит / не содержит битые данные (например, кэфы <= 1)
    let corrupted: Bool
    /// Справочник поддерживаемых типов основных маркетов
    let outcomesTypes: [OutcomeType]
    /// Полная роспись с полным списком маркетов
    let outcomes: OutcomeFullMap?
    /// Основные исходы на победителя для всех блоков
    let outcomesWinner: OutcomeMap?
    /// Основные исходы на двойной шанс для всех блоков
    let outcomesDouble: OutcomeMap?
    /// Основные исходы на фору (первая выборка) для всех блоков
    let outcomesHandicap1: OutcomeMap?
    /// Основные исходы на фору (вторая выборка) для всех блоков
    let outcomesHandicap2: OutcomeMap?
    /// Основные исходы на тотал (первая выборка) для всех блоков
    let outcomesTotal1: OutcomeMap?
    /// Основные исходы на тотал (вторая выборка) для всех блоков
    let outcomesTotal2: OutcomeMap?
    /// Основные исходы на фору (третья выборка) для всех блоков
    let outcomesTotal3: OutcomeMap?
    /// Основные исходы на чет / нечет для всех блоков
    let outcomesOdd: OutcomeMap?
    /// Основные исходы на следующий гол для всех блоков
    let outcomesNextGoal: OutcomeMap?
    /// Основные исходы на да / нет для всех блоков,
    let outcomesYepNope: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesSummary: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesStrz: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesWin1: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesCompare: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesPlace3Way: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesPolePosition: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesQualify3: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesQualify4: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesQualifyGroup: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesGroupPlace2: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomesGroupPlace4: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomes1Goal2Way: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomes2Goals1Way: OutcomeMap?
    // FIXME: - Выяснить название поля
    let outcomes3Goals1Way: OutcomeMap?
    /// Объект счетчиков
    let counters: CounterMap
    // swiftlint:disable:next line_length
    /// Словарь виджетов, доступных для отображения для данного события (не указывает на то, есть ли привязанный виджет к событию, указывает только на возможность отображения данного типа виджета в интерфейсе)
    let widget: Widget
    /// Типы предложения
    let proposedTypes: [ProposedType]?
    
    
    init(id: Int,
         priority: EventPriority,
         popularity: EventPopularity,
         ids: EventIdentifiers,
         gameTitle: String,
         topicTitle: String,
         tournamentTitle: String,
         categoryTitle: String,
         hash: Double,
         namespace: Namespace?,
         gameTimestamp: Date,
         gameId: Int,
         name: String,
         title: String,
         status: Int,
         event: Event,
         scores: ScoreMap,
         parts: PartMap,
         hasUnlocked: Bool,
         corrupted: Bool,
         outcomesTypes: [OutcomeType],
         outcomes: OutcomeFullMap?,
         outcomesWinner: OutcomeMap?,
         outcomesDouble: OutcomeMap?,
         outcomesHandicap1: OutcomeMap?,
         outcomesHandicap2: OutcomeMap?,
         outcomesTotal1: OutcomeMap?,
         outcomesTotal2: OutcomeMap?,
         outcomesTotal3: OutcomeMap?,
         outcomesOdd: OutcomeMap?,
         outcomesNextGoal: OutcomeMap?,
         outcomesYepNope: OutcomeMap?,
         outcomesSummary: OutcomeMap?,
         outcomesStrz: OutcomeMap?,
         outcomesWin1: OutcomeMap?,
         outcomesCompare: OutcomeMap?,
         outcomesPlace3Way: OutcomeMap?,
         outcomesPolePosition: OutcomeMap?,
         outcomesQualify3: OutcomeMap?,
         outcomesQualify4: OutcomeMap?,
         outcomesQualifyGroup: OutcomeMap?,
         outcomesGroupPlace2: OutcomeMap?,
         outcomesGroupPlace4: OutcomeMap?,
         outcomes1Goal2Way: OutcomeMap?,
         outcomes2Goals1Way: OutcomeMap?,
         outcomes3Goals1Way: OutcomeMap?,
         counters: CounterMap,
         widget: Widget,
         proposedTypes: [ProposedType]?) {
        
        self.id = id
        self.priority = priority
        self.popularity = popularity
        self.ids = ids
        self.gameTitle = gameTitle
        self.topicTitle = topicTitle
        self.tournamentTitle = tournamentTitle
        self.categoryTitle = categoryTitle
        self.hash = hash
        self.namespace = namespace
        self.gameTimestamp = gameTimestamp
        self.gameId = gameId
        self.name = name
        self.title = title
        self.status = status
        self.event = event
        self.scores = scores
        self.parts = parts
        self.hasUnlocked = hasUnlocked
        self.corrupted = corrupted
        self.outcomesTypes = outcomesTypes
        self.outcomes = outcomes
        self.outcomesWinner = outcomesWinner
        self.outcomesDouble = outcomesDouble
        self.outcomesHandicap1 = outcomesHandicap1
        self.outcomesHandicap2 = outcomesHandicap2
        self.outcomesTotal1 = outcomesTotal1
        self.outcomesTotal2 = outcomesTotal2
        self.outcomesTotal3 = outcomesTotal3
        self.outcomesOdd = outcomesOdd
        self.outcomesNextGoal = outcomesNextGoal
        self.outcomesYepNope = outcomesYepNope
        self.outcomesSummary = outcomesSummary
        self.outcomesStrz = outcomesStrz
        self.outcomesWin1 = outcomesWin1
        self.outcomesCompare = outcomesCompare
        self.outcomesPlace3Way = outcomesPlace3Way
        self.outcomesPolePosition = outcomesPolePosition
        self.outcomesQualify3 = outcomesQualify3
        self.outcomesQualify4 = outcomesQualify4
        self.outcomesQualifyGroup = outcomesQualifyGroup
        self.outcomesGroupPlace2 = outcomesGroupPlace2
        self.outcomesGroupPlace4 = outcomesGroupPlace4
        self.outcomes1Goal2Way = outcomes1Goal2Way
        self.outcomes2Goals1Way = outcomes2Goals1Way
        self.outcomes3Goals1Way = outcomes3Goals1Way
        self.counters = counters
        self.widget = widget
        self.proposedTypes = proposedTypes
    }
    
}
