//
//  OutcomeFullMap.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Полный список исходов с маркетами
typealias OutcomeFullMap = [PartKey: OutcomeFullInfo]
