//
//  Market.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Ставка
final class Market {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор маркета
    let id: Int
    /// Тип маркета
    let type: MarketType?
    /// Заголовок маркета
    let title: String
    
    
    // MARK: - Инициализация
    
    init(id: Int, type: MarketType?, title: String) {
        self.id = id
        self.type = type
        self.title = title
    }
    
}
