//
//  DBSport.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import RealmSwift

/// Вид спорта
final class DBSport: Object {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор вида спорта
    @objc dynamic var gameId = 0
    /// Название спорта
    @objc dynamic var title = ""
    /// Пространство имен
    @objc dynamic var namespace = ""
    /// Порядок отображения
    @objc dynamic var displaySequence = 0
    /// Дата обновления записи
    @objc dynamic var updateDate = Date()
    
    
    // MARK: - Object
    
    override static func primaryKey() -> String? {
        return "gameId"
    }
    
    override static func indexedProperties() -> [String] {
        return ["displaySequence"]
    }
    
}
