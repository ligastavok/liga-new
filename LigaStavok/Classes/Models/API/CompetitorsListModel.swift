//
//  CompetitorsListModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Список участников события
final class CompetitorsListModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Участники события
    let competitors: [CompetitorModel]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.competitors = try container.decode([CompetitorModel].self)
    }
    
}


// MARK: - Публичные свойства

extension CompetitorsListModel {
    
    /// Объекты
    var entities: [Competitor] {
        return self.competitors.map { $0.entity }
    }
    
}
