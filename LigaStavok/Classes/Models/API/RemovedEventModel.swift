//
//  RemovedEventModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель удаленного события
final class RemovedEventModel: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        
        /// Пространство имен
        case ns
        /// Идентификатор события
        case id
        /// Данные
        case data
        
    }
    
    private enum DataCodingKeys: String, CodingKey {
        
        /// ID вида спорта (см. справочник видов спорта)
        case gameId
        /// Удаленное событие
        case event
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Пространство имен
    let ns: String
    /// Идентификатор события
    let id: Int
    /// ID вида спорта (см. справочник видов спорта)
    let gameId: Int
    /// Удаленное событие
    let event: EventModel
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.ns = try container.decode(String.self, forKey: .ns)
        self.id = try container.decode(Int.self, forKey: .id)
        let dataContainer = try container.nestedContainer(keyedBy: DataCodingKeys.self, forKey: .data)
        self.gameId = try dataContainer.decode(Int.self, forKey: .gameId)
        self.event = try dataContainer.decode(EventModel.self, forKey: .event)
    }
    
}


// MARK: - Публичные свойства

extension RemovedEventModel {
    
    /// Объект
    var entity: RemovedEvent {
        let namespace = Namespace(rawValue: self.ns)
        let event = self.event.entity
        return RemovedEvent(id: self.id,
                            namespace: namespace,
                            gameId: self.gameId,
                            event: event)
    }
    
}
