//
//  CountersModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Счетчики всех событий
final class CountersModel: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case counters = "result"
    }
    
    
    // MARK: - Публичные свойства
    
    /// Данные счетчиков всех событий
    let counters: CountersDataModel
    
}


// MARK: - Публичные свойства

extension CountersModel {
    
    /// Объект
    var entity: Counters {
        return self.counters.entity
    }
    
}
