//
//  MarketInfoModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация о ставке
final class MarketInfoModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Ставка
    let market: MarketModel
    /// Исходы
    let outcomes: OutcomesListModel? // FIXME: - В документации { "<outcome_key>": <outcome> }, а от сервера пришло null
    
}


// MARK: - Публичные свойства

extension MarketInfoModel {
    
    /// Объект
    var entity: MarketInfo {
        let market = self.market.entity
        let outcomes = self.outcomes?.entities
        return MarketInfo(market: market, outcomes: outcomes)
    }
    
}
