//
//  CountersDataModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 05.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Данные счетчиков всех событий
final class CountersDataModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Список идентификаторов видов спорта
    let sportsLive24: [String: Int]
    // FIXME: - Выяснить название поля
    let soon: Int
    // FIXME: - Выяснить название поля
    let live24: Int
    // FIXME: - Выяснить название поля
    let live: Int
    // FIXME: - Выяснить название поля
    let scoreboardPages: Int
    // FIXME: - Выяснить название поля
    let prematch: Int
    // FIXME: - Выяснить название поля
    let video: Int
    // FIXME: - Выяснить название поля
    let top: Int
    // FIXME: - Выяснить название поля
    let externalEmbed: Int
    // FIXME: - Выяснить название поля
    let all: Int
    /// Список идентификаторов видов спорта
    let sportsScoreboard: [String: Int]
    
}


// MARK: - Публичные свойства

extension CountersDataModel {
    
    /// Объект
    var entity: Counters {
        return Counters(sportsLive24: self.sportsLive24,
                        soon: self.soon,
                        live24: self.live24,
                        live: self.live,
                        scoreboardPages: self.scoreboardPages,
                        prematch: self.prematch,
                        video: self.video,
                        top: self.top,
                        externalEmbed: self.externalEmbed,
                        all: self.all,
                        sportsScoreboard: self.sportsScoreboard)
    }
    
}
