//
//  TopicsListModel.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/2/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель списка топиков
final class TopicsListModel: Decodable {

    private enum CodingKeys: String, CodingKey {
        case topics = "result"
    }
    
    
    // MARK: - Публичные свойства
    
    let topics: [TopicModel]
    
}


// MARK: - Публичные свойства

extension TopicsListModel {
    
    /// Объекты
    var entities: [Topic] {
        return self.topics.map { $0.entity }
    }
    
}
