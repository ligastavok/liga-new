//
//  MarketModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Ставка
final class MarketModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор маркета
    let id: Int
    /// Один из типов маркета (WIN, ODD, NXG, DBL, TTL, HAN, ERR, WIN2, STRZ)
    let type: String
    /// Заголовок маркета
    let title: String
    
}


// MARK: - Публичные свойства

extension MarketModel {
    
    /// Объект
    var entity: Market {
        let marketType = MarketType(rawValue: self.type)
        return Market(id: self.id, type: marketType, title: self.title)
    }
    
}
