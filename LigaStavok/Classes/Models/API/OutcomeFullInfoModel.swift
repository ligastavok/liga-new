//
//  OutcomeFullInfoModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Исход с маркетами
final class OutcomeFullInfoModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// ID блока
    let id: Int
    /// Название блока
    let title: String
    /// Код блока
    let code: String?
    /// Порядок сортировки блока
    let position: Int
    /// Флаг блокировки блока (все исходы в нем заблокированы)
    let locked: Bool
    /// Флаг поломанности блока (все исходы в нем поломаны)
    let corrupted: Bool
    /// Коллекция маркетов
    let nsub: OutcomeFullNsubModel
    
}


// MARK: - Публичные свойства

extension OutcomeFullInfoModel {
    
    /// Объект
    var entity: OutcomeFullInfo {
        let nsub = self.nsub.entity
        
        return OutcomeFullInfo(id: self.id,
                               title: self.title,
                               code: self.code,
                               position: self.position,
                               locked: self.locked,
                               corrupted: self.corrupted,
                               nsub: nsub)
    }
    
}
