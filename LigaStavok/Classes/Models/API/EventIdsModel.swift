//
//  EventIdsModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Идентификаторы связанных сущностей с событием
final class EventIdsModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// ID топика
    let topicId: Int
    /// ID чемпионата
    let tournamentId: Int
    /// ID категории
    let categoryId: Int
    /// ID вида спорта
    let gameId: Int
    // swiftlint:disable:next line_length
    /// Оригинальный ID события в системе поставщика (Betradar, Betgenius, etc.), только для импортированных событий, а также только для событий с extId допускается использование виджета LMT, при условии покрытия вида спорта
    let extId: Int?
    /// Внутренний идентификатор поставщика
    let serviceId: Int?
    
    
    // MARK: - Инициализация
    
    // FIXME: - Баг компилятора Swift 4.1
    @available(*, deprecated, message: "Do not use.")
    init() {
        fatalError("Swift 4.1")
    }
    
}


// MARK: - Публичные свойства

extension EventIdsModel {
    
    /// Объект
    var entity: EventIdentifiers {
        return EventIdentifiers(topicId: self.topicId,
                                tournamentId: self.tournamentId,
                                categoryId: self.categoryId,
                                gameId: self.gameId,
                                externalId: self.extId,
                                serviceId: self.serviceId)
    }
    
}
