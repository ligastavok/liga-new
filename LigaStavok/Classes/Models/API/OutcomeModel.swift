//
//  OutcomeModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Объект исхода
final class OutcomeModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор исхода
    let id: Int
    /// Идентификатор значения исхода
    let facId: Int64
    /// Позиция исхода, предназначено для сортировки
    let position: Int
    /// Заголовок исхода
    let title: String
    /// Значение исхода
    let value: Double
    /// Опциональное поле, обычно содержит базу маркетов (например, для тоталов и фор)
    let adValue: String
    /// Опциональное поле, содержит дополнительное название исхода
    let adTitle: String?
    /// Флаг блокировки исхода
    let locked: Bool
    /// Флаг поломанности исхода
    let corrupted: Bool
    /// Ключ исхода
    let outcomeKey: String
    /// Идентификатор события
    let eventId: Int
    /// Тип события (live или prematch)
    let eventType: String
    /// Идентификатор вида спорта
    let eventGameId: Int
    /// Опциональное поле, комментарий к событию
    let eventComment: String
    /// Словарь учавствующих команд в событии
    let eventTeam: EventTeamModel
    /// Идентификатор блока
    let partId: Int
    /// Имя блока
    let partName: String
    /// Тип блока
    let partType: String?
    /// Заголовок блока
    let partTitle: String
    /// Идентификатор маркета
    let marketId: Int
    /// Один из типов маркета (WIN, ODD, NXG, DBL, TTL, HAN, ERR, WIN2, STRZ)
    let marketType: String
    /// Заголовок маркета
    let marketTitle: String
    /// Идентификатор топика
    let topicId: Int
    
}


// MARK: - Публичные свойства

extension OutcomeModel {
    
    /// Объект
    var entity: Outcome {
        let eventTeam = self.eventTeam.entity
        let marketType = MarketType(rawValue: self.marketType)
        
        return Outcome(id: self.id,
                       facId: self.facId,
                       position: self.position,
                       title: self.title,
                       value: self.value,
                       adValue: self.adValue,
                       adTitle: self.adTitle,
                       locked: self.locked,
                       corrupted: self.corrupted,
                       outcomeKey: self.outcomeKey,
                       eventId: self.eventId,
                       eventType: self.eventType,
                       eventGameId: self.eventGameId,
                       eventComment: self.eventComment,
                       eventTeam: eventTeam,
                       partId: self.partId,
                       partName: self.partName,
                       partType: self.partType,
                       partTitle: self.partTitle,
                       marketId: self.marketId,
                       marketType: marketType,
                       marketTitle: self.marketTitle,
                       topicId: self.topicId)
    }
    
}
