//
//  SportsListModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель списка видов спорта
final class SportsListModel: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case sports = "result"
    }
    
    
    // MARK: - Публичные свойства
    
    /// Список видов спорта
    let sports: [SportModel]
    
}


// MARK: - Публичные свойства

extension SportsListModel {
    
    /// Объекты
    var entities: [Sport] {
        return self.sports.map { $0.entity }
    }
    
}
