//
//  EventModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация о событии
final class EventModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// ID топика (лига, соревнование etc.)
    let topicId: Int
    /// Название топика
    let topicTitle: String
    /// Extra заголовок топика
    let subtopic: String?
    /// Человекопонятная дата начала события
    let gameDt: String // FIXME: - Выяснить формат "2018-12-31 23:59:00.0"
    // swiftlint:disable:next line_length
    /// Номер события в линии, внутренний параметр сортировки линии по приоритету (уникальный в рамках конкретной версии документа) - устаревает в силу ввода TopicLinePriority в сервисе Dispenser
    let position: Int
    /// Приоритет события (сейчас может быть 0 или 1) - устаревает в силу ввода EventLinePriority в сервисе Dispenser
    let priority: Int
    /// Приоритет топика
    let topicPriority: Int
    /// Название команды #1
    let team1: String
    /// Название команды #2
    let team2: String?
    /// Строка вида `КОМ {имя_команды} играет за CT` (существует для обратной совместимости с сервисами терминалов)
    let currentCtTeam: String?
    // swiftlint:disable:next line_length
    /// ID внешнего события в линии (Betradar, Betgenius, etc.), только для импортированных событий, а также только для событий с extId допускается использование виджета LMT, при условии покрытия вида спорта
    let extId: String?
    /// Код статуса игры
    let status: String?
    /// Человекопонятное название состояния игры
    let statusTranslated: String?
    /// Прошедшее время матча
    let matchTime: String?
    /// Ожидаемое время окончания матча
    let remainingTime: String?
    /// Подающий игрок (теннис, настольный теннис, волейбол и т.п.)
    let server: String?
    /// Комментарий букмекера к матчу
    let comment: String
    /// Количество минут до начала матча
    let minutesBeforeStart: Int
    /// Тай-брейк
    let tiebreak: String?
    /// Счет в серии, обычно используется для вывода в комментарии к событию в доматчевых событиях
    let seriesScore: String?
    /// Название события
    let eventTitle: String
    /// Признак завершенного события (атрибут появляется когда событие завершилось)
    let expired: Bool
    /// Список участников события
    let competitors: CompetitorsListModel
    /// Номер сезона
    let season: Int?
    /// Номер игрового дня
    let matchDay: Int?
    /// Номер канала в видеопотоке для видеотрансляции
    let videoChannel: String?
    /// URL видеопотока для видеотрансляции
    let videoStream: String?
    /// URL HLS видеопотока
    let videoStreamHls: String?
    /// Ссылка на обзор события
    let reviewUrl: String?
    /// Внутренний идентификатор поставщика
    let serviceId: Int
    
}


// MARK: - Публичные свойства

extension EventModel {
    
    /// Объект
    var entity: Event {
        let competitors = self.competitors.entities
        
        return Event(topicId: self.topicId,
                     topicTitle: self.topicTitle,
                     subtopic: self.subtopic,
                     gameDate: self.gameDt,
                     position: self.position,
                     priority: self.priority,
                     topicPriority: self.topicPriority,
                     firstTeamName: self.team1,
                     secondTeamName: self.team2,
                     currentCtTeam: self.currentCtTeam,
                     externalId: self.extId,
                     status: self.status,
                     statusTranslated: self.statusTranslated,
                     matchTime: self.matchTime,
                     remainingTime: self.remainingTime,
                     server: self.server,
                     comment: self.comment,
                     minutesBeforeStart: self.minutesBeforeStart,
                     tiebreak: self.tiebreak,
                     seriesScore: self.seriesScore,
                     eventTitle: self.eventTitle,
                     expired: self.expired,
                     competitors: competitors,
                     season: self.season,
                     matchDay: self.matchDay,
                     videoChannel: self.videoChannel,
                     videoStream: self.videoStream,
                     videoStreamHls: self.videoStreamHls,
                     reviewUrl: self.reviewUrl,
                     serviceId: self.serviceId)
    }
    
}
