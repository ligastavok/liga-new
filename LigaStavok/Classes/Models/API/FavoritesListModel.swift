//
//  FavoritesListModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель списка избранных событий
final class FavoritesListModel: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case favorites = "result"
    }
    
    
    // MARK: - Публичные свойства
    
    /// Список избранных событий
    let favorites: [FavoriteModel]
    
}


// MARK: - Публичные свойства

extension FavoritesListModel {
    
    /// Объекты
    var entities: [Favorite] {
        return self.favorites.map { $0.entity }
    }
    
}
