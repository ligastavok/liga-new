//
//  EventPriorityModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Приоритет события по признакам
final class EventPriorityModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Вид спорта
    let sport: Int
    /// Топик
    let topic: Int
    /// Событие
    let event: Int
    /// Категория
    let category: Int
    /// Турнир
    let tournament: Int
    
}


// MARK: - Публичные свойства

extension EventPriorityModel {
    
    /// Объект
    var entity: EventPriority {
        return EventPriority(sport: self.sport,
                             topic: self.topic,
                             event: self.event,
                             category: self.category,
                             tournament: self.tournament)
    }
    
}
