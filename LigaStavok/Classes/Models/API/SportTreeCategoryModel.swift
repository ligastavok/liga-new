//
//  SportTreeCategoryModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель элемента дерева видов спорта (категория)
final class SportTreeCategoryModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Список категорий
    let count: Int
    /// Количество событий по данному виду спорта
    let gameId: Int
    /// ID вида спорта
    let id: Int
    /// Приоритет вида спорта
    let priority: Int
    /// Транслитеризированное название вида спорта, допускается использовать это название в качестве ЧПУ
    let sname: String
    /// Название вида спорта
    let title: String
    /// Список чемпионатов
    let tournaments: [SportTreeTournamentModel]
    
}


// MARK: - Публичные свойства

extension SportTreeCategoryModel {

    /// Объект
    var entity: SportTreeCategory {
        let tournaments = self.tournaments.map { $0.entity }
        return SportTreeCategory(count: self.count,
                                 gameId: self.gameId,
                                 id: self.id,
                                 priority: self.priority,
                                 sname: self.sname,
                                 title: self.title,
                                 tournaments: tournaments)
    }

}
