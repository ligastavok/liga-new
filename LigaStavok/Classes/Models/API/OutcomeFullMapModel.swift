//
//  OutcomeFullMapModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Полный список исходов с маркетами
final class OutcomeFullMapModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Исходов
    let outcomes: [String: OutcomeFullInfoModel]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.outcomes = try container.decode([String: OutcomeFullInfoModel].self)
    }
    
}


// MARK: - Публичные свойства

extension OutcomeFullMapModel {
    
    /// Объекты
    var entities: OutcomeFullMap {
        var outcomes: [PartKey: OutcomeFullInfo] = [:]
        for (key, value) in self.outcomes {
            guard let key = PartKey(key: key) else { continue }
            outcomes[key] = value.entity
        }
        
        return outcomes
    }
    
}
