//
//  EventsListModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация по событиям
final class EventsListModel: Decodable {
    
    private enum ContainerKeys: String, CodingKey {
        
        /// Данные
        case result
        
    }
    
    private enum CodingKeys: String, CodingKey {
        
        /// Информация по событиям
        case data
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Информация по событиям
    let events: [EventObjectModel]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ContainerKeys.self)
        let resultContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .result)
        self.events = try resultContainer.decode([EventObjectModel].self, forKey: .data)
    }
    
}


// MARK: - Публичные свойства

extension EventsListModel {
    
    /// Объекты
    var entities: [EventObject] {
        return self.events.map { $0.entity }
    }
    
}
