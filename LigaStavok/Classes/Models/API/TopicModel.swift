//
//  TopicModel.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/2/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель топика
final class TopicModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Id вида спорта
    let gameId: Int? // FIXME: - Выяснить почему не приходит значение
    /// Id топика
    let topicId: Int
    /// Название спорта
    let title: String
    /// Пространство имен
    let ns: [String]
    /// Количество событий по виду спорта
    let qty: Int
    
}


// MARK: - Публичные свойства

extension TopicModel {
    
    /// Объект
    var entity: Topic {
        let namespace = self.ns.compactMap { Namespace(rawValue: $0) }
        return Topic(gameId: self.gameId,
                     topicId: self.topicId,
                     title: self.title,
                     namespace: namespace,
                     quantity: self.qty)
    }
    
}
