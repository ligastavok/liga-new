//
//  SportTreeSportModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель элемента дерева видов спорта (вид спорта)
final class SportTreeSportModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Список категорий
    let categories: [SportTreeCategoryModel]
    /// Количество событий по данному виду спорта
    let count: Int
    /// ID вида спорта
    let id: Int
    /// Приоритет вида спорта
    let priority: Int
    /// Транслитеризированное название вида спорта, допускается использовать это название в качестве ЧПУ
    let sname: String
    /// Название вида спорта
    let title: String
    
}


// MARK: - Публичные свойства

extension SportTreeSportModel {

    /// Объект
    var entity: SportTreeSport {
        let categories = self.categories.map { $0.entity }
        return SportTreeSport(categories: categories,
                              count: self.count,
                              id: self.id,
                              priority: self.priority,
                              sname: self.sname,
                              title: self.title)
    }

}
