//
//  ErrorModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель ошибки
final class ErrorModel: Decodable {
    
    private enum ContainerKeys: String, CodingKey {
        
        /// Ошибка
        case error
        
    }
    
    private enum CodingKeys: String, CodingKey {
        
        /// Сообщение об ошибке
        case message
        /// Код ошибки
        case code
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Сообщение об ошибке
    let message: String
    /// Код ошибки
    let code: Int
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ContainerKeys.self)
        let errorContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .error)
        self.message = try errorContainer.decode(String.self, forKey: .message)
        self.code = try errorContainer.decode(Int.self, forKey: .code)
    }
    
}
