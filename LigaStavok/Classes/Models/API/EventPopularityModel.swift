//
//  EventPopularityModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 11.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Список параметров, указывающих на популярность события по признакам
final class EventPopularityModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Популярность чемпионата
    let tournament: Int
    
}


// MARK: - Публичные свойства

extension EventPopularityModel {
    
    /// Объект
    var entity: EventPopularity {
        return EventPopularity(tournament: self.tournament)
    }
    
}
