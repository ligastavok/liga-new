//
//  EventObjectModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Общая структура объекта события
final class EventObjectModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// ID события
    let id: Int
    /// Список параметров, указывающих на приоритет события по признакам
    let priority: EventPriorityModel
    /// Список параметров, указывающих на популярность события по признакам
    let popularity: EventPopularityModel
    /// Идентификаторы связанных сущностей с событием
    let ids: EventIdsModel
    /// Заголовок вида спорта
    let gameTitle: String
    /// Заголовок топика
    let topicTitle: String
    /// Заголовок турнира (турнир, матч, чемпионат)
    let tournamentTitle: String
    /// Заголовок категории (как правило это страна)
    let categoryTitle: String
    /// Последовательность, идентифицирующая версию документа линии, из которого взят образ события
    let hash: Double
    /// Пространство имен
    let ns: String
    /// Время начала события (unixtimestamp)
    let gameTs: Date
    /// ID вида спорта (см. справочник видов спорта)
    let gameId: Int
    /// Внутреннее имя названия спорта
    let name: String
    /// Название спорта
    let title: String
    /// Зарезервированное поле
    let status: Int
    /// Информация о событии
    let event: EventModel
    /// Информация о счете матча
    let scores: ScoreMapModel
    /// Коллекция блоков события (блоки, части игры: сеты, таймы, периоды etc.)
    let parts: PartMapModel
    /// Флаг подтверждающий, что событие содержит незаблокированные исходы
    let hasUnlocked: Bool
    /// Флаг подтверждающий, что событие содержит / не содержит битые данные (например, кэфы <= 1)
    let corrupted: Bool
    /// Справочник поддерживаемых типов основных маркетов
    let outcomesTypes: [String]
    /// Полная роспись с полным списком маркетов
    let outcomes: OutcomeFullMapModel?
    /// Основные исходы на победителя для всех блоков
    let outcomesWinner: OutcomeMapModel?
    /// Основные исходы на двойной шанс для всех блоков
    let outcomesDouble: OutcomeMapModel?
    /// Основные исходы на фору (первая выборка) для всех блоков
    let outcomesHandicap1: OutcomeMapModel?
    /// Основные исходы на фору (вторая выборка) для всех блоков
    let outcomesHandicap2: OutcomeMapModel?
    /// Основные исходы на тотал (первая выборка) для всех блоков
    let outcomesTotal1: OutcomeMapModel?
    /// Основные исходы на тотал (вторая выборка) для всех блоков
    let outcomesTotal2: OutcomeMapModel?
    /// Основные исходы на фору (третья выборка) для всех блоков
    let outcomesTotal3: OutcomeMapModel?
    /// Основные исходы на чет / нечет для всех блоков
    let outcomesOdd: OutcomeMapModel?
    /// Основные исходы на следующий гол для всех блоков
    let outcomesNextGoal: OutcomeMapModel?
    /// Основные исходы на да / нет для всех блоков,
    let outcomesYepNope: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesSummary: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesStrz: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesWin1: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesCompare: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesPlace3Way: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesPolePosition: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesQualify3: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesQualify4: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesQualifyGroup: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesGroupPlace2: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomesGroupPlace4: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomes1Goal2Way: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomes2Goals1Way: OutcomeMapModel?
    // FIXME: - Выяснить название поля
    let outcomes3Goals1Way: OutcomeMapModel?
    /// Объект счетчиков
    let counters: CounterMapModel
    // swiftlint:disable:next line_length
    /// Словарь виджетов, доступных для отображения для данного события (не указывает на то, есть ли привязанный виджет к событию, указывает только на возможность отображения данного типа виджета в интерфейсе)
    let widget: WidgetModel
    /// Типы предложения
    let proposedTypes: [String]?
    
}


// MARK: - Публичные свойства

extension EventObjectModel {
    
    /// Объект
    var entity: EventObject {
        let priority = self.priority.entity
        let popularity = self.popularity.entity
        let ids = self.ids.entity
        let namespace = Namespace(rawValue: self.ns)
        let event = self.event.entity
        let scores = self.scores.entity
        let parts = self.parts.entities
        let outcomesTypes = self.outcomesTypes.compactMap { OutcomeType(rawValue: $0) }
        let outcomes = self.outcomes?.entities
        let outcomesWinner = self.outcomesWinner?.entities
        let outcomesDouble = self.outcomesDouble?.entities
        let outcomesHandicap1 = self.outcomesHandicap1?.entities
        let outcomesHandicap2 = self.outcomesHandicap2?.entities
        let outcomesTotal1 = self.outcomesTotal1?.entities
        let outcomesTotal2 = self.outcomesTotal2?.entities
        let outcomesTotal3 = self.outcomesTotal3?.entities
        let outcomesOdd = self.outcomesOdd?.entities
        let outcomesNextGoal = self.outcomesNextGoal?.entities
        let outcomesYepNope = self.outcomesYepNope?.entities
        let outcomesSummary = self.outcomesSummary?.entities
        let outcomesStrz = self.outcomesStrz?.entities
        let outcomesWin1 = self.outcomesWin1?.entities
        let outcomesCompare = self.outcomesCompare?.entities
        let outcomesPlace3Way = self.outcomesPlace3Way?.entities
        let outcomesPolePosition = self.outcomesPolePosition?.entities
        let outcomesQualify3 = self.outcomesQualify3?.entities
        let outcomesQualify4 = self.outcomesQualify4?.entities
        let outcomesQualifyGroup = self.outcomesQualifyGroup?.entities
        let outcomesGroupPlace2 = self.outcomesGroupPlace2?.entities
        let outcomesGroupPlace4 = self.outcomesGroupPlace4?.entities
        let outcomes1Goal2Way = self.outcomes1Goal2Way?.entities
        let outcomes2Goals1Way = self.outcomes2Goals1Way?.entities
        let outcomes3Goals1Way = self.outcomes3Goals1Way?.entities
        let counters = self.counters.entity
        let widget = self.widget.entities
        let proposedTypes = self.proposedTypes?.compactMap { ProposedType(rawValue: $0) }
        
        return EventObject(id: self.id,
                           priority: priority,
                           popularity: popularity,
                           ids: ids,
                           gameTitle: self.gameTitle,
                           topicTitle: self.topicTitle,
                           tournamentTitle: self.tournamentTitle,
                           categoryTitle: self.categoryTitle,
                           hash: self.hash,
                           namespace: namespace,
                           gameTimestamp: self.gameTs,
                           gameId: self.gameId,
                           name: self.name,
                           title: self.title,
                           status: self.status,
                           event: event,
                           scores: scores,
                           parts: parts,
                           hasUnlocked: self.hasUnlocked,
                           corrupted: self.corrupted,
                           outcomesTypes: outcomesTypes,
                           outcomes: outcomes,
                           outcomesWinner: outcomesWinner,
                           outcomesDouble: outcomesDouble,
                           outcomesHandicap1: outcomesHandicap1,
                           outcomesHandicap2: outcomesHandicap2,
                           outcomesTotal1: outcomesTotal1,
                           outcomesTotal2: outcomesTotal2,
                           outcomesTotal3: outcomesTotal3,
                           outcomesOdd: outcomesOdd,
                           outcomesNextGoal: outcomesNextGoal,
                           outcomesYepNope: outcomesYepNope,
                           outcomesSummary: outcomesSummary,
                           outcomesStrz: outcomesStrz,
                           outcomesWin1: outcomesWin1,
                           outcomesCompare: outcomesCompare,
                           outcomesPlace3Way: outcomesPlace3Way,
                           outcomesPolePosition: outcomesPolePosition,
                           outcomesQualify3: outcomesQualify3,
                           outcomesQualify4: outcomesQualify4,
                           outcomesQualifyGroup: outcomesQualifyGroup,
                           outcomesGroupPlace2: outcomesGroupPlace2,
                           outcomesGroupPlace4: outcomesGroupPlace4,
                           outcomes1Goal2Way: outcomes1Goal2Way,
                           outcomes2Goals1Way: outcomes2Goals1Way,
                           outcomes3Goals1Way: outcomes3Goals1Way,
                           counters: counters,
                           widget: widget,
                           proposedTypes: proposedTypes)
    }
    
}
