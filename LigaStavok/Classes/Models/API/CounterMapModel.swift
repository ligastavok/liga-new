//
//  CounterMapModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Счетчик
final class CounterMapModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Общее количество исходов
    let outcomesQty: Int
    /// Количество неактивных исходов
    let outcomesDisabledQty: Int
    /// Количество активных исходов
    let outcomesEnabledQty: Int
    /// Количество исходов в основном блоке
    let outcomesMainQty: Int
    /// Количество основных блоков
    let outcomesRowsQty: Int
    
}


// MARK: - Публичные свойства

extension CounterMapModel {
    
    /// Объект
    var entity: CounterMap {
        return CounterMap(outcomesQuantity: self.outcomesQty,
                          outcomesDisabledQuantity: self.outcomesDisabledQty,
                          outcomesEnabledQuantity: self.outcomesEnabledQty,
                          outcomesMainQuantity: self.outcomesMainQty,
                          outcomesRowsQuantity: self.outcomesRowsQty)
    }
    
}
