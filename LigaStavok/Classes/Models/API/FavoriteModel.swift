//
//  FavoriteModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель избранного события
final class FavoriteModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// ID Вида спорта
    let _id: Int
    /// Общее количество событий по выборке
    let total: Int
    /// События
    let events: [EventObjectModel]

}


// MARK: - Публичные свойства

extension FavoriteModel {
    
    /// Объект
    var entity: Favorite {
        let events = self.events.map { $0.entity }
        return Favorite(id: self._id, total: self.total, events: events)
    }

}
