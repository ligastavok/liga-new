//
//  OutcomesListModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Коллекция исходов
final class OutcomesListModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Исходы
    let parts: [String: OutcomeModel]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.parts = try container.decode([String: OutcomeModel].self)
    }
    
}


// MARK: - Публичные свойства

extension OutcomesListModel {
    
    /// Объекты
    var entities: OutcomesList {
        var parts: [OutcomeKey: Outcome] = [:]
        for (key, value) in self.parts {
            guard let key = OutcomeKey(key: key) else { continue }
            parts[key] = value.entity
        }
        
        return parts
    }
    
}
