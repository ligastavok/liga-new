//
//  WidgetModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Виджет
final class WidgetModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Словарь виджетов, доступных для отображения для данного события
    let dictionary: [String: Bool]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.dictionary = try container.decode([String: Bool].self)
    }
    
}


// MARK: - Публичные свойства

extension WidgetModel {
    
    /// Объекты
    var entities: Widget {
        return self.dictionary
    }
    
}
