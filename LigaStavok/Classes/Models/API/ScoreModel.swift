//
//  ScoreModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Счет
final class ScoreModel: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        
        /// Название типа объекта счета
        case ttl
        /// Node id (внутренний ID генератора линии)
        case nid
        /// Тип объекта счета
        case type = "Type"
        /// Счет команды #1
        case scoreTeam1 = "ScoreTeam1"
        /// Счет команды #2
        case scoreTeam2 = "ScoreTeam2"
        /// ID блока
        case id = "Id"
        /// Номер части матча (в нотации счетов)
        case partNumber = "PartNumber"
        /// Код блока (в нотации счетов)
        case part = "Part"
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Название типа объекта счета
    let ttl: String
    /// Node id (внутренний ID генератора линии)
    let nid: Int
    /// Тип объекта счета
    let type: String
    /// Счет команды #1
    let scoreTeam1: String
    /// Счет команды #2
    let scoreTeam2: String
    /// ID блока
    let id: String
    /// Номер части матча (в нотации счетов)
    let partNumber: String
    /// Код блока (в нотации счетов)
    let part: String
    
}


// MARK: - Публичные свойства

extension ScoreModel {
    
    /// Объект
    var entity: Score {
        return Score(title: self.ttl,
                     nodeId: self.nid,
                     type: self.type,
                     firstTeamScore: self.scoreTeam1,
                     secondTeamScore: self.scoreTeam2,
                     id: self.id,
                     partNumber: self.partNumber,
                     part: self.part)
    }
    
}
