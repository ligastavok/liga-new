//
//  SportTreeTournamentModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель элемента дерева видов спорта (чемпионат)
final class SportTreeTournamentModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// ID категории
    let categoryId: Int?
    /// Количество событий в чемпионате
    let count: Int
    /// ID событий
    let eventIds: [Int]
    /// ID вида спорта
    let gameId: Int
    /// ID чемпионата
    let id: Int
    /// Популярность чемпионата
    let popularity: Int
    /// Приоритет чемпионата
    let priority: Int
    /// Транслитеризированное название чемпионата, допускается использовать это название в качестве ЧПУ
    let sname: String
    /// Название чемпионата
    let title: String
    
}


// MARK: - Публичные свойства

extension SportTreeTournamentModel {

    /// Объект
    var entity: SportTreeTournament {
        return SportTreeTournament(categoryId: self.categoryId,
                                   count: self.count,
                                   eventIds: self.eventIds,
                                   gameId: self.gameId,
                                   id: self.id,
                                   popularity: self.popularity,
                                   priority: self.priority,
                                   sname: self.sname,
                                   title: self.title)
    }

}
