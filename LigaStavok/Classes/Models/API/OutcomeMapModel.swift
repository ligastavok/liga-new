//
//  OutcomeMapModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Список исходов без маркетов
final class OutcomeMapModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Список частей матча
    let outcomes: [String: MarketInfoModel]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.outcomes = try container.decode([String: MarketInfoModel].self)
    }
    
}


// MARK: - Публичные свойства

extension OutcomeMapModel {
    
    /// Объекты
    var entities: OutcomeMap {
        var outcomes: [MarketKey: MarketInfo] = [:]
        for (key, value) in self.outcomes {
            guard let key = MarketKey(key: key) else { continue }
            outcomes[key] = value.entity
        }
        
        return outcomes
    }
    
}
