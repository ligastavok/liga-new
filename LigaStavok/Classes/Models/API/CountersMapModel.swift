//
//  CountersMapModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 05.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Счетчики всех событий
final class CountersMapModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Данные счетчиков всех событий
    let data: CountersDataModel
    
}


// MARK: - Публичные свойства

extension CountersMapModel {
    
    /// Объект
    var entity: Counters {
        return self.data.entity
    }
    
}
