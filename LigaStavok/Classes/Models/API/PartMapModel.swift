//
//  PartMapModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Список частей матча
final class PartMapModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Список частей матча
    let parts: [String: PartModel]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.parts = try container.decode([String: PartModel].self)
    }
    
}


// MARK: - Публичные свойства

extension PartMapModel {
    
    /// Объекты
    var entities: PartMap {
        var parts: [PartKey: Part] = [:]
        for (key, value) in self.parts {
            guard let key = PartKey(key: key) else { continue }
            parts[key] = value.entity
        }
        
        return parts
    }
    
}
