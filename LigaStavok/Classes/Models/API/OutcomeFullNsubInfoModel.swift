//
//  OutcomeFullNsubInfoModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация о маркете
final class OutcomeFullNsubInfoModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// ID маркета
    let id: Int
    /// Название маркета
    let title: String
    /// Тип маркета ('WIN', 'TTL', 'HAN' etc.)
    let type: String
    /// Порядок сортировки маркета
    let position: Int
    /// Флаг блокировки маркета (все исходы в нем заблокированы)
    let locked: Bool
    /// Флаг поломанности маркета (все исходы в нем поломаны)
    let corrupted: Bool
    /// Коллекция исходов
    let nsub: OutcomesListModel
    
}


// MARK: - Публичные свойства

extension OutcomeFullNsubInfoModel {
    
    /// Объект
    var entity: OutcomeFullNsubInfo {
        let nsub = self.nsub.entities
        
        return OutcomeFullNsubInfo(id: self.id,
                                   title: self.title,
                                   type: self.type,
                                   position: self.position,
                                   locked: self.locked,
                                   corrupted: self.corrupted,
                                   nsub: nsub)
    }
    
}
