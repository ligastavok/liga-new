//
//  EventsListWithJSONModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 02.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

/// Информация по событиям
final class EventsListWithJSONModel: Decodable {
    
    private enum ContainerKeys: String, CodingKey {
        
        /// Данные
        case result
        
    }
    
    private enum CodingKeys: String, CodingKey {
        
        /// Информация по событиям
        case data
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Информация по событиям
    let events: [EventObjectModel]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ContainerKeys.self)
        let resultContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .result)
        self.events = try resultContainer.decode([EventObjectModel].self, forKey: .data)
    }
    
}


// MARK: - Публичные методы

extension EventsListWithJSONModel {
    
    /// Подготовка JSON
    func generateEntities(with requestResult: RequestResult) -> [EventObjectWithJSON]? {
        guard let data = requestResult.data else { return nil }
        
        guard let json = JSON(data)[ContainerKeys.result.rawValue][CodingKeys.data.rawValue].array else { return nil }
        return self.events.enumerated().compactMap { tuple in
            let entity = tuple.element.entity
            let json = json[tuple.offset]
            
            return EventObjectWithJSON(eventObject: entity, json: json)
        }
    }
    
}
