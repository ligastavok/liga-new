//
//  SportModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель вида спорта
final class SportModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Id вида спорта
    let gameId: Int
    /// Название спорта
    let title: String
    /// Неймспейс
    let ns: [String]
    /// Количество текущих событий по виду спорта
    let qty: Int
    
}


// MARK: - Публичные свойства

extension SportModel {
    
    /// Объект
    var entity: Sport {
        let namespace = self.ns.compactMap { Namespace(rawValue: $0) }
        return Sport(gameId: self.gameId, title: self.title, namespace: namespace, quantity: self.qty)
    }
    
}
