//
//  DiffMapModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 14.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация о диффах
final class DiffMapModel: Decodable {
    
    // MARK: - Публичные свойства
    
    // FIXME: - Выяснить название поля
    let type: String
    // FIXME: - Выяснить название поля
    let id: Int
    // FIXME: - Выяснить название поля
    let data: DiffCollectionModel
    
}
