//
//  DiffModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 06.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

/// Дифф
final class DiffModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Тип
    let op: String
    /// Исходный путь
    let from: String?
    /// Путь
    let path: String
    /// Значение
    let value: JSON?
    
}


// MARK: - Публичные методы

extension DiffModel {
    
    /// Перевести в RFC 6902 совместимую реализацию операции JSONPatch
    func translateToJSONPatchOperation(allowsNilValue: Bool) throws -> JSONPatchOperation {
        guard let operationType = JSONPatchOperation.OperationType(rawValue: self.op) else {
            throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.invalidOperation)
        }
        
        var from: JSONPointer?
        if operationType == .move || operationType == .copy {
            guard let fromValue = self.from else {
                // swiftlint:disable:next line_length
                throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.fromElementNotFound)
            }
            
            from = try JSONPointer(rawValue: fromValue)
        }
        
        if (operationType == .add || operationType == .replace) && self.value?.null != nil && !allowsNilValue {
            // swiftlint:disable:next line_length
            throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.valueElementNotFound)
        }
        
        let pointer = try JSONPointer(rawValue: self.path)
        return JSONPatchOperation(type: operationType, pointer: pointer, value: self.value, from: from)
    }
    
}
