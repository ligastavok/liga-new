//
//  SportTreeModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.08.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель дерева видов спорта
final class SportTreeModel: Decodable {
    
    private enum ContainerKeys: String, CodingKey {
        
        /// Результаты запроса
        case result
        
    }
    
    private enum CodingKeys: String, CodingKey {
        
        /// Список популярных чемпионатов
        case popular
        /// Список видов спорта
        case tree
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Список популярных чемпионатов
    let popular: [SportTreeTournamentModel]
    /// Список видов спорта
    let tree: [SportTreeSportModel]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ContainerKeys.self)
        let resultContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .result)
        self.popular = try resultContainer.decode([SportTreeTournamentModel].self, forKey: .popular)
        self.tree = try resultContainer.decode([SportTreeSportModel].self, forKey: .tree)
    }
    
}


// MARK: - Публичные свойства

extension SportTreeModel {

    /// Объект
    var entity: SportTree {
        let popular = self.popular.map { $0.entity }
        let tree = self.tree.map { $0.entity }

        return SportTree(popular: popular, tree: tree)
    }

}
