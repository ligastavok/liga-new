//
//  DiffCollectionModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 14.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Коллекция диффов в формате RFC 6902
final class DiffCollectionModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Все, кроме ключа outcomes
    let headers: [DiffModel]
    /// Только ключ outcomes
    let outcomes: [DiffModel]
    
}
