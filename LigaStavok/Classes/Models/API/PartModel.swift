//
//  PartModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Часть матча
final class PartModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// ID блока
    let id: Int
    /// Название блока (части матча)
    let title: String
    /// Код блока ('FULLTIME', 'OVERTIME', 'GAMESCORE')
    let code: String?
    /// Флаг основной части матча (признак для отображения исходов этого блока в списке событий)
    let main: Bool
    
}


// MARK: - Публичные свойства

extension PartModel {
    
    /// Объект
    var entity: Part {
        return Part(id: self.id, title: self.title, code: self.code, main: self.main)
    }
    
}
