//
//  NewEventModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Модель нового события
final class NewEventModel: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        case event = "data"
    }
    
    
    // MARK: - Публичные свойства
    
    /// Новое событие
    let event: EventObjectModel
    
}


// MARK: - Публичные свойства

extension NewEventModel {
    
    /// Объект
    var entity: EventObject {
        return self.event.entity
    }
    
}
