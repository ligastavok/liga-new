//
//  ScoreMapModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Информация о счете матча
final class ScoreMapModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Общий счет
    let total: ScoreModel?
    /// Текущий счет
    let current: ScoreModel?
    /// Красные карточки
    let red: ScoreModel?
    /// Счет игры
    let gamescore: ScoreModel?
    /// Общий счет с учетом овертайма
    let overtime: ScoreModel?
    /// Пенальти
    let penalty: ScoreModel?
    /// Коллекция данных по счетам разных частей матча
    let all: [ScoreModel]
    
}


// MARK: - Публичные свойства

extension ScoreMapModel {
    
    /// Объект
    var entity: ScoreMap {
        let total = self.total?.entity
        let current = self.current?.entity
        let red = self.red?.entity
        let gamescore = self.gamescore?.entity
        let overtime = self.overtime?.entity
        let penalty = self.penalty?.entity
        let all = self.all.map { $0.entity }
        
        return ScoreMap(total: total,
                        current: current,
                        red: red,
                        gamescore: gamescore,
                        overtime: overtime,
                        penalty: penalty,
                        all: all)
    }
    
}
