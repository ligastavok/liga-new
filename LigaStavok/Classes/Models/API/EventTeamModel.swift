//
//  EventTeamModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Словарь учавствующих команд в событии
final class EventTeamModel: Decodable {
    
    private enum CodingKeys: String, CodingKey {
        
        /// Первая команда
        case first = "1"
        /// Вторая команда
        case second = "2"
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Первая команда
    let first: String
    /// Вторая команда
    let second: String?
    
}


// MARK: - Публичные свойства

extension EventTeamModel {
    
    /// Объект
    var entity: EventTeam {
        return EventTeam(first: self.first, second: self.second)
    }
    
}
