//
//  CompetitorModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Участник события
final class CompetitorModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Идентификатор участника
    let id: Int
    /// Имя участника
    let name: String
    /// Порядковый номер участника
    let number: Int
    /// Статус участника
    let state: String
    /// Результат участника (атрибут появляется, когда известен результат по участнику)
    let resultPlace: Int
    
}


// MARK: - Публичные свойства

extension CompetitorModel {
    
    /// Объект
    var entity: Competitor {
        var state: CompetitorState? = nil
        if self.state == "Active" {
            state = .active
        } else if self.state == "InActive" {
            state = .inActive
        }
        
        return Competitor(id: self.id,
                          name: self.name,
                          number: self.number,
                          state: state,
                          resultPlace: self.resultPlace)
    }
    
}
