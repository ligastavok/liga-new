//
//  OutcomeFullNsubModel.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 03.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Коллекция маркетов
final class OutcomeFullNsubModel: Decodable {
    
    // MARK: - Публичные свойства
    
    /// Маркеты
    let nsubs: [String: OutcomeFullNsubInfoModel]
    
    
    // MARK: - Инициализация
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        self.nsubs = try container.decode([String: OutcomeFullNsubInfoModel].self)
    }
    
}


// MARK: - Публичные свойства

extension OutcomeFullNsubModel {
    
    /// Объект
    var entity: OutcomeFullNsub {
        var nsubs: [MarketKey: OutcomeFullNsubInfo] = [:]
        for (key, value) in self.nsubs {
            guard let key = MarketKey(key: key) else { continue }
            nsubs[key] = value.entity
        }
        
        return nsubs
    }
    
}
