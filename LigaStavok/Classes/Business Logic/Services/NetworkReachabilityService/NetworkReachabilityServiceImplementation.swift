//
//  NetworkReachabilityServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 20.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Reachability

/// Сервис для отселживания информации о доступности интернет-соединения
final class NetworkReachabilityServiceImplementation {
    
    /// Интернет-соединение доступно
    typealias Reachable = (ConnectionType) -> Void
    /// Интернет-соединение недоступно
    typealias NotReachable = () -> Void
    
    
    // MARK: - Приватные свойства
    
    // swiftlint:disable:next weak_delegate
    private let multicastDelegate = MulticastClosureDelegate<Reachable, NotReachable>()
    
    /// Менеджер отслеживания доступности интернет-соединения
    private let reachability: Reachability?
    
    
    // MARK: - Инициализация
    
    init() {
        self.reachability = Reachability()
        
        self.reachability?.whenReachable = { [weak self] reachability in
            self?.networkReachabilityStatusChanged(reachability.connection)
        }
        self.reachability?.whenUnreachable = { [weak self] reachability in
            self?.networkReachabilityStatusChanged(reachability.connection)
        }
    }
    
    deinit {
        self.reachability?.stopNotifier()
    }
    
}


// MARK: - Приватные методы

extension NetworkReachabilityServiceImplementation {
    
    /// Получение статуса из соединения
    func getStatus(from connection: Reachability.Connection) -> NetworkReachabilityStatus {
        switch connection {
        case .none:
            return .notReachable
        case .wifi:
            return .reachable(connectionType: .wifi)
        case .cellular:
            return .reachable(connectionType: .cellular)
        }
    }
    
    /// Изменен статус доступности интернет-соединения
    func networkReachabilityStatusChanged(_ connection: Reachability.Connection) {
        let status = getStatus(from: connection)
        
        switch status {
        case .reachable(let connectionType):
            self.multicastDelegate.getSuccessTuples(removeAfter: false).forEach { closure, queue in
                queue.async {
                    closure(connectionType)
                }
            }
        case .notReachable:
            self.multicastDelegate.getFailureTuples(removeAfter: false).forEach { closure, queue in
                queue.async {
                    closure()
                }
            }
        case .unknown:
            break
        }
    }
    
}


// MARK: - NetworkReachabilityService

extension NetworkReachabilityServiceImplementation: NetworkReachabilityService {
    
    var currentStatus: NetworkReachabilityStatus? {
        guard let connection = self.reachability?.connection else { return nil }
        return getStatus(from: connection)
    }
    
    func listenNetworkReachability(_ object: AnyObject,
                                   queue: DispatchQueue?,
                                   reachable: @escaping Reachable,
                                   notReachable: @escaping NotReachable) {
        
        let tmpListenersCount = self.multicastDelegate.count
        self.multicastDelegate.addClosurePair(for: object, success: reachable, failure: notReachable)
        
        if tmpListenersCount == 0 {
            try? self.reachability?.startNotifier()
        }
    }
    
    func stopListenNetworkReachability(_ object: AnyObject) {
        self.multicastDelegate.removeClosuresPairs(for: object)
        
        // swiftlint:disable:next empty_count
        if self.multicastDelegate.count == 0 {
            self.reachability?.stopNotifier()
        }
    }
    
}
