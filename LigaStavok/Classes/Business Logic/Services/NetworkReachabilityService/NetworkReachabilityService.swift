//
//  NetworkReachabilityService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 20.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для отселживания информации о доступности интернет-соединения
protocol NetworkReachabilityService: class {
    
    /// Текущий статус доступности интернет-соединения
    var currentStatus: NetworkReachabilityStatus? { get }
    
    /// Слушать информацию о доступности интернет-соединения
    func listenNetworkReachability(_ object: AnyObject,
                                   queue: DispatchQueue?,
                                   reachable: @escaping (ConnectionType) -> Void,
                                   notReachable: @escaping () -> Void)
    
    /// Остановить прослушивание информации о доступности интернет-соединения
    func stopListenNetworkReachability(_ object: AnyObject)
    
}

extension NetworkReachabilityService {
    
    func listenNetworkReachability(_ object: AnyObject,
                                   queue: DispatchQueue? = .main,
                                   reachable: @escaping (ConnectionType) -> Void,
                                   notReachable: @escaping () -> Void) {
        
        listenNetworkReachability(object,
                                  queue: queue,
                                  reachable: reachable,
                                  notReachable: notReachable)
    }
    
}
