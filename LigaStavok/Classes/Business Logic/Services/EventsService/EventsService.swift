//
//  EventsService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для работы с событиями
protocol EventsService: class {
    
    // MARK: Repository
    
    /// Запрос списка видов спорта
    func obtainSportsListCached(completion: @escaping ([Sport]) -> Void)
    
    
    // MARK: Web
    
    /// Получение списка событий
    func obtainEventsList(gameId: Int?,
                          eventIds: [Int]?,
                          categoryIds: [Int]?,
                          tournamentIds: [Int]?,
                          topicId: [Int]?,
                          period: Int?,
                          limit: Int?,
                          namespace: Namespace?,
                          matchDay: Int?,
                          fullEvents: Bool?,
                          topEvents: Bool?,
                          completionHandler completion: @escaping (WebService.ResultContainer<[EventObject]>) -> Void) -> Operation
    
    /// Получение списка событий с исходными JSON
    func obtainEventsListWithJSONs(gameId: Int?,
                                   eventIds: [Int]?,
                                   categoryIds: [Int]?,
                                   tournamentIds: [Int]?,
                                   topicId: [Int]?,
                                   period: Int?,
                                   limit: Int?,
                                   namespace: Namespace?,
                                   matchDay: Int?,
                                   fullEvents: Bool?,
                                   topEvents: Bool?,
                                   completionHandler completion: @escaping (WebService.ResultContainer<[EventObjectWithJSON]>) -> Void) -> Operation
    
    /// Запрос списка видов спорта
    func obtainSportsList(namespace: Namespace?,
                          period: Int?,
                          completionHandler completion: @escaping (WebService.ResultContainer<[Sport]>) -> Void) -> Operation
    
    /// Запрос списка топиков по виду спорта
    func obtainTopicsList(gameId: Int?,
                          period: Int?,
                          namespace: Namespace?,
                          completionHandler completion: @escaping (WebService.ResultContainer<[Topic]>) -> Void) -> Operation
    
    /// Запрос списка избранных событий
    func obtainFavoritesList(gameIds: [Int]?,
                             eventIds: [Int]?,
                             topicIds: [Int]?,
                             limit: Int?,
                             completionHandler completion: @escaping (WebService.ResultContainer<[Favorite]>) -> Void) -> Operation
    
    /// Получение дерева видов спорта
    func obtainSportsTree(completionHandler completion: @escaping (WebService.ResultContainer<SportTree>) -> Void) -> Operation
    
}
