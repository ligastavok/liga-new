//
//  EventsServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для работы с событиями
final class EventsServiceImplementation {
    
    // MARK: - Приватные свойства
    
    /// Сервис для работы с событиями в локальной бд
    private let eventsRepositoryService: EventsRepositoryService
    /// Сервис для работы с событиями через web-интерфейс
    private let eventsWebService: EventsWebService
    
    
    // MARK: - Инициализация
    
    init(eventsRepositoryService: EventsRepositoryService,
         eventsWebService: EventsWebService) {
        
        self.eventsRepositoryService = eventsRepositoryService
        self.eventsWebService = eventsWebService
    }
    
}


// MARK: - EventsService

extension EventsServiceImplementation: EventsService {
    
    // MARK: Repository
    
    func obtainSportsListCached(completion: @escaping ([Sport]) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            guard let sports = self?.eventsRepositoryService.querySports() else { return }
            
            DispatchQueue.main.async {
                completion(sports)
            }
        }
    }
    
    
    // MARK: Web
    
    func obtainEventsList(gameId: Int?,
                          eventIds: [Int]?,
                          categoryIds: [Int]?,
                          tournamentIds: [Int]?,
                          topicId: [Int]?,
                          period: Int?,
                          limit: Int?,
                          namespace: Namespace?,
                          matchDay: Int?,
                          fullEvents: Bool?,
                          topEvents: Bool?,
                          completionHandler completion: @escaping (WebService.ResultContainer<[EventObject]>) -> Void) -> Operation {
        
        return self.eventsWebService.eventsList(gameId: gameId,
                                                eventIds: eventIds,
                                                categoryIds: categoryIds,
                                                tournamentIds: tournamentIds,
                                                topicId: topicId,
                                                period: period,
                                                limit: limit,
                                                namespace: namespace,
                                                matchDay: matchDay,
                                                fullEvents: fullEvents,
                                                topEvents: topEvents,
                                                completionHandler: completion)
    }
    
    func obtainEventsListWithJSONs(gameId: Int?,
                                   eventIds: [Int]?,
                                   categoryIds: [Int]?,
                                   tournamentIds: [Int]?,
                                   topicId: [Int]?,
                                   period: Int?,
                                   limit: Int?,
                                   namespace: Namespace?,
                                   matchDay: Int?,
                                   fullEvents: Bool?,
                                   topEvents: Bool?,
                                   completionHandler completion: @escaping (WebService.ResultContainer<[EventObjectWithJSON]>) -> Void) -> Operation {
        
        return self.eventsWebService.eventsListWithJSONs(gameId: gameId,
                                                         eventIds: eventIds,
                                                         categoryIds: categoryIds,
                                                         tournamentIds: tournamentIds,
                                                         topicId: topicId,
                                                         period: period,
                                                         limit: limit,
                                                         namespace: namespace,
                                                         matchDay: matchDay,
                                                         fullEvents: fullEvents,
                                                         topEvents: topEvents,
                                                         completionHandler: completion)
    }
    
    func obtainSportsList(namespace: Namespace?,
                          period: Int?,
                          completionHandler completion: @escaping (WebService.ResultContainer<[Sport]>) -> Void) -> Operation {
        
        return self.eventsWebService.sportsList(namespace: namespace,
                                                period: period,
                                                completionHandler: completion)
    }
    
    func obtainTopicsList(gameId: Int?,
                          period: Int?,
                          namespace: Namespace?,
                          completionHandler completion: @escaping (WebService.ResultContainer<[Topic]>) -> Void) -> Operation {
        
        return self.eventsWebService.topicsList(gameId: gameId,
                                                period: period,
                                                namespace: namespace,
                                                completionHandler: completion)
    }
    
    func obtainFavoritesList(gameIds: [Int]?,
                             eventIds: [Int]?,
                             topicIds: [Int]?,
                             limit: Int?,
                             completionHandler completion: @escaping (WebService.ResultContainer<[Favorite]>) -> Void) -> Operation {
        
        return self.eventsWebService.favoritesList(gameIds: gameIds,
                                                   eventIds: eventIds,
                                                   topicIds: topicIds,
                                                   limit: limit,
                                                   completionHandler: completion)
    }
    
    func obtainSportsTree(completionHandler completion: @escaping (WebService.ResultContainer<SportTree>) -> Void) -> Operation {
        return self.eventsWebService.getSportsTree(completionHandler: completion)
    }
    
}
