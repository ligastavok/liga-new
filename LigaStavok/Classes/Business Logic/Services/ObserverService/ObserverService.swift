//
//  ObserverService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 13.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для получения обновления по событиям
protocol ObserverService: class {
    
    /// Подписаться на обновления событий
    func subscribeToEventsUpdates(ids: [Int], onMessage: @escaping ([DiffMapModel]) -> Void) -> Operation
    
    /// Добавить подписку на обновления события
    func addSubscriptionToEventUpdates(withId id: Int, in operation: Operation)
    
    /// Удалить подписку на обновления события
    func removeSubscriptionToEventUpdates(withId id: Int, in operation: Operation)
    
}
