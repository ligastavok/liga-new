//
//  ObserverServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 13.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для получения обновления по событиям
final class ObserverServiceImplementation: SocketService { }


// MARK: - ObserverService

extension ObserverServiceImplementation: ObserverService {
    
    func subscribeToEventsUpdates(ids: [Int], onMessage: @escaping ([DiffMapModel]) -> Void) -> Operation {
        let namespace = constructNamespace(with: .observer, version: .v1)
        let parameters = SocketIOParameters(url: self.environment.socketUrl, namespace: namespace)
        
        let socket = SocketIO(parameters: parameters)
        
        socket.on(clientEvent: .connect) { _ in
            print("socket connected")
            
            let method = API.Endpoint.Observer.observe.rawValue
            socket.emit("message", with: ["method": method, "params": ["eventIds": ids]])
        }
        
        socket.on(clientEvent: .disconnect) { data in
            print("socket disconnect \(data)")
        }
        
        socket.on(clientEvent: .error) { data in
            print("error \(data)")
        }
        
        socket.on(API.MessageType.Observer.update.rawValue) { data in
            guard let array = data as? [String], !array.isEmpty  else { return }
            
            let jsonDecoder = JSONDecoder()
            let models: [[DiffMapModel]] = array.compactMap {
                guard let data = $0.data(using: .utf8) else { return nil }
                return try? jsonDecoder.decode([DiffMapModel].self, from: data)
            }
            
            onMessage(models.flatMap { $0 })
        }
        
        let operation = SocketIOOperation(socket: socket)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
    func addSubscriptionToEventUpdates(withId id: Int, in operation: Operation) {
        guard let operation = operation as? SocketIOOperation, !operation.isCancelled else { return }
        guard operation.socket.isConnected else { return }
        
        let method = API.Endpoint.Observer.addObserve.rawValue
        operation.socket.emit("message", with: ["method": method, "params": ["eventIds": [id]]])
    }
    
    func removeSubscriptionToEventUpdates(withId id: Int, in operation: Operation) {
        guard let operation = operation as? SocketIOOperation, !operation.isCancelled else { return }
        guard operation.socket.isConnected else { return }
        
        let method = API.Endpoint.Observer.removeObserve.rawValue
        operation.socket.emit("message", with: ["method": method, "params": ["eventIds": [id]]])
    }
    
}
