//
//  SocketService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SocketIO

/// Socket сервис
class SocketService {
    
    // MARK: - Публичные свойства
    
    /// Очередь операций
    let operationQueue: OperationQueue
    
    /// Среда
    let environment: Environment
    
    
    // MARK: - Инициализация
    
    init(operationQueue: OperationQueue, environment: Environment) {
        self.operationQueue = operationQueue
        self.environment = environment
    }
    
    
    // MARK: - Деинициализация
    
    deinit {
        self.operationQueue.cancelAllOperations()
    }
    
}


// MARK: - Публичные методы

extension SocketService {
    
    /// Собрать пространсво имен
    func constructNamespace(with namespace: API.Namespace, version: API.Version) -> String {
        return "/" + namespace.rawValue + "/" + version.rawValue
    }
    
}
