//
//  BroadcastWebServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для получения публичной информации через web-интерфейс
final class BroadcastWebServiceImplementation: WebService { }


// MARK: - BroadcastWebService

extension BroadcastWebServiceImplementation: BroadcastWebService {
    
    func obtainCounters(completionHandler completion: @escaping (WebService.ResultContainer<Counters>) -> Void) {
        let requestCompletion: WebTransportOperation.WebTransportOperationCompletionBlock = { [weak self] operation, requestResult in
            guard let `self` = self else { return }
            
            guard self.validateRequestResult(requestResult, completionHandler: completion) else { return }
            guard let model: CountersModel = self.parseRequestResult(requestResult,
                                                                completionHandler: completion) else { return }
            
            let entity = model.entity
            
            guard !operation.isCancelled else { return }
            
            self.dispatchSuccess(completionHandler: completion, value: entity)
        }
        
        let address = constructAdress(with: .rest, namespace: .broadcast, version: .v1)
        let parameters = BroadcastParameters.post(baseUrl: self.environment.baseUrl.absoluteString,
                                               address: address,
                                               endpoint: API.Endpoint.Broadcast.getCounters.rawValue)
        
        let transport = WebTransport()
        let operation = WebTransportOperation(transport: transport,
                                              parameters: parameters,
                                              requestCompletion: requestCompletion)
        
        self.operationQueue.addOperation(operation)
    }
    
}
