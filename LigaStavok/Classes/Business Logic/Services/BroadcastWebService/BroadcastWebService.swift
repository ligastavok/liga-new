//
//  BroadcastWebService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для получения публичной информации через web-интерфейс
protocol BroadcastWebService: class {
    
    /// Получение списка всех счетчиков по событиям
    func obtainCounters(completionHandler completion: @escaping (WebService.ResultContainer<Counters>) -> Void)
    
}
