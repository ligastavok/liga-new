//
//  ServiceComponentsAssembly.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Swinject

/// Блок сервисов
final class ServiceComponentsAssembly { }


// MARK: - Приватные свойства

private extension ServiceComponentsAssembly {
    
    /// Очередь операций
    var operationQueue: OperationQueue {
        let operationQueue = OperationQueue()
        operationQueue.qualityOfService = .utility
        
        return operationQueue
    }
    
}


// MARK: - Assembly

extension ServiceComponentsAssembly: Assembly {
    
    func assemble(container: Container) {
        
        // Среда
        container.register(Environment.self) { _ in
            DefaultEnvironment()
        }.inObjectScope(.container)
        
        // Сервис для отселживания информации о доступности интернет-соединения
        container.register(NetworkReachabilityService.self) { _ in
            NetworkReachabilityServiceImplementation()
        }.inObjectScope(.container)
        
        // Сервис для работы с событиями в локальной бд
        container.register(EventsRepositoryService.self) { _ in
            EventsRepositoryServiceImplementation()
        }
        
        // Сервис для работы с событиями через web-интерфейс
        container.register(EventsWebService.self) { resolver in
            guard let environment = resolver.resolve(Environment.self) else { preconditionFailure() }
            guard let eventsRepositoryService = resolver.resolve(EventsRepositoryService.self) else { preconditionFailure() }
            return EventsWebServiceImplementation(operationQueue: self.operationQueue,
                                                  environment: environment,
                                                  eventsRepositoryService: eventsRepositoryService)
        }
        
        // Сервис для работы с событиями
        container.register(EventsService.self) { resolver in
            guard let eventsRepositoryService = resolver.resolve(EventsRepositoryService.self) else { preconditionFailure() }
            guard let eventsWebService = resolver.resolve(EventsWebService.self) else { preconditionFailure() }
            return EventsServiceImplementation(eventsRepositoryService: eventsRepositoryService,
                                               eventsWebService: eventsWebService)
        }
        
        // Сервис для поиска
        container.register(SearchService.self) { resolver in
            guard let environment = resolver.resolve(Environment.self) else { preconditionFailure() }
            return SearchServiceImplementation(operationQueue: self.operationQueue, environment: environment)
        }
        
        // Сервис ???
        container.register(ScoreboardService.self) { resolver in
            guard let environment = resolver.resolve(Environment.self) else { preconditionFailure() }
            return ScoreboardServiceImplementation(operationQueue: self.operationQueue, environment: environment)
        }
        
        // Сервис для получения публичной информации через web-интерфейс
        container.register(BroadcastWebService.self) { resolver in
            guard let environment = resolver.resolve(Environment.self) else { preconditionFailure() }
            return BroadcastWebServiceImplementation(operationQueue: self.operationQueue,
                                                  environment: environment)
        }
        
        // Сервис для получения публичной информации через socket-интерфейс
        container.register(BroadcastSocketService.self) { resolver in
            guard let environment = resolver.resolve(Environment.self) else { preconditionFailure() }
            return BroadcastSocketServiceImplementation(operationQueue: self.operationQueue,
                                                     environment: environment)
        }
        
        // Сервис для получения публичной информации
        container.register(BroadcastService.self) { resolver in
            guard let broadcastWebService = resolver.resolve(BroadcastWebService.self) else { preconditionFailure() }
            guard let broadcastSocketService = resolver.resolve(BroadcastSocketService.self) else { preconditionFailure() }
            return BroadcastServiceImplementation(broadcastWebService: broadcastWebService,
                                                  broadcastSocketService: broadcastSocketService)
        }
        
        // Сервис для получения обновления по событиям
        container.register(ObserverService.self) { resolver in
            guard let environment = resolver.resolve(Environment.self) else { preconditionFailure() }
            return ObserverServiceImplementation(operationQueue: self.operationQueue, environment: environment)
        }
    }
    
}
