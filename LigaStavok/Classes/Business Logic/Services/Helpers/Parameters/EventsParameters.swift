//
//  EventsParameters.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class EventsParameters: Parameters {
    
    private enum Keys: String {
        
        /// Идентификатор вида спорта
        case gameId
        /// Список идентификаторов событий
        case eventIds
        /// Список идентификаторов категорий
        case categoryIds
        /// Список идентификаторов турниров
        case tournamentIds
        /// Идентификатор топика
        case topicId
        /// Выборка по времени события
        case period
        /// Лимит выборки
        case limit
        /// Неймспейс
        case namespace = "ns"
        /// Фильтр по matchDay
        case matchDay
        /// Признак получения полного списка исходов события (поле outcomes в объекте события), по умолчанию false
        case fullEvents
        /// Только топовые события
        case topEvents
        /// Список идентификаторов видов спорта
        case gameIds
        /// Список идентификаторов топиков
        case topicIds
        
    }
    
}


// MARK: - Публичные методы

extension EventsParameters {
    
    /// Идентификатор вида спорта
    @discardableResult
    func with(gameId: Int) -> Self {
        return withParameter(parameter: Keys.gameId.rawValue, value: gameId as AnyObject)
    }
    
    /// Список идентификаторов событий
    @discardableResult
    func with(eventIds: [Int]) -> Self {
        return withParameter(parameter: Keys.eventIds.rawValue, value: eventIds as AnyObject)
    }
    
    /// Список идентификаторов категорий
    @discardableResult
    func with(categoryIds: [Int]) -> Self {
        return withParameter(parameter: Keys.categoryIds.rawValue, value: categoryIds as AnyObject)
    }
    
    /// Список идентификаторов турниров
    @discardableResult
    func with(tournamentIds: [Int]) -> Self {
        return withParameter(parameter: Keys.tournamentIds.rawValue, value: tournamentIds as AnyObject)
    }
    
    /// Идентификатор топика
    @discardableResult
    func with(topicId: [Int]) -> Self {
        return withParameter(parameter: Keys.topicId.rawValue, value: topicId as AnyObject)
    }
    
    /// Выборка по времени события
    @discardableResult
    func with(period: Int) -> Self {
        return withParameter(parameter: Keys.period.rawValue, value: period as AnyObject)
    }
    
    /// Лимит выборки
    @discardableResult
    func with(limit: Int) -> Self {
        return withParameter(parameter: Keys.limit.rawValue, value: limit as AnyObject)
    }
    
    /// Неймспейс
    @discardableResult
    func with(namespace: Namespace) -> Self {
        return withParameter(parameter: Keys.namespace.rawValue, value: namespace.rawValue as AnyObject)
    }
    
    /// Фильтр по matchDay
    @discardableResult
    func with(matchDay: Int) -> Self {
        return withParameter(parameter: Keys.matchDay.rawValue, value: matchDay as AnyObject)
    }
    
    /// Признак получения полного списка исходов события (поле outcomes в объекте события), по умолчанию false
    @discardableResult
    func with(fullEvents: Bool) -> Self {
        return withParameter(parameter: Keys.fullEvents.rawValue, value: fullEvents as AnyObject)
    }
    
    /// Только топовые события
    @discardableResult
    func with(topEvents: Bool) -> Self {
        return withParameter(parameter: Keys.topEvents.rawValue, value: topEvents as AnyObject)
    }
    
    /// Список идентификаторов видов спорта
    @discardableResult
    func with(gameIds: [Int]) -> Self {
        return withParameter(parameter: Keys.gameIds.rawValue, value: gameIds as AnyObject)
    }
    
    /// Список идентификаторов топиков
    @discardableResult
    func with(topicIds: [Int]) -> Self {
        return withParameter(parameter: Keys.topicIds.rawValue, value: topicIds as AnyObject)
    }
    
}
