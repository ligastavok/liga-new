//
//  SearchParameters.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class SearchParameters: Parameters {
    
    private enum Keys: String {
        
        /// Поисковая строка (поиск по названию команд)
        case search
        /// Идентификатор игры
        case gameId
        // FIXME: - Узнать значение
        case skip
        /// Лимит выборки
        case limit
        
    }
    
}


// MARK: - Публичные методы

extension SearchParameters {
    
    /// Поисковая строка (поиск по названию команд)
    @discardableResult
    func with(search: String) -> Self {
        return withParameter(parameter: Keys.search.rawValue, value: search as AnyObject)
    }
    
    /// Идентификатор игры
    @discardableResult
    func with(gameId: Int) -> Self {
        return withParameter(parameter: Keys.gameId.rawValue, value: gameId as AnyObject)
    }
    
    // FIXME: - Узнать значение
    @discardableResult
    func with(skip: Int) -> Self {
        return withParameter(parameter: Keys.skip.rawValue, value: skip as AnyObject)
    }
    
    /// Лимит выборки
    @discardableResult
    func with(limit: Int) -> Self {
        return withParameter(parameter: Keys.limit.rawValue, value: limit as AnyObject)
    }
    
}
