//
//  Parameters.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Alamofire

class Parameters: WebRequestParameters { }


// MARK: - Публичные методы

extension Parameters {
    
    /// GET-запрос
    class func get(baseUrl: String,
                   address: String,
                   endpoint: String,
                   timeout: TimeInterval? = nil,
                   consoleOutputEnabled: Bool = true) -> Self {
        
        let url = slashConcatenated(elements: baseUrl, address, endpoint)
        return self.init(endpoint: url,
                         method: .get,
                         encoder: URLEncoding.`default`,
                         timeout: timeout,
                         consoleOutputEnabled: consoleOutputEnabled)
    }
    
    /// POST-запрос
    class func post(baseUrl: String,
                    address: String,
                    endpoint: String,
                    timeout: TimeInterval? = nil,
                    consoleOutputEnabled: Bool = true) -> Self {
        
        let url = slashConcatenated(elements: baseUrl, address, endpoint)
        return self.init(endpoint: url,
                         method: .post,
                         encoder: JSONEncoding.`default`,
                         timeout: timeout,
                         consoleOutputEnabled: consoleOutputEnabled)
    }
    
}


// MARK: - Приватные методы

private extension Parameters {
    
    /// Соединить значения
    class func slashConcatenated(elements: String...) -> String {
        var elements = elements.filter { !$0.isEmpty }
        
        guard var result = elements.first else { return .empty }
        _ = elements.removeFirst()
        
        result = elements.reduce(result) { $0 + "/" + $1 }
        return result
    }
    
}
