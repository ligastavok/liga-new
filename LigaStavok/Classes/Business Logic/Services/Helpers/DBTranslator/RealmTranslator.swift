//
//  RealmTranslator.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

import RealmSwift

class RealmTranslator<Model: Entity, RealmModel: Object> {
    
    // MARK: - Объект -> Запись
    
    /// Преобразовать объект в запись
    func toEntry(_ entity: Model) -> RealmModel {
        preconditionFailure()
    }
    
    /// Преобразовать объекты в записи
    func toEntries(_ entities: [Model]?) -> List<RealmModel> {
        let list = List<RealmModel>()
        guard let entities = entities else { return list }
        
        let entries = entities.map { self.toEntry($0) }
        list.append(objectsIn: entries)
        return list
    }
    
    
    // MARK: - Запись -> Объект
    
    /// Преобразовать запись в объект
    func toEntity(_ entry: RealmModel) -> Model {
        preconditionFailure()
    }
    
    /// Преобразовать записи в объекты
    func toEntities(_ entries: List<RealmModel>) -> [Model] {
        let entities: [Model] = entries.map { self.toEntity($0) }
        return entities
    }
    
}
