//
//  SportTranslator.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class SportTranslator: RealmTranslator<Sport, DBSport> {
    
    // MARK: - RealmTranslator
    
    override func toEntry(_ entity: Sport) -> DBSport {
        let sport = DBSport()
        sport.gameId = entity.gameId
        sport.title = entity.title
        sport.namespace = entity.namespace.map { $0.rawValue }.joined(separator: "|")
        
        return sport
    }
    
    override func toEntity(_ entry: DBSport) -> Sport {
        let namespace = entry.namespace.components(separatedBy: "|").compactMap { Namespace(rawValue: $0) }
        return Sport(
            gameId: entry.gameId,
            title: entry.title,
            namespace: namespace,
            quantity: 0
        )
    }
    
}
