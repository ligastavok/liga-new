//
//  ResponseErrorParser.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Парсер ошибки при выполнении запроса
final class ResponseErrorParser {
    
    /// Попытка получить информацию об ошибке
    class func getError(from data: Data) -> ErrorModel? {
        let decoder = JSONDecoder()
        return try? decoder.decode(ErrorModel.self, from: data)
    }
    
}
