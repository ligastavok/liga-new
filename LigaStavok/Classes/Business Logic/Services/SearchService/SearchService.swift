//
//  SearchService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для поиска
protocol SearchService: class {
    
    /// Поиск событий
    @discardableResult
    func searchEvents(search: String?,
                      gameId: Int?,
                      skip: Int?,
                      limit: Int?,
                      completionHandler completion: @escaping (WebService.ResultContainer<[EventObject]>) -> Void) -> Operation
    
}
