//
//  SearchServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для поиска
final class SearchServiceImplementation: WebService { }


// MARK: - SearchService

extension SearchServiceImplementation: SearchService {
    
    @discardableResult
    func searchEvents(search: String?,
                      gameId: Int?,
                      skip: Int?,
                      limit: Int?,
                      completionHandler completion: @escaping (WebService.ResultContainer<[EventObject]>) -> Void) -> Operation {
        
        let requestCompletion: WebTransportOperation.WebTransportOperationCompletionBlock = { [weak self] operation, requestResult in
            guard let `self` = self else { return }
            
            guard self.validateRequestResult(requestResult, completionHandler: completion) else { return }
            guard let model: EventsListModel = self.parseRequestResult(requestResult,
                                                                       dateDecodingStrategy: .millisecondsSince1970,
                                                                       completionHandler: completion) else { return }
            
            let entities = model.entities
            
            guard !operation.isCancelled else { return }
            
            self.dispatchSuccess(completionHandler: completion, value: entities)
        }
        
        let address = constructAdress(with: .rest, namespace: .search, version: .v1)
        let parameters = SearchParameters.post(baseUrl: self.environment.baseUrl.absoluteString,
                                               address: address,
                                               endpoint: API.Endpoint.Search.searchEvents.rawValue,
                                               consoleOutputEnabled: false)
        if let search = search {
            parameters.with(search: search)
        }
        if let gameId = gameId {
            parameters.with(gameId: gameId)
        }
        if let skip = skip {
            parameters.with(skip: skip)
        }
        if let limit = limit {
            parameters.with(limit: limit)
        }
        
        let transport = WebTransport()
        let operation = WebTransportOperation(transport: transport,
                                              parameters: parameters,
                                              requestCompletion: requestCompletion)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
}
