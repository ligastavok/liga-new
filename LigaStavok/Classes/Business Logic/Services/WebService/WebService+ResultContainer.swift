//
//  WebService+ResultContainer.swift
//  LigaStavok
//
//  Created by Sergey Sergeyev on 4/2/18.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension WebService {
    
    /// Содержит значение или ошибку
    enum ResultContainer<T> {
        
        /// Успешное завершение запроса
        case success(value: T)
        /// Неудачное завершение запроса
        case failure(error: Error)
        
    }
    
}


// MARK: - Публичные свойства

extension WebService.ResultContainer {
    
    /// Результат
    var isSuccess: Bool {
        switch self {
        case .success:
            return true
        case .failure:
            return false
        }
    }
    
    /// Результат
    var isFailure: Bool {
        return !self.isSuccess
    }
    
    /// Полученное значение
    var value: T? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    /// Возникшая ошибка
    var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
    
}
