//
//  WebService+Error.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension WebService {
    
    enum Error: Swift.Error {
        
        /// Пустое тело ответа сервера
        case emptyResponse
        /// Некорректный ответ сервера
        case incorrectServerAnswer(error: Swift.Error)
        /// Превышен лимит времени
        case timeout
        
        
        // MARK: NSURLErrorDomain
        
        /// Отменен
        case cancelled
        /// Ошибка при подключении к интернету
        case networkError(message: String)
        
        
        // MARK: Коды ошибок HTTP
        
        /// Содержимое затребованного ресурса не изменилось (304)
        case notModified(message: String?)
        /// Ошибка приложения в запросе (400)
        case badRequest(message: String?)
        /// Приложение не подписало вызванный метод API либо подпись неверна или авторизация не проведена (401)
        case unauthorized(message: String?)
        /// Приложение не имеет доступа к запрошенному ресурсу (403)
        case forbidden(message: String?)
        /// Запрашиваемый ресурс отсутствует (404)
        case notFound(message: String?)
        /// Приложение использует неправильный метод HTTP (405)
        case methodNotAllowed(message: String?)
        /// Запрошенная операция не может быть выполнена в силу ограничений данных (406)
        case notAcceptable(message: String?)
        /// Время, переданное в заголовке X-Auth-Time, отличается от времени сервера более чем на 60 секунд в меньшую или большую сторону (410)
        case gone(message: String?)
        /// Сервер не поддерживает запрошенное приложением представление возвращаемых данных (415)
        case unsupportedMediaType(message: String?)
        /// Ошибка сервера (500)
        case internalServerError(message: String?)
        /// В настоящее время сервер не может обслужить запрос (503)
        case serviceUnavailable(message: String?)
        
    }
    
}


// MARK: - LocalizedError

extension WebService.Error: LocalizedError {
    
    var errorDescription: String? {
        switch self {
        #if DEBUG
        case .emptyResponse:
            return "Некорректный ответ сервера на запрос."
        case .incorrectServerAnswer(let error):
            return "Некорректный ответ сервера на запрос. \(String(describing: error))"
        #else
        case .emptyResponse, .incorrectServerAnswer:
            return "Некорректный ответ сервера на запрос."
        #endif
        case .timeout:
            return "Превышен лимит времени на запрос."
            
        case .cancelled:
            return "Отменено."
        case .networkError(let message):
            return message
            
        case .notModified(let message):
            return message ?? "Содержимое затребованного ресурса не изменилось."
        case .badRequest(let message):
            return message ?? "Ошибка приложения в запросе."
        case .unauthorized(let message):
            return message ?? "Приложение не подписало вызванный метод API."
        case .forbidden(let message):
            return message ?? "Приложение не имеет доступа к запрошенному ресурсу."
        case .notFound(let message):
            return message ?? "Запрашиваемый ресурс отсутствует."
        case .methodNotAllowed(let message):
            return message ?? "Приложение использует неправильный метод HTTP."
        case .notAcceptable(let message):
            return message ?? "Запрошенная операция не может быть выполнена в силу ограничений данных."
        case .gone(let message):
            return message ?? "Время отличается от времени сервера."
        case .unsupportedMediaType(let message):
            return message ?? "Сервер не поддерживает запрошенное приложением представление возвращаемых данных."
        case .internalServerError(let message):
            return message ?? "Ошибка сервера."
        case .serviceUnavailable(let message):
            return message ?? "В настоящее время сервер не может обслужить запрос."
        }
    }
    
}
