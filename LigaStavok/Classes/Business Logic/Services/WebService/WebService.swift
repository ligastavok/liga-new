//
//  WebService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Alamofire

/// Web сервис
class WebService {
    
    // MARK: - Типы данных
    
    /// Запрос завершен
    typealias VoidCompletion = () -> Void
    
    
    // MARK: - Публичные свойства
    
    /// Очередь операций
    let operationQueue: OperationQueue
    
    /// Среда
    let environment: Environment
    
    
    // MARK: - Инициализация
    
    init(operationQueue: OperationQueue, environment: Environment) {
        self.operationQueue = operationQueue
        self.environment = environment
    }
    
    
    // MARK: - Деинициализация
    
    deinit {
        self.operationQueue.cancelAllOperations()
    }
    
}


// MARK: - Публичные методы

extension WebService {
    
    /// Собрать адрес веб-сервиса
    func constructAdress(with prefix: API.Prefix, namespace: API.Namespace, version: API.Version) -> String {
        return prefix.rawValue + "/" + namespace.rawValue + "/" + version.rawValue
    }
    
    /// Проверить результат запроса на ошибку
    func checkForError(requestResult: RequestResult) throws {
        guard let requestResult = requestResult as? WebRequestResult else { return }
        
        switch requestResult.status {
        case .success:
            guard !requestResult.httpStatus.isSuccessHTTPCode else { return }
            
            let message: String?
            if let data = requestResult.data, let error = ResponseErrorParser.getError(from: data) {
                message = error.message
            } else {
                message = nil
            }
            
            let error: Error?
            switch requestResult.httpStatus {
            case 304:
                error = Error.notModified(message: message)
            case 400:
                error = Error.badRequest(message: message)
            case 401:
                error = Error.unauthorized(message: message)
            case 403:
                error = Error.forbidden(message: message)
            case 404:
                error = Error.notFound(message: message)
            case 405:
                error = Error.methodNotAllowed(message: message)
            case 406:
                error = Error.notAcceptable(message: message)
            case 410:
                error = Error.gone(message: message)
            case 415:
                error = Error.unsupportedMediaType(message: message)
            case 500:
                error = Error.internalServerError(message: message)
            case 503:
                error = Error.serviceUnavailable(message: message)
                
            default:
                error = nil
            }
            
            if let error = error {
                throw error
            }
        case .systemError(let error):
            throw Error.networkError(message: error.localizedDescription)
        case .timeout:
            throw Error.timeout
        case .cancelled:
            throw Error.cancelled
        }
    }
    
    /// Отправить ошибку
    func dispatchFailure<R>(completionHandler completion: @escaping (ResultContainer<R>) -> Void, error: Error) {
        DispatchQueue.main.async {
            completion(.failure(error: error))
        }
    }
    
    /// Отправить успешное завершение
    func dispatchSuccess<R>(completionHandler completion: @escaping (ResultContainer<R>) -> Void, value: R) {
        DispatchQueue.main.async {
            completion(.success(value: value))
        }
    }
    
    /// Валидация результата запроса
    func validateRequestResult<R>(_ requestResult: RequestResult,
                                  completionHandler completion: @escaping (ResultContainer<R>) -> Void) -> Bool {
        
        do {
            try checkForError(requestResult: requestResult)
            return true
        } catch let error as Error {
            dispatchFailure(completionHandler: completion, error: error)
        } catch { }
        
        return false
    }
    
    /// Парсинг результата запроса
    func parseRequestResult<T: Decodable, R>(_ requestResult: RequestResult,
                                             dateDecodingStrategy: JSONDecoder.DateDecodingStrategy? = nil,
                                             completionHandler completion: @escaping (ResultContainer<R>) -> Void) -> T? {
        
        let jsonDecoder = JSONDecoder()
        if let dateDecodingStrategy = dateDecodingStrategy {
            jsonDecoder.dateDecodingStrategy = dateDecodingStrategy
        }
        
        guard let data = requestResult.data else {
            dispatchFailure(completionHandler: completion, error: .emptyResponse)
            return nil
        }
        
        do {
            let model = try jsonDecoder.decode(T.self, from: data)
            return model
        } catch let error {
            dispatchFailure(completionHandler: completion, error: .incorrectServerAnswer(error: error))
            return nil
        }
    }
    
}
