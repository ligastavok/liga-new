//
//  EventsRepositoryServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для работы с событиями в локальной бд
final class EventsRepositoryServiceImplementation { }


// MARK: - EventsRepositoryService

extension EventsRepositoryServiceImplementation: EventsRepositoryService {
    
    func querySports() -> [Sport] {
        let limitDate = Date(timeIntervalSinceNow: -1 * 60 * 60 * 3) // Время жизни - 3 часа
        let predicat = NSPredicate(format: "\(#keyPath(DBSport.updateDate)) >= %@", limitDate as NSDate)
        guard let entries = try? DatabaseManager.getObjects(DBSport.self, filter: predicat) else { return [] }
        
        let translator = SportTranslator()
        let entities: [Sport] = entries
            .sorted(byKeyPath: #keyPath(DBSport.displaySequence), ascending: true)
            .map { translator.toEntity($0) }
    
        return entities
    }
    
    func saveSports(_ sports: [Sport]) {
        try? DatabaseManager.removeObjects(DBSport.self)
        
        if !sports.isEmpty {
            let translator = SportTranslator()
            let entries: [DBSport] = sports.enumerated().map { index, sport in
                let entry = translator.toEntry(sport)
                entry.displaySequence = index
                
                return entry
            }
            
            try? DatabaseManager.write(entries)
        }
    }
    
}
