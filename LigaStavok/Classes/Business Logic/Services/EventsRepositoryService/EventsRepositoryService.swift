//
//  EventsRepositoryService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для работы с событиями в локальной бд
protocol EventsRepositoryService: class {
    
    /// Запрос списка видов спорта
    func querySports() -> [Sport]
    
    /// Запись списка видов спорта
    func saveSports(_ sports: [Sport])
    
}
