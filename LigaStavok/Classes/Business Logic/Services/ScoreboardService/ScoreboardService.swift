//
//  ScoreboardService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис ???
protocol ScoreboardService: class {
    
    /// Выборка росписей c видео и виджетом Betradar’а
    func obtainActionLineVideo(completionHandler completion: @escaping (WebService.ResultContainer<[EventObject]>) -> Void) -> Operation
    
}
