//
//  ScoreboardServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис ???
final class ScoreboardServiceImplementation: WebService { }


// MARK: - ScoreboardService

extension ScoreboardServiceImplementation: ScoreboardService {
    
    func obtainActionLineVideo(completionHandler completion: @escaping (WebService.ResultContainer<[EventObject]>) -> Void) -> Operation {
        let requestCompletion: WebTransportOperation.WebTransportOperationCompletionBlock = { [weak self] operation, requestResult in
            guard let `self` = self else { return }
            
            guard self.validateRequestResult(requestResult, completionHandler: completion) else { return }
            guard let model: EventsListModel = self.parseRequestResult(requestResult,
                                                                       dateDecodingStrategy: .millisecondsSince1970,
                                                                       completionHandler: completion) else { return }
            
            let entities = model.entities
            
            guard !operation.isCancelled else { return }
            
            self.dispatchSuccess(completionHandler: completion, value: entities)
        }
        
        let address = constructAdress(with: .rest, namespace: .scoreboard, version: .v1)
        let parameters = ScoreboardParameters.post(baseUrl: self.environment.baseUrl.absoluteString,
                                                   address: address,
                                                   endpoint: API.Endpoint.Scoreboard.actionLineVideo.rawValue,
                                                   timeout: 60,
                                                   consoleOutputEnabled: false)
        
        let transport = WebTransport()
        let operation = WebTransportOperation(transport: transport,
                                              parameters: parameters,
                                              requestCompletion: requestCompletion)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
}
