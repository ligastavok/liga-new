//
//  BroadcastService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для получения публичной информации
protocol BroadcastService: class {
    
    // MARK: Web
    
    /// Получение списка всех счетчиков по событиям
    func obtainCounters(completionHandler completion: @escaping (WebService.ResultContainer<Counters>) -> Void)
    
    
    // MARK: Socket
    
    /// Подписаться на обновления счетчиков всех событий
    func subscribeToCountersUpdates(onMessage: @escaping (Counters) -> Void)
    
    /// Подписаться на добавление новых событий в линию
    func subscribeToNewEvents(onMessage: @escaping (EventObject) -> Void) -> Operation
    
    /// Подписаться на удаление событий из линии
    func subscribeToRemoveEvents(onMessage: @escaping (RemovedEvent) -> Void) -> Operation
    
}
