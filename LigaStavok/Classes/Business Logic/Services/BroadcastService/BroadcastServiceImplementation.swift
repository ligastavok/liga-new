//
//  BroadcastServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для получения публичной информации
final class BroadcastServiceImplementation {
    
    // MARK: - Приватные свойства
    
    /// Сервис для получения публичной информации через web-интерфейс
    private let broadcastWebService: BroadcastWebService
    /// Сервис для получения публичной информации через socket-интерфейс
    private let broadcastSocketService: BroadcastSocketService
    
    
    // MARK: - Инициализация
    
    init(broadcastWebService: BroadcastWebService,
         broadcastSocketService: BroadcastSocketService) {
        
        self.broadcastWebService = broadcastWebService
        self.broadcastSocketService = broadcastSocketService
    }
    
}


// MARK: - BroadcastService

extension BroadcastServiceImplementation: BroadcastService {
    
    // MARK: Web
    
    func obtainCounters(completionHandler completion: @escaping (WebService.ResultContainer<Counters>) -> Void) {
        self.broadcastWebService.obtainCounters(completionHandler: completion)
    }
    
    
    // MARK: Socket
    
    func subscribeToCountersUpdates(onMessage: @escaping (Counters) -> Void) {
        self.broadcastSocketService.subscribeToCountersUpdates(onMessage: onMessage)
    }
    
    func subscribeToNewEvents(onMessage: @escaping (EventObject) -> Void) -> Operation {
        return self.broadcastSocketService.subscribeToNewEvents(onMessage: onMessage)
    }
    
    func subscribeToRemoveEvents(onMessage: @escaping (RemovedEvent) -> Void) -> Operation {
        return self.broadcastSocketService.subscribeToRemoveEvents(onMessage: onMessage)
    }
    
}
