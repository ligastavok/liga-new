//
//  EventsWebService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для работы с событиями через web-интерфейс
protocol EventsWebService: class {
    
    /// Получение списка событий
    func eventsList(gameId: Int?,
                    eventIds: [Int]?,
                    categoryIds: [Int]?,
                    tournamentIds: [Int]?,
                    topicId: [Int]?,
                    period: Int?,
                    limit: Int?,
                    namespace: Namespace?,
                    matchDay: Int?,
                    fullEvents: Bool?,
                    topEvents: Bool?,
                    completionHandler completion: @escaping (WebService.ResultContainer<[EventObject]>) -> Void) -> Operation
    
    /// Получение списка событий с исходными JSON
    func eventsListWithJSONs(gameId: Int?,
                             eventIds: [Int]?,
                             categoryIds: [Int]?,
                             tournamentIds: [Int]?,
                             topicId: [Int]?,
                             period: Int?,
                             limit: Int?,
                             namespace: Namespace?,
                             matchDay: Int?,
                             fullEvents: Bool?,
                             topEvents: Bool?,
                             completionHandler completion: @escaping (WebService.ResultContainer<[EventObjectWithJSON]>) -> Void) -> Operation
    
    /// Запрос списка видов спорта
    func sportsList(namespace: Namespace?,
                    period: Int?,
                    completionHandler completion: @escaping (WebService.ResultContainer<[Sport]>) -> Void) -> Operation
    
    /// Запрос списка топиков по виду спорта
    func topicsList(gameId: Int?,
                    period: Int?,
                    namespace: Namespace?,
                    completionHandler completion: @escaping (WebService.ResultContainer<[Topic]>) -> Void) -> Operation
    
    /// Запрос списка избранных событий
    func favoritesList(gameIds: [Int]?,
                       eventIds: [Int]?,
                       topicIds: [Int]?,
                       limit: Int?,
                       completionHandler completion: @escaping (WebService.ResultContainer<[Favorite]>) -> Void) -> Operation
    
    /// Получение дерева видов спорта
    func getSportsTree(completionHandler completion: @escaping (WebService.ResultContainer<SportTree>) -> Void) -> Operation

}
