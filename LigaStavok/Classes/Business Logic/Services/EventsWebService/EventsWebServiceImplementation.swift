//
//  EventsWebServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для работы с событиями через web-интерфейс
final class EventsWebServiceImplementation: WebService {
    
    // MARK: - Приватные свойства
    
    /// Сервис для работы с событиями в локальной бд
    private let eventsRepositoryService: EventsRepositoryService
    
    
    // MARK: - Инициализация
    
    init(operationQueue: OperationQueue, environment: Environment, eventsRepositoryService: EventsRepositoryService) {
        self.eventsRepositoryService = eventsRepositoryService
        super.init(operationQueue: operationQueue, environment: environment)
    }
    
}


// MARK: - EventsWebService

extension EventsWebServiceImplementation: EventsWebService {
    
    func eventsList(gameId: Int?,
                    eventIds: [Int]?,
                    categoryIds: [Int]?,
                    tournamentIds: [Int]?,
                    topicId: [Int]?,
                    period: Int?,
                    limit: Int?,
                    namespace: Namespace?,
                    matchDay: Int?,
                    fullEvents: Bool?,
                    topEvents: Bool?,
                    completionHandler completion: @escaping (ResultContainer<[EventObject]>) -> Void) -> Operation {
        
        let requestCompletion: WebTransportOperation.WebTransportOperationCompletionBlock = { [weak self] operation, requestResult in
            guard let `self` = self else { return }
            
            guard self.validateRequestResult(requestResult, completionHandler: completion) else { return }
            guard let model: EventsListModel = self.parseRequestResult(requestResult,
                                                                       dateDecodingStrategy: .millisecondsSince1970,
                                                                       completionHandler: completion) else { return }
            
            let entities = model.entities
            
            guard !operation.isCancelled else { return }
            
            self.dispatchSuccess(completionHandler: completion, value: entities)
        }
        
        let address = constructAdress(with: .rest, namespace: .events, version: .v2)
        let parameters = EventsParameters.post(baseUrl: self.environment.baseUrl.absoluteString,
                                               address: address,
                                               endpoint: API.Endpoint.Events.eventsList.rawValue,
                                               consoleOutputEnabled: false)
        if let gameId = gameId {
            parameters.with(gameId: gameId)
        }
        if let eventIds = eventIds {
            parameters.with(eventIds: eventIds)
        }
        if let categoryIds = categoryIds {
            parameters.with(categoryIds: categoryIds)
        }
        if let tournamentIds = tournamentIds {
            parameters.with(tournamentIds: tournamentIds)
        }
        if let topicId = topicId {
            parameters.with(topicId: topicId)
        }
        if let period = period {
            parameters.with(period: period)
        }
        if let limit = limit {
            parameters.with(limit: limit)
        }
        if let namespace = namespace {
            parameters.with(namespace: namespace)
        }
        if let matchDay = matchDay {
            parameters.with(matchDay: matchDay)
        }
        if let fullEvents = fullEvents {
            parameters.with(fullEvents: fullEvents)
        }
        if let topEvents = topEvents {
            parameters.with(topEvents: topEvents)
        }
        
        let transport = WebTransport()
        let operation = WebTransportOperation(transport: transport,
                                              parameters: parameters,
                                              requestCompletion: requestCompletion)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
    func eventsListWithJSONs(gameId: Int?,
                             eventIds: [Int]?,
                             categoryIds: [Int]?,
                             tournamentIds: [Int]?,
                             topicId: [Int]?,
                             period: Int?,
                             limit: Int?,
                             namespace: Namespace?,
                             matchDay: Int?,
                             fullEvents: Bool?,
                             topEvents: Bool?,
                             completionHandler completion: @escaping (ResultContainer<[EventObjectWithJSON]>) -> Void) -> Operation {
        
        let requestCompletion: WebTransportOperation.WebTransportOperationCompletionBlock = { [weak self] operation, requestResult in
            guard let `self` = self else { return }
            
            guard self.validateRequestResult(requestResult, completionHandler: completion) else { return }
            guard let model: EventsListWithJSONModel = self.parseRequestResult(requestResult,
                                                                       dateDecodingStrategy: .millisecondsSince1970,
                                                                       completionHandler: completion) else { return }
            
            guard let entities = model.generateEntities(with: requestResult) else { return }
            
            guard !operation.isCancelled else { return }
            
            self.dispatchSuccess(completionHandler: completion, value: entities)
        }
        
        let address = constructAdress(with: .rest, namespace: .events, version: .v2)
        let parameters = EventsParameters.post(baseUrl: self.environment.baseUrl.absoluteString,
                                               address: address,
                                               endpoint: API.Endpoint.Events.eventsList.rawValue,
                                               consoleOutputEnabled: false)
        if let gameId = gameId {
            parameters.with(gameId: gameId)
        }
        if let eventIds = eventIds {
            parameters.with(eventIds: eventIds)
        }
        if let categoryIds = categoryIds {
            parameters.with(categoryIds: categoryIds)
        }
        if let tournamentIds = tournamentIds {
            parameters.with(tournamentIds: tournamentIds)
        }
        if let topicId = topicId {
            parameters.with(topicId: topicId)
        }
        if let period = period {
            parameters.with(period: period)
        }
        if let limit = limit {
            parameters.with(limit: limit)
        }
        if let namespace = namespace {
            parameters.with(namespace: namespace)
        }
        if let matchDay = matchDay {
            parameters.with(matchDay: matchDay)
        }
        if let fullEvents = fullEvents {
            parameters.with(fullEvents: fullEvents)
        }
        if let topEvents = topEvents {
            parameters.with(topEvents: topEvents)
        }
        
        let transport = WebTransport()
        let operation = WebTransportOperation(transport: transport,
                                              parameters: parameters,
                                              requestCompletion: requestCompletion)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
    func sportsList(namespace: Namespace?,
                    period: Int?,
                    completionHandler completion: @escaping (ResultContainer<[Sport]>) -> Void) -> Operation {
        
        let requestCompletion: WebTransportOperation.WebTransportOperationCompletionBlock = { [weak self] operation, requestResult in
            guard let `self` = self else { return }
            
            guard self.validateRequestResult(requestResult, completionHandler: completion) else { return }
            guard let model: SportsListModel = self.parseRequestResult(requestResult,
                                                                       completionHandler: completion) else { return }
            
            let entities = model.entities
            self.eventsRepositoryService.saveSports(entities)
            
            guard !operation.isCancelled else { return }
            
            self.dispatchSuccess(completionHandler: completion, value: entities)
        }
        
        let address = constructAdress(with: .rest, namespace: .events, version: .v1)
        let parameters = EventsParameters.post(baseUrl: self.environment.baseUrl.absoluteString,
                                               address: address,
                                               endpoint: API.Endpoint.Events.sportsList.rawValue)
        if let namespace = namespace {
            parameters.with(namespace: namespace)
        }
        if let period = period {
            parameters.with(period: period)
        }
        
        let transport = WebTransport()
        let operation = WebTransportOperation(transport: transport,
                                              parameters: parameters,
                                              requestCompletion: requestCompletion)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
    func topicsList(gameId: Int?,
                    period: Int?,
                    namespace: Namespace?,
                    completionHandler completion: @escaping (ResultContainer<[Topic]>) -> Void) -> Operation {
        
        let requestCompletion: WebTransportOperation.WebTransportOperationCompletionBlock = { [weak self] operation, requestResult in
            guard let `self` = self else { return }
            
            guard self.validateRequestResult(requestResult, completionHandler: completion) else { return }
            guard let model: TopicsListModel = self.parseRequestResult(requestResult,
                                                                       completionHandler: completion) else { return }
            
            let entities = model.entities
            
            guard !operation.isCancelled else { return }
            
            self.dispatchSuccess(completionHandler: completion, value: entities)
        }
        
        let address = constructAdress(with: .rest, namespace: .events, version: .v1)
        let parameters = EventsParameters.post(baseUrl: self.environment.baseUrl.absoluteString,
                                               address: address,
                                               endpoint: API.Endpoint.Events.topicsList.rawValue)
        if let gameId = gameId {
            parameters.with(gameId: gameId)
        }
        if let period = period {
            parameters.with(period: period)
        }
        if let namespace = namespace {
            parameters.with(namespace: namespace)
        }
        
        let transport = WebTransport()
        let operation = WebTransportOperation(transport: transport,
                                              parameters: parameters,
                                              requestCompletion: requestCompletion)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
    func favoritesList(gameIds: [Int]?,
                       eventIds: [Int]?,
                       topicIds: [Int]?,
                       limit: Int?,
                       completionHandler completion: @escaping (ResultContainer<[Favorite]>) -> Void) -> Operation {
        
        let requestCompletion: WebTransportOperation.WebTransportOperationCompletionBlock = { [weak self] operation, requestResult in
            guard let `self` = self else { return }
            
            guard self.validateRequestResult(requestResult, completionHandler: completion) else { return }
            guard let model: FavoritesListModel = self.parseRequestResult(requestResult,
                                                                          dateDecodingStrategy: .millisecondsSince1970,
                                                                          completionHandler: completion) else { return }
            
            let entities = model.entities
            
            guard !operation.isCancelled else { return }
            
            self.dispatchSuccess(completionHandler: completion, value: entities)
        }
        
        let address = constructAdress(with: .rest, namespace: .events, version: .v2)
        let parameters = EventsParameters.post(baseUrl: self.environment.baseUrl.absoluteString,
                                               address: address,
                                               endpoint: API.Endpoint.Events.favorites.rawValue,
                                               consoleOutputEnabled: false)
        if let gameIds = gameIds {
            parameters.with(gameIds: gameIds)
        }
        if let eventIds = eventIds {
            parameters.with(eventIds: eventIds)
        }
        if let topicIds = topicIds {
            parameters.with(topicIds: topicIds)
        }
        if let limit = limit {
            parameters.with(limit: limit)
        }
        
        let transport = WebTransport()
        let operation = WebTransportOperation(transport: transport,
                                              parameters: parameters,
                                              requestCompletion: requestCompletion)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
    func getSportsTree(completionHandler completion: @escaping (WebService.ResultContainer<SportTree>) -> Void) -> Operation {
        let requestCompletion: WebTransportOperation.WebTransportOperationCompletionBlock = { [weak self] operation, requestResult in
            guard let `self` = self else { return }
            
            guard self.validateRequestResult(requestResult, completionHandler: completion) else { return }
            guard let model: SportTreeModel = self.parseRequestResult(requestResult,
                                                                      completionHandler: completion) else { return }
            
            let entity = model.entity
            
            guard !operation.isCancelled else { return }
            
            self.dispatchSuccess(completionHandler: completion, value: entity)
        }
        
        let address = constructAdress(with: .rest, namespace: .events, version: .v2)
        let parameters = EventsParameters.post(baseUrl: self.environment.baseUrl.absoluteString,
                                               address: address,
                                               endpoint: API.Endpoint.Events.getSportsTree.rawValue)
        
        let transport = WebTransport()
        let operation = WebTransportOperation(transport: transport,
                                              parameters: parameters,
                                              requestCompletion: requestCompletion)
        
        self.operationQueue.addOperation(operation)
        return operation
    }

}
