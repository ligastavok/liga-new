//
//  BroadcastSocketServiceImplementation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для получения публичной информации через web-интерфейс
final class BroadcastSocketServiceImplementation: SocketService { }


// MARK: - BroadcastSocketService

extension BroadcastSocketServiceImplementation: BroadcastSocketService {
    
    func subscribeToCountersUpdates(onMessage: @escaping (Counters) -> Void) {
        let namespace = constructNamespace(with: .broadcast, version: .v1)
        let parameters = SocketIOParameters(url: self.environment.socketUrl, namespace: namespace, log: true)
        let socket = SocketIO(parameters: parameters)
        
        socket.on(clientEvent: .connect) { _ in
            print("socket connected")
        }
        
        socket.on(clientEvent: .disconnect) { data in
            print("socket disconnect \(data)")
        }
        
        socket.on(clientEvent: .error) { data in
            print("error \(data)")
        }
        
        socket.on(API.MessageType.Broadcast.counters.rawValue) { data in
            guard let array = data as? [[String: Any]],
                let value = array.first  else { return }
            
            let jsonDecoder = JSONDecoder()
            guard let data = try? JSONSerialization.data(withJSONObject: value, options: []),
                let model = try? jsonDecoder.decode(CountersMapModel.self, from: data) else { return }
            
            let entity = model.entity
            onMessage(entity)
        }
        
        let operation = SocketIOOperation(socket: socket)
        self.operationQueue.addOperation(operation)
    }
    
    func subscribeToNewEvents(onMessage: @escaping (EventObject) -> Void) -> Operation {
        let namespace = constructNamespace(with: .broadcast, version: .v1)
        let parameters = SocketIOParameters(url: self.environment.socketUrl, namespace: namespace, log: true)
        let socket = SocketIO(parameters: parameters)
        
        socket.on(clientEvent: .connect) { _ in
            print("socket connected")
        }
        
        socket.on(clientEvent: .disconnect) { data in
            print("socket disconnect \(data)")
        }
        
        socket.on(clientEvent: .error) { data in
            print("error \(data)")
        }
        
        socket.on(API.MessageType.Broadcast.new.rawValue) { data in
            guard let array = data as? [[String: Any]],
                let value = array.first  else { return }
            
            let jsonDecoder = JSONDecoder()
            guard let data = try? JSONSerialization.data(withJSONObject: value, options: []),
                let model = try? jsonDecoder.decode(NewEventModel.self, from: data) else { return }
            
            let entity = model.entity
            onMessage(entity)
        }
        
        let operation = SocketIOOperation(socket: socket)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
    func subscribeToRemoveEvents(onMessage: @escaping (RemovedEvent) -> Void) -> Operation {
        let namespace = constructNamespace(with: .broadcast, version: .v1)
        let parameters = SocketIOParameters(url: self.environment.socketUrl, namespace: namespace, log: true)
        let socket = SocketIO(parameters: parameters)
        
        socket.on(clientEvent: .connect) { _ in
            print("socket connected")
        }
        
        socket.on(clientEvent: .disconnect) { data in
            print("socket disconnect \(data)")
        }
        
        socket.on(clientEvent: .error) { data in
            print("error \(data)")
        }
        
        socket.on(API.MessageType.Broadcast.remove.rawValue) { data in
            guard let array = data as? [[String: Any]],
                let value = array.first  else { return }
            
            let jsonDecoder = JSONDecoder()
            guard let data = try? JSONSerialization.data(withJSONObject: value, options: []),
                let model = try? jsonDecoder.decode(RemovedEventModel.self, from: data) else { return }

            let entity = model.entity
            onMessage(entity)
        }
        
        let operation = SocketIOOperation(socket: socket)
        
        self.operationQueue.addOperation(operation)
        return operation
    }
    
}
