//
//  BroadcastSocketService.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 04.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Сервис для получения публичной информации через web-интерфейс
protocol BroadcastSocketService: class {
    
    /// Подписаться на обновления счетчиков всех событий
    func subscribeToCountersUpdates(onMessage: @escaping (Counters) -> Void)
    
    /// Подписаться на добавление новых событий в линию
    func subscribeToNewEvents(onMessage: @escaping (EventObject) -> Void) -> Operation
    
    /// Подписаться на удаление событий из линии
    func subscribeToRemoveEvents(onMessage: @escaping (RemovedEvent) -> Void) -> Operation
    
}
