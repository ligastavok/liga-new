//
//  MigrationManager.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

final class MigrationManager {
    
    // MARK: - Приватные свойства
    
    private static let userDefaults: UserDefaults = .standard
    
}


// MARK: - Приватные свойства

private extension MigrationManager {
    
    /// Версия приложения
    static var applicationVersion: Int {
        get {
            return self.userDefaults.integer(forKey: SettingsKeys.applicationVersion)
        }
        set {
            self.userDefaults.set(newValue, forKey: SettingsKeys.applicationVersion)
        }
    }
    
}


// MARK: - Публичные методы

extension MigrationManager {
    
    /// Настроить значения по умолчанию
    static func configureDefaults(defaultVersion: Int) {
        let defaults: [String: Any] = [
            SettingsKeys.applicationVersion: defaultVersion
        ]
        
        self.userDefaults.register(defaults: defaults)
    }
    
    /// Присвоить приложению версию
    static func assignApplicationVersion(_ version: Int) {
        let currentVersion = self.applicationVersion
        guard version > currentVersion else { return }
        
        if currentVersion < 1 {
            // Код миграции
        }
        
        self.applicationVersion = version
    }
    
}
