//
//  MigrationManager+SettingsKeys.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension MigrationManager {
    
    /// Ключи настроек
    enum SettingsKeys {
        
        /// Название сервиса
        private static let service = String(describing: MigrationManager.self)
        
        /// Версия приложения
        static let applicationVersion = service + ".application-version"
        
    }
    
}
