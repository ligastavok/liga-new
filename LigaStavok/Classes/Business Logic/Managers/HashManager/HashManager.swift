//
//  HashManager.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 10.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class HashManager {
    
    // MARK: - Инициализация
    
    private init() { }
    
}


// MARK: - Публичные методы

extension HashManager {
    
    /// Комбинация хешей
    class func combineHashes(_ hashes: [Int]) -> Int {
        return hashes.reduce(0, HashManager.combineHashValues)
    }
    
    /// Хеш массива
    class func hashArray<T: Hashable>(_ array: [T]?) -> Int {
        guard let array = array else { return 0 }
        return array.reduce(5381) { ($0 << 5) &+ $0 &+ $1.hashValue }
    }
    
    /// Хеш словаря
    class func hashDictionary<T, U: Hashable>(_ dictionary: [T: U]?) -> Int {
        guard let dictionary = dictionary else { return 0 }
        return dictionary.reduce(5381) { combineHashValues($0, combineHashValues($1.key.hashValue, $1.value.hashValue)) }
    }
    
}


// MARK: - Приватные методы

private extension HashManager {
    
    /// Выполнить комбинацию хешей
    class func combineHashValues(_ initial: Int, _ other: Int) -> Int {
        #if arch(x86_64) || arch(arm64)
        let magic: UInt = 0x9e3779b97f4a7c15
        #elseif arch(i386) || arch(arm)
        let magic: UInt = 0x9e3779b9
        #endif
        
        var lhs = UInt(bitPattern: initial)
        let rhs = UInt(bitPattern: other)
        lhs ^= rhs &+ magic &+ (lhs << 6) &+ (lhs >> 2)
        
        return Int(bitPattern: lhs)
    }
    
}
