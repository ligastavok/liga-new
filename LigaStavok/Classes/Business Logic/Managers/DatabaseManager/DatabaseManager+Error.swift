//
//  DatabaseManager+Error.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

extension DatabaseManager {
    
    enum Error: Swift.Error {
        
        /// Завершено с ошибкой
        case failed(message: String)
        
    }
    
}
