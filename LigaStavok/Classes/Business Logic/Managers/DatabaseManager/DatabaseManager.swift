//
//  DatabaseManager.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2017 MobSolutions. All rights reserved.
//

import RealmSwift

final class DatabaseManager {
    
    // MARK: - Приватные свойства
    
    /// Ошибка при получении экземпляра базы данных
    private static let realmError: Error = .failed(message: "Ошибка при доступе к базе данных.")
    
}


// MARK: - Публичные свойства

extension DatabaseManager {
    
    /// URL базы данных
    static var databaseUrl: URL? {
        return Realm.Configuration.defaultConfiguration.fileURL
    }
    
}


// MARK: - Приватные свойства

private extension DatabaseManager {
    
    /// Получить экземпляр Realm
    static var realm: Realm? {
        return try? Realm()
    }
    
}


// MARK: - Публичные методы

extension DatabaseManager {
    
    // MARK: Настройка
    
    /// Присвоить базе данных версию
    static func assignDatabaseVersion(_ version: UInt64) {
        
        // Миграция базы данных
        let migrationBlock: MigrationBlock = { migration, oldSchemaVersion in
            if oldSchemaVersion < 1 {
                // Код миграции
            }
        }
        
        Realm.Configuration.defaultConfiguration = Realm.Configuration(schemaVersion: version,
                                                                       migrationBlock: migrationBlock)
    }
    
    
    // MARK: Запись данных
    
    /// Создать или обновить объект
    static func write<T: Object>(_ object: T, update: Bool = false) throws {
        try write([object], update: update)
    }
    
    /// Создать или обновить объекты
    static func write<T: Object>(_ objects: [T], update: Bool = false) throws {
        guard let realm = self.realm else { throw self.realmError }
        
        do {
            try realm.write {
                objects.forEach { realm.add($0, update: update) }
            }
        } catch {
            throw self.realmError
        }
    }
    
    /// Создать или обновить объекты
    static func write<T: Object>(_ objects: List<T>, update: Bool = false) throws {
        guard let realm = self.realm else { throw self.realmError }
        
        do {
            try realm.write {
                objects.forEach { realm.add($0, update: update) }
            }
        } catch {
            throw self.realmError
        }
    }
    
    /// Создать или обновить объекты
    static func write<T: Object>(_ objects: Results<T>, update: Bool = false) throws {
        guard let realm = self.realm else { throw self.realmError }
        
        do {
            try realm.write {
                objects.forEach { realm.add($0, update: update) }
            }
        } catch {
            throw self.realmError
        }
    }
    
    /// Транзакция записи
    static func writeTransaction(_ block: (_ realm: Realm) -> Void) throws {
        guard let realm = self.realm else { throw self.realmError }
        
        do {
            try realm.write {
                block(realm)
            }
        } catch {
            throw self.realmError
        }
    }
    
    /// Операция добавления объекта в базу данных
    static func addOperation<T: Object>(_ object: T, update: Bool = false, in realm: Realm) {
        realm.add(object, update: update)
    }
    
    /// Операция добавления объектов в базу данных
    static func addOperation<T: Object>(_ objects: Results<T>, update: Bool = false, in realm: Realm) {
        objects.forEach { realm.add($0, update: update) }
    }
    
    
    // MARK: Запрос данных
    
    /// Получить объект из базы данных
    static func getObject<T: Object>(_ type: T.Type, filter predicate: NSPredicate? = nil) throws -> T? {
        let objects = try getObjects(type, filter: predicate)
        
        let object = objects.first
        return object
    }
    
    /// Получить объекты из базы данных
    static func getObjects<T: Object>(_ type: T.Type, filter predicate: NSPredicate? = nil) throws -> Results<T> {
        guard let realm = self.realm else { throw self.realmError }
        
        let objects: Results<T>
        if let predicate = predicate {
            objects = realm.objects(type).filter(predicate)
        } else {
            objects = realm.objects(type)
        }
        
        return objects
    }
    
    /// Получить массив объектов из базы данных
    static func getArrayOfObjects<T: Object>(_ type: T.Type, filter predicate: NSPredicate? = nil) throws -> [T] {
        let objects = try getObjects(type, filter: predicate)
        return Array(objects)
    }
    
    
    // MARK: Удаление данных
    
    /// Удалить все данные
    static func removeAll() throws {
        guard let realm = self.realm else { throw self.realmError }
        
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            throw self.realmError
        }
    }
    
    /// Удалить объект
    static func remove<T: Object>(_ object: T) throws {
        try remove([object])
    }
    
    /// Удалить объекты
    static func remove<T: Object>(_ objects: [T]) throws {
        guard let realm = self.realm else { throw self.realmError }
        
        do {
            try realm.write {
                realm.delete(objects)
            }
        } catch {
            throw self.realmError
        }
    }
    
    /// Удалить объекты
    static func remove<T: Object>(_ objects: List<T>) throws {
        guard let realm = self.realm else { throw self.realmError }
        
        do {
            try realm.write {
                realm.delete(objects)
            }
        } catch {
            throw self.realmError
        }
    }
    
    /// Удалить объекты
    static func remove<T: Object>(_ objects: Results<T>) throws {
        guard let realm = self.realm else { throw self.realmError }
        
        do {
            try realm.write {
                realm.delete(objects)
            }
        } catch {
            throw self.realmError
        }
    }
    
    /// Удалить объект
    static func removeObject<T: Object>(_ type: T.Type, filter predicate: NSPredicate) throws {
        if let object = try getObject(type, filter: predicate) {
            try remove(object)
        }
    }
    
    /// Удалить объекты
    static func removeObjects<T: Object>(_ type: T.Type, filter predicate: NSPredicate? = nil) throws {
        let objects = try getObjects(type, filter: predicate)
        try remove(objects)
    }
    
}
