//
//  PatchEventsViewModelsOperation+Path.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 06.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension PatchEventsViewModelsOperation {
    
    /// Пути
    enum Path: String {
        
        /// Признак завершенного события
        case expired = "/event/expired"
        /// Счет первой команды
        case firstScore = "/scores/current/ScoreTeam1"
        /// Счет второй команды
        case secondScore = "/scores/current/ScoreTeam2"
        /// Флаг подтверждающий, что событие содержит незаблокированные исходы
        case hasUnlocked = "/hasUnlocked"
        /// Коэффициент на победу первой команды (fulltime)
        case firstWinnerBetValueMain = "/outcomesWinner/main/outcomes/_1/value"
        /// Коэффициент на победу первой команды (over all time)
        case firstWinnerBetValueOverAllTime = "/outcomesWinner/ot/outcomes/_1/value"
        /// Коэффициент на ничью (fulltime)
        case drawBetValueMain = "/outcomesWinner/main/outcomes/x/value"
        /// Коэффициент на ничью (over all time)
        case drawBetValueOverAllTime = "/outcomesWinner/ot/outcomes/x/value"
        /// Коэффициент на победу второй команды (fulltime)
        case secondWinnerBetValueMain = "/outcomesWinner/main/outcomes/_2/value"
        /// Коэффициент на победу второй команды (over all time)
        case secondWinnerBetValueOverAllTime = "/outcomesWinner/ot/outcomes/_2/value"
        
    }
    
}


// MARK: - EnumCollection

extension PatchEventsViewModelsOperation.Path: EnumCollection { }
