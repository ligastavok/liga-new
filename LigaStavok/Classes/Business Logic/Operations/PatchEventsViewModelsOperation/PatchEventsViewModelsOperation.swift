//
//  PatchEventsViewModelsOperation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

final class PatchEventsViewModelsOperation: Operation {
    
    // MARK: - Публичные свойства
    
    /// Состояние
    var isViewModelsChanged = false
    
    
    // MARK: - Приватные свойства
    
    /// Изменения
    private let updates: [DiffMapModel]
    /// События
    private var viewModels: SynchronizedValue<[EventViewModel]>
    
    /// Пути
    private lazy var paths = Path.allValues
    
    
    // MARK: - Инициализация
    
    init(updates: [DiffMapModel] = [], viewModels: SynchronizedValue<[EventViewModel]>) {
        self.updates = updates
        self.viewModels = viewModels
        
        super.init()
        
        self.qualityOfService = .utility
    }
    
}


// MARK: - Operation

extension PatchEventsViewModelsOperation {
    
    override func main() {
        guard !self.isCancelled else { return }
        
        for update in self.updates {
            guard !self.isCancelled else { return }
            
            // Получение изменяемой модели
            var viewModel: EventViewModel! = nil
            self.viewModels.syncSet { viewModels in
                guard let index = viewModels.index(where: { $0.id == update.id }) else { return }
                viewModel = viewModels[index]
            }
            
            guard viewModel != nil else { continue }
            guard !self.isCancelled else { return }
            
            // Применение изменений
            var isChanged = false
            
            applyPatches(update.data.headers, for: &viewModel!, changed: &isChanged)
            
            guard !self.isCancelled else { return }
            
            applyPatches(update.data.outcomes, for: &viewModel!, changed: &isChanged)
            
            guard isChanged else { continue }
            guard !self.isCancelled else { return }
            
            // Запись измененной модели
            self.viewModels.syncSet { viewModels in
                guard let index = viewModels.index(where: { $0.id == update.id }) else { return }
                viewModels[index] = viewModel
            }
            
            // Установка отметки об изменении моделей
            if !self.isViewModelsChanged {
                self.isViewModelsChanged = true
            }
        }
    }
    
}


// MARK: - Приватные методы

private extension PatchEventsViewModelsOperation {
    
    /// Применить изменения к модели
    func applyPatches(_ patches: [DiffModel], for viewModel: inout EventViewModel, changed isChanged: inout Bool) {
        guard !self.isCancelled else { return }
        
        // Применение каждого изменения
        for patch in patches {
            guard !self.isCancelled else { return }
            
            // Проверка каждого пути
            for path in self.paths {
                guard !self.isCancelled else { return }
                
                // Получение нового значения
                var newValue: JSON? = nil
                if path.rawValue == patch.path {
                    if patch.op != "remove" {
                        guard let value = patch.value else { continue }
                        newValue = value
                    }
                } else if path.rawValue.hasPrefix(patch.path) {
                    if patch.op != "remove" {
                        guard var value = patch.value else { continue }
                        
                        let croppedPath = path.rawValue.deletingPrefix(patch.path)
                        var components = croppedPath.components(separatedBy: "/")
                        
                        if let firstComponent = components.first, firstComponent == .empty {
                            components.removeFirst()
                        }
                        
                        var isValid = true
                        while !components.isEmpty {
                            let component = components.removeFirst()
                            value = value[component]
                            
                            guard value.exists() else {
                                isValid = false
                                break
                            }
                        }
                        
                        guard isValid else { continue }
                        newValue = value
                    }
                } else {
                    continue
                }
                
                // Установка нового значения
                setValue(newValue, by: path, in: &viewModel)
                
                // Установка отметки об изменении модели
                if !isChanged {
                    isChanged = true
                }
            }
        }
    }
    
    /// Установить значение в модели
    func setValue(_ value: JSON?, by path: Path, in viewModel: inout EventViewModel) {
        switch path {
        case .expired:
            let value = value?.bool ?? false
            viewModel.expired = value
        case .firstScore:
            let value = value?.string ?? "0"
            viewModel.firstScore = value
        case .secondScore:
            let value = value?.string ?? "0"
            viewModel.secondScore = value
        case .hasUnlocked:
            let value = value?.bool ?? true
            viewModel.isUnlocked = value
        case .firstWinnerBetValueMain:
            let value = value?.double ?? 0
            viewModel.recalculateFirstWinnerBetValue(firstWinnerBetValueMain: value)
        case .firstWinnerBetValueOverAllTime:
            let value = value?.double ?? 0
            viewModel.recalculateFirstWinnerBetValue(firstWinnerBetValueOverAllTime: value)
        case .drawBetValueMain:
            let value = value?.double ?? 0
            viewModel.recalculateDrawBetValue(drawBetValueMain: value)
        case .drawBetValueOverAllTime:
            let value = value?.double ?? 0
            viewModel.recalculateDrawBetValue(drawBetValueOverAllTime: value)
        case .secondWinnerBetValueMain:
            let value = value?.double ?? 0
            viewModel.recalculateSecondWinnerBetValue(secondWinnerBetValueMain: value)
        case .secondWinnerBetValueOverAllTime:
            let value = value?.double ?? 0
            viewModel.recalculateSecondWinnerBetValue(secondWinnerBetValueOverAllTime: value)
        }
    }
    
}
