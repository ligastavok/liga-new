//
//  PatchEventOperation.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 04.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

final class PatchEventsOperation: Operation {
    
    // MARK: - Приватные свойства
    
    private lazy var jsonDecoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .millisecondsSince1970
        
        return jsonDecoder
    }()
    
    /// События
    private var sourceEvent: SynchronizedValue<EventObjectWithJSON>
    /// Изменения
    private var updates: [DiffMapModel]
    
    /// Измененное событие
    private var patchedEvent: EventObjectWithJSON!
    
    
    // MARK: - Инициализация
    
    init(event: SynchronizedValue<EventObjectWithJSON>, updates: [DiffMapModel]) {
        self.sourceEvent = event
        self.updates = updates
        
        super.init()
        
        self.qualityOfService = .utility
    }
    
}


// MARK: - Operation

extension PatchEventsOperation {
    
    override func main() {
        guard !self.isCancelled else { return }
        
        for update in self.updates {
            guard !self.isCancelled else { return }
            
            // Получение JSON события
            var json: JSON! = nil
            self.sourceEvent.syncSet {event in 
                json = event.json
            }
            
            guard json != nil else { continue }
            guard !self.isCancelled else { return }
            
            // Применение патча к JSON
            var isChanged = false
            
            guard !self.isCancelled else { return }
            
            do {
                let jsonPatch = try JSONPatch(update.data.headers, allowsNilValue: true)
                json = try JSONPatcher.applyPatch(jsonPatch, to: json)
                
                if !isChanged {
                    isChanged = true
                }
            } catch { }
            
            guard !self.isCancelled else { return }
            
            do {
                let jsonPatch = try JSONPatch(update.data.outcomes, allowsNilValue: true)
                json = try JSONPatcher.applyPatch(jsonPatch, to: json)
                
                if !isChanged {
                    isChanged = true
                }
            } catch { }
            
            guard isChanged else { continue }
            guard !self.isCancelled else { return }
            
            // Запись изменений в объект
            guard let data = try? json.rawData() else { return }
            guard let model = try? self.jsonDecoder.decode(EventObjectModel.self, from: data) else { return }
            
            let event = model.entity
            let result = EventObjectWithJSON(eventObject: event, json: json)
            
            self.sourceEvent.syncSet { event in
                event = result
            }
            
            guard !self.isCancelled else { return }
            
            // Сохранение измененного события
            self.patchedEvent = result
        }
    }
    
}


// MARK: - PatchEventsDataProvider

extension PatchEventsOperation: PatchEventsDataProvider {
    
    var event: EventObjectWithJSON {
        return self.patchedEvent
    }
    
}
