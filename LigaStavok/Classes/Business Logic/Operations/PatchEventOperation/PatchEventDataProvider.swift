//
//  PatchEventDataProvider.swift
//  LigaStavok
//
//  Created by Барбэу Юрий Юрьевич on 04.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol PatchEventsDataProvider: class {
    
    /// Событие
    var event: EventObjectWithJSON { get }
    
}
