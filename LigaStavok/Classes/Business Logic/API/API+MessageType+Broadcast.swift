//
//  API+MessageType+Broadcast.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 05.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API.MessageType {
    
    enum Broadcast: String {
        
        /// Обновление счетчиков всех событий
        case counters
        /// Добавление новых событий в линию
        case new
        /// Удаление событий из линии
        case remove
        
    }
    
}
