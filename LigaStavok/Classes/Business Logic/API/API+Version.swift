//
//  API+Version.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API {
    
    /// Версия
    enum Version: String {
        
        /// Версия 1
        case v1
        /// Версия 2
        case v2
        
    }
    
}
