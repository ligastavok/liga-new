//
//  API+Endpoint.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API {
    
    /// Конечная точка
    final class Endpoint { }
    
    /// Тип сообщения
    final class MessageType { }
    
}
