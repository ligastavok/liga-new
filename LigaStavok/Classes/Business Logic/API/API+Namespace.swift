//
//  API+Namespace.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API {
    
    /// Пространство имен
    enum Namespace: String {
        
        /// Сервис для работы с событиями
        case events
        /// Сервис для поиска
        case search
        /// Сервис ???
        case scoreboard
        /// Сервис для получения публичной информации
        case broadcast
        /// Сервис для получения обновления по событиям
        case observer
        
    }
    
}
