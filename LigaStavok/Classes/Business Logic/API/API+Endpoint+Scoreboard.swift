//
//  API+Endpoint+Scoreboard.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API.Endpoint {
    
    enum Scoreboard: String {
        
        /// Выборка росписей c видео и виджетом Betradar'а
        case actionLineVideo
        
    }
    
}
