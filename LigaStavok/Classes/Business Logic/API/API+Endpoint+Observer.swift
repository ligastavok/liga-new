//
//  API+Endpoint+Observer.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 13.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API.Endpoint {
    
    enum Observer: String {
        
        /// Подписка на обновление событий по списку идентификаторов
        case observe
        /// Добавление к списку подписок событий
        case addObserve
        /// Удаление из подписок событий
        case removeObserve
        
    }
    
}
