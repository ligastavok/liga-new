//
//  API+Endpoint+Broadcast.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 23.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API.Endpoint {
    
    enum Broadcast: String {
        
        /// Получение списка всех счетчиков по событиям
        case getCounters
        
    }
    
}
