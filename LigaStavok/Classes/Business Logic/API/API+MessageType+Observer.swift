//
//  API+MessageType+Observer.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API.MessageType {
    
    enum Observer: String {
        
        /// Список обновленных полей событий
        case update
        
    }
    
}
