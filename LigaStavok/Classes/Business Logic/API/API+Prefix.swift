//
//  API+Prefix.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API {
    
    /// Префикс запроса
    enum Prefix: String {
        
        /// REST
        case rest
        /// Socket.IO
        case socketIO = "socket.io"
        
    }
    
}
