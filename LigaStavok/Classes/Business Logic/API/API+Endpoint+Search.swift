//
//  API+Endpoint+Search.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API.Endpoint {
    
    enum Search: String {
        
        /// Поиск событий
        case searchEvents
        
    }
    
}
