//
//  API+Endpoint+Events.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension API.Endpoint {
    
   enum Events: String {
    
        /// Получение списка событий
        case eventsList
        /// Список видов спорта
        case sportsList
        /// Список избранных событий
        case favorites
        /// Список топиков
        case topicsList
        /// Получение дерева видов спорта
        case getSportsTree
        
    }
    
}
