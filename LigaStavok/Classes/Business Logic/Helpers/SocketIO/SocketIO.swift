//
//  SocketIO.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SocketIO

// FIXME: - В библиотеке SocketIO есть баг, происходят утечки памяти

final class SocketIO {
    
    // MARK: - Приватные свойства
    
    private let socket: SocketIOClient
    
    
    // MARK: - Инициализация
    
    init(parameters: SocketIOParameters) {
        let config: SocketIOClientConfiguration = [
            .nsp(parameters.namespace),
            .log(parameters.log),
            .forcePolling(true)
        ]
        
        let socket = SocketIOClient(socketURL: parameters.url, config: config)
        self.socket = socket
    }
    
}


// MARK: - Публичные свойства

extension SocketIO {
    
    /// Состояние
    var isConnected: Bool {
        return self.socket.status == .connected
    }
    
}


// MARK: - Публичные методы

extension SocketIO {
    
    /// Добавить обработчика события
    func on(clientEvent: SocketIOClientEvent, callback: @escaping (Any) -> Void) {
        let socketClientEvent: SocketClientEvent
        switch clientEvent {
        case .connect:
            socketClientEvent = .connect
        case .disconnect:
            socketClientEvent = .disconnect
        case .error:
            socketClientEvent = .error
        }
        
        self.socket.on(clientEvent: socketClientEvent) { data, _ in
            callback(data)
        }
    }
    
    /// Добавить обработчика события
    func on(_ event: String, callback: @escaping (Any) -> Void) {
        self.socket.on(event) { data, _ in
            callback(data)
        }
    }
    
    /// Отправить сообщение
    func emit(_ event: String, with json: [String: Any]) {
        self.socket.emit(event, json)
    }
    
    /// Подключить сокет к серверу
    func connect() {
        self.socket.connect()
    }
    
    /// Отключить сокет от сервера
    func disconnect() {
        self.socket.disconnect()
        self.socket.removeAllHandlers()
    }
    
}
