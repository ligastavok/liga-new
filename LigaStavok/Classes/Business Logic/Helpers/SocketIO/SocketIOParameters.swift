//
//  SocketIOParameters.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class SocketIOParameters {
    
    // MARK: - Публичные свойства
    
    /// URL сокета
    let url: URL
    /// Пространство имен
    let namespace: String
    /// Выполнять логирование
    let log: Bool
    
    
    // MARK: - Инициализация
    
    init(url: URL, namespace: String, log: Bool = false) {
        self.url = url
        self.namespace = namespace
        self.log = log
    }
    
}
