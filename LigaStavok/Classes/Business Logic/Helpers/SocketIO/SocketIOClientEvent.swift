//
//  SocketIOClientEvent.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// События создаваемые клиентом
enum SocketIOClientEvent {
    
    /// Успешное подключение
    case connect
    /// Потеря подключения
    case disconnect
    /// Ошибка
    case error
    
}
