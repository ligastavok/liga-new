//
//  SocketIOConsoleLogger.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SocketIO

final class SocketIOConsoleLogger {
    
    // MARK: - Публичные свойства
    
    var log = true
    
}


// MARK: - SocketLogger

extension SocketIOConsoleLogger: SocketLogger {
    
    func log(_ message: @autoclosure () -> String, type: String) {
        guard self.log else { return }
        
        print("--log--")
        defer { print("-------") }
        
        let message = message()
        print("message: \(message)")
        print("type: \(type)")
    }
    
    func error(_ message: @autoclosure () -> String, type: String) {
        guard self.log else { return }
        
        print("--error--")
        defer { print("---------") }
        
        let message = message()
        print("message: \(message)")
        print("type: \(type)")
    }
    
}
