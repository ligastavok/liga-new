//
//  SocketIOOperation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Операция получения данных через интерфейс socket.io
class SocketIOOperation: AsyncOperation {
    
    // MARK: - Публичные свойства
    
    /// Сокет
    let socket: SocketIO
    
    
    // MARK: - Инициализация
    
    required init(socket: SocketIO) {
        self.socket = socket
        
        super.init()
        
        self.qualityOfService = .utility
    }
    
}


// MARK: - Operation

extension SocketIOOperation {
    
    override func main() {
        guard !self.isCancelled else { return }
        
        self.socket.connect()
    }
    
    override func cancel() {
        self.socket.disconnect()
        super.cancel()
    }
    
}
