//
//  EnumCollection.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 06.05.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

protocol EnumCollection: Hashable {
    
    /// Последовательность значений
    static func cases() -> AnySequence<Self>
    
    /// Массив всех значений
    static var allValues: [Self] { get }
    
}

extension EnumCollection {
    
    static func cases() -> AnySequence<Self> {
        return AnySequence { () -> AnyIterator<Self> in
            var raw = 0
            return AnyIterator {
                let current: Self = withUnsafePointer(to: &raw) { $0.withMemoryRebound(to: self, capacity: 1) { $0.pointee } }
                guard current.hashValue == raw else { return nil }
                
                raw += 1
                return current
            }
        }
    }
    
    static var allValues: [Self] {
        return Array(self.cases())
    }
    
}
