//
//  JSONPointer.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// RFC 6902 совместимая реализация пути JSONPatch
struct JSONPointer {
    
    // MARK: - Публичные свойства
    
    /// Не рассчитанное значение
    let rawValue: String
    /// Путь
    let pointerValue: [String]
    
    
    // MARK: - Инициализация
    
    init(rawValue: String, pointerValue: [String]) {
        self.rawValue = rawValue
        self.pointerValue = pointerValue
    }
    
    init(rawValue: String) throws {
        guard rawValue.isEmpty || rawValue.contains(JSONPatchConstants.JSONPointer.delimiter) else {
            throw JSONPointerError.valueDoesNotContainDelimiter
        }
        guard rawValue.isEmpty || rawValue.hasPrefix(JSONPatchConstants.JSONPointer.delimiter) else {
            throw JSONPointerError.nonEmptyPointerDoesNotStartWithDelimiter
        }
        
        let pointerValueWithoutFirstElement = Array(rawValue.components(separatedBy: JSONPatchConstants.JSONPointer.delimiter).dropFirst())
        
        guard rawValue.isEmpty || !pointerValueWithoutFirstElement.contains(JSONPatchConstants.JSONPointer.emptyString) else {
            throw JSONPointerError.containsEmptyReferenceToken
        }
        
        let pointerValueAfterDecodingDelimiter = pointerValueWithoutFirstElement.map { $0.replacingOccurrences(of: JSONPatchConstants.JSONPointer.escapedDelimiter, with: JSONPatchConstants.JSONPointer.delimiter) } // swiftlint:disable:this line_length
        let pointerValue: [String] = pointerValueAfterDecodingDelimiter.map { $0.replacingOccurrences(of: JSONPatchConstants.JSONPointer.escapedEscapeCharacter, with: JSONPatchConstants.JSONPointer.escapeCharater) } // swiftlint:disable:this line_length
        
        self.init(rawValue: rawValue, pointerValue: pointerValue)
    }
    
    private init(pointerValue: [String]) {
        let rawValue = JSONPatchConstants.JSONPointer.delimiter + pointerValue.joined(separator: JSONPatchConstants.JSONPointer.delimiter)
        self.init(rawValue: rawValue, pointerValue: pointerValue)
    }
    
}


// MARK: - Публичные методы

extension JSONPointer {
    
    /// Копия пути без первого элемента
    func traverse() -> JSONPointer {
        let pointerValue = Array(self.pointerValue.dropFirst())
        return JSONPointer(pointerValue: pointerValue)
    }
    
}
