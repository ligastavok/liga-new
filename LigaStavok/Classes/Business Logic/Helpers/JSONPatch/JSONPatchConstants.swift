//
//  JSONPatchConstants.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct JSONPatchConstants {
    
    /// Константы для объекта патча
    struct JSONPatch {
        
        /// Ключи JSON
        struct Parameter {
            
            static let op = "op"
            static let path = "path"
            static let value = "value"
            static let from = "from"
            
        }
        
        /// Сообщения об ошибке при инициализации
        struct InitialisationErrorMessages {
            
            static let patchWithEmptyError = "Patch array does not contain elements."
            static let invalidRootElement = "Root element is not an array of dictionaries or a single dictionary."
            static let opElementNotFound = "Could not find 'op' element."
            static let pathElementNotFound = "Could not find 'path' element."
            static let invalidOperation = "Operation is invalid."
            static let fromElementNotFound = "Could not find 'from' element."
            static let valueElementNotFound = "Could not find 'value' element."
            
        }
        
    }
    
    /// Константы для объекта пути
    struct JSONPointer {
        
        static let delimiter = "/"
        static let emptyString = ""
        static let escapeCharater = "~"
        static let escapedDelimiter = "~1"
        static let escapedEscapeCharacter = "~0"
        
    }
    
}
