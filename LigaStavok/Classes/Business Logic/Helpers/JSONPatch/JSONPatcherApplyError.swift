//
//  JSONPatcherApplyError.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Ошибка при применении патча к JSON
enum JSONPatcherApplyError: Error {
    
    /// Ошибка валидации
    case validationError
    /// Индекс вышел за границы массива
    case arrayIndexOutOfBounds
    /// Невалидный JSON
    case invalidJSON
    
}
