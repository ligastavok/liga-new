//
//  JSONPatch+Initialization.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 02.07.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension JSONPatch {
    
    init(_ diffs: [DiffModel], allowsNilValue: Bool) throws {
        guard !diffs.isEmpty else {
            // swiftlint:disable:next line_length
            throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.patchWithEmptyError)
        }
        
        let operations = try diffs.map { try $0.translateToJSONPatchOperation(allowsNilValue: allowsNilValue) }
        self.operations = operations
    }
    
}
