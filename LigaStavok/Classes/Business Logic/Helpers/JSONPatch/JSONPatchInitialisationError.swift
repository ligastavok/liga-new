//
//  JSONPatchInitialisationError.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Ошибка при инициализации патча
enum JSONPatchInitialisationError: Error {
    
    /// Недопустимый формат патча
    case invalidPatchFormat(message: String)
    
}


// MARK: - LocalizedError

extension JSONPatchInitialisationError: LocalizedError {
    
    var errorDescription: String? {
        switch self {
        case .invalidPatchFormat(let message):
            return message
        }
    }

}
