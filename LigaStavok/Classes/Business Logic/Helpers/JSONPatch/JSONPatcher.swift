//
//  JSONPatcher.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

/// RFC 6902 совместимая реализация JSONPatch
struct JSONPatcher { }


// MARK: - Публичные методы

extension JSONPatcher {
    
    /// Применить патч к JSON
    static func applyPatch(_ patch: JSONPatch, to json: JSON) throws -> JSON {
        var json = json
        try patch.operations.forEach { operation in
            switch operation.type {
            case .add:
                json = try JSONPatcher.add(operation, to: json)
            case .remove:
                json = try JSONPatcher.remove(operation, to: json)
            case .replace:
                json = try JSONPatcher.replace(operation, to: json)
            case .move:
                json = try JSONPatcher.move(operation, to: json)
            case .copy:
                json = try JSONPatcher.copy(operation, to: json)
            case .test:
                json = try JSONPatcher.test(operation, to: json)
            }
        }
        
        return json
    }
    
}


// MARK: - Приватные методы

private extension JSONPatcher {
    
    /// Применить операцию к JSON
    static func applyOperation(to json: JSON,
                               pointer: JSONPointer,
                               operation: (JSON, JSONPointer) throws -> JSON) throws -> JSON {
        
        if pointer.pointerValue.count == 1 {
            return try operation(json, pointer)
        } else {
            if var array = json.array, let indexString = pointer.pointerValue.first, let index = Int(indexString) {
                guard index < array.count else {
                    throw JSONPatcherApplyError.arrayIndexOutOfBounds
                }
                
                array[index] = try applyOperation(to: array[index], pointer: pointer.traverse(), operation: operation)
                return JSON(array)
            } else if var dictionary = json.dictionary, let key = pointer.pointerValue.first {
                guard let json = dictionary[key] else {
                    throw JSONPatcherApplyError.invalidJSON
                }
                
                dictionary[key] = try applyOperation(to: json, pointer: pointer.traverse(), operation: operation)
                return JSON(dictionary)
            }
        }
        
        return json
    }
    
    /// Операция добавления
    static func add(_ operation: JSONPatchOperation, to json: JSON) throws -> JSON {
        guard let value = operation.value else { return json }
        
        guard !operation.pointer.pointerValue.isEmpty else { return value }
        return try JSONPatcher.applyOperation(to: json, pointer: operation.pointer) { json, pointer in
            var json = json
            if var array = json.arrayObject,
                let indexString = pointer.pointerValue.first,
                let index = Int(indexString) {
                
                guard index <= array.count else {
                    throw JSONPatcherApplyError.arrayIndexOutOfBounds
                }
                
                array.insert(value.object, at: index)
                json.object = array
            } else if var dictionary = json.dictionaryObject, let key = pointer.pointerValue.first {
                dictionary[key] = value.object
                json.object = dictionary
            }
            
            return json
        }
    }
    
    /// Операция удаления
    static func remove(_ operation: JSONPatchOperation, to json: JSON) throws -> JSON {
        return try JSONPatcher.applyOperation(to: json, pointer: operation.pointer) { json, pointer in
            var json = json
            if var array = json.arrayObject,
                let indexString = pointer.pointerValue.first,
                let index = Int(indexString) {
                
                guard index < array.count else {
                    throw JSONPatcherApplyError.arrayIndexOutOfBounds
                }
                
                array.remove(at: index)
                json.object = array
            } else if var dictionary = json.dictionaryObject, let key = pointer.pointerValue.first {
                dictionary.removeValue(forKey: key)
                json.object = dictionary
            }
            
            return json
        }
    }
    
    /// Операция замены
    static func replace(_ operation: JSONPatchOperation, to json: JSON) throws -> JSON {
        guard let value = operation.value else { return json }
        
        return try JSONPatcher.applyOperation(to: json, pointer: operation.pointer) { json, pointer in
            var json = json
            if var array = json.arrayObject,
                let indexString = pointer.pointerValue.first,
                let index = Int(indexString) {
                
                guard index < array.count else {
                    throw JSONPatcherApplyError.arrayIndexOutOfBounds
                }
                
                array[index] = value.object
                json.object = array
            } else if var dictionary = json.dictionaryObject, let key = pointer.pointerValue.first {
                dictionary[key] = value.object
                json.object = dictionary
            }
            
            return json
        }
    }
    
    /// Операция перемещения
    static func move(_ operation: JSONPatchOperation, to json: JSON) throws -> JSON {
        var result = json
        
        guard let from = operation.from else { return result }
        return try JSONPatcher.applyOperation(to: json, pointer: from) { json, pointer in
            let removeOperation = JSONPatchOperation(type: .remove, pointer: from, value: nil, from: nil)
            result = try JSONPatcher.remove(removeOperation, to: result)
            
            let jsonToAdd: JSON
            if let array = json.array, let indexString = pointer.pointerValue.first, let index = Int(indexString) {
                guard index < array.count else {
                    throw JSONPatcherApplyError.arrayIndexOutOfBounds
                }
                
                jsonToAdd = array[index]
            } else {
                jsonToAdd = json[pointer.pointerValue]
            }
            
            let addOperation = JSONPatchOperation(type: .add, pointer: operation.pointer, value: jsonToAdd, from: nil)
            result = try JSONPatcher.add(addOperation, to: result)
            
            return result
        }
    }
    
    /// Операция копирования
    static func copy(_ operation: JSONPatchOperation, to json: JSON) throws -> JSON {
        var result = json
        
        guard let from = operation.from else { return result }
        return try JSONPatcher.applyOperation(to: json, pointer: from) { json, pointer in
            let jsonToAdd: JSON
            if let array = json.array, let indexString = pointer.pointerValue.first, let index = Int(indexString) {
                guard index < array.count else {
                    throw JSONPatcherApplyError.arrayIndexOutOfBounds
                }
                
                jsonToAdd = array[index]
            } else {
                jsonToAdd = json[pointer.pointerValue]
            }
            
            let addOperation = JSONPatchOperation(type: .add, pointer: operation.pointer, value: jsonToAdd, from: nil)
            result = try JSONPatcher.add(addOperation, to: result)
            
            return result
        }
    }
    
    /// Операция сравнения
    static func test(_ operation: JSONPatchOperation, to json: JSON) throws -> JSON {
        return try JSONPatcher.applyOperation(to: json, pointer: operation.pointer) { json, pointer in
            let jsonToValidate = json[pointer.pointerValue]
            guard jsonToValidate == operation.value else {
                throw JSONPatcherApplyError.validationError
            }
            
            return json
        }
    }
    
}
