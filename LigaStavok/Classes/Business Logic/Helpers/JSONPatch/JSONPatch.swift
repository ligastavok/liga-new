//
//  JSONPatch.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

/// Патч
struct JSONPatch {
    
    // MARK: - Публичные свойства
    
    /// Операции
    let operations: [JSONPatchOperation]
    
    
    // MARK: - Инициализация
    
    init(_ json: JSON, allowsNilValue: Bool) throws {
        if case .dictionary = json.type {
            let operation = try JSONPatch.extractOperation(from: json, allowsNilValue: allowsNilValue)
            self.operations = [operation]
        } else if case .array = json.type {
            guard !json.isEmpty else {
                // swiftlint:disable:next line_length
                throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.patchWithEmptyError)
            }
            
            var operations: [JSONPatchOperation] = []
            for (_, json) in json {
                operations.append(try JSONPatch.extractOperation(from: json, allowsNilValue: allowsNilValue))
            }
            
            self.operations = operations
        } else {
            // swiftlint:disable:next line_length
            throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.invalidRootElement)
        }
    }
    
}


// MARK: - Приватные методы

private extension JSONPatch {
    
    /// Извлечение информации об операции
    static func extractOperation(from json: JSON, allowsNilValue: Bool) throws -> JSONPatchOperation {
        guard let operation = json[JSONPatchConstants.JSONPatch.Parameter.op].string else {
            throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.opElementNotFound)
        }
        guard let operationType = JSONPatchOperation.OperationType(rawValue: operation) else {
            throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.invalidOperation)
        }
        guard let path = json[JSONPatchConstants.JSONPatch.Parameter.path].string else {
            // swiftlint:disable:next line_length
            throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.pathElementNotFound)
        }
        
        var from: JSONPointer?
        if operationType == .move || operationType == .copy {
            guard let fromValue = json[JSONPatchConstants.JSONPatch.Parameter.from].string else {
                // swiftlint:disable:next line_length
                throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.fromElementNotFound)
            }
            
            from = try JSONPointer(rawValue: fromValue)
        }
        
        let value = json[JSONPatchConstants.JSONPatch.Parameter.value]
        if (operationType == .add || operationType == .replace) && value.null != nil && !allowsNilValue {
            // swiftlint:disable:next line_length
            throw JSONPatchInitialisationError.invalidPatchFormat(message: JSONPatchConstants.JSONPatch.InitialisationErrorMessages.valueElementNotFound)
        }
        
        let pointer = try JSONPointer(rawValue: path)
        return JSONPatchOperation(type: operationType, pointer: pointer, value: value, from: from)
    }
    
}


// MARK: - Equatable

extension JSONPatch: Equatable {
    
    static func == (lhs: JSONPatch, rhs: JSONPatch) -> Bool {
        guard lhs.operations.count == rhs.operations.count else { return false }
        for index in 0..<lhs.operations.count {
            guard lhs.operations[index] == rhs.operations[index] else { return false }
        }
        
        return true
    }
    
}
