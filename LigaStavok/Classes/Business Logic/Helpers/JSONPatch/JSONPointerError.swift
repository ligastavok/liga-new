//
//  JSONPointerError.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Ошибка при инициализации пути
enum JSONPointerError: Error {
    
    /// Значение не содержит разделителя
    case valueDoesNotContainDelimiter
    /// Значение не начинается с разделителя
    case nonEmptyPointerDoesNotStartWithDelimiter
    /// Путь содержит пустой интервал
    case containsEmptyReferenceToken
    
}
