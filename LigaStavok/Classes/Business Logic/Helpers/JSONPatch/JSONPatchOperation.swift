//
//  JSONPatchOperation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 15.04.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import SwiftyJSON

/// RFC 6902 совместимая реализация операции JSONPatch
struct JSONPatchOperation {
    
    // MARK: - Типы данных
    
    /// Тип операции
    enum OperationType: String {
        
        /// Добавление
        case add
        /// Удаление
        case remove
        /// Замена
        case replace
        /// Перемещение
        case move
        /// Копирование
        case copy
        /// Сравнение
        case test
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Тип
    let type: OperationType
    /// Путь
    let pointer: JSONPointer
    /// Значение
    let value: JSON?
    /// Исходный путь
    let from: JSONPointer?
    
}


extension JSONPatchOperation: Equatable {
    
    static func == (lhs: JSONPatchOperation, rhs: JSONPatchOperation) -> Bool {
        return lhs.type == rhs.type
    }
    
}
