//
//  Int+IsSuccessHTTPCode.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension Int {
    
    /// Является значение успешным HTTP кодом
    var isSuccessHTTPCode: Bool {
        return self >= 200 && self < 300
    }
    
}
