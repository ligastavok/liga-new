//
//  WebTransport.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Alamofire

#if DEBUG
import ResponseDetective
#endif

/// Транспорт Web-сервиса
class WebTransport {
    
    // MARK: - Публичные свойства
    
    /// Тайм-аут семафора по-умолчанию
    static let defaultSemaphoreTimeoutGap: TimeInterval = 3
    
    /// Тайм-аут семафора
    let semaphoreTimeoutGap: TimeInterval
    
    /// Возможность отправки запросов на главном потоке
    let allowNetworkingOnMainThread: Bool
    
    
    // MARK: - Приватные свойства
    
    /// Запрос на получение данных
    private weak var dataRequest: DataRequest?
    
    /// Состояние
    private var isCanceled = false
    
    
    // MARK: - Инициализация
    
    /**
     Транспорт Web-сервиса
     
     - parameter semaphoreTimeout: Таймаут семафора
     */
    init(semaphoreTimeoutGap: TimeInterval = WebTransport.defaultSemaphoreTimeoutGap,
         allowNetworkingOnMainThread: Bool = false) {
        
        self.semaphoreTimeoutGap = semaphoreTimeoutGap
        self.allowNetworkingOnMainThread = allowNetworkingOnMainThread
    }
    
}


// MARK: - Публичные методы

extension WebTransport {
    
    /// Отправить запрос на транспортирование данных с web-сервиса
    func request(with parameters: WebRequestParameters) -> WebRequestResult {
        return self.dataRequest(with: parameters)
    }
    
}


// MARK: - Приватные методы

private extension WebTransport {
    
    /// Создать менеджер запроса
    func createRequestManager(with parameters: WebRequestParameters) -> SessionManager {
        let sessionConfiguration = URLSessionConfiguration.`default`
        sessionConfiguration.timeoutIntervalForRequest = parameters.timeout
        
        #if DEBUG
        if parameters.consoleOutputEnabled {
            ResponseDetective.enable(inConfiguration: sessionConfiguration)
        }
        #endif
        
        return SessionManager(configuration: sessionConfiguration)
    }
    
    /// Отправить запрос на получение данных
    func dataRequest(with parameters: WebRequestParameters) -> WebRequestResult {
        guard !self.isCanceled else { return WebRequestResult.cancelledResult }
        guard !Thread.isMainThread || self.allowNetworkingOnMainThread else { preconditionFailure("Networking on the main thread") }
        
        let requestManager = createRequestManager(with: parameters)
        var result = WebRequestResult.timeoutResult
        
        let semaphore = DispatchSemaphore(value: 0)
        
        self.dataRequest = requestManager.request(parameters).response(queue: .global(qos: .utility)) { requestResponse in
            let httpStatus = self.retrieveHTTPStatus(from: requestResponse)
            let requestStatus = self.retrieveRequestStatus(from: requestResponse)
            
            let data = self.retrieveData(from: requestResponse)
            let headers = self.retrieveHeaders(from: requestResponse)
            let resultError = self.retrieveError(from: requestResponse)
            
            result = WebRequestResult(httpStatus: httpStatus,
                                      headers: headers,
                                      status: requestStatus,
                                      data: data,
                                      error: resultError)
            
            semaphore.signal()
        }
        
        let gap: TimeInterval = parameters.timeout + self.semaphoreTimeoutGap
        _ = semaphore.wait(timeout: DispatchTime.now() + gap)
        
        return result
    }
    
    /// Получить HTTP-статус
    func retrieveHTTPStatus(from requestResponse: DefaultDataResponse) -> Int {
        if let response = requestResponse.response {
            return response.statusCode
        } else {
            return WebRequestResult.HTTPStatusNone
        }
    }
    
    /// Получить статус запроса
    func retrieveRequestStatus(from requestResponse: DefaultDataResponse) -> RequestStatus {
        if let error = requestResponse.error {
            if error._domain == NSURLErrorDomain && error._code == NSURLErrorCancelled {
                return .cancelled
            } else {
                return .systemError(error: error)
            }
        } else {
            return .success
        }
    }
    
    /// Получить результат запроса
    func retrieveData(from requestResponse: DefaultDataResponse) -> Data? {
        return requestResponse.data
    }
    
    /// Получить HTTP-заголовки ответа
    func retrieveHeaders(from requestResponse: DefaultDataResponse) -> HTTPHeaders {
        return requestResponse.response?.allHeaderFields as? HTTPHeaders ?? [:]
    }
    
    /// Получить ошибку
    func retrieveError(from requestResponse: DefaultDataResponse) -> Swift.Error? {
        return requestResponse.error
    }
    
}


// MARK: - Transport

extension WebTransport: Transport {
    
    func request(parameters: RequestParameters) -> RequestResult {
        guard let webParameters = parameters as? WebRequestParameters else { preconditionFailure() }
        return self.request(with: webParameters)
    }
    
    func cancel() {
        self.isCanceled = true
        self.dataRequest?.cancel()
    }
    
}
