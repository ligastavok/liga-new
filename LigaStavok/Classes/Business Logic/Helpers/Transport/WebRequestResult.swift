//
//  WebRequestResult.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Alamofire

/// Результат HTTP-запроса
class WebRequestResult: RequestResult {
    
    // MARK: - Публичные свойства
    
    /// Значение поля `httpStatus` на случай, если HTTP-запрос не был отправлен
    static let HTTPStatusNone = -1
    
    /// Статус HTTP-запроса
    let httpStatus: Int
    
    /// Заголовки из выдачи сервера
    let headers: HTTPHeaders
    
    
    // MARK: - Инициализация
    
    /**
     Результат HTTP-запроса
     
     - parameter httpStatus: HTTP-статус запроса
     - parameter headers: Заголовки из выдачи сервера
     - parameter status: Статус прохождения запроса
     - parameter data: Тело из выдачи сервера
     - parameter error: Ошибка, возникшая в ходе проведения запроса
     */
    required init(httpStatus: Int = WebRequestResult.HTTPStatusNone,
                  headers: HTTPHeaders = [:],
                  status: RequestStatus,
                  data: Data? = nil,
                  error: Error? = nil) {
        
        self.httpStatus = httpStatus
        self.headers = headers
        
        super.init(status: status, data: data, error: error)
    }
    
}


// MARK: - Публичные свойства

extension WebRequestResult {
    
    /// Результат HTTP-запроса, завершившегося отменой
    class var cancelledResult: WebRequestResult {
        return WebRequestResult(status: .cancelled)
    }
    
    /// Результат HTTP-запроса, завершившегося превышением таймаута семафора
    class var timeoutResult: WebRequestResult {
        return WebRequestResult(status: .timeout)
    }
    
}
