//
//  RequestResult.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Результат запроса транспортировки данных
class RequestResult {
    
    // MARK: - Публичные свойства
    
    /// Статус запроса
    let status: RequestStatus
    /// Результат запроса
    let data: Data?
    /// Ошибка, возникшая в ходе проведения запроса
    let error: Error?
    
    
    // MARK: - Инициализация
    
    /**
     Результат запроса транспортировки данных
     
     - parameter status: Статус запроса
     - parameter data: Результат запроса
     - parameter error: Ошибка, возникшая в ходе проведения запроса
     */
    init(status: RequestStatus, data: Data? = nil, error: Error? = nil) {
        self.status = status
        self.data = data
        self.error  = error
    }
    
}
