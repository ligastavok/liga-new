//
//  WebRequestParameters.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

import Alamofire

/// Параметры для проведения HTTP-запросов
class WebRequestParameters: RequestParameters {
    
    // MARK: - Публичные свойства
    
    /// Таймаут HTTP-запроса по-умолчанию
    static let defaultTimeout: TimeInterval = 30
    
    /// Метод HTTP-запроса
    let method: Alamofire.HTTPMethod
    
    /// URL, на который будет отправлен запрос
    let endpoint: String
    
    /// Загловки HTTP-запроса
    var headers: HTTPHeaders
    
    /// Параметры, с которыми будет отправлен запрос (в зависимости от типа запроса сериализуются в тело или в URL)
    var parameters: Alamofire.Parameters
    
    /// Сериализатор параметров
    let encoder: ParameterEncoding
    
    /// Таймаут запроса
    let timeout: TimeInterval
    
    /// Доступность вывода в консоль
    let consoleOutputEnabled: Bool
    
    
    // MARK: - Инициализация
    
    /**
     Параметры для проведения HTTP-запросов
     
     - parameter endpoint: URL, на который будет отправлен запрос
     - parameter method: HTTP-метод запроса
     - parameter headers: Заголовки HTTP-запроса
     - parameter parameters: Параметры, с которыми будет отправлен запрос (в зависимости от типа запроса сериализуются в тело или в URL)
     - parameter encoder: Сериализатор параметров
     - parameter timeout: Таймаут запроса
     - parameter consoleOutputEnabled: Доступность вывода в консоль
     - parameter base: Базовые параметры (endpoint, headers, parameters), которые нужно достроить
     */
    required init(endpoint: String,
                  method: Alamofire.HTTPMethod = .get,
                  headers: HTTPHeaders = [:],
                  parameters: Alamofire.Parameters = [:],
                  encoder: ParameterEncoding? = nil,
                  timeout: TimeInterval? = nil,
                  consoleOutputEnabled: Bool = true,
                  base: WebRequestParameters? = nil) {
        
        var mergedEndpoint = endpoint
        var mergedHeaders = headers
        var mergedParameters = parameters
        
        if let base = base {
            if endpoint.hasPrefix("?") || endpoint.hasPrefix("/") || endpoint.isEmpty {
                mergedEndpoint = base.endpoint + endpoint
            } else {
                mergedEndpoint = base.endpoint + "/" + endpoint
            }
            
            mergedHeaders = base.headers
            mergedParameters = base.parameters
            
            headers.forEach { header, value in
                mergedHeaders[header] = value
            }
            
            parameters.forEach { key, value in
                mergedParameters[key] = value
            }
        }
        
        self.endpoint = mergedEndpoint
        self.headers = mergedHeaders
        self.parameters = mergedParameters
        self.method = method
        self.encoder = encoder ?? base?.encoder ?? JSONEncoding.`default`
        self.timeout = timeout ?? base?.timeout ?? WebRequestParameters.defaultTimeout
        self.consoleOutputEnabled = consoleOutputEnabled
    }
    
}


// MARK: - Публичные методы

extension WebRequestParameters {
    
    /// Задать заголовок запроса
    @discardableResult
    func withHeader(header: String, value: String) -> Self {
        self.headers[header] = value
        return self
    }
    
    /// Добавить Cookie в запрос
    @discardableResult
    func withCookie(cookieName: String, value: String) -> Self {
        let newValue: String
        if let currentValue = self.headers["Cookie"] {
            newValue = currentValue + cookieName + "=" + value + "; "
        } else {
            newValue = cookieName + "=" + value + "; "
        }
        
        self.headers["Cookie"] = newValue
        return self
    }
    
    /// Добавить Cookie в запрос
    @discardableResult
    func withCookie(cookie: HTTPCookie) -> Self {
        let headers = HTTPCookie.requestHeaderFields(with: [cookie])
        for (header, value) in headers {
            self.headers[header] = value
        }
        return self
    }
    
    /// Добавить параметр в запрос
    @discardableResult
    func withParameter(parameter: String, value: AnyObject) -> Self {
        self.parameters[parameter] = value
        return self
    }
    
}


// MARK: - Приватные методы

private extension WebRequestParameters {
    
    /// Создать url-запрос
    func createURLRequest() throws -> URLRequest {
        let request = try self.constructURLRequest(self.method, urlString: self.endpoint, headers: self.headers)
        let parameters = self.parameters.isEmpty ? nil : self.parameters
        
        return try self.encoder.encode(request, with: parameters)
    }
    
    /// Собрать url-запрос
    func constructURLRequest(_ method: Alamofire.HTTPMethod,
                             urlString: String,
                             headers: HTTPHeaders) throws -> URLRequest {
        
        guard let url = URL(string: urlString) else { throw WebTransport.Error.cannotInitURLWithString(urlString: urlString) }
        
        var request = URLRequest(url: url, timeoutInterval: self.timeout)
        request.httpMethod = method.rawValue
        
        for (headerField, headerValue) in headers {
            request.setValue(headerValue, forHTTPHeaderField: headerField)
        }
        
        return request
    }
    
}


// MARK: - URLRequestConvertible

extension WebRequestParameters: URLRequestConvertible {
    
    func asURLRequest() throws -> URLRequest {
        return try self.createURLRequest()
    }
    
}
