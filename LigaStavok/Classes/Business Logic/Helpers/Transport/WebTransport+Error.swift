//
//  WebTransport+Error.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 16.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension WebTransport {
    
    enum Error: Swift.Error {
        
        /// Невозможно создать URL из строки
        case cannotInitURLWithString(urlString: String)
        
    }
    
}


// MARK: - LocalizedError

extension WebTransport.Error: LocalizedError {
    
    var errorDescription: String? {
        switch self {
        case .cannotInitURLWithString:
            return "Cannot convert String to URL"
        }
    }

}
