//
//  RequestStatus.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Статус запроса
enum RequestStatus {
    
    /// Запрос был успешно проведен
    case success
    /// В ходе запроса возникла системная ошибка
    case systemError(error: Error)
    /// Превышен таймаут семафора
    case timeout
    /// Запрос был отмененен
    case cancelled
    
}
