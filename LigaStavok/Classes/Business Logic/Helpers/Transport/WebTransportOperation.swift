//
//  WebTransportOperation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Операция получения данных с Web-сервиса
class WebTransportOperation: Operation {
    
    // MARK: - Типы данных
    
    /// Тип callback'a
    typealias WebTransportOperationCompletionBlock = (_ operation: WebTransportOperation, _ requestResult: WebRequestResult) -> Void
    
    
    // MARK: - Публичные свойства
    
    /// Транспорт, который будет совершать операцию
    let transport: WebTransport
    
    /// Параметры запроса
    var parameters: WebRequestParameters
    
    /// Callback, вызываемый при завершении операции
    let requestCompletionBlock: WebTransportOperationCompletionBlock
    
    
    // MARK: - Инициализация
    
    /**
     Операция получения данных с Web-сервиса
     
     - parameter transport: Транспорт, который будет совершать операцию
     - parameter parameters: Параметры запроса
     - parameter requestCompletion: Callback, вызываемый при завершении операции
     */
    required init(transport: WebTransport,
                  parameters: WebRequestParameters,
                  requestCompletion completionBlock: @escaping WebTransportOperationCompletionBlock) {
        
        self.transport = transport
        self.parameters = parameters
        self.requestCompletionBlock = completionBlock
        
        super.init()
        
        self.qualityOfService = .utility
    }
    
}


// MARK: - Operation

extension WebTransportOperation {
    
    override func main() {
        guard !self.isCancelled else { return }
        
        let result = self.transport.request(with: self.parameters)
        
        guard !self.isCancelled else { return }
        
        self.requestCompletionBlock(self, result)
    }
    
    override func cancel() {
        self.transport.cancel()
        
        super.cancel()
    }
    
}
