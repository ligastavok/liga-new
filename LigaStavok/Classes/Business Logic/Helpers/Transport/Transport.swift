//
//  Transport.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 01.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Транспорт
protocol Transport: class {
    
    /// Отправка запроса транспортирования данных
    func request(parameters: RequestParameters) -> RequestResult
    /// Отмена отправки запроса транспортирования данных
    func cancel()
    
}
