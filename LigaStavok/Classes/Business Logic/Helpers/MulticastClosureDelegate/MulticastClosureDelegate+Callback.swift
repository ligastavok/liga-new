//
//  MulticastClosureDelegate+Callback.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 20.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

extension MulticastClosureDelegate {
    
    /// Пара замыканий
    final class Callback {
        
        // MARK: - Публичные свойства
        
        /// Очередь
        let queue: DispatchQueue
        /// Замыкание для состояния "успех"
        let success: Success
        /// Замыкание для состояния "ошибка"
        let failure: Failure
        
        
        // MARK: - Инициализация
        
        init(queue: DispatchQueue, success: Success, failure: Failure) {
            self.queue = queue
            self.success = success
            self.failure = failure
        }
    }
    
}
