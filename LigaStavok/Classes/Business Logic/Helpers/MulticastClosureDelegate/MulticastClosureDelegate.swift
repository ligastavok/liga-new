//
//  MulticastClosureDelegate.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 20.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class MulticastClosureDelegate<Success, Failure> {
    
    // MARK: - Публичные свойства
    
    /// Количество делегатов
    var count: Int {
        return getCallbacks().count
    }
    
    
    // MARK: - Приватные свойства
    
    /// Коллекция делегатов
    private var mapTable = SynchronizedValue(NSMapTable<AnyObject, NSMutableArray>.weakToStrongObjects())
    
}


// MARK: - Публичные методы

extension MulticastClosureDelegate {
    
    /// Добавить пару замыканий
    func addClosurePair(for objectKey: AnyObject, queue: DispatchQueue = .main, success: Success, failure: Failure) {
        self.mapTable.syncSet { mapTable in
            let callBack = Callback(queue: queue, success: success, failure: failure)
            let array = mapTable.object(forKey: objectKey) ?? NSMutableArray()
            array.add(callBack)
            
            mapTable.setObject(array, forKey: objectKey)
        }
    }
    
    /// Удалить пары замыканий
    func removeClosuresPairs(for objectKey: AnyObject) {
        self.mapTable.syncSet { $0.removeObject(forKey: objectKey) }
    }
    
    /// Получить замыкания для состояния "успех"
    func getSuccessTuples(removeAfter: Bool = true) -> [(Success, DispatchQueue)] {
        return getCallbacks(removeAfter: removeAfter).map {
            return ($0.success, $0.queue)
        }
    }
    
    /// Получить замыкания для состояния "ошибка"
    func getFailureTuples(removeAfter: Bool = true) -> [(Failure, DispatchQueue)] {
        return getCallbacks(removeAfter: removeAfter).map {
            return ($0.failure, $0.queue)
        }
    }
    
}


// MARK: - Приватные методы

private extension MulticastClosureDelegate {
    
    /// Получить пары
    func getCallbacks(removeAfter: Bool = false) -> [Callback] {
        var callBacks: [Callback] = []
        self.mapTable.syncSet { mapTable in
            let objects = mapTable.keyEnumerator().allObjects as [AnyObject]
            callBacks = objects.reduce(callBacks) { combinedArray, objectKey in
                guard let array = mapTable.object(forKey: objectKey) as? [Callback] else { return combinedArray }
                return combinedArray + array
            }
            
            guard removeAfter else { return }
            objects.forEach { mapTable.removeObject(forKey: $0) }
        }
        
        return callBacks
    }
    
}
