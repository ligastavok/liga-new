//
//  AsyncOperation.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 20.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

class AsyncOperation: Operation {
    
    enum State: String {
        
        /// Выполняется
        case executing
        /// Закончила выполнение
        case finished
        /// Готова к выполенению
        case ready
        
        
        // MARK: - Приватные свойства
        
        fileprivate var keyPath: String {
            return "is" + rawValue.capitalized
        }
        
    }
    
    
    // MARK: - Публичные свойства
    
    /// Состояние
    var state: State = .ready {
        willSet {
            willChangeValue(forKey: newValue.keyPath)
            willChangeValue(forKey: self.state.keyPath)
        }
        didSet {
            didChangeValue(forKey: oldValue.keyPath)
            didChangeValue(forKey: self.state.keyPath)
        }
    }
}


// MARK: - Operation

extension AsyncOperation {
    
    override func start() {
        guard !self.isCancelled else {
            self.state = .finished
            return
        }
        
        main()
        self.state = .executing
    }
    
    override func cancel() {
        self.state = .finished
    }
    
    override var isExecuting: Bool {
        return self.state == .executing
    }
    
    override var isFinished: Bool {
        return self.state == .finished
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override var isReady: Bool {
        return super.isReady && self.state == .ready
    }
    
}
