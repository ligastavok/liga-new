//
//  GenericCodingKeys.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 09.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

struct GenericCodingKeys: CodingKey {
    
    // MARK: - Публичные свойства
    
    /// Значение для использования в индексированной коллекции
    var intValue: Int?
    /// Значение для использования в именованной коллекции
    var stringValue: String
    
    
    // MARK: - Инициализация
    
    init?(intValue: Int) {
        self.intValue = intValue
        self.stringValue = "\(intValue)"
    }
    
    init?(stringValue: String) {
        self.stringValue = stringValue
    }
    
}


// MARK: - Публичные методы

extension GenericCodingKeys {
    
    /// Создать ключ
    static func makeKey(name: String) -> GenericCodingKeys {
        return GenericCodingKeys(stringValue: name)!
    }
    
}
