//
//  Environment.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

/// Среда
protocol Environment: class {
    
    /// Базовый URL
    var baseUrl: URL { get }
    /// URL для общения по сокетам
    var socketUrl: URL { get }
    
}
