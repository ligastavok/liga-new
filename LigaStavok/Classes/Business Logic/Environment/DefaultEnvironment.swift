//
//  DefaultEnvironment.swift
//  LigaStavok
//
//  Created by Илья Халяпин on 21.03.2018.
//  Copyright © 2018 MobSolutions. All rights reserved.
//

final class DefaultEnvironment: Environment {
    
    // MARK: - Публичные свойства
    
    let baseUrl: URL
    let socketUrl: URL
    
    
    // MARK: - Инициализация
    
    init() {
        guard let infoDictionary = Bundle.main.infoDictionary else { preconditionFailure() }
        
        guard let socketUrlValue = infoDictionary["API_SOCKET_URL"] as? String,
            let socketUrl = URL(string: socketUrlValue) else { preconditionFailure() }
        self.socketUrl = socketUrl
        
        guard let baseUrlValue = infoDictionary["API_BASE_URL"] as? String,
            let baseUrl = URL(string: baseUrlValue) else { preconditionFailure() }
        self.baseUrl = baseUrl
    }
    
}
